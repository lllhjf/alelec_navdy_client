package com.navdy.service.library.log;

import android.text.TextUtils;
import com.navdy.client.app.framework.models.Destination;
import java.util.regex.Pattern;

public class TagFilter implements Filter {
    private boolean invert;
    private Pattern pattern;

    public static TagFilter pass(String... tags) {
        return new TagFilter(startsWithAny(tags));
    }

    public static TagFilter block(String... tags) {
        return new TagFilter(startsWithAny(tags), 0, true);
    }

    public static String startsWithAny(String... tags) {
        StringBuilder builder = new StringBuilder("^(?:");
        builder.append(TextUtils.join(Destination.IDENTIFIER_CONTACT_ID_SEPARATOR, tags));
        builder.append(")");
        return builder.toString();
    }

    public TagFilter(String regex, int flags, boolean invert) {
        this.invert = false;
        this.pattern = Pattern.compile(regex, flags);
        this.invert = invert;
    }

    public TagFilter(String regex) {
        this(regex, 0, false);
    }

    public TagFilter(Pattern pattern, boolean invert) {
        this.invert = false;
        this.pattern = pattern;
        this.invert = invert;
    }

    public boolean matches(int logLevel, String tag, String message) {
        boolean matches = this.pattern.matcher(tag).find();
        if (this.invert) {
            return !matches;
        } else {
            return matches;
        }
    }
}
