package com.navdy.service.library.device.discovery;

import android.content.Context;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdManager.RegistrationListener;
import android.net.nsd.NsdServiceInfo;
import com.navdy.service.library.log.Logger;
import java.util.Timer;
import java.util.TimerTask;

public abstract class MDNSDeviceBroadcaster implements RemoteDeviceBroadcaster {
    public static final Logger sLogger = new Logger(MDNSDeviceBroadcaster.class);
    protected NsdManager mNsdManager;
    protected RegistrationListener mRegistrationListener;
    protected NsdServiceInfo mServiceInfo;
    protected boolean registered;

    protected MDNSDeviceBroadcaster() {
    }

    public boolean start() {
        if (this.mRegistrationListener != null) {
            sLogger.e("Already started: " + this.mServiceInfo.getServiceType());
            return false;
        }
        this.mRegistrationListener = getRegistrationListener();
        return registerRecord(this.mServiceInfo, this.mRegistrationListener);
    }

    public boolean stop() {
        if (this.mRegistrationListener == null) {
            sLogger.e("Already stopped: " + this.mServiceInfo.getServiceType());
            return false;
        }
        sLogger.e("Unregistering: " + this.mServiceInfo.getServiceType());
        return unregisterRecord(this.mRegistrationListener);
    }

    public MDNSDeviceBroadcaster(Context context, NsdServiceInfo serviceInfo) {
        this.mNsdManager = (NsdManager) context.getSystemService("servicediscovery");
        if (serviceInfo == null) {
            throw new IllegalArgumentException("Service info required.");
        }
        this.mServiceInfo = serviceInfo;
    }

    protected RegistrationListener getRegistrationListener() {
        return new RegistrationListener() {
            public void onServiceRegistered(NsdServiceInfo nsdServiceInfo) {
                MDNSDeviceBroadcaster.sLogger.d("SERVICE REGISTERED: " + MDNSDeviceBroadcaster.this.mServiceInfo.getServiceType());
            }

            public void onRegistrationFailed(NsdServiceInfo nsdServiceInfo, int errorCode) {
                MDNSDeviceBroadcaster.sLogger.d("SERVICE REGISTRATION FAILED" + MDNSDeviceBroadcaster.this.mServiceInfo.getServiceType());
            }

            public void onServiceUnregistered(NsdServiceInfo nsdServiceInfo) {
                MDNSDeviceBroadcaster.sLogger.d("SERVICE UNREGISTERED" + MDNSDeviceBroadcaster.this.mServiceInfo.getServiceType());
            }

            public void onUnregistrationFailed(NsdServiceInfo nsdServiceInfo, int errorCode) {
                MDNSDeviceBroadcaster.sLogger.d("SERVICE UNREGISTRATION FAILED: " + MDNSDeviceBroadcaster.this.mServiceInfo.getServiceType());
            }
        };
    }

    protected boolean registerRecord(final NsdServiceInfo serviceInfo, final RegistrationListener registrationListener) {
        sLogger.e("Registering: " + serviceInfo.getServiceType());
        new Timer().schedule(new TimerTask() {
            public void run() {
                MDNSDeviceBroadcaster.sLogger.e("Register timer fired: " + serviceInfo.getServiceType());
                MDNSDeviceBroadcaster.this.mNsdManager.registerService(serviceInfo, 1, registrationListener);
                MDNSDeviceBroadcaster.this.registered = true;
                MDNSDeviceBroadcaster.sLogger.e("Registered: " + serviceInfo.getServiceType());
            }
        }, 2000);
        return true;
    }

    protected boolean unregisterRecord(RegistrationListener registrationListener) {
        if (this.registered) {
            this.registered = false;
            this.mNsdManager.unregisterService(registrationListener);
        }
        return true;
    }
}
