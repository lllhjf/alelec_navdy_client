package com.navdy.service.library.device.connection;

import com.navdy.service.library.device.connection.Connection.ConnectionFailureCause;
import com.navdy.service.library.device.connection.Connection.DisconnectCause;
import com.navdy.service.library.device.connection.Connection.Status;
import com.navdy.service.library.network.SocketAdapter;
import com.navdy.service.library.network.SocketFactory;
import java.io.IOException;
import java.net.SocketException;

public class SocketConnection extends Connection {
    private SocketFactory connector;
    protected boolean mBlockReconnect;
    protected ConnectThread mConnectThread;
    private SocketAdapter socket;

    private class ConnectThread extends Thread {
        private SocketAdapter mmSocket;

        public ConnectThread() {
            setName("ConnectThread");
        }

        public void run() {
            SocketConnection.this.logger.i("BEGIN mConnectThread");
            ConnectionFailureCause cause = ConnectionFailureCause.UNKNOWN;
            try {
                this.mmSocket = SocketConnection.this.connector.build();
                this.mmSocket.connect();
                synchronized (SocketConnection.this) {
                    SocketConnection.this.mConnectThread = null;
                }
                SocketConnection.this.connected(this.mmSocket);
            } catch (Exception e) {
                SocketConnection.this.logger.e("Unable to connect - " + e.getMessage());
                if (e instanceof SocketException) {
                    String message = e.getMessage();
                    if (message.contains("ETIMEDOUT")) {
                        cause = ConnectionFailureCause.CONNECTION_TIMED_OUT;
                    } else if (message.contains("ECONNREFUSED")) {
                        cause = ConnectionFailureCause.CONNECTION_REFUSED;
                    }
                }
                synchronized (SocketConnection.this) {
                    SocketConnection.this.logger.d("Connect failed, closing socket");
                    SocketConnection.this.closeQuietly(this.mmSocket);
                    this.mmSocket = null;
                    Status oldStatus = SocketConnection.this.mStatus;
                    SocketConnection.this.mStatus = Status.DISCONNECTED;
                    switch (oldStatus) {
                        case CONNECTING:
                            SocketConnection.this.connectionFailed(cause);
                            return;
                        case DISCONNECTING:
                            SocketConnection.this.dispatchDisconnectEvent(DisconnectCause.NORMAL);
                            return;
                        default:
                            SocketConnection.this.logger.w("unexpected state during connection failure: " + oldStatus);
                            return;
                    }
                }
            }
        }

        public void cancel() {
            SocketConnection.this.logger.d("[SOCK]Cancelling connect, closing socket");
            SocketConnection.this.closeQuietly(this.mmSocket);
            this.mmSocket = null;
        }
    }

    public SocketConnection(ConnectionInfo connectionInfo, SocketFactory connector) {
        super(connectionInfo);
        this.connector = connector;
    }

    public SocketConnection(ConnectionInfo connectionInfo, SocketAdapter socket) {
        super(connectionInfo);
        setExistingSocketConnection(socket);
    }

    protected synchronized void setExistingSocketConnection(SocketAdapter socketAdapter) {
        this.mBlockReconnect = true;
        this.mStatus = Status.CONNECTING;
        connected(socketAdapter);
    }

    public synchronized boolean connect() {
        boolean z = false;
        synchronized (this) {
            if (!this.mBlockReconnect) {
                this.logger.d("connect to: " + this.mConnectionInfo);
                if (this.mStatus != Status.DISCONNECTED) {
                    this.logger.d("can't connect: state: " + this.mStatus);
                } else {
                    this.mStatus = Status.CONNECTING;
                    this.mConnectThread = new ConnectThread();
                    this.mConnectThread.start();
                    z = true;
                }
            }
        }
        return z;
    }

    public synchronized boolean disconnect() {
        if (this.mStatus == Status.DISCONNECTED || this.mStatus == Status.DISCONNECTING) {
            this.logger.d("can't disconnect: state: " + this.mStatus);
        } else {
            this.logger.d("disconnect - connectThread:" + this.mConnectThread + " socket:" + this.socket);
            this.mStatus = Status.DISCONNECTING;
            if (this.mConnectThread != null) {
                this.mConnectThread.cancel();
                this.mConnectThread = null;
            } else if (this.socket != null) {
                try {
                    this.socket.close();
                } catch (IOException e) {
                    this.logger.e("Exception closing socket" + e.getMessage());
                }
                this.socket = null;
            } else {
                this.mStatus = Status.DISCONNECTED;
                dispatchDisconnectEvent(DisconnectCause.NORMAL);
            }
        }
        return true;
    }

    public SocketAdapter getSocket() {
        return this.socket;
    }

    public synchronized void connected(SocketAdapter socket) {
        this.logger.i("connected");
        if (this.mConnectThread != null) {
            this.mConnectThread.cancel();
            this.mConnectThread = null;
        }
        if (this.mStatus != Status.CONNECTING || socket == null) {
            this.logger.e("No longer connecting - disconnecting.");
            closeQuietly(socket);
            this.mStatus = Status.DISCONNECTED;
            dispatchDisconnectEvent(DisconnectCause.NORMAL);
        } else {
            this.socket = socket;
            this.mStatus = Status.CONNECTED;
            dispatchConnectEvent();
        }
    }

    private void connectionFailed(ConnectionFailureCause cause) {
        dispatchConnectionFailedEvent(cause);
    }

    private void closeQuietly(SocketAdapter socket) {
        if (socket != null) {
            try {
                socket.close();
            } catch (Throwable e) {
                this.logger.e("[SOCK] Exception while closing", e);
            }
        }
    }
}
