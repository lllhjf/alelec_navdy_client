package com.navdy.service.library.device.connection;

import java.util.HashSet;
import java.util.Set;

public enum ConnectionType {
    HTTP("_http._tcp."),
    TCP_PROTOBUF(TCPConnectionInfo.SERVICE_TYPE),
    BT_PROTOBUF,
    EA_PROTOBUF,
    BT_TUNNEL,
    EA_TUNNEL,
    BT_IAP2_LINK;
    
    private final String mServiceType;

    private ConnectionType(String serviceType) {
        this.mServiceType = serviceType;
    }

    public String getServiceType() {
        return this.mServiceType;
    }

    public static ConnectionType fromServiceType(String serviceType) {
        for (ConnectionType type : values()) {
            if (serviceType.equals(type.getServiceType())) {
                return type;
            }
        }
        return null;
    }

    public static Set<String> getServiceTypes() {
        HashSet<String> names = new HashSet(values().length);
        for (ConnectionType type : values()) {
            if (type.getServiceType() != null) {
                names.add(type.getServiceType());
            }
        }
        return names;
    }
}
