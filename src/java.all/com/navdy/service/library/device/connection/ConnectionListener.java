package com.navdy.service.library.device.connection;

import android.content.Context;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.Listenable;
import java.io.IOException;

public abstract class ConnectionListener extends Listenable {
    protected final Logger logger;
    AcceptThread mAcceptThread;
    protected final Context mContext;

    public interface Listener extends com.navdy.service.library.util.Listenable.Listener {
        void onConnected(ConnectionListener connectionListener, Connection connection);

        void onConnectionFailed(ConnectionListener connectionListener);

        void onStartFailure(ConnectionListener connectionListener);

        void onStarted(ConnectionListener connectionListener);

        void onStopped(ConnectionListener connectionListener);
    }

    protected abstract class AcceptThread extends Thread {
        abstract void cancel();

        protected AcceptThread() {
        }
    }

    protected interface EventDispatcher extends EventDispatcher<ConnectionListener, Listener> {
    }

    protected abstract AcceptThread getNewAcceptThread() throws IOException;

    public abstract ConnectionType getType();

    public ConnectionListener(Context context, String tag) {
        this.mContext = context;
        this.logger = new Logger(tag);
    }

    public synchronized boolean start() {
        boolean z = false;
        synchronized (this) {
            if (this.mAcceptThread != null) {
                if (this.mAcceptThread.isAlive()) {
                    this.logger.e("Already running");
                } else {
                    this.logger.e("clearing existing accept thread");
                    this.mAcceptThread.cancel();
                    this.mAcceptThread = null;
                }
            }
            try {
                this.mAcceptThread = getNewAcceptThread();
                if (this.mAcceptThread != null) {
                    this.mAcceptThread.start();
                }
                z = true;
            } catch (Throwable e) {
                this.logger.e("Unable to start accept thread: ", e);
                dispatchStartFailure();
            }
        }
        return z;
    }

    public synchronized boolean stop() {
        boolean z;
        if (this.mAcceptThread == null) {
            this.logger.e("Already stopped.");
            z = false;
        } else {
            this.mAcceptThread.cancel();
            this.mAcceptThread = null;
            z = true;
        }
        return z;
    }

    public void dispatchStarted() {
        dispatchToListeners(new EventDispatcher() {
            public void dispatchEvent(ConnectionListener source, Listener listener) {
                listener.onStarted(source);
            }
        });
    }

    public void dispatchStartFailure() {
        dispatchToListeners(new EventDispatcher() {
            public void dispatchEvent(ConnectionListener source, Listener listener) {
                listener.onStartFailure(source);
            }
        });
    }

    public void dispatchStopped() {
        dispatchToListeners(new EventDispatcher() {
            public void dispatchEvent(ConnectionListener source, Listener listener) {
                listener.onStopped(source);
            }
        });
    }

    public void dispatchConnected(final Connection connection) {
        dispatchToListeners(new EventDispatcher() {
            public void dispatchEvent(ConnectionListener source, Listener listener) {
                listener.onConnected(source, connection);
            }
        });
    }

    public void dispatchConnectionFailed() {
        dispatchToListeners(new EventDispatcher() {
            public void dispatchEvent(ConnectionListener source, Listener listener) {
                listener.onConnectionFailed(source);
            }
        });
    }
}
