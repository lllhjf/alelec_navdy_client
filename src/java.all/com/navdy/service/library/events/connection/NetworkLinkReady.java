package com.navdy.service.library.events.connection;

import com.squareup.wire.Message;

public final class NetworkLinkReady extends Message {
    private static final long serialVersionUID = 0;

    public static final class Builder extends com.squareup.wire.Message.Builder<NetworkLinkReady> {
        public Builder(NetworkLinkReady message) {
            super(message);
        }

        public NetworkLinkReady build() {
            return new NetworkLinkReady(this);
        }
    }

    private NetworkLinkReady(Builder builder) {
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        return other instanceof NetworkLinkReady;
    }

    public int hashCode() {
        return 0;
    }
}
