package com.navdy.service.library.events.input;

import com.squareup.wire.ProtoEnum;

public enum Gesture implements ProtoEnum {
    GESTURE_SWIPE_LEFT(1),
    GESTURE_SWIPE_RIGHT(2),
    GESTURE_SWIPE_UP(3),
    GESTURE_SWIPE_DOWN(4),
    GESTURE_FINGER_UP(5),
    GESTURE_FINGER(6),
    GESTURE_FINGER_DOWN(7),
    GESTURE_FINGER_TO_PINCH(8),
    GESTURE_PINCH_TO_FINGER(9),
    GESTURE_PINCH(10),
    GESTURE_HAND_UP(11),
    GESTURE_PALM(12),
    GESTURE_HAND_TO_FIST(13),
    GESTURE_FIST(14),
    GESTURE_FIST_TO_HAND(15),
    GESTURE_HAND_DOWN(16);
    
    private final int value;

    private Gesture(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
