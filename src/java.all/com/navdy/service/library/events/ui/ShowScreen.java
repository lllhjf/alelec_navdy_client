package com.navdy.service.library.events.ui;

import com.navdy.service.library.events.glances.KeyValue;
import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;
import java.util.Collections;
import java.util.List;

public final class ShowScreen extends Message {
    public static final List<KeyValue> DEFAULT_ARGUMENTS = Collections.emptyList();
    public static final Screen DEFAULT_SCREEN = Screen.SCREEN_DASHBOARD;
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REPEATED, messageType = KeyValue.class, tag = 2)
    public final List<KeyValue> arguments;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final Screen screen;

    public static final class Builder extends com.squareup.wire.Message.Builder<ShowScreen> {
        public List<KeyValue> arguments;
        public Screen screen;

        public Builder(ShowScreen message) {
            super(message);
            if (message != null) {
                this.screen = message.screen;
                this.arguments = Message.copyOf(message.arguments);
            }
        }

        public Builder screen(Screen screen) {
            this.screen = screen;
            return this;
        }

        public Builder arguments(List<KeyValue> arguments) {
            this.arguments = com.squareup.wire.Message.Builder.checkForNulls(arguments);
            return this;
        }

        public ShowScreen build() {
            checkRequiredFields();
            return new ShowScreen(this, null);
        }
    }

    public ShowScreen(Screen screen, List<KeyValue> arguments) {
        this.screen = screen;
        this.arguments = Message.immutableCopyOf(arguments);
    }

    private ShowScreen(Builder builder) {
        this(builder.screen, builder.arguments);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof ShowScreen)) {
            return false;
        }
        ShowScreen o = (ShowScreen) other;
        if (equals((Object) this.screen, (Object) o.screen) && equals(this.arguments, o.arguments)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        result = ((this.screen != null ? this.screen.hashCode() : 0) * 37) + (this.arguments != null ? this.arguments.hashCode() : 1);
        this.hashCode = result;
        return result;
    }
}
