package com.navdy.service.library.events.navigation;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.ProtoField;

public final class NavigationRouteCancelRequest extends Message {
    public static final String DEFAULT_HANDLE = "";
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 1, type = Datatype.STRING)
    public final String handle;

    public static final class Builder extends com.squareup.wire.Message.Builder<NavigationRouteCancelRequest> {
        public String handle;

        public Builder(NavigationRouteCancelRequest message) {
            super(message);
            if (message != null) {
                this.handle = message.handle;
            }
        }

        public Builder handle(String handle) {
            this.handle = handle;
            return this;
        }

        public NavigationRouteCancelRequest build() {
            return new NavigationRouteCancelRequest(this);
        }
    }

    public NavigationRouteCancelRequest(String handle) {
        this.handle = handle;
    }

    private NavigationRouteCancelRequest(Builder builder) {
        this(builder.handle);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (other instanceof NavigationRouteCancelRequest) {
            return equals((Object) this.handle, (Object) ((NavigationRouteCancelRequest) other).handle);
        }
        return false;
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = this.handle != null ? this.handle.hashCode() : 0;
        this.hashCode = hashCode;
        return hashCode;
    }
}
