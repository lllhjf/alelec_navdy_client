package com.navdy.service.library.events.navigation;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class NavigationSessionRequest extends Message {
    public static final String DEFAULT_LABEL = "";
    public static final NavigationSessionState DEFAULT_NEWSTATE = NavigationSessionState.NAV_SESSION_ENGINE_NOT_READY;
    public static final Boolean DEFAULT_ORIGINDISPLAY = Boolean.valueOf(false);
    public static final String DEFAULT_ROUTEID = "";
    public static final Integer DEFAULT_SIMULATIONSPEED = Integer.valueOf(0);
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String label;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final NavigationSessionState newState;
    @ProtoField(tag = 5, type = Datatype.BOOL)
    public final Boolean originDisplay;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String routeId;
    @ProtoField(tag = 4, type = Datatype.UINT32)
    public final Integer simulationSpeed;

    public static final class Builder extends com.squareup.wire.Message.Builder<NavigationSessionRequest> {
        public String label;
        public NavigationSessionState newState;
        public Boolean originDisplay;
        public String routeId;
        public Integer simulationSpeed;

        public Builder(NavigationSessionRequest message) {
            super(message);
            if (message != null) {
                this.newState = message.newState;
                this.label = message.label;
                this.routeId = message.routeId;
                this.simulationSpeed = message.simulationSpeed;
                this.originDisplay = message.originDisplay;
            }
        }

        public Builder newState(NavigationSessionState newState) {
            this.newState = newState;
            return this;
        }

        public Builder label(String label) {
            this.label = label;
            return this;
        }

        public Builder routeId(String routeId) {
            this.routeId = routeId;
            return this;
        }

        public Builder simulationSpeed(Integer simulationSpeed) {
            this.simulationSpeed = simulationSpeed;
            return this;
        }

        public Builder originDisplay(Boolean originDisplay) {
            this.originDisplay = originDisplay;
            return this;
        }

        public NavigationSessionRequest build() {
            checkRequiredFields();
            return new NavigationSessionRequest(this);
        }
    }

    public NavigationSessionRequest(NavigationSessionState newState, String label, String routeId, Integer simulationSpeed, Boolean originDisplay) {
        this.newState = newState;
        this.label = label;
        this.routeId = routeId;
        this.simulationSpeed = simulationSpeed;
        this.originDisplay = originDisplay;
    }

    private NavigationSessionRequest(Builder builder) {
        this(builder.newState, builder.label, builder.routeId, builder.simulationSpeed, builder.originDisplay);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof NavigationSessionRequest)) {
            return false;
        }
        NavigationSessionRequest o = (NavigationSessionRequest) other;
        if (equals((Object) this.newState, (Object) o.newState) && equals((Object) this.label, (Object) o.label) && equals((Object) this.routeId, (Object) o.routeId) && equals((Object) this.simulationSpeed, (Object) o.simulationSpeed) && equals((Object) this.originDisplay, (Object) o.originDisplay)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.newState != null ? this.newState.hashCode() : 0) * 37;
        if (this.label != null) {
            hashCode = this.label.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.routeId != null) {
            hashCode = this.routeId.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.simulationSpeed != null) {
            hashCode = this.simulationSpeed.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.originDisplay != null) {
            i = this.originDisplay.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
