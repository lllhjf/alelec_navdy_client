package com.navdy.service.library.events.navigation;

import com.navdy.service.library.events.RequestStatus;
import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class NavigationSessionResponse extends Message {
    public static final NavigationSessionState DEFAULT_PENDINGSESSIONSTATE = NavigationSessionState.NAV_SESSION_ENGINE_NOT_READY;
    public static final String DEFAULT_ROUTEID = "";
    public static final RequestStatus DEFAULT_STATUS = RequestStatus.REQUEST_SUCCESS;
    public static final String DEFAULT_STATUSDETAIL = "";
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 3, type = Datatype.ENUM)
    public final NavigationSessionState pendingSessionState;
    @ProtoField(tag = 4, type = Datatype.STRING)
    public final String routeId;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final RequestStatus status;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String statusDetail;

    public static final class Builder extends com.squareup.wire.Message.Builder<NavigationSessionResponse> {
        public NavigationSessionState pendingSessionState;
        public String routeId;
        public RequestStatus status;
        public String statusDetail;

        public Builder(NavigationSessionResponse message) {
            super(message);
            if (message != null) {
                this.status = message.status;
                this.statusDetail = message.statusDetail;
                this.pendingSessionState = message.pendingSessionState;
                this.routeId = message.routeId;
            }
        }

        public Builder status(RequestStatus status) {
            this.status = status;
            return this;
        }

        public Builder statusDetail(String statusDetail) {
            this.statusDetail = statusDetail;
            return this;
        }

        public Builder pendingSessionState(NavigationSessionState pendingSessionState) {
            this.pendingSessionState = pendingSessionState;
            return this;
        }

        public Builder routeId(String routeId) {
            this.routeId = routeId;
            return this;
        }

        public NavigationSessionResponse build() {
            checkRequiredFields();
            return new NavigationSessionResponse(this);
        }
    }

    public NavigationSessionResponse(RequestStatus status, String statusDetail, NavigationSessionState pendingSessionState, String routeId) {
        this.status = status;
        this.statusDetail = statusDetail;
        this.pendingSessionState = pendingSessionState;
        this.routeId = routeId;
    }

    private NavigationSessionResponse(Builder builder) {
        this(builder.status, builder.statusDetail, builder.pendingSessionState, builder.routeId);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof NavigationSessionResponse)) {
            return false;
        }
        NavigationSessionResponse o = (NavigationSessionResponse) other;
        if (equals((Object) this.status, (Object) o.status) && equals((Object) this.statusDetail, (Object) o.statusDetail) && equals((Object) this.pendingSessionState, (Object) o.pendingSessionState) && equals((Object) this.routeId, (Object) o.routeId)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.status != null ? this.status.hashCode() : 0) * 37;
        if (this.statusDetail != null) {
            hashCode = this.statusDetail.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.pendingSessionState != null) {
            hashCode = this.pendingSessionState.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.routeId != null) {
            i = this.routeId.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
