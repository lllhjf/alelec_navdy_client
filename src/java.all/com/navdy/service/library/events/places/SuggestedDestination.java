package com.navdy.service.library.events.places;

import com.navdy.service.library.events.destination.Destination;
import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoEnum;
import com.squareup.wire.ProtoField;

public final class SuggestedDestination extends Message {
    public static final Integer DEFAULT_DURATION_TRAFFIC = Integer.valueOf(0);
    public static final SuggestionType DEFAULT_TYPE = SuggestionType.SUGGESTION_RECOMMENDATION;
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REQUIRED, tag = 1)
    public final Destination destination;
    @ProtoField(tag = 2, type = Datatype.UINT32)
    public final Integer duration_traffic;
    @ProtoField(tag = 3, type = Datatype.ENUM)
    public final SuggestionType type;

    public static final class Builder extends com.squareup.wire.Message.Builder<SuggestedDestination> {
        public Destination destination;
        public Integer duration_traffic;
        public SuggestionType type;

        public Builder(SuggestedDestination message) {
            super(message);
            if (message != null) {
                this.destination = message.destination;
                this.duration_traffic = message.duration_traffic;
                this.type = message.type;
            }
        }

        public Builder destination(Destination destination) {
            this.destination = destination;
            return this;
        }

        public Builder duration_traffic(Integer duration_traffic) {
            this.duration_traffic = duration_traffic;
            return this;
        }

        public Builder type(SuggestionType type) {
            this.type = type;
            return this;
        }

        public SuggestedDestination build() {
            checkRequiredFields();
            return new SuggestedDestination(this);
        }
    }

    public enum SuggestionType implements ProtoEnum {
        SUGGESTION_RECOMMENDATION(0),
        SUGGESTION_CALENDAR(1);
        
        private final int value;

        private SuggestionType(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    public SuggestedDestination(Destination destination, Integer duration_traffic, SuggestionType type) {
        this.destination = destination;
        this.duration_traffic = duration_traffic;
        this.type = type;
    }

    private SuggestedDestination(Builder builder) {
        this(builder.destination, builder.duration_traffic, builder.type);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof SuggestedDestination)) {
            return false;
        }
        SuggestedDestination o = (SuggestedDestination) other;
        if (equals((Object) this.destination, (Object) o.destination) && equals((Object) this.duration_traffic, (Object) o.duration_traffic) && equals((Object) this.type, (Object) o.type)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.destination != null ? this.destination.hashCode() : 0) * 37;
        if (this.duration_traffic != null) {
            hashCode = this.duration_traffic.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.type != null) {
            i = this.type.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
