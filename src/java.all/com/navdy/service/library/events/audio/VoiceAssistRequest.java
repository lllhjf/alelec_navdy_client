package com.navdy.service.library.events.audio;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.ProtoField;

public final class VoiceAssistRequest extends Message {
    public static final Boolean DEFAULT_END = Boolean.valueOf(false);
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 1, type = Datatype.BOOL)
    public final Boolean end;

    public static final class Builder extends com.squareup.wire.Message.Builder<VoiceAssistRequest> {
        public Boolean end;

        public Builder(VoiceAssistRequest message) {
            super(message);
            if (message != null) {
                this.end = message.end;
            }
        }

        public Builder end(Boolean end) {
            this.end = end;
            return this;
        }

        public VoiceAssistRequest build() {
            return new VoiceAssistRequest(this);
        }
    }

    public VoiceAssistRequest(Boolean end) {
        this.end = end;
    }

    private VoiceAssistRequest(Builder builder) {
        this(builder.end);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (other instanceof VoiceAssistRequest) {
            return equals((Object) this.end, (Object) ((VoiceAssistRequest) other).end);
        }
        return false;
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = this.end != null ? this.end.hashCode() : 0;
        this.hashCode = hashCode;
        return hashCode;
    }
}
