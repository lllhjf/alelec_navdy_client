package com.navdy.service.library.events.audio;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.ProtoField;
import okio.ByteString;

public final class MusicArtworkResponse extends Message {
    public static final String DEFAULT_ALBUM = "";
    public static final String DEFAULT_AUTHOR = "";
    public static final String DEFAULT_COLLECTIONID = "";
    public static final MusicCollectionSource DEFAULT_COLLECTIONSOURCE = MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN;
    public static final MusicCollectionType DEFAULT_COLLECTIONTYPE = MusicCollectionType.COLLECTION_TYPE_UNKNOWN;
    public static final String DEFAULT_NAME = "";
    public static final ByteString DEFAULT_PHOTO = ByteString.EMPTY;
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String album;
    @ProtoField(tag = 4, type = Datatype.STRING)
    public final String author;
    @ProtoField(tag = 7, type = Datatype.STRING)
    public final String collectionId;
    @ProtoField(tag = 1, type = Datatype.ENUM)
    public final MusicCollectionSource collectionSource;
    @ProtoField(tag = 6, type = Datatype.ENUM)
    public final MusicCollectionType collectionType;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String name;
    @ProtoField(tag = 5, type = Datatype.BYTES)
    public final ByteString photo;

    public static final class Builder extends com.squareup.wire.Message.Builder<MusicArtworkResponse> {
        public String album;
        public String author;
        public String collectionId;
        public MusicCollectionSource collectionSource;
        public MusicCollectionType collectionType;
        public String name;
        public ByteString photo;

        public Builder(MusicArtworkResponse message) {
            super(message);
            if (message != null) {
                this.collectionSource = message.collectionSource;
                this.name = message.name;
                this.album = message.album;
                this.author = message.author;
                this.photo = message.photo;
                this.collectionType = message.collectionType;
                this.collectionId = message.collectionId;
            }
        }

        public Builder collectionSource(MusicCollectionSource collectionSource) {
            this.collectionSource = collectionSource;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder album(String album) {
            this.album = album;
            return this;
        }

        public Builder author(String author) {
            this.author = author;
            return this;
        }

        public Builder photo(ByteString photo) {
            this.photo = photo;
            return this;
        }

        public Builder collectionType(MusicCollectionType collectionType) {
            this.collectionType = collectionType;
            return this;
        }

        public Builder collectionId(String collectionId) {
            this.collectionId = collectionId;
            return this;
        }

        public MusicArtworkResponse build() {
            return new MusicArtworkResponse(this);
        }
    }

    public MusicArtworkResponse(MusicCollectionSource collectionSource, String name, String album, String author, ByteString photo, MusicCollectionType collectionType, String collectionId) {
        this.collectionSource = collectionSource;
        this.name = name;
        this.album = album;
        this.author = author;
        this.photo = photo;
        this.collectionType = collectionType;
        this.collectionId = collectionId;
    }

    private MusicArtworkResponse(Builder builder) {
        this(builder.collectionSource, builder.name, builder.album, builder.author, builder.photo, builder.collectionType, builder.collectionId);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof MusicArtworkResponse)) {
            return false;
        }
        MusicArtworkResponse o = (MusicArtworkResponse) other;
        if (equals((Object) this.collectionSource, (Object) o.collectionSource) && equals((Object) this.name, (Object) o.name) && equals((Object) this.album, (Object) o.album) && equals((Object) this.author, (Object) o.author) && equals((Object) this.photo, (Object) o.photo) && equals((Object) this.collectionType, (Object) o.collectionType) && equals((Object) this.collectionId, (Object) o.collectionId)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.collectionSource != null ? this.collectionSource.hashCode() : 0) * 37;
        if (this.name != null) {
            hashCode = this.name.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.album != null) {
            hashCode = this.album.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.author != null) {
            hashCode = this.author.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.photo != null) {
            hashCode = this.photo.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.collectionType != null) {
            hashCode = this.collectionType.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.collectionId != null) {
            i = this.collectionId.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
