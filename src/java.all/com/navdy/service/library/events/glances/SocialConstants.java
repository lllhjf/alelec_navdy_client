package com.navdy.service.library.events.glances;

import com.squareup.wire.ProtoEnum;

public enum SocialConstants implements ProtoEnum {
    SOCIAL_FROM(0),
    SOCIAL_TO(1),
    SOCIAL_MESSAGE(2);
    
    private final int value;

    private SocialConstants(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
