package com.navdy.service.library.events.glances;

import com.squareup.wire.Message;

public final class ClearGlances extends Message {
    private static final long serialVersionUID = 0;

    public static final class Builder extends com.squareup.wire.Message.Builder<ClearGlances> {
        public Builder(ClearGlances message) {
            super(message);
        }

        public ClearGlances build() {
            return new ClearGlances(this);
        }
    }

    private ClearGlances(Builder builder) {
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        return other instanceof ClearGlances;
    }

    public int hashCode() {
        return 0;
    }
}
