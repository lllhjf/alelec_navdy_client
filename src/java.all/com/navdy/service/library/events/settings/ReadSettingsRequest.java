package com.navdy.service.library.events.settings;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;
import java.util.Collections;
import java.util.List;

public final class ReadSettingsRequest extends Message {
    public static final List<String> DEFAULT_SETTINGS = Collections.emptyList();
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REPEATED, tag = 1, type = Datatype.STRING)
    public final List<String> settings;

    public static final class Builder extends com.squareup.wire.Message.Builder<ReadSettingsRequest> {
        public List<String> settings;

        public Builder(ReadSettingsRequest message) {
            super(message);
            if (message != null) {
                this.settings = Message.copyOf(message.settings);
            }
        }

        public Builder settings(List<String> settings) {
            this.settings = com.squareup.wire.Message.Builder.checkForNulls(settings);
            return this;
        }

        public ReadSettingsRequest build() {
            return new ReadSettingsRequest(this);
        }
    }

    public ReadSettingsRequest(List<String> settings) {
        this.settings = Message.immutableCopyOf(settings);
    }

    private ReadSettingsRequest(Builder builder) {
        this(builder.settings);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (other instanceof ReadSettingsRequest) {
            return equals(this.settings, ((ReadSettingsRequest) other).settings);
        }
        return false;
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = this.settings != null ? this.settings.hashCode() : 1;
        this.hashCode = hashCode;
        return hashCode;
    }
}
