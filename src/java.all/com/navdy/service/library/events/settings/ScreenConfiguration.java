package com.navdy.service.library.events.settings;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class ScreenConfiguration extends Message {
    public static final Integer DEFAULT_MARGINBOTTOM = Integer.valueOf(0);
    public static final Integer DEFAULT_MARGINLEFT = Integer.valueOf(0);
    public static final Integer DEFAULT_MARGINRIGHT = Integer.valueOf(0);
    public static final Integer DEFAULT_MARGINTOP = Integer.valueOf(0);
    public static final Float DEFAULT_SCALEX = Float.valueOf(1.0f);
    public static final Float DEFAULT_SCALEY = Float.valueOf(1.0f);
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REQUIRED, tag = 4, type = Datatype.INT32)
    public final Integer marginBottom;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.INT32)
    public final Integer marginLeft;
    @ProtoField(label = Label.REQUIRED, tag = 3, type = Datatype.INT32)
    public final Integer marginRight;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.INT32)
    public final Integer marginTop;
    @ProtoField(label = Label.REQUIRED, tag = 5, type = Datatype.FLOAT)
    public final Float scaleX;
    @ProtoField(label = Label.REQUIRED, tag = 6, type = Datatype.FLOAT)
    public final Float scaleY;

    public static final class Builder extends com.squareup.wire.Message.Builder<ScreenConfiguration> {
        public Integer marginBottom;
        public Integer marginLeft;
        public Integer marginRight;
        public Integer marginTop;
        public Float scaleX;
        public Float scaleY;

        public Builder(ScreenConfiguration message) {
            super(message);
            if (message != null) {
                this.marginLeft = message.marginLeft;
                this.marginTop = message.marginTop;
                this.marginRight = message.marginRight;
                this.marginBottom = message.marginBottom;
                this.scaleX = message.scaleX;
                this.scaleY = message.scaleY;
            }
        }

        public Builder marginLeft(Integer marginLeft) {
            this.marginLeft = marginLeft;
            return this;
        }

        public Builder marginTop(Integer marginTop) {
            this.marginTop = marginTop;
            return this;
        }

        public Builder marginRight(Integer marginRight) {
            this.marginRight = marginRight;
            return this;
        }

        public Builder marginBottom(Integer marginBottom) {
            this.marginBottom = marginBottom;
            return this;
        }

        public Builder scaleX(Float scaleX) {
            this.scaleX = scaleX;
            return this;
        }

        public Builder scaleY(Float scaleY) {
            this.scaleY = scaleY;
            return this;
        }

        public ScreenConfiguration build() {
            checkRequiredFields();
            return new ScreenConfiguration(this);
        }
    }

    public ScreenConfiguration(Integer marginLeft, Integer marginTop, Integer marginRight, Integer marginBottom, Float scaleX, Float scaleY) {
        this.marginLeft = marginLeft;
        this.marginTop = marginTop;
        this.marginRight = marginRight;
        this.marginBottom = marginBottom;
        this.scaleX = scaleX;
        this.scaleY = scaleY;
    }

    private ScreenConfiguration(Builder builder) {
        this(builder.marginLeft, builder.marginTop, builder.marginRight, builder.marginBottom, builder.scaleX, builder.scaleY);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof ScreenConfiguration)) {
            return false;
        }
        ScreenConfiguration o = (ScreenConfiguration) other;
        if (equals((Object) this.marginLeft, (Object) o.marginLeft) && equals((Object) this.marginTop, (Object) o.marginTop) && equals((Object) this.marginRight, (Object) o.marginRight) && equals((Object) this.marginBottom, (Object) o.marginBottom) && equals((Object) this.scaleX, (Object) o.scaleX) && equals((Object) this.scaleY, (Object) o.scaleY)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.marginLeft != null ? this.marginLeft.hashCode() : 0) * 37;
        if (this.marginTop != null) {
            hashCode = this.marginTop.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.marginRight != null) {
            hashCode = this.marginRight.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.marginBottom != null) {
            hashCode = this.marginBottom.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.scaleX != null) {
            hashCode = this.scaleX.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.scaleY != null) {
            i = this.scaleY.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
