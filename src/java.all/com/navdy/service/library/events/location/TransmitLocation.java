package com.navdy.service.library.events.location;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.ProtoField;

public final class TransmitLocation extends Message {
    public static final Boolean DEFAULT_SENDLOCATION = Boolean.valueOf(false);
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 1, type = Datatype.BOOL)
    public final Boolean sendLocation;

    public static final class Builder extends com.squareup.wire.Message.Builder<TransmitLocation> {
        public Boolean sendLocation;

        public Builder(TransmitLocation message) {
            super(message);
            if (message != null) {
                this.sendLocation = message.sendLocation;
            }
        }

        public Builder sendLocation(Boolean sendLocation) {
            this.sendLocation = sendLocation;
            return this;
        }

        public TransmitLocation build() {
            return new TransmitLocation(this);
        }
    }

    public TransmitLocation(Boolean sendLocation) {
        this.sendLocation = sendLocation;
    }

    private TransmitLocation(Builder builder) {
        this(builder.sendLocation);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (other instanceof TransmitLocation) {
            return equals((Object) this.sendLocation, (Object) ((TransmitLocation) other).sendLocation);
        }
        return false;
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = this.sendLocation != null ? this.sendLocation.hashCode() : 0;
        this.hashCode = hashCode;
        return hashCode;
    }
}
