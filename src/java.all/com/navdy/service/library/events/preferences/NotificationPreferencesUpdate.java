package com.navdy.service.library.events.preferences;

import com.navdy.service.library.events.RequestStatus;
import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class NotificationPreferencesUpdate extends Message {
    public static final Long DEFAULT_SERIAL_NUMBER = Long.valueOf(0);
    public static final RequestStatus DEFAULT_STATUS = RequestStatus.REQUEST_SUCCESS;
    public static final String DEFAULT_STATUSDETAIL = "";
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 4)
    public final NotificationPreferences preferences;
    @ProtoField(label = Label.REQUIRED, tag = 3, type = Datatype.INT64)
    public final Long serial_number;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final RequestStatus status;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String statusDetail;

    public static final class Builder extends com.squareup.wire.Message.Builder<NotificationPreferencesUpdate> {
        public NotificationPreferences preferences;
        public Long serial_number;
        public RequestStatus status;
        public String statusDetail;

        public Builder(NotificationPreferencesUpdate message) {
            super(message);
            if (message != null) {
                this.status = message.status;
                this.statusDetail = message.statusDetail;
                this.serial_number = message.serial_number;
                this.preferences = message.preferences;
            }
        }

        public Builder status(RequestStatus status) {
            this.status = status;
            return this;
        }

        public Builder statusDetail(String statusDetail) {
            this.statusDetail = statusDetail;
            return this;
        }

        public Builder serial_number(Long serial_number) {
            this.serial_number = serial_number;
            return this;
        }

        public Builder preferences(NotificationPreferences preferences) {
            this.preferences = preferences;
            return this;
        }

        public NotificationPreferencesUpdate build() {
            checkRequiredFields();
            return new NotificationPreferencesUpdate(this);
        }
    }

    public NotificationPreferencesUpdate(RequestStatus status, String statusDetail, Long serial_number, NotificationPreferences preferences) {
        this.status = status;
        this.statusDetail = statusDetail;
        this.serial_number = serial_number;
        this.preferences = preferences;
    }

    private NotificationPreferencesUpdate(Builder builder) {
        this(builder.status, builder.statusDetail, builder.serial_number, builder.preferences);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof NotificationPreferencesUpdate)) {
            return false;
        }
        NotificationPreferencesUpdate o = (NotificationPreferencesUpdate) other;
        if (equals((Object) this.status, (Object) o.status) && equals((Object) this.statusDetail, (Object) o.statusDetail) && equals((Object) this.serial_number, (Object) o.serial_number) && equals((Object) this.preferences, (Object) o.preferences)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.status != null ? this.status.hashCode() : 0) * 37;
        if (this.statusDetail != null) {
            hashCode = this.statusDetail.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.serial_number != null) {
            hashCode = this.serial_number.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.preferences != null) {
            i = this.preferences.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
