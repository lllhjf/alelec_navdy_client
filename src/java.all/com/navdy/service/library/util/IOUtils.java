package com.navdy.service.library.util;

import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.net.LocalSocket;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.os.StatFs;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import com.amazonaws.services.s3.internal.Constants;
import com.navdy.client.app.framework.util.SupportTicketService;
import com.navdy.service.library.log.Logger;
import com.vividsolutions.jts.geom.Dimension;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicLong;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.apache.commons.io.FileUtils;
import org.json.JSONObject;

public final class IOUtils {
    private static final int BUFFER_SIZE = 16384;
    private static final int BUFFER_SIZE_FOR_DOWNLOADS = 1024;
    private static final String CHECKSUM_FILE_NAME = ".checksum";
    private static final boolean CHECKSUM_UPDATE_MODE = false;
    private static final int DIGEST_BUFFER_SIZE = 1048576;
    private static final int END_OF_STREAM = -1;
    private static final int INPUT_BUFFER_SIZE = 16384;
    private static final String TAG = IOUtils.class.getName();
    private static final String TRASH_DIR_NAME = ".trash";
    public static final String UTF_8 = "UTF-8";
    private static final AtomicLong sCounter = new AtomicLong(1);
    private static File sExternalTrashDir;
    private static final Object sLock = new Object();
    private static Logger sLogger = new Logger(IOUtils.class);
    private static volatile File sTrashDir;

    private interface OnFileTraversal {
        void onFileTraversal(File file);
    }

    public static void closeObject(Closeable obj) {
        if (obj != null) {
            try {
                obj.close();
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }

    public static void closeStream(Closeable stream) {
        closeObject(stream);
    }

    public static void fileSync(FileOutputStream fileOutputStream) {
        if (fileOutputStream != null) {
            try {
                fileOutputStream.getFD().sync();
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }

    public static boolean deleteFile(Context context, String file) {
        if (file == null) {
            return false;
        }
        File deleteFile = new File(file);
        if (!deleteFile.exists()) {
            return false;
        }
        File newFile;
        if (isAppsInternalFile(context, file)) {
            newFile = new File(getNewTrashEntryPath(context));
        } else if (isAppsExternalFile(context, file)) {
            newFile = new File(getNewExternalTrashEntryPath(context));
        } else {
            newFile = new File(getTrashEntryPathInSameFolder(deleteFile));
        }
        sLogger.d("Deleted file: " + file + ":" + deleteFile.renameTo(newFile));
        return newFile.delete();
    }

    public static void deleteDirectory(Context context, File oldDir) {
        if (oldDir != null && oldDir.exists()) {
            File newDir;
            if (isAppsInternalFile(context, oldDir.getAbsolutePath())) {
                newDir = new File(getNewTrashEntryPath(context));
            } else if (isAppsExternalFile(context, oldDir.getAbsolutePath())) {
                newDir = new File(getNewExternalTrashEntryPath(context));
            } else {
                newDir = new File(getTrashEntryPathInSameFolder(oldDir));
            }
            boolean worked = oldDir.renameTo(newDir);
            if (!worked) {
                sLogger.e("Unable to rename dir " + oldDir + " to: " + newDir);
            }
            if (!worked) {
                newDir = oldDir;
            }
            try {
                deleteDirectoryInternal(context, newDir);
            } catch (Throwable iex) {
                sLogger.e(iex);
            }
        }
    }

    public static boolean createDirectory(String path) {
        return createDirectory(new File(path));
    }

    public static boolean createDirectory(File dir) {
        boolean worked = false;
        if (dir != null && !dir.exists()) {
            worked = dir.mkdirs();
        } else if (dir != null && dir.isDirectory()) {
            worked = true;
            sLogger.v("Directory already exists: " + dir.getPath());
        }
        if (worked) {
            sLogger.v("Successfully created directory: " + dir.getPath());
        } else {
            sLogger.e("Unable to create directory: " + (dir != null ? dir.getPath() : Constants.NULL_VERSION_ID));
        }
        return worked;
    }

    private static boolean isAppsInternalFile(Context context, String path) {
        return path.startsWith(context.getFilesDir().getAbsolutePath());
    }

    private static boolean isAppsExternalFile(Context context, String path) {
        return path.startsWith(context.getExternalFilesDir(null).getAbsolutePath());
    }

    private static void deleteDirectoryInternal(Context context, File directory) throws IOException {
        if (directory.exists()) {
            cleanDirectory(context, directory);
            if (!directory.delete()) {
                throw new IOException("Unable to delete directory " + directory);
            }
        }
    }

    private static void cleanDirectory(Context context, File directory) throws IOException {
        if (directory.exists() && directory.isDirectory()) {
            File[] files = directory.listFiles();
            if (files != null) {
                IOException exception = null;
                for (File file : files) {
                    try {
                        forceDelete(context, file);
                    } catch (IOException ioe) {
                        exception = ioe;
                    }
                }
                if (exception != null) {
                    throw exception;
                }
            }
        }
    }

    private static void forceDelete(Context context, File file) throws IOException {
        if (file.isDirectory()) {
            deleteDirectory(context, file);
        } else if (!file.delete()) {
            sLogger.e("Unable to delete kernel crash file: " + file);
        }
    }

    private static String getNewTrashEntryPath(Context context) {
        if (sTrashDir == null) {
            synchronized (sLock) {
                if (sTrashDir == null) {
                    sTrashDir = new File(context.getFilesDir().getAbsolutePath() + File.separator + TRASH_DIR_NAME);
                    createDirectory(sTrashDir);
                }
            }
        }
        return sTrashDir.getAbsolutePath() + File.separator + System.currentTimeMillis() + "_" + sCounter.getAndIncrement();
    }

    public static String getNewExternalTrashEntryPath(Context context) {
        String newExternalTrashEntryPath = null;
        if (context != null) {
            synchronized (sLock) {
                if (sExternalTrashDir == null) {
                    File externalFilesDir = context.getExternalFilesDir(null);
                    if (externalFilesDir != null) {
                        sExternalTrashDir = new File(externalFilesDir.getAbsolutePath() + File.separator + TRASH_DIR_NAME);
                        createDirectory(sExternalTrashDir);
                    }
                }
                newExternalTrashEntryPath = sExternalTrashDir.getAbsolutePath() + File.separator + System.currentTimeMillis() + "_" + sCounter.getAndIncrement();
            }
        }
        return newExternalTrashEntryPath;
    }

    public static String getTrashEntryPathInSameFolder(File file) {
        if (file == null) {
            return null;
        }
        return file.getParent() + File.separator + System.currentTimeMillis() + "_" + sCounter.getAndIncrement();
    }

    public static String convertInputStreamToString(InputStream inputStream, String charSet) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] buffer = new byte[16384];
        while (true) {
            int n = inputStream.read(buffer);
            if (n == -1) {
                return byteArrayOutputStream.toString(charSet);
            }
            byteArrayOutputStream.write(buffer, 0, n);
        }
    }

    public static String convertFileToString(String path) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(path);
        try {
            String convertInputStreamToString = convertInputStreamToString(fileInputStream, "UTF-8");
            return convertInputStreamToString;
        } finally {
            closeStream(fileInputStream);
        }
    }

    public static byte[] readInputStreamToByteArray(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] buffer = new byte[16384];
        while (true) {
            int n = inputStream.read(buffer);
            if (n == -1) {
                return byteArrayOutputStream.toByteArray();
            }
            byteArrayOutputStream.write(buffer, 0, n);
        }
    }

    public static byte[] readBinaryFile(String path) throws IOException {
        File file = new File(path);
        byte[] buffer = new byte[((int) file.length())];
        FileInputStream ios = new FileInputStream(file);
        try {
            ios.read(buffer);
            return buffer;
        } finally {
            ios.close();
        }
    }

    public static void closeFD(int fd) {
        if (fd != -1) {
            try {
                ParcelFileDescriptor.adoptFd(fd).close();
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }

    public static int getSocketFD(BluetoothSocket bluetoothSocket) {
        if (bluetoothSocket != null) {
            try {
                Field field = BluetoothSocket.class.getDeclaredField("mSocket");
                field.setAccessible(true);
                LocalSocket localSocket = (LocalSocket) field.get(bluetoothSocket);
                if (localSocket != null) {
                    field = LocalSocket.class.getDeclaredField("impl");
                    field.setAccessible(true);
                    Object localSocketImpl = field.get(localSocket);
                    if (localSocketImpl != null) {
                        field = localSocketImpl.getClass().getDeclaredField("fd");
                        field.setAccessible(true);
                        FileDescriptor fileDescriptor = (FileDescriptor) field.get(localSocketImpl);
                        if (fileDescriptor != null) {
                            field = FileDescriptor.class.getDeclaredField("descriptor");
                            field.setAccessible(true);
                            return ((Integer) field.get(fileDescriptor)).intValue();
                        }
                    }
                }
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
        return -1;
    }

    public static boolean copyFile(String srcPath, String targetPath) throws IOException {
        Throwable th;
        FileInputStream fileInputStream = null;
        try {
            File file = new File(srcPath);
            if (file.exists()) {
                InputStream fileInputStream2 = new FileInputStream(file);
                try {
                    copyFile(targetPath, fileInputStream2);
                    closeStream(null);
                    return true;
                } catch (Throwable th2) {
                    th = th2;
                    InputStream fileInputStream3 = fileInputStream2;
                    closeStream(fileInputStream3);
                    throw th;
                }
            }
            closeStream(null);
            return false;
        } catch (Throwable th3) {
            th = th3;
            closeStream(fileInputStream3);
            throw th;
        }
    }

    public static int copyFile(String targetPath, InputStream srcStream) throws IOException {
        Throwable th;
        FileOutputStream targetFile = null;
        int bytesCopied = 0;
        try {
            FileOutputStream targetFile2 = new FileOutputStream(targetPath);
            try {
                byte[] data = new byte[16384];
                while (true) {
                    int count = srcStream.read(data);
                    if (count > 0) {
                        targetFile2.write(data, 0, count);
                        bytesCopied += count;
                    } else {
                        closeStream(srcStream);
                        fileSync(targetFile2);
                        closeStream(targetFile2);
                        return bytesCopied;
                    }
                }
            } catch (Throwable th2) {
                th = th2;
                targetFile = targetFile2;
                closeStream(srcStream);
                fileSync(targetFile);
                closeStream(targetFile);
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            closeStream(srcStream);
            fileSync(targetFile);
            closeStream(targetFile);
            throw th;
        }
    }

    public static int copyFile(String targetPath, byte[] data) throws IOException {
        return copyFile(targetPath, data, true);
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x001a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int copyFile(String targetPath, byte[] data, boolean sync) throws IOException {
        Throwable th;
        FileOutputStream targetFile = null;
        try {
            FileOutputStream targetFile2 = new FileOutputStream(targetPath);
            try {
                targetFile2.write(data, 0, data.length);
                int bytesCopied = 0 + data.length;
                if (sync) {
                    fileSync(targetFile2);
                }
                closeStream(targetFile2);
                return bytesCopied;
            } catch (Throwable th2) {
                th = th2;
                targetFile = targetFile2;
                if (sync) {
                    fileSync(targetFile);
                }
                closeStream(targetFile);
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            if (sync) {
            }
            closeStream(targetFile);
            throw th;
        }
    }

    public static String hashForKey(String key) {
        if (key == null) {
            return null;
        }
        try {
            return hashForBytes(key.getBytes());
        } catch (NoSuchAlgorithmException e) {
            return String.valueOf(key.hashCode());
        }
    }

    public static String hashForBitmap(Bitmap bitmap) {
        String str = null;
        if (bitmap == null) {
            return str;
        }
        try {
            return hashForBytes(bitmap2ByteBuffer(bitmap));
        } catch (NoSuchAlgorithmException e) {
            return str;
        }
    }

    public static String hashForBytes(byte[] bytes) throws NoSuchAlgorithmException {
        if (bytes == null || bytes.length <= 0) {
            return null;
        }
        MessageDigest mDigest = MessageDigest.getInstance("MD5");
        if (mDigest == null) {
            return null;
        }
        mDigest.update(bytes);
        return bytesToHexString(mDigest.digest());
    }

    public static String hashForFile(File file) {
        return hashForFile(file, file.length());
    }

    public static String hashForFile(File file, long offset) {
        Throwable t;
        Throwable th;
        sLogger.recordStartTime();
        FileInputStream fis = null;
        if (file == null || !file.exists() || !file.canRead() || offset > file.length()) {
            throw new IllegalArgumentException();
        }
        try {
            MessageDigest mDigest = MessageDigest.getInstance("MD5");
            long totalRead = 0;
            byte[] buffer = new byte[1048576];
            FileInputStream fis2 = new FileInputStream(file);
            int bytesRead;
            do {
                try {
                    bytesRead = fis2.read(buffer, 0, (int) Math.min(offset - totalRead, (long) 1048576));
                    mDigest.update(buffer, 0, bytesRead);
                    totalRead += (long) bytesRead;
                    if (totalRead >= offset) {
                        break;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    fis = fis2;
                    closeStream(fis);
                    throw th;
                }
            } while (bytesRead != -1);
            String digest = bytesToHexString(mDigest.digest());
            sLogger.logTimeTaken("Time taken to calculate hash of the file of size :" + file.length());
            closeStream(fis2);
            fis = fis2;
            return digest;
        } catch (Throwable th3) {
            t = th3;
            sLogger.e("Exception while calculating md5 checksum for the file ", t);
            closeStream(fis);
            return null;
        }
    }

    public static String hashForPath(String path, boolean recursive, String... excludeFileNames) {
        sLogger.recordStartTime();
        try {
            final MessageDigest mDigest = MessageDigest.getInstance("MD5");
            final byte[] buffer = new byte[1048576];
            traverseFiles(path, recursive, Arrays.asList(excludeFileNames), new OnFileTraversal() {
                public void onFileTraversal(File file) {
                    Throwable t;
                    Throwable th;
                    FileInputStream fileInputStream = null;
                    try {
                        long length = file.length();
                        FileInputStream fileInputStream2 = new FileInputStream(file);
                        long totalRead = 0;
                        int bytesRead;
                        do {
                            try {
                                bytesRead = fileInputStream2.read(buffer, 0, (int) Math.min(length - totalRead, FileUtils.ONE_MB));
                                mDigest.update(buffer, 0, bytesRead);
                                totalRead += (long) bytesRead;
                                if (totalRead >= length) {
                                    break;
                                }
                            } catch (Throwable th2) {
                                th = th2;
                                fileInputStream = fileInputStream2;
                            }
                        } while (bytesRead != -1);
                        IOUtils.closeStream(fileInputStream2);
                        fileInputStream = fileInputStream2;
                    } catch (Throwable th3) {
                        t = th3;
                        try {
                            IOUtils.sLogger.e("Exception while calculating md5 checksum for a path ", t);
                            IOUtils.closeStream(fileInputStream);
                        } catch (Throwable th4) {
                            th = th4;
                            IOUtils.closeStream(fileInputStream);
                            throw th;
                        }
                    }
                }
            });
            String digest = bytesToHexString(mDigest.digest());
            sLogger.logTimeTaken("Time taken to calculate hash of all files on path:" + path);
            return digest;
        } catch (Throwable t) {
            sLogger.e("Exception while calculating md5 checksum for a path ", t);
            return null;
        }
    }

    private static void traverseFiles(String path, boolean recursive, List<String> excludeFileNames, OnFileTraversal onFileTraversal) throws FileNotFoundException {
        File containingDirectory = new File(path);
        if (containingDirectory.isDirectory()) {
            File[] files = containingDirectory.listFiles();
            if (files != null) {
                Arrays.sort(files);
                for (File file : files) {
                    if (!file.isDirectory() && !excludeFileNames.contains(file.getName())) {
                        onFileTraversal.onFileTraversal(file);
                    } else if (recursive) {
                        traverseFiles(file.getPath(), true, excludeFileNames, onFileTraversal);
                    }
                }
            }
        }
    }

    public static void checkIntegrity(Context context, String path, int md5ReferenceRes) {
        Throwable e;
        Throwable th;
        sLogger.i("integrity check for " + path + " starting");
        long l1 = SystemClock.elapsedRealtime();
        String buildChecksum = null;
        String cachedChecksum = null;
        InputStream buildChecksumInputStream = null;
        try {
            buildChecksumInputStream = context.getResources().openRawResource(md5ReferenceRes);
            buildChecksum = convertInputStreamToString(buildChecksumInputStream, "UTF-8");
            cachedChecksum = convertFileToString(path + File.separator + CHECKSUM_FILE_NAME);
        } catch (Throwable e2) {
            sLogger.i("error while retrieving integrity checksum from filesystem, might not be present", e2);
        } finally {
            closeStream(buildChecksumInputStream);
        }
        boolean cachedAndBuildChecksumsAreSame = (buildChecksum == null || cachedChecksum == null || !buildChecksum.equals(cachedChecksum)) ? false : true;
        if (cachedAndBuildChecksumsAreSame) {
            sLogger.v("checksum for " + path + " is fine, no-op");
        } else {
            String actualChecksum = hashForPath(path, true, CHECKSUM_FILE_NAME);
            boolean actualAndBuildChecksumsAreSame = (actualChecksum == null || buildChecksum == null || !actualChecksum.equals(buildChecksum)) ? false : true;
            if (actualAndBuildChecksumsAreSame) {
                sLogger.v("checksum for " + path + " is fine, writing integrity checksum on filesystem");
                PrintWriter printWriter = null;
                try {
                    PrintWriter printWriter2 = new PrintWriter(path + File.separator + CHECKSUM_FILE_NAME);
                    try {
                        printWriter2.print(actualChecksum);
                        closeStream(printWriter2);
                        printWriter = printWriter2;
                    } catch (Throwable th2) {
                        th = th2;
                        printWriter = printWriter2;
                        closeStream(printWriter);
                        throw th;
                    }
                } catch (Throwable th3) {
                    e2 = th3;
                    sLogger.e("could not write integrity checksum on filesystem", e2);
                    closeStream(printWriter);
                    sLogger.i("integrity check for " + path + " took " + (SystemClock.elapsedRealtime() - l1) + " ms");
                }
            }
            sLogger.w("files on " + path + " are out of date or corrupted, redoing");
            deleteDirectory(context, new File(path));
            createDirectory(path);
        }
        sLogger.i("integrity check for " + path + " took " + (SystemClock.elapsedRealtime() - l1) + " ms");
    }

    public static byte[] bitmap2ByteBuffer(Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(CompressFormat.PNG, 100, stream);
        return stream.toByteArray();
    }

    public static String bytesToHexString(byte[] bytes, int offset, int length) {
        StringBuilder sb = new StringBuilder();
        for (int i = offset; i < length; i++) {
            String hex = Integer.toHexString(bytes[i] & 255);
            if (hex.length() == 1) {
                sb.append(Dimension.SYM_P);
            }
            sb.append(hex);
        }
        return sb.toString();
    }

    public static String bytesToHexString(byte[] bytes) {
        return bytesToHexString(bytes, 0, bytes.length);
    }

    public static long getFreeSpace(String directory) {
        File file = new File(directory);
        if (!file.exists() || !file.isDirectory()) {
            return -1;
        }
        StatFs statFs = new StatFs(directory);
        return ((long) statFs.getFreeBlocks()) * ((long) statFs.getBlockSize());
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x003e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static JSONObject getJSONFromURL(String url) {
        JSONObject jSONObject;
        Throwable e;
        Throwable th;
        HttpURLConnection urlConnection = null;
        InputStreamReader input = null;
        try {
            urlConnection = (HttpURLConnection) new URL(url).openConnection();
            InputStreamReader input2 = new InputStreamReader(urlConnection.getInputStream(), "UTF-8");
            try {
                jSONObject = new JSONObject(convertInputStreamToString(urlConnection.getInputStream(), "UTF-8"));
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                closeStream(input2);
                input = input2;
            } catch (Throwable th2) {
                th = th2;
                input = input2;
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                closeStream(input);
                throw th;
            }
        } catch (Throwable th3) {
            e = th3;
            sLogger.e("Exception while downloading/parsing json", e);
            jSONObject = null;
            if (urlConnection != null) {
            }
            closeStream(input);
            return jSONObject;
        }
        return jSONObject;
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0059  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] downloadImage(String url) {
        Throwable e;
        byte[] bArr;
        Throwable th;
        HttpURLConnection urlConnection = null;
        BufferedInputStream input = null;
        ByteArrayOutputStream output = null;
        try {
            ByteArrayOutputStream output2;
            urlConnection = (HttpURLConnection) new URL(url).openConnection();
            BufferedInputStream input2 = new BufferedInputStream(urlConnection.getInputStream());
            try {
                output2 = new ByteArrayOutputStream();
            } catch (Throwable th2) {
                th = th2;
                input = input2;
                if (urlConnection != null) {
                }
                closeStream(input);
                closeStream(output);
                throw th;
            }
            try {
                byte[] buf = new byte[1024];
                while (true) {
                    int n = input2.read(buf);
                    if (n < 0) {
                        break;
                    }
                    output2.write(buf, 0, n);
                }
                bArr = output2.toByteArray();
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                closeStream(input2);
                closeStream(output2);
                output = output2;
                input = input2;
            } catch (Throwable th3) {
                th = th3;
                output = output2;
                input = input2;
                if (urlConnection != null) {
                }
                closeStream(input);
                closeStream(output);
                throw th;
            }
        } catch (Throwable th4) {
            e = th4;
            sLogger.e("Exception while downloading binary resource", e);
            bArr = null;
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            closeStream(input);
            closeStream(output);
            return bArr;
        }
        return bArr;
    }

    public static void compressFilesToZip(Context context, File[] zipEntryFiles, String zipFilePath) {
        Throwable t;
        Throwable th;
        File file = new File(zipFilePath);
        if (file.exists()) {
            deleteFile(context, zipFilePath);
        }
        try {
            if (file.createNewFile() && zipEntryFiles != null) {
                FileOutputStream fos = null;
                FileInputStream fis = null;
                ZipOutputStream zos = null;
                try {
                    byte[] buffer = new byte[16384];
                    FileOutputStream fos2 = new FileOutputStream(zipFilePath);
                    try {
                        ZipOutputStream zos2 = new ZipOutputStream(fos2);
                        try {
                            int length = zipEntryFiles.length;
                            int i = 0;
                            while (true) {
                                FileInputStream fis2 = fis;
                                if (i < length) {
                                    try {
                                        File entryFile = zipEntryFiles[i];
                                        if (!entryFile.exists()) {
                                            fis = fis2;
                                        } else if (entryFile.canRead()) {
                                            fis = new FileInputStream(entryFile);
                                            zos2.putNextEntry(new ZipEntry(entryFile.getName()));
                                            while (true) {
                                                int length2 = fis.read(buffer);
                                                if (length2 <= 0) {
                                                    break;
                                                }
                                                zos2.write(buffer, 0, length2);
                                            }
                                            zos2.closeEntry();
                                            closeStream(fis);
                                            fis = null;
                                        } else {
                                            fis = fis2;
                                        }
                                        i++;
                                    } catch (Throwable th2) {
                                        th = th2;
                                        zos = zos2;
                                        fis = fis2;
                                        fos = fos2;
                                        closeStream(zos);
                                        closeStream(fis);
                                        closeStream(fos);
                                        throw th;
                                    }
                                }
                                closeStream(zos2);
                                closeStream(fis2);
                                closeStream(fos2);
                                return;
                            }
                        } catch (Throwable th3) {
                            th = th3;
                            zos = zos2;
                            fos = fos2;
                        }
                    } catch (Throwable th4) {
                        th = th4;
                        fos = fos2;
                        closeStream(zos);
                        closeStream(fis);
                        closeStream(fos);
                        throw th;
                    }
                } catch (Throwable th5) {
                    t = th5;
                    sLogger.e("Error while compressing files ", t);
                    closeStream(zos);
                    closeStream(fis);
                    closeStream(fos);
                }
            }
        } catch (IOException e) {
            sLogger.e("IO Exception while creating new file");
        }
    }

    public static boolean isExternalStorageReadable() {
        return "mounted".equals(Environment.getExternalStorageState());
    }

    public static File getTempFile(@Nullable Logger logger, String fileName) {
        if (Environment.getExternalStorageState().equals("mounted")) {
            File file;
            if (TextUtils.isEmpty(fileName)) {
                file = new File(Environment.getExternalStorageDirectory(), getTempFilename());
            } else {
                file = new File(Environment.getExternalStorageDirectory(), fileName);
            }
            try {
                if (!file.createNewFile()) {
                    return file;
                }
                if (logger != null) {
                    logger.v("Created " + file.getAbsolutePath());
                    return file;
                }
                Log.v(TAG, "Created " + file.getAbsolutePath());
                return file;
            } catch (IOException e) {
                if (logger != null) {
                    logger.e("FileUtils:: Unable to create file. ", e);
                    return file;
                }
                Log.e(TAG, "FileUtils:: Unable to create file. ", e);
                return file;
            }
        }
        if (logger != null) {
            logger.e("FileUtils:: External Storage not mounted!");
        } else {
            Log.e(TAG, "FileUtils:: External Storage not mounted!");
        }
        return null;
    }

    public static String getExternalStorageFolderPath() {
        if (Environment.getExternalStorageState().equals("mounted")) {
            File externalStorageDirectory = Environment.getExternalStorageDirectory();
            if (externalStorageDirectory != null && externalStorageDirectory.exists()) {
                return externalStorageDirectory.getPath();
            }
        }
        return null;
    }

    public static String getTempFilename() {
        return new SimpleDateFormat(SupportTicketService.NEW_FOLDER_FORMAT, Locale.US).format(new Date(System.currentTimeMillis())) + ".jpg";
    }
}
