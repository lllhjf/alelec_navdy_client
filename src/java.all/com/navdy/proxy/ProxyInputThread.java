package com.navdy.proxy;

import com.navdy.service.library.log.Logger;
import java.io.IOException;
import java.io.InputStream;

public class ProxyInputThread extends Thread {
    private static final boolean VERBOSE_DBG = false;
    private static final Logger sLogger = new Logger(ProxyInputThread.class);
    InputStream mInputStream;
    ProxyThread mProxyThread;

    public ProxyInputThread(ProxyThread proxyThread, InputStream inputStream) {
        this.mProxyThread = proxyThread;
        this.mInputStream = inputStream;
    }

    public void run() {
        super.run();
        byte[] buffer = new byte[1024];
        while (true) {
            try {
                int bytesRead = this.mInputStream.read(buffer);
                if (bytesRead < 0) {
                    break;
                } else if (bytesRead > 0) {
                    this.mProxyThread.receiveBytes(buffer, bytesRead);
                }
            } catch (IOException e) {
                sLogger.i("mInputStream closed: " + e);
            }
        }
        this.mProxyThread.inputThreadWillFinish();
    }
}
