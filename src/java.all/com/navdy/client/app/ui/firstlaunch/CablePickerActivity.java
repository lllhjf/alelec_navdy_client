package com.navdy.client.app.ui.firstlaunch;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.alelec.navdyclient.R;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.Install;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsUtils;

public class CablePickerActivity extends BaseActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.fle_cable_picker);
        loadImage(R.id.illustration, R.drawable.image_install_cords);
        loadImage(R.id.obd2_illustration, R.drawable.image_obd_plug);
        loadImage(R.id.cla_illustration, R.drawable.image_12_v_plug);
    }

    protected void onResume() {
        super.onResume();
        hideSystemUI();
        Tracker.tagScreen(Install.CABLE_PICKER);
    }

    public void onObd2Click(View view) {
        SettingsUtils.getSharedPreferences().edit().putString(SettingsConstants.POWER_CABLE_SELECTION, "OBD").apply();
        Intent intent = new Intent(getApplicationContext(), InstallActivity.class);
        intent.putExtra("extra_step", R.layout.fle_install_locate_obd);
        startActivity(intent);
    }

    public void onClaClick(View view) {
        SettingsUtils.getSharedPreferences().edit().putString(SettingsConstants.POWER_CABLE_SELECTION, SettingsConstants.POWER_CABLE_SELECTION_CLA).apply();
        startActivity(new Intent(getApplicationContext(), InstallClaActivity.class));
    }

    public void onBackClick(View view) {
        finish();
    }
}
