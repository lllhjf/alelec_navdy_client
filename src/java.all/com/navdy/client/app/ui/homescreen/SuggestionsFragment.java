package com.navdy.client.app.ui.homescreen;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.support.v4.app.FragmentActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.view.ActionMode.Callback;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.here.android.mpa.common.GeoPolyline;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.Map.Animation;
import com.here.android.mpa.mapping.MapMarker;
import com.here.android.mpa.mapping.MapPolyline;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.Injector;
import com.navdy.client.app.framework.location.NavdyLocationManager;
import com.navdy.client.app.framework.location.NavdyLocationManager.OnNavdyLocationChangedListener;
import com.navdy.client.app.framework.map.MapUtils;
import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.framework.models.Suggestion;
import com.navdy.client.app.framework.models.Suggestion.SuggestionType;
import com.navdy.client.app.framework.navigation.NavdyRouteHandler;
import com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo;
import com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteListener;
import com.navdy.client.app.framework.servicehandler.NetworkStatusManager.ReachabilityEvent;
import com.navdy.client.app.framework.suggestion.DestinationSuggestionService;
import com.navdy.client.app.framework.util.CustomItemClickListener;
import com.navdy.client.app.framework.util.CustomSuggestionsLongItemClickListener;
import com.navdy.client.app.framework.util.GmsUtils;
import com.navdy.client.app.framework.util.SuggestionManager;
import com.navdy.client.app.framework.util.SuggestionManager.SuggestionBuildListener;
import com.navdy.client.app.providers.NavdyContentProvider.QueryResultCallback;
import com.navdy.client.app.tracking.SetDestinationTracker;
import com.navdy.client.app.tracking.TrackerConstants.Attributes.DestinationAttributes.SOURCE_VALUES;
import com.navdy.client.app.tracking.TrackerConstants.Attributes.DestinationAttributes.TYPE_VALUES;
import com.navdy.client.app.ui.UiUtils;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.base.BaseGoogleMapFragment;
import com.navdy.client.app.ui.base.BaseGoogleMapFragment.OnShowMapListener;
import com.navdy.client.app.ui.base.BaseHereMapFragment;
import com.navdy.client.app.ui.base.BaseHereMapFragment.OnHereMapFragmentInitialized;
import com.navdy.client.app.ui.base.BaseHereMapFragment.OnHereMapFragmentReady;
import com.navdy.client.app.ui.base.BaseSupportFragment;
import com.navdy.client.app.ui.customviews.TripCardView;
import com.navdy.client.app.ui.details.DetailsActivity;
import com.navdy.client.app.ui.search.DropPinActivity;
import com.navdy.client.app.ui.search.SearchActivity;
import com.navdy.client.app.ui.search.SearchConstants.SEARCH_TYPES;
import com.navdy.client.app.ui.settings.GestureDialogActivity;
import com.navdy.client.app.ui.settings.OtaSettingsActivity;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.client.ota.OTAUpdateListener.DownloadUpdateStatus;
import com.navdy.client.ota.OTAUpdateListener.UploadToHUDStatus;
import com.navdy.client.ota.OTAUpdateService;
import com.navdy.client.ota.OTAUpdateService.State;
import com.navdy.client.ota.OTAUpdateServiceInterface;
import com.navdy.client.ota.OTAUpdateUIClient;
import com.navdy.client.ota.OTAUpdateUIClient.Error;
import com.navdy.client.ota.model.UpdateInfo;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Subscribe;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class SuggestionsFragment extends BaseSupportFragment implements OTAUpdateUIClient, ServiceConnection, NavdyRouteListener, SuggestionBuildListener {
    private static final int HERE_PADDING = MapUtils.hereMapSidePadding;
    private static final int HERE_TOP_PADDING;
    private static final String LAST_ARRIVAL_MARKER_STRING;
    private static final int MIN_DISTANCE_REBUILD_SUGGESTIONS = 500;
    private static final boolean VERBOSE = true;
    private static final Logger logger = new Logger(SuggestionsFragment.class);
    private final CustomItemClickListener clickListener = new CustomItemClickListener() {
        public void onClick(View v, int position) {
            SuggestionsFragment.logger.v("position clicked: " + position);
            if (SuggestionsFragment.this.suggestionAdapter == null) {
                SuggestionsFragment.logger.e("onClick with a null adapter! WTF!");
                return;
            }
            boolean isHeader;
            Suggestion suggestion = SuggestionsFragment.this.suggestionAdapter.getItem(position);
            if (position == 0) {
                isHeader = true;
            } else {
                isHeader = false;
            }
            if (isHeader || (v instanceof TripCardView)) {
                boolean isInPendingTrip = SuggestionsFragment.this.navdyRouteHandler.isInOneOfThePendingTripStates();
                boolean isInActiveTrip = SuggestionsFragment.this.navdyRouteHandler.isInOneOfTheActiveTripStates();
                if (isInPendingTrip) {
                    SuggestionsFragment.this.suggestionAdapter.clickPendingTripCard(SuggestionsFragment.this.getActivity());
                } else if (isInActiveTrip) {
                    SuggestionsFragment.this.suggestionAdapter.clickActiveTripCard();
                } else {
                    SuggestionsFragment.this.startActivity(new Intent(SuggestionsFragment.this.getContext(), DropPinActivity.class));
                }
            } else if (suggestion != null) {
                final HomescreenActivity activity = (HomescreenActivity) SuggestionsFragment.this.getActivity();
                if (suggestion.getType() == SuggestionType.LOADING) {
                    return;
                }
                if (suggestion.isTip()) {
                    switch (suggestion.getType()) {
                        case ENABLE_MICROPHONE:
                            handleTipClick(SettingsConstants.USER_ALREADY_SAW_MICROPHONE_TIP, new Runnable() {
                                public void run() {
                                    SuggestionsFragment.this.startActivity(new Intent(SuggestionsFragment.this.getContext(), MicPermissionDialogActivity.class));
                                }
                            });
                            return;
                        case VOICE_SEARCH:
                            handleTipClick(SettingsConstants.USER_ALREADY_SAW_MICROPHONE_TIP, new Runnable() {
                                public void run() {
                                    Intent i = new Intent(SuggestionsFragment.this.getContext(), InfoDialogActivity.class);
                                    i.putExtra(InfoDialogActivity.EXTRA_LAYOUT, R.layout.dialog_voice_search);
                                    SuggestionsFragment.this.startActivity(i);
                                }
                            });
                            return;
                        case GOOGLE_NOW:
                            handleTipClick(SettingsConstants.USER_ALREADY_SAW_GOOGLE_NOW_TIP, new Runnable() {
                                public void run() {
                                    Intent i = new Intent(SuggestionsFragment.this.getContext(), InfoDialogActivity.class);
                                    i.putExtra(InfoDialogActivity.EXTRA_LAYOUT, R.layout.dialog_google_now);
                                    SuggestionsFragment.this.startActivity(i);
                                }
                            });
                            return;
                        case HUD_LOCAL_MUSIC_BROWSER:
                            handleTipClick(SettingsConstants.USER_ALREADY_SAW_LOCAL_MUSIC_BROWSER_TIP, new Runnable() {
                                public void run() {
                                    Intent i = new Intent(SuggestionsFragment.this.getContext(), InfoDialogActivity.class);
                                    i.putExtra(InfoDialogActivity.EXTRA_LAYOUT, R.layout.dialog_hud_local_music_browser);
                                    SuggestionsFragment.this.startActivity(i);
                                }
                            });
                            return;
                        case ENABLE_GESTURES:
                            handleTipClick(SettingsConstants.USER_ALREADY_SAW_GESTURE_TIP, new Runnable() {
                                public void run() {
                                    activity.goToGesture();
                                }
                            });
                            return;
                        case TRY_GESTURES:
                            handleTipClick(SettingsConstants.USER_TRIED_GESTURES_ONCE_BEFORE, new Runnable() {
                                public void run() {
                                    SuggestionsFragment.this.startActivity(new Intent(SuggestionsFragment.this.getContext(), GestureDialogActivity.class));
                                }
                            });
                            return;
                        case ADD_HOME:
                            handleTipClick(SettingsConstants.USER_ALREADY_SAW_ADD_HOME_TIP, new Runnable() {
                                public void run() {
                                    activity.goToFavoritesTab();
                                    SearchActivity.startSearchActivityFor(SEARCH_TYPES.HOME, false, activity);
                                }
                            });
                            return;
                        case ADD_WORK:
                            handleTipClick(SettingsConstants.USER_ALREADY_SAW_ADD_WORK_TIP, new Runnable() {
                                public void run() {
                                    activity.goToFavoritesTab();
                                    SearchActivity.startSearchActivityFor(SEARCH_TYPES.WORK, false, activity);
                                }
                            });
                            return;
                        case ADD_FAVORITE:
                            SearchActivity.startSearchActivityFor(SEARCH_TYPES.FAVORITE, false, activity);
                            return;
                        case OTA:
                            SuggestionsFragment.this.startActivity(new Intent(SuggestionsFragment.this.getContext(), OtaSettingsActivity.class));
                            return;
                        case DEMO_VIDEO:
                            SuggestionsFragment.this.startActivity(new Intent(SuggestionsFragment.this.getContext(), DemoVideoDialogActivity.class));
                            return;
                        default:
                            handleTipClick(SettingsConstants.USER_ENABLED_GLANCES_ONCE_BEFORE, new Runnable() {
                                public void run() {
                                    activity.goToGlancesTab();
                                    SuggestionsFragment.this.startActivity(new Intent(SuggestionsFragment.this.getContext(), GlanceDialogActivity.class));
                                }
                            });
                            return;
                    }
                }
                final Destination destination = suggestion.destination;
                SuggestionsFragment.logger.v("destination object status: " + destination.toString());
                final SetDestinationTracker setDestinationTracker = SetDestinationTracker.getInstance();
                setDestinationTracker.setSourceValue(SOURCE_VALUES.SUGGESTION_LIST);
                setDestinationTracker.setDestinationType(TYPE_VALUES.getSetDestinationTypeFromSuggestion(suggestion));
                if (v.getId() != R.id.nav_button) {
                    activity.showRequestNewRouteDialog(new Runnable() {
                        public void run() {
                            setDestinationTracker.tagSetDestinationEvent(destination);
                            SuggestionsFragment.this.navdyRouteHandler.requestNewRoute(destination);
                        }
                    });
                } else if (v instanceof ImageButton) {
                    DetailsActivity.startDetailsActivity(destination, activity);
                }
            }
        }

        private void handleTipClick(String sharedPrefKey, Runnable runnable) {
            Editor sharedPrefEditor = SettingsUtils.getSharedPreferences().edit();
            sharedPrefEditor.putBoolean(sharedPrefKey, true);
            sharedPrefEditor.apply();
            if (SuggestionsFragment.this.suggestionAdapter != null) {
                SuggestionsFragment.this.suggestionAdapter.rebuildSuggestions();
            }
            runnable.run();
        }
    };
    private MapMarker destinationMarker;
    private BaseGoogleMapFragment googleMapFragment;
    private BaseHereMapFragment hereMapFragment;
    private Marker lastArrivalMarker;
    private Coordinate lastUpdate;
    private LinearLayoutManager layoutManager;
    private final OnNavdyLocationChangedListener locationListener = new OnNavdyLocationChangedListener() {
        public void onPhoneLocationChanged(@NonNull Coordinate phoneLocation) {
            if (SuggestionsFragment.this.suggestionAdapter == null) {
                return;
            }
            if (SuggestionsFragment.this.lastUpdate == null || MapUtils.distanceBetween(phoneLocation, SuggestionsFragment.this.lastUpdate) > 500.0d) {
                SuggestionsFragment.this.lastUpdate = phoneLocation;
                SuggestionsFragment.this.suggestionAdapter.rebuildSuggestions();
            }
        }

        public void onCarLocationChanged(@NonNull Coordinate carLocation) {
            if (SuggestionsFragment.this.suggestionAdapter == null) {
                return;
            }
            if (SuggestionsFragment.this.lastUpdate == null || MapUtils.distanceBetween(carLocation, SuggestionsFragment.this.lastUpdate) > 500.0d) {
                SuggestionsFragment.this.lastUpdate = carLocation;
                SuggestionsFragment.this.suggestionAdapter.rebuildSuggestions();
            }
        }
    };
    private final Object lock = new Object();
    private final NavdyLocationManager navdyLocationManager = NavdyLocationManager.getInstance();
    private final NavdyRouteHandler navdyRouteHandler = NavdyRouteHandler.getInstance();
    private RelativeLayout offlineBanner;
    private final CustomSuggestionsLongItemClickListener onLongClickListener = new CustomSuggestionsLongItemClickListener() {
        public boolean onItemLongClick(View selectedCardRow, int actionModePosition, Suggestion suggestion) {
            HomescreenActivity homescreenActivity = (HomescreenActivity) SuggestionsFragment.this.getActivity();
            if (homescreenActivity == null || homescreenActivity.isFinishing()) {
                return false;
            }
            homescreenActivity.startSelectionActionMode(new CustomSuggestionsActionCallback(suggestion, homescreenActivity, actionModePosition, selectedCardRow));
            return true;
        }
    };
    private OTAUpdateServiceInterface otaUpdateService;
    private MapPolyline routePolyline;
    private MapPolyline routeProgressPolyline;
    private SuggestionScrollListener scrollListener;
    SuggestedDestinationsAdapter suggestionAdapter;
    private WeakReference<SuggestionBuildListener> suggestionListener = new WeakReference(this);
    private RecyclerView suggestionRecycler;

    private class CustomSuggestionsActionCallback implements Callback {
        private final int actionModePosition;
        private final HomescreenActivity homescreenActivity;
        private final View selectedCardRow;
        private final Suggestion suggestion;

        CustomSuggestionsActionCallback(Suggestion suggestion, HomescreenActivity homescreenActivity, int actionModePosition, View selectedCardRow) {
            this.suggestion = suggestion;
            this.homescreenActivity = homescreenActivity;
            this.actionModePosition = actionModePosition;
            this.selectedCardRow = selectedCardRow;
        }

        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater inflater = mode.getMenuInflater();
            if (this.suggestion.isActiveTrip()) {
                inflater.inflate(R.menu.menu_active_trip_edit, menu);
                mode.setTitle((int) R.string.active_trip_title);
            } else if (this.suggestion.isPendingTrip()) {
                inflater.inflate(R.menu.menu_next_trip_edit, menu);
                mode.setTitle((int) R.string.next_trip_title);
            } else if (this.suggestion.canBeRoutedTo()) {
                inflater.inflate(R.menu.menu_suggestion_edit, menu);
                mode.setTitle((int) R.string.edit_suggestion_title);
            }
            return true;
        }

        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            if (SuggestionsFragment.this.suggestionAdapter != null) {
                SuggestionsFragment.this.suggestionAdapter.setSelectedSuggestion(this.selectedCardRow, this.suggestion.getType(), this.actionModePosition);
                Suggestion suggestion = SuggestionsFragment.this.suggestionAdapter.getItem(this.actionModePosition);
                if (!(suggestion == null || suggestion.destination == null || !suggestion.destination.isFavoriteDestination())) {
                    SuggestionsFragment.logger.v("Suggestion is a Favorite. Save option is disabled");
                    menu.findItem(R.id.menu_save).setVisible(false);
                }
            }
            hideTabs();
            return true;
        }

        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            if (SuggestionsFragment.this.suggestionAdapter == null) {
                return false;
            }
            Suggestion suggestion = SuggestionsFragment.this.suggestionAdapter.getItem(this.actionModePosition);
            switch (item.getItemId()) {
                case R.id.menu_save /*2131756062*/:
                    saveSuggestionAsFavorite(suggestion);
                    break;
                case R.id.menu_stop /*2131756063*/:
                    stopRoute(suggestion);
                    break;
                case R.id.menu_delete /*2131756065*/:
                    deleteSuggestionInMenu(suggestion);
                    break;
                default:
                    return false;
            }
            mode.finish();
            return true;
        }

        public void onDestroyActionMode(ActionMode mode) {
            SuggestionsFragment.logger.v("onDestroyActionMode from Suggestion: " + mode);
            if (SuggestionsFragment.this.suggestionAdapter != null) {
                SuggestionsFragment.this.suggestionAdapter.endSelectionMode();
            }
            showTabs();
        }

        private void hideTabs() {
            if (this.homescreenActivity != null && !this.homescreenActivity.isFinishing()) {
                View tabs = this.homescreenActivity.findViewById(R.id.tabs_cover);
                if (tabs != null) {
                    tabs.setVisibility(VISIBLE);
                }
            }
        }

        private void showTabs() {
            if (this.homescreenActivity != null && !this.homescreenActivity.isFinishing()) {
                View tabs = this.homescreenActivity.findViewById(R.id.tabs_cover);
                if (tabs != null) {
                    tabs.setVisibility(GONE);
                }
            }
        }

        private void saveSuggestionAsFavorite(Suggestion suggestion) {
            SuggestionsFragment.logger.v("menu save");
            if (suggestion.destination != null) {
                SuggestionsFragment.logger.v("suggestion type: " + suggestion.getType());
                SuggestionsFragment.logger.v("suggestion name: " + suggestion.destination.name);
                Destination destination = suggestion.destination;
                if (destination.isFavoriteDestination()) {
                    showFavoriteEditErrorToast();
                }
                SuggestionsFragment.this.showProgressDialog();
                destination.saveDestinationAsFavoritesAsync(-1, new QueryResultCallback() {
                    public void onQueryCompleted(int nbRows, Uri uri) {
                        SuggestionsFragment.this.hideProgressDialog();
                        if (nbRows <= 0) {
                            CustomSuggestionsActionCallback.this.showFavoriteEditErrorToast();
                            return;
                        }
                        CustomSuggestionsActionCallback.this.showFavoriteEditSuccessToast();
                        if (!CustomSuggestionsActionCallback.this.homescreenActivity.isFinishing()) {
                            CustomSuggestionsActionCallback.this.homescreenActivity.onFavoriteListChanged();
                        }
                    }
                });
            }
        }

        private void deleteSuggestionInMenu(final Suggestion suggestion) {
            SuggestionsFragment.logger.v("menu delete");
            if (suggestion != null && suggestion.destination != null) {
                new Builder(SuggestionsFragment.this.getActivity()).setTitle(R.string.are_you_sure).setMessage(R.string.do_not_suggest_desc).setPositiveButton(R.string.YES, new OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        suggestion.destination.doNotSuggest = true;
                        DestinationSuggestionService.reset(NavdyApplication.getAppContext());
                        suggestion.destination.updateDoNotSuggestAsync();
                        if (SuggestionsFragment.this.suggestionAdapter != null) {
                            SuggestionsFragment.this.suggestionAdapter.rebuildSuggestions();
                        }
                    }
                }).setNegativeButton(R.string.no_thanks, null).show();
            }
        }

        private void stopRoute(Suggestion suggestion) {
            SuggestionsFragment.logger.v("menu stop");
            SuggestionsFragment.this.navdyRouteHandler.stopRouting();
            if (this.suggestion.isPendingTrip() && suggestion != null && suggestion.destination != null) {
                if (SuggestionsFragment.this.suggestionAdapter != null) {
                    SuggestionsFragment.this.suggestionAdapter.endSelectionMode();
                }
                SuggestionsFragment.this.scrollToTheTop();
            }
        }

        private void showFavoriteEditSuccessToast() {
            BaseActivity.showLongToast(R.string.toast_favorite_edit_saved, new Object[0]);
        }

        private void showFavoriteEditErrorToast() {
            BaseActivity.showLongToast(R.string.toast_favorite_edit_error, new Object[0]);
        }
    }

    private class SuggestionScrollListener extends OnScrollListener {
        private boolean drawerIsOpen;
        private boolean drawerWasOpen;
        private int mapSizeInPixels;
        private int y;

        private SuggestionScrollListener() {
            this.mapSizeInPixels = 0;
            this.y = 0;
            this.drawerIsOpen = false;
            this.drawerWasOpen = false;
        }

//        /* synthetic */ SuggestionScrollListener(SuggestionsFragment x0, AnonymousClass1 x1) {
//            this();
//        }

        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            boolean z = false;
            this.y += dy;
            if (this.y < 0) {
                this.y = 0;
            }
            if (this.drawerIsOpen != this.drawerWasOpen) {
                return;
            }
            if ((this.drawerWasOpen && this.y < this.mapSizeInPixels) || (!this.drawerWasOpen && this.y > 0)) {
                if (!this.drawerWasOpen) {
                    z = true;
                }
                this.drawerWasOpen = z;
                updateChevron(this.drawerWasOpen);
            }
        }

        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            if (newState == 0) {
                if (SuggestionsFragment.this.suggestionAdapter == null) {
                    if (this.mapSizeInPixels == 0) {
                        return;
                    }
                } else if (SuggestionsFragment.this.hereMapFragment == null || !SuggestionsFragment.this.hereMapFragment.isVisible()) {
                    this.mapSizeInPixels = SuggestionsFragment.this.suggestionAdapter.getGoogleMapHeight();
                } else {
                    this.mapSizeInPixels = SuggestionsFragment.this.suggestionAdapter.getHereMapHeight();
                }
                SuggestionsFragment.logger.d("Scrolled to: " + this.y + " drawer was opened: " + this.drawerIsOpen + " mapSizeInPixels: " + this.mapSizeInPixels);
                if (SuggestionsFragment.this.layoutManager != null) {
                    if (!this.drawerIsOpen && this.y > 0) {
                        if (this.y < this.mapSizeInPixels) {
                            SuggestionsFragment.this.layoutManager.scrollToPositionWithOffset(0, -this.mapSizeInPixels);
                            this.y = this.mapSizeInPixels;
                        }
                        this.drawerIsOpen = true;
                    } else if (this.drawerIsOpen && this.y < this.mapSizeInPixels) {
                        if (this.y != 0) {
                            if (SuggestionsFragment.this.suggestionRecycler != null) {
                                SuggestionsFragment.this.suggestionRecycler.smoothScrollToPosition(0);
                            }
                            this.y = 0;
                        }
                        this.drawerIsOpen = false;
                    }
                    this.drawerWasOpen = this.drawerIsOpen;
                    updateChevron(this.drawerIsOpen);
                    return;
                }
                SuggestionsFragment.logger.e("Unable to get layoutManager !");
            }
        }

        public void reset() {
            this.y = 0;
            this.drawerIsOpen = false;
            updateChevron(false);
        }

        private void updateChevron(boolean drawerIsOpen) {
            ImageView chevron = null;
            if (SuggestionsFragment.this.suggestionRecycler != null) {
                chevron = (ImageView) SuggestionsFragment.this.suggestionRecycler.findViewById(R.id.chevron);
            }
            if (chevron != null) {
                if (drawerIsOpen) {
                    chevron.setImageResource(R.drawable.icon_suggest_arrow_down);
                } else {
                    chevron.setImageResource(R.drawable.ic_chevron);
                }
            }
            FragmentActivity activity = SuggestionsFragment.this.getActivity();
            if (activity != null && !activity.isFinishing()) {
                ((HomescreenActivity) activity).hideConnectionBanner(null);
            }
        }
    }

    static {
        Resources res = NavdyApplication.getAppContext().getResources();
        HERE_TOP_PADDING = (res.getDimensionPixelSize(R.dimen.search_bar_height) + (res.getDimensionPixelSize(R.dimen.search_bar_margin) * 2)) + MapUtils.hereMapTopDownPadding;
        LAST_ARRIVAL_MARKER_STRING = res.getString(R.string.last_arrival_marker);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = null;
        FragmentActivity activity = getActivity();
        try {
            rootView = inflater.inflate(R.layout.hs_fragment_suggestions, container, false);
            Injector.inject(NavdyApplication.getAppContext(), this);
            this.offlineBanner = (RelativeLayout) rootView.findViewById(R.id.offline_banner);
        } catch (Throwable t) {
            logger.e("Error while inflating the suggestions fragment.", t);
            GmsUtils.finishIfGmsIsNotUpToDate(activity);
        }
        this.googleMapFragment = (BaseGoogleMapFragment) activity.getFragmentManager().findFragmentById(R.id.home_fragment_google_map_fragment);
        if (this.googleMapFragment != null) {
            this.googleMapFragment.hide();
        }
        this.hereMapFragment = (BaseHereMapFragment) activity.getFragmentManager().findFragmentById(R.id.home_fragment_here_map);
        if (this.hereMapFragment != null) {
            this.hereMapFragment.hide();
            this.hereMapFragment.setUsableArea(HERE_TOP_PADDING, HERE_PADDING, HERE_PADDING, HERE_PADDING);
        }
        if (rootView != null) {
            setRecyclerView(rootView);
        }
        SuggestionManager.addListener(this.suggestionListener);
        return rootView;
    }

    @WorkerThread
    public void onSuggestionBuildComplete(final ArrayList<Suggestion> suggestions) {
        this.handler.post(new Runnable() {
            public void run() {
                if (SuggestionsFragment.this.suggestionAdapter != null) {
                    SuggestionsFragment.this.suggestionAdapter.onSuggestionBuildComplete(suggestions);
                }
                SharedPreferences sharedPrefs = SettingsUtils.getSharedPreferences();
                if (sharedPrefs.getBoolean(SettingsConstants.FIRST_TIME_ON_HOMESCREEN, true)) {
                    sharedPrefs.edit().putBoolean(SettingsConstants.FIRST_TIME_ON_HOMESCREEN, false).apply();
                    if (!sharedPrefs.getBoolean(SettingsConstants.USER_WATCHED_THE_DEMO, false)) {
                        SuggestionsFragment.this.startActivity(new Intent(SuggestionsFragment.this.getContext(), DemoVideoDialogActivity.class));
                    }
                }
            }
        });
    }

    public void onResume() {
        super.onResume();
        this.lastUpdate = this.navdyLocationManager.getSmartStartCoordinates();
        updateOfflineBannerVisibility();
        if (this.suggestionAdapter == null) {
            initSuggestionAdapter();
        } else {
            try {
                this.suggestionAdapter.rebuildSuggestions();
            } catch (Exception e) {
                logger.e("rebuilding suggestions failed: " + this.suggestionAdapter);
            }
        }
        connectToService();
        this.navdyRouteHandler.addListener(this);
        this.navdyLocationManager.addListener(this.locationListener);
    }

    public void onPause() {
        this.navdyLocationManager.removeListener(this.locationListener);
        this.navdyRouteHandler.removeListener(this);
        if (this.suggestionAdapter != null) {
            this.suggestionAdapter.endSelectionMode();
            this.suggestionAdapter.stopRefreshingAllEtas();
        }
        if (this.otaUpdateService != null) {
            this.otaUpdateService.unregisterUIClient();
        }
        disconnectService();
        super.onPause();
    }

    public void onDestroy() {
        SuggestionManager.removeListener(this.suggestionListener);
        super.onDestroy();
    }

    public void onServiceConnected(ComponentName name, IBinder service) {
        if (service != null) {
            this.otaUpdateService = (OTAUpdateServiceInterface) service;
            this.otaUpdateService.registerUIClient(this);
            if (!this.otaUpdateService.isCheckingForUpdate()) {
                State state = this.otaUpdateService.getOTAUpdateState();
                if (state != State.UPLOADING && state != State.DOWNLOADING_UPDATE) {
                    this.otaUpdateService.checkForUpdate();
                }
            }
        }
    }

    public void onServiceDisconnected(ComponentName name) {
        this.otaUpdateService = null;
    }

    public void onErrorCheckingForUpdate(Error error) {
    }

    public void onStateChanged(final State state, UpdateInfo updateInfo) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                SuggestionManager.updateOtaStatus(state);
            }
        }, 1);
    }

    public void onDownloadProgress(DownloadUpdateStatus progress, long bytesDownloaded, byte percentage) {
    }

    public void onUploadProgress(UploadToHUDStatus progress, long completed, byte percentage) {
    }

    public void onPendingRouteCalculating(@NonNull Destination destination) {
        clearMapObjects();
        addTripDestinationMarkerAndCenterMap(destination);
    }

    public void onPendingRouteCalculated(@NonNull NavdyRouteHandler.Error error, @Nullable NavdyRouteInfo pendingRoute) {
        clearMapObjects();
        if (pendingRoute != null) {
            addTripDestinationMarkerAndCenterMap(pendingRoute.getDestination());
        }
        if (error == NavdyRouteHandler.Error.NONE) {
            addTripRouteAndCenterMap(pendingRoute);
        }
    }

    public void onRouteCalculating(@NonNull Destination destination) {
        clearLastArrival();
        if (this.hereMapFragment != null) {
            this.hereMapFragment.centerOnUserLocation();
        }
    }

    public void onRouteCalculated(@NonNull NavdyRouteHandler.Error error, @NonNull NavdyRouteInfo route) {
        startMapRoute(route);
    }

    public void onRouteStarted(@NonNull NavdyRouteInfo route) {
        logger.v("onRouteStarted");
        startMapRoute(route);
    }

    public void onTripProgress(@NonNull final NavdyRouteInfo progress) {
        if (this.hereMapFragment == null) {
            logger.e("Calling onTripProgress with a null hereMapFragment");
        } else {
            this.hereMapFragment.whenInitialized(new OnHereMapFragmentInitialized() {
                public void onInit(@NonNull Map hereMap) {
                    SuggestionsFragment.this.setProgressPolyline(hereMap, progress.getProgress());
                }

                public void onError(@NonNull BaseHereMapFragment.Error error) {
                }
            });
        }
    }

    public void onReroute() {
    }

    public void onRouteArrived(@NonNull final Destination destination) {
        if (this.googleMapFragment == null) {
            logger.e("Calling onRouteArrived with a null googleMapFragment");
        } else {
            this.googleMapFragment.getMapAsync(new OnMapReadyCallback() {
                public void onMapReady(GoogleMap googleMap) {
                    BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(destination.getRecentPinAsset());
                    LatLng position = new LatLng(destination.getDisplayLat(), destination.getDisplayLng());
                    if (SuggestionsFragment.this.lastArrivalMarker != null) {
                        SuggestionsFragment.this.lastArrivalMarker.setIcon(icon);
                        SuggestionsFragment.this.lastArrivalMarker.setPosition(position);
                        return;
                    }
                    SuggestionsFragment.this.lastArrivalMarker = googleMap.addMarker(new MarkerOptions().position(position).title(SuggestionsFragment.LAST_ARRIVAL_MARKER_STRING).icon(icon));
                }
            });
        }
    }

    public void onStopRoute() {
        logger.v("onStopRoute");
        clearMapObjects();
        showGoogleMap();
        this.suggestionAdapter.rebuildSuggestions();
    }

    private void setRecyclerView(View rootView) {
        this.suggestionRecycler = (RecyclerView) rootView.findViewById(R.id.home_fragment_recycler_view);
        this.layoutManager = new LinearLayoutManager(getContext());
        this.layoutManager.setOrientation(1);
        this.scrollListener = new SuggestionScrollListener(this, null);
        if (this.suggestionRecycler != null) {
            this.suggestionRecycler.setLayoutManager(this.layoutManager);
            this.suggestionRecycler.addOnScrollListener(this.scrollListener);
        }
    }

    private void initSuggestionAdapter() {
        Resources resources = getResources();
        Activity activity = getActivity();
        int displayHeight = UiUtils.getDisplayHeight(activity);
        int toolbarHeight = UiUtils.getActionBarHeight(activity);
        int tabsHeight = UiUtils.getTabsHeight(resources);
        this.suggestionAdapter = new SuggestedDestinationsAdapter(getContext(), ((displayHeight - toolbarHeight) - tabsHeight) - UiUtils.getGoogleMapBottomPadding(resources), ((displayHeight - toolbarHeight) - tabsHeight) - UiUtils.getHereMapBottomPadding(resources), resources.getDimensionPixelSize(R.dimen.list_header_height));
        if (this.suggestionRecycler != null) {
            this.suggestionRecycler.setAdapter(this.suggestionAdapter);
        }
        this.suggestionAdapter.setClickListener(this.clickListener);
        this.suggestionAdapter.setSuggestionLongClickListener(this.onLongClickListener);
    }

    private void addTripDestinationMarkerAndCenterMap(final Destination destination) {
        scrollToTheTop();
        showHereMap();
        if (this.hereMapFragment == null) {
            logger.e("Calling addTripDestinationMarkerAndCenterMap with a null hereMapFragment");
        } else {
            this.hereMapFragment.whenInitialized(new OnHereMapFragmentInitialized() {
                public void onInit(@NonNull Map hereMap) {
                    SuggestionsFragment.this.setDestinationMarker(hereMap, destination);
                    SuggestionsFragment.this.hereMapFragment.centerOnUserLocationAndDestination(destination, Animation.LINEAR);
                }

                public void onError(@NonNull BaseHereMapFragment.Error error) {
                }
            });
        }
    }

    private void addTripRouteAndCenterMap(final NavdyRouteInfo route) {
        if (this.hereMapFragment == null) {
            logger.e("Calling addTripRouteAndCenterMap with a null hereMapFragment");
        } else {
            this.hereMapFragment.whenReady(new OnHereMapFragmentReady() {
                public void onReady(@NonNull Map hereMap) {
                    GeoPolyline geoPolyline = route.getRoute();
                    SuggestionsFragment.this.setRoutePolyline(hereMap, geoPolyline);
                    if (geoPolyline != null) {
                        SuggestionsFragment.this.hereMapFragment.centerOnRoute(geoPolyline, Animation.LINEAR);
                    }
                }

                public void onError(@NonNull BaseHereMapFragment.Error error) {
                }
            });
        }
    }

    private void clearMapObjects() {
        if (this.hereMapFragment == null) {
            logger.e("Calling clearMapObjects with a null hereMapFragment");
        } else {
            this.hereMapFragment.whenInitialized(new OnHereMapFragmentInitialized() {
                public void onInit(@NonNull Map hereMap) {
                    SuggestionsFragment.this.setDestinationMarker(hereMap, null);
                    SuggestionsFragment.this.setRoutePolyline(hereMap, null);
                    SuggestionsFragment.this.setProgressPolyline(hereMap, null);
                }

                public void onError(@NonNull BaseHereMapFragment.Error error) {
                }
            });
        }
    }

    private void setDestinationMarker(@NonNull Map hereMap, @Nullable Destination destination) {
        MapMarker marker;
        if (destination != null) {
            marker = destination.getHereMapMarker();
        } else {
            marker = null;
        }
        synchronized (this.lock) {
            if (this.destinationMarker != null) {
                hereMap.removeMapObject(this.destinationMarker);
            }
            this.destinationMarker = marker;
            if (this.destinationMarker != null) {
                hereMap.addMapObject(this.destinationMarker);
            }
        }
    }

    private void setRoutePolyline(@NonNull Map hereMap, @Nullable GeoPolyline route) {
        MapPolyline polyline;
        if (route != null) {
            polyline = MapUtils.generateRoutePolyline(route);
        } else {
            polyline = null;
        }
        synchronized (this.lock) {
            if (this.routePolyline != null) {
                hereMap.removeMapObject(this.routePolyline);
            }
            this.routePolyline = polyline;
            if (this.routePolyline != null) {
                hereMap.addMapObject(this.routePolyline);
            }
        }
    }

    private synchronized void setProgressPolyline(@NonNull Map hereMap, @Nullable GeoPolyline progressPolyline) {
        MapPolyline polyline;
        if (progressPolyline != null) {
            polyline = MapUtils.generateProgressPolyline(progressPolyline);
        } else {
            polyline = null;
        }
        synchronized (this.lock) {
            if (this.routeProgressPolyline != null) {
                hereMap.removeMapObject(this.routeProgressPolyline);
            }
            this.routeProgressPolyline = polyline;
            if (this.routeProgressPolyline != null) {
                hereMap.addMapObject(this.routeProgressPolyline);
            }
        }
    }

    private void clearLastArrival() {
        if (this.lastArrivalMarker != null) {
            this.lastArrivalMarker.remove();
            this.lastArrivalMarker = null;
        }
    }

    private void showHereMap() {
        logger.v("showHereMap");
        if (isInForeground()) {
            if (this.googleMapFragment != null) {
                this.googleMapFragment.hide();
            }
            if (this.hereMapFragment != null) {
                this.hereMapFragment.show();
            }
            scrollToTheTop();
            return;
        }
        logger.w("showHereMap, not in foreground, no-op");
    }

    private void showGoogleMap() {
        logger.v("showGoogleMap");
        if (isInForeground()) {
            if (this.hereMapFragment != null) {
                this.hereMapFragment.hide();
            }
            if (this.googleMapFragment != null) {
                this.googleMapFragment.show(new OnShowMapListener() {
                    public void onShow() {
                        SuggestionsFragment.logger.v("onShow googleMap");
                    }
                });
            }
            scrollToTheTop();
            return;
        }
        logger.w("showGoogleMap, not in foreground, no-op");
    }

    private void startMapRoute(@NonNull NavdyRouteInfo route) {
        clearMapObjects();
        addTripDestinationMarkerAndCenterMap(route.getDestination());
        addTripRouteAndCenterMap(route);
    }

    private void connectToService() {
        Context context = getContext();
        try {
            context.bindService(OTAUpdateService.getServiceIntent(context), this, 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void disconnectService() {
        try {
            getContext().unbindService(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void scrollToTheTop() {
        this.layoutManager.scrollToPosition(0);
        this.scrollListener.reset();
    }

    public void onDetach() {
        if (this.suggestionAdapter != null) {
            this.suggestionAdapter.clearAllAnimations();
            this.suggestionAdapter.stopRefreshingAllEtas();
        }
        super.onDetach();
    }

    @Subscribe
    public void handleReachabilityStateChange(ReachabilityEvent reachabilityEvent) {
        if (reachabilityEvent != null) {
            logger.v("reachability event received: " + reachabilityEvent.isReachable);
            if (this.suggestionAdapter != null) {
                this.suggestionAdapter.notifyDataSetChanged();
            }
            updateOfflineBannerVisibility();
        }
    }

    private void updateOfflineBannerVisibility() {
        if (this.offlineBanner != null) {
            this.offlineBanner.setVisibility(AppInstance.getInstance().canReachInternet() ? GONE : VISIBLE);
        }
    }
}
