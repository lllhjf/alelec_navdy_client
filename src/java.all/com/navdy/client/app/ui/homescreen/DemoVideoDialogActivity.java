package com.navdy.client.app.ui.homescreen;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.alelec.navdyclient.R;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.firstlaunch.VideoPlayerActivity;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsUtils;

public class DemoVideoDialogActivity extends BaseActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.dialog_demo_video);
    }

    public void onWatchVideoClick(View v) {
        SettingsUtils.getSharedPreferences().edit().putBoolean(SettingsConstants.USER_WATCHED_THE_DEMO, true).apply();
        String videoUrl = getString(R.string.demo_video_url);
        Intent intent = new Intent(getApplicationContext(), VideoPlayerActivity.class);
        intent.putExtra(VideoPlayerActivity.EXTRA_VIDEO_URL, videoUrl);
        startActivity(intent);
        finish();
    }

    public void onCloseClick(View view) {
        onBackPressed();
    }
}
