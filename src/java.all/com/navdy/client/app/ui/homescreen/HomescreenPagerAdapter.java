package com.navdy.client.app.ui.homescreen;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.ui.favorites.FavoritesFragment;
import com.navdy.client.app.ui.glances.GlancesFragment;
import com.navdy.client.app.ui.trips.TripsFragment;
import com.navdy.service.library.log.Logger;
import java.util.Arrays;

class HomescreenPagerAdapter extends FragmentPagerAdapter {
    private static final int FAVORITES_FRAGMENT_POSITION = 1;
    private static final int GLANCES_FRAGMENT_POSITION = 2;
    private static final int HOME_FRAGMENT_POSITION = 0;
    private static final int TOTAL_FRAGMENTS = 4;
    private static final int TRIPS_FRAGMENT_POSITION = 3;
    private static final Logger logger = new Logger(HomescreenPagerAdapter.class);
    private final Boolean[] fragmentExists = new Boolean[4];
    private final Fragment[] fragmentList = new Fragment[4];
    private final String[] fragmentNames = new String[4];
    private int hiddenFragmentsCount = 1;

    public HomescreenPagerAdapter(FragmentManager manager) {
        super(manager);
        Context context = NavdyApplication.getAppContext();
        this.fragmentList[0] = new SuggestionsFragment();
        this.fragmentNames[0] = context.getString(R.string.home_fragment);
        this.fragmentList[1] = new FavoritesFragment();
        this.fragmentNames[1] = context.getString(R.string.favorites_fragment);
        this.fragmentList[2] = new GlancesFragment();
        this.fragmentNames[2] = context.getString(R.string.glances_fragment);
        Arrays.fill(this.fragmentExists, Boolean.valueOf(false));
    }

    public Object instantiateItem(ViewGroup container, int position) {
        if (this.fragmentExists[position].booleanValue()) {
            return this.fragmentList[position];
        }
        this.fragmentExists[position] = Boolean.valueOf(true);
        return super.instantiateItem(container, position);
    }

    public void destroyItem(ViewGroup container, int position, Object object) {
    }

    public Fragment getItem(int position) {
        return this.fragmentList[position];
    }

    public int getCount() {
        return this.fragmentList.length - this.hiddenFragmentsCount;
    }

    public CharSequence getPageTitle(int position) {
        return this.fragmentNames[position].toUpperCase();
    }

    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        super.setPrimaryItem(container, position, object);
    }

    public void showHiddenTabs() {
        Context context = NavdyApplication.getAppContext();
        this.hiddenFragmentsCount = 0;
        this.fragmentNames[1] = NavdyApplication.getAppContext().getString(R.string.favs_fragment);
        this.fragmentList[3] = new TripsFragment();
        this.fragmentNames[3] = context.getString(R.string.trips_fragment);
        notifyDataSetChanged();
    }
}
