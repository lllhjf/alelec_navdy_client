package com.navdy.client.app.ui.settings;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import com.alelec.navdyclient.R;
import com.navdy.client.app.ui.base.BaseToolbarActivity;
import com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder;

public class FeatureVideosActivity extends BaseToolbarActivity {
    private LinearLayoutManager layoutManager;
    private FeatureVideosAdapter mFeatureAdapter;
    private RecyclerView mFeatureRecycler;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.settings_feature_videos_activity);
        new ToolbarBuilder().title((int) R.string.feature_videos).build();
        this.mFeatureRecycler = (RecyclerView) findViewById(R.id.features_recycler_view);
        this.mFeatureAdapter = new FeatureVideosAdapter(this);
        this.layoutManager = new LinearLayoutManager(this);
        this.layoutManager.setOrientation(1);
        this.mFeatureRecycler.setLayoutManager(this.layoutManager);
        this.mFeatureRecycler.setAdapter(this.mFeatureAdapter);
    }

    protected void onResume() {
        super.onResume();
        if (this.mFeatureAdapter != null) {
            this.mFeatureAdapter.notifyDataSetChanged();
        }
    }
}
