package com.navdy.client.app.ui.settings;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.alelec.navdyclient.R;

public class FeatureVideoViewHolder extends ViewHolder {
    protected TextView duration;
    protected ImageView image;
    protected View row;
    protected TextView title;
    protected RelativeLayout watchedLayout;

    public FeatureVideoViewHolder(View itemView) {
        super(itemView);
        this.row = itemView;
        this.duration = (TextView) itemView.findViewById(R.id.duration);
        this.image = (ImageView) itemView.findViewById(R.id.illustration);
        this.title = (TextView) itemView.findViewById(R.id.title);
        this.watchedLayout = (RelativeLayout) itemView.findViewById(R.id.watched_layout);
    }
}
