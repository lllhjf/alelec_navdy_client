package com.navdy.client.app.ui.details;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.amazonaws.services.s3.internal.Constants;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.Builder;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.alelec.navdyclient.BuildConfig;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.DeviceConnection.DeviceConnectedEvent;
import com.navdy.client.app.framework.DeviceConnection.DeviceDisconnectedEvent;
import com.navdy.client.app.framework.location.NavdyLocationManager;
import com.navdy.client.app.framework.map.NavCoordsAddressProcessor;
import com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Error;
import com.navdy.client.app.framework.map.NavCoordsAddressProcessor.OnCompleteCallback;
import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.framework.models.Destination.SearchType;
import com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult;
import com.navdy.client.app.framework.navigation.NavdyRouteHandler;
import com.navdy.client.app.framework.search.GooglePlacesSearch;
import com.navdy.client.app.framework.search.GooglePlacesSearch.GoogleSearchListener;
import com.navdy.client.app.framework.search.GooglePlacesSearch.Query;
import com.navdy.client.app.framework.servicehandler.NetworkStatusManager.ReachabilityEvent;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.providers.NavdyContentProvider;
import com.navdy.client.app.providers.NavdyContentProvider.QueryResultCallback;
import com.navdy.client.app.tracking.SetDestinationTracker;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Attributes.DestinationAttributes.SOURCE_VALUES;
import com.navdy.client.app.tracking.TrackerConstants.Screen;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.base.BaseGoogleMapFragment;
import com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment;
import com.navdy.client.app.ui.customviews.UnitSystemTextView;
import com.navdy.client.app.ui.favorites.FavoritesEditActivity;
import com.navdy.client.app.ui.search.SearchConstants;
import com.navdy.client.app.ui.search.SearchRecyclerAdapter;
import com.navdy.client.app.ui.settings.BluetoothPairActivity;
import com.navdy.service.library.events.contacts.Contact;
import com.navdy.service.library.events.contacts.PhoneNumberType;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Subscribe;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;

public class DetailsActivity extends BaseActivity implements LocationListener, ConnectionCallbacks, GoogleSearchListener, OnConnectionFailedListener {
    private static final String EXTRA_LOCATION = "location";
    public static final String EXTRA_UPDATED_DESTINATION = "updated_destination";
    public static final int FINISH_PARENT_CODE = 1;
    private ImageButton addToFavoritesButton;
    private TextView address;
    private ImageView badge;
    private Destination destination;
    private UnitSystemTextView distance;
    private ImageView editFavoriteButton;
    private ImageView fab;
    private GoogleMap googleMap;
    private BaseGoogleMapFragment googleMapFragment;
    private boolean hasUpdatedDestination = false;
    private boolean isActivityForResult = false;
    private Location lastLocation;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private Toolbar myToolbar;
    private final NavdyLocationManager navdyLocationManager = NavdyLocationManager.getInstance();
    private TextView openHoursFirstLine;
    private TextView openHoursSecondLine;
    private TextView phone;
    private boolean requestedLocationUpdates;
    private TextView title;
    private TextView website;

    public static void startDetailsActivity(Destination destination, Context context) {
        if (context == null || !(context instanceof Activity)) {
            sLogger.w("startDetailsActivity, invalid context");
            return;
        }
        Intent i = new Intent(context, DetailsActivity.class);
        i.putExtra(SearchConstants.SEARCH_RESULT, destination);
        context.startActivity(i);
    }

    public static void startDetailsActivityForResult(Destination destination, Context context) {
        if (context == null || !(context instanceof Activity)) {
            sLogger.w("startDetailsActivity, invalid context");
            return;
        }
        Intent i = new Intent(context, DetailsActivity.class);
        i.putExtra(SearchConstants.SEARCH_RESULT, destination);
        ((Activity) context).startActivityForResult(i, 5);
    }

    private void initFavoriteButton() {
        if (this.destination.isFavoriteDestination()) {
            if (this.addToFavoritesButton != null) {
                this.addToFavoritesButton.setVisibility(INVISIBLE);
            }
            if (this.editFavoriteButton != null) {
                this.editFavoriteButton.setVisibility(VISIBLE);
            }
        } else {
            if (this.addToFavoritesButton != null) {
                this.addToFavoritesButton.setVisibility(VISIBLE);
            }
            if (this.editFavoriteButton != null) {
                this.editFavoriteButton.setVisibility(GONE);
            }
        }
        OnClickListener favoriteClickListener = new OnClickListener() {
            public void onClick(View v) {
                DetailsActivity.this.hasUpdatedDestination = true;
                if (DetailsActivity.this.destination.isFavoriteDestination()) {
                    FavoritesEditActivity.startFavoriteEditActivity(DetailsActivity.this, DetailsActivity.this.destination);
                    return;
                }
                DetailsActivity.this.showProgressDialog();
                DetailsActivity.this.destination.saveDestinationAsFavoritesAsync(-1, new QueryResultCallback() {
                    public void onQueryCompleted(final int nbRows, @Nullable Uri uri) {
                        DetailsActivity.this.logger.d("saveDestinationAsFavorite");
                        DetailsActivity.this.runOnUiThread(new Runnable() {
                            public void run() {
                                DetailsActivity.this.hideProgressDialog();
                                if (nbRows > 0) {
                                    DetailsActivity.this.updateDestinationType();
                                    BaseActivity.showShortToast(R.string.toast_favorite_edit_saved, new Object[0]);
                                    return;
                                }
                                BaseActivity.showShortToast(R.string.toast_favorite_edit_error, new Object[0]);
                            }
                        });
                    }
                });
            }
        };
        if (this.addToFavoritesButton != null) {
            this.addToFavoritesButton.setOnClickListener(favoriteClickListener);
        }
        if (this.editFavoriteButton != null) {
            this.editFavoriteButton.setOnClickListener(favoriteClickListener);
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        this.logger.v("::onCreate");
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.details_activity);
        Intent i = getIntent();
        if (i != null) {
            if (getCallingActivity() != null) {
                this.isActivityForResult = true;
            }
            this.destination = (Destination) i.getParcelableExtra(SearchConstants.SEARCH_RESULT);
            this.lastLocation = (Location) i.getParcelableExtra(EXTRA_LOCATION);
            if (i.hasExtra("Type")) {
                i.getStringExtra("Type");
            }
            this.logger.v("lastLocation coordinates: " + (this.lastLocation != null ? this.lastLocation.getLatitude() + "," + this.lastLocation.getLongitude() : Constants.NULL_VERSION_ID));
        }
        this.myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(this.myToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            setTitle(this.destination.name);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        this.googleMapFragment = (BaseGoogleMapFragment) getFragmentManager().findFragmentById(R.id.place_details_map_view);
        this.badge = (ImageView) findViewById(R.id.badge);
        this.title = (TextView) findViewById(R.id.details_place_title);
        this.address = (TextView) findViewById(R.id.place_details_address);
        this.distance = (UnitSystemTextView) findViewById(R.id.place_details_distance_first_line);
        this.phone = (TextView) findViewById(R.id.place_details_contact_first_line);
        this.website = (TextView) findViewById(R.id.place_details_contact_second_line);
        this.openHoursFirstLine = (TextView) findViewById(R.id.place_details_hours_first_line);
        this.openHoursSecondLine = (TextView) findViewById(R.id.place_details_hours_second_line);
        fillTextViews();
        updateDestinationType();
        this.fab = (ImageView) findViewById(R.id.fab);
        this.fab.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                DetailsActivity.this.showRequestNewRouteDialog(new Runnable() {
                    public void run() {
                        SetDestinationTracker.getInstance().tagSetDestinationEvent(DetailsActivity.this.destination);
                        NavdyRouteHandler.getInstance().requestNewRoute(DetailsActivity.this.destination);
                        DetailsActivity.this.finishThisAndParent();
                    }
                });
            }
        });
        if (this.googleMapFragment != null) {
            this.googleMapFragment.getMapAsync(new OnMapReadyCallback() {
                public void onMapReady(GoogleMap gMap) {
                    DetailsActivity.this.googleMap = gMap;
                    DetailsActivity.this.addMarkerAndCenterMap();
                }
            });
        }
        if (this.destination.isContact()) {
            showPhoneNumbers();
        }
        GooglePlacesSearch googlePlacesSearch = new GooglePlacesSearch(this, this.handler);
        if (this.destination.placeId != null) {
            googlePlacesSearch.runDetailsSearchWebApi(this.destination.placeId);
            updateFavoriteButtonIfAlreadyFavorited(this.destination.placeId);
        } else {
            if (!this.destination.hasOneValidSetOfCoordinates()) {
                if (this.addToFavoritesButton != null) {
                    this.addToFavoritesButton.setVisibility(INVISIBLE);
                }
                if (this.editFavoriteButton != null) {
                    this.editFavoriteButton.setVisibility(GONE);
                }
                showProgressDialog();
            }
            this.logger.v("Getting location coordinates for: " + this.destination.getAddressForDisplay());
            NavCoordsAddressProcessor.processDestination(this.destination, new OnCompleteCallback() {
                public void onSuccess(Destination destination) {
                    DetailsActivity.this.destination = destination;
                    DetailsActivity.this.addMarkerAndCenterMap();
                    DetailsActivity.this.badge.setImageResource(destination.getBadgeAsset());
                    DetailsActivity.this.initFavoriteButton();
                    DetailsActivity.this.hideProgressDialog();
                }

                public void onFailure(Destination destination, Error error) {
                    DetailsActivity.this.destination = destination;
                    if (!destination.hasOneValidSetOfCoordinates()) {
                        BaseActivity.showLongToast(R.string.invalid_address, new Object[0]);
                        DetailsActivity.this.finish();
                    }
                    if (DetailsActivity.this.googleMap != null && DetailsActivity.this.googleMapFragment != null) {
                        Location location;
                        if (destination.hasValidDisplayCoordinates()) {
                            location = new Location("");
                            location.setLatitude(destination.displayLat);
                            location.setLongitude(destination.displayLong);
                            DetailsActivity.this.googleMapFragment.moveMap(location, 14.0f, false);
                        } else if (destination.hasValidNavCoordinates()) {
                            location = new Location("");
                            location.setLatitude(destination.navigationLat);
                            location.setLongitude(destination.navigationLong);
                            DetailsActivity.this.googleMapFragment.moveMap(location, 14.0f, false);
                        } else {
                            DetailsActivity.this.googleMapFragment.moveMap(DetailsActivity.this.navdyLocationManager.getSmartStartLocation(), 14.0f, false);
                        }
                    }
                }
            });
        }
        requestLocationPermission(new Runnable() {
            public void run() {
                DetailsActivity.this.buildAndConnectGoogleApiClient();
            }
        }, new Runnable() {
            public void run() {
                BaseActivity.showLongToast(R.string.cant_calculate_distances_without_location_permission, new Object[0]);
            }
        });
        Location currentLocation = this.navdyLocationManager.getSmartStartLocation();
        if (currentLocation != null) {
            this.lastLocation = currentLocation;
        }
        SetDestinationTracker.getInstance().setSourceValue(SOURCE_VALUES.DETAILS);
    }

    protected void onResume() {
        super.onResume();
        updateOfflineBannerVisibility();
        new AsyncTask<Object, Object, Destination>() {
            protected Destination doInBackground(Object... params) {
                if (DetailsActivity.this.destination != null) {
                    return NavdyContentProvider.getThisDestination(DetailsActivity.this.destination.id);
                }
                return null;
            }

            protected void onPostExecute(Destination d) {
                if (d != null) {
                    DetailsActivity.this.destination = d;
                }
                DetailsActivity.this.fillTextViews();
                DetailsActivity.this.updateDestinationType();
            }
        }.execute(new Object[0]);
        Tracker.tagScreen(Screen.DETAILS);
    }

    public void finish() {
        if (this.isActivityForResult && this.hasUpdatedDestination) {
            this.logger.d("Destination was updated. Suggestions will be rebuilt");
            Intent i = new Intent();
            i.putExtra(EXTRA_UPDATED_DESTINATION, true);
            setResult(-1, i);
        }
        super.finish();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        updateConnectionIndicator();
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onConnected(Bundle bundle) {
        this.logger.v("onConnected");
        try {
            this.lastLocation = LocationServices.FusedLocationApi.getLastLocation(this.mGoogleApiClient);
        } catch (SecurityException securityException) {
            this.logger.e("Security Exception: ", securityException);
        }
        if (this.lastLocation == null) {
            this.logger.e("lastLocation was null. Now requesting location update.");
            requestLocationUpdate();
        }
    }

    public void onGoogleSearchResult(List<GoogleTextSearchDestinationResult> destinations, Query queryType, GooglePlacesSearch.Error error) {
        this.logger.v("OnGoogleSearchResult");
        if (destinations != null && !destinations.isEmpty()) {
            displayGooglePlaceDetailData((GoogleTextSearchDestinationResult) destinations.listIterator().next());
        }
    }

    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        this.logger.e("onConnectionFailed: ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());
    }

    public void onConnectionSuspended(int i) {
        this.logger.e("GoogleApiClient connection suspended.");
    }

    public void onLocationChanged(Location location) {
        if (this.lastLocation == null) {
            this.logger.v("Location changed");
            this.lastLocation = location;
            stopLocationUpdates();
        }
    }

    @Subscribe
    public void appInstanceDeviceConnectionEvent(DeviceConnectedEvent deviceConnectedEvent) {
        this.logger.v("appInstanceDeviceConnectionEvent");
        updateConnectionIndicator();
    }

    @Subscribe
    public void appInstanceDeviceDisconnectedEvent(DeviceDisconnectedEvent deviceDisconnectedEvent) {
        this.logger.v("appInstanceDeviceDisconnectedEvent");
        updateConnectionIndicator();
    }

    private void updateDestinationType() {
        addMarkerAndCenterMap();
        this.badge.setImageResource(this.destination.getBadgeAsset());
        this.addToFavoritesButton = (ImageButton) findViewById(R.id.add_to_favorites_button);
        this.editFavoriteButton = (ImageView) findViewById(R.id.edit_favorites_button);
        initFavoriteButton();
    }

    private void addMarkerAndCenterMap() {
        if (this.googleMap != null) {
            this.googleMap.clear();
        }
        LatLng destinationLatLng = null;
        if (this.destination.hasValidDisplayCoordinates()) {
            destinationLatLng = new LatLng(this.destination.displayLat, this.destination.displayLong);
        } else if (this.destination.hasValidNavCoordinates()) {
            destinationLatLng = new LatLng(this.destination.navigationLat, this.destination.navigationLong);
        }
        if (destinationLatLng != null && this.googleMap != null) {
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.title(this.destination.name);
            markerOptions.draggable(false);
            markerOptions.position(destinationLatLng);
            markerOptions.icon(BitmapDescriptorFactory.fromResource(this.destination.getPinAsset()));
            this.googleMap.addMarker(markerOptions);
            Location location = new Location("");
            location.setLatitude(destinationLatLng.latitude);
            location.setLongitude(destinationLatLng.longitude);
            if (this.googleMapFragment != null) {
                this.googleMapFragment.moveMap(location, 14.0f, false);
            }
        }
    }

    private void updateConnectionIndicator() {
        Context applicationContext = getApplicationContext();
        if (AppInstance.getInstance().isDeviceConnected()) {
            this.myToolbar.getMenu().removeItem(0);
            this.fab.setImageResource(R.drawable.button_start);
            return;
        }
        Intent intent = new Intent(applicationContext, BluetoothPairActivity.class);
        intent.putExtra(BluetoothFramelayoutFragment.EXTRA_KILL_ACTIVITY_ON_CANCEL, true);
        this.myToolbar.getMenu().add(0, 0, 0, R.string.navdy_not_connected).setIcon(R.drawable.icon_display_disconnected).setIntent(intent).setShowAsAction(1);
        this.fab.setImageResource(R.drawable.button_start_offline);
    }

    private void fillTextViews() {
        ArrayList<String> addressLines = this.destination.getAddressLines();
        if (StringUtils.isEmptyAfterTrim(this.destination.name)) {
            if (this.title != null) {
                this.title.setText((CharSequence) addressLines.remove(0));
            }
            if (this.address != null) {
                this.address.setText(StringUtils.join(addressLines, "\n"));
                return;
            }
            return;
        }
        if (this.title != null) {
            this.title.setText(this.destination.name);
        }
        if (this.address != null) {
            this.address.setText(StringUtils.join(addressLines, "\n"));
        }
    }

    private void buildAndConnectGoogleApiClient() {
        this.mGoogleApiClient = new Builder(this).addConnectionCallbacks(this).addOnConnectionFailedListener(this).addApi(Places.GEO_DATA_API).addApi(Places.PLACE_DETECTION_API).addApi(LocationServices.API).build();
        this.mGoogleApiClient.connect();
    }

    private void showPhoneNumbers() {
        ArrayList<Contact> contacts = this.destination.getContacts();
        ArrayList<String> phoneNumbers = new ArrayList();
        Iterator it = contacts.iterator();
        while (it.hasNext()) {
            Contact contact = (Contact) it.next();
            if (!StringUtils.isEmptyAfterTrim(contact.number)) {
                String type = getLocalizedNumberTypeString(contact.numberType, contact.label);
                phoneNumbers.add(getString(R.string.phone_number_with_type, new Object[]{contact.number, type}));
            }
        }
        if (phoneNumbers.size() > 0) {
            this.phone.setText(StringUtils.join(phoneNumbers, "\n"));
            this.phone.setVisibility(VISIBLE);
            TextView infoTitle = (TextView) findViewById(R.id.contact_title);
            if (infoTitle != null) {
                infoTitle.setVisibility(VISIBLE);
            }
        }
    }

    @NonNull
    private String getLocalizedNumberTypeString(PhoneNumberType numberType, @Nullable String label) {
        switch (numberType) {
            case PHONE_NUMBER_HOME:
                return getString(R.string.home);
            case PHONE_NUMBER_WORK:
                return getString(R.string.work);
            case PHONE_NUMBER_MOBILE:
                return getString(R.string.mobile);
            default:
                if (StringUtils.isEmptyAfterTrim(label)) {
                    return getString(R.string.other);
                }
                return label;
        }
    }

    private void displayGooglePlaceDetailData(GoogleTextSearchDestinationResult destinationResult) {
        this.logger.v("destinationResult: " + destinationResult);
        destinationResult.toModelDestinationObject(SearchType.DETAILS, this.destination);
        this.destination.persistPlaceDetailInfoAndUpdateListsAsync();
        fillTextViews();
        addMarkerAndCenterMap();
        this.badge.setImageResource(this.destination.getBadgeAsset());
        if (!(destinationResult.lat == null || destinationResult.lng == null || this.lastLocation == null)) {
            Location destinationLocation = new Location("");
            double destinationLat = Double.parseDouble(destinationResult.lat);
            double destinationLng = Double.parseDouble(destinationResult.lng);
            destinationLocation.setLatitude(destinationLat);
            destinationLocation.setLongitude(destinationLng);
            this.logger.v("Destination Location lat long " + destinationLocation.getLatitude() + " " + destinationLocation.getLongitude());
            Float floatDistanceMeters = Float.valueOf(this.lastLocation.distanceTo(destinationLocation));
            this.distance.setDistance((double) floatDistanceMeters.floatValue());
            this.distance.setVisibility(VISIBLE);
        }
        if (!StringUtils.isEmptyAfterTrim(destinationResult.phone)) {
            this.phone.setText(destinationResult.phone);
            this.phone.setVisibility(VISIBLE);
        }
        if (!StringUtils.isEmptyAfterTrim(destinationResult.url)) {
            this.website.setText(StringUtils.fromHtml("<a href=\"" + destinationResult.url + "\">" + getString(R.string.website) + "</a>"));
            this.website.setVisibility(VISIBLE);
            this.website.setMovementMethod(LinkMovementMethod.getInstance());
        }
        if (destinationResult.open_hours != null) {
            this.logger.v("destinationResult is not null");
            if (!destinationResult.open_hours.isEmpty()) {
                String openHours = (String) destinationResult.open_hours.get(getDayFromCalendarOrdinal(Calendar.getInstance().get(7)));
                openHours = openHours.substring(openHours.indexOf(58));
                if (destinationResult.open_now) {
                    this.openHoursFirstLine.setText(R.string.open_now);
                } else {
                    this.openHoursFirstLine.setText(R.string.today);
                }
                this.openHoursFirstLine.setVisibility(VISIBLE);
                this.openHoursSecondLine.setText(openHours);
                this.openHoursSecondLine.setVisibility(VISIBLE);
            }
        }
        String typeString = null;
        if (destinationResult.types != null && destinationResult.types.length > 0) {
            TextView type = (TextView) findViewById(R.id.place_details_place_type);
            if (type != null) {
                try {
                    JSONArray array = new JSONArray(destinationResult.types[0]);
                    if (array.length() > 0) {
                        typeString = (String) array.get(0);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (isValidPlaceType(typeString)) {
                    int stringId = getResources().getIdentifier("place_type_" + typeString, "string", BuildConfig.APPLICATION_ID);
                    if (stringId > 0) {
                        type.setText(stringId);
                        type.setVisibility(VISIBLE);
                    } else {
                        this.logger.v("Unable to find a type string for type: " + typeString);
                    }
                }
            }
        }
        if (!StringUtils.isEmptyAfterTrim(destinationResult.price_level)) {
            TextView cost = (TextView) findViewById(R.id.place_details_cost);
            if (cost != null) {
                cost.setText(SearchRecyclerAdapter.getPriceString(destinationResult.price_level));
                cost.setVisibility(VISIBLE);
            }
        }
        if (destinationResult.open_hours != null || isValidPlaceType(typeString) || !StringUtils.isEmptyAfterTrim(destinationResult.price_level) || !StringUtils.isEmptyAfterTrim(destinationResult.phone) || !StringUtils.isEmptyAfterTrim(destinationResult.url)) {
            TextView infoTitle = (TextView) findViewById(R.id.contact_title);
            if (infoTitle != null) {
                infoTitle.setVisibility(VISIBLE);
            }
        }
    }

    private boolean isValidPlaceType(String typeString) {
        return (typeString == null || "street_address".equals(typeString)) ? false : true;
    }

    private int getDayFromCalendarOrdinal(int day) {
        switch (day) {
            case 1:
                return 6;
            case 3:
                return 1;
            case 4:
                return 2;
            case 5:
                return 3;
            case 6:
                return 4;
            case 7:
                return 5;
            default:
                return 0;
        }
    }

    private void requestLocationUpdate() {
        createLocationRequest();
        this.logger.i("calling requestLocationUpdates from FusedLocationApi");
        try {
            LocationServices.FusedLocationApi.requestLocationUpdates(this.mGoogleApiClient, this.mLocationRequest, (LocationListener) this);
        } catch (Throwable se) {
            sLogger.e(se);
        }
    }

    private void createLocationRequest() {
        this.logger.i("creating LocationRequest Object");
        this.mLocationRequest = LocationRequest.create().setPriority(100).setInterval(10000).setFastestInterval(1000);
    }

    private void stopLocationUpdates() {
        if (this.requestedLocationUpdates) {
            LocationServices.FusedLocationApi.removeLocationUpdates(this.mGoogleApiClient, (LocationListener) this);
            this.requestedLocationUpdates = false;
        }
    }

    private void finishThisAndParent() {
        setResult(1);
        finish();
    }

    private void updateFavoriteButtonIfAlreadyFavorited(final String placeId) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                final Destination destinationFromDb = NavdyContentProvider.getThisDestinationWithPlaceId(placeId);
                if (destinationFromDb != null && destinationFromDb.isFavoriteDestination()) {
                    DetailsActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            DetailsActivity.this.destination = destinationFromDb;
                            DetailsActivity.this.initFavoriteButton();
                            DetailsActivity.this.updateDestinationType();
                        }
                    });
                }
            }
        }, 1);
    }

    @Subscribe
    public void handleReachabilityStateChange(ReachabilityEvent reachabilityEvent) {
        if (reachabilityEvent != null) {
            updateOfflineBannerVisibility();
        }
    }

    private void updateOfflineBannerVisibility() {
        View offlineBanner = findViewById(R.id.offline_banner);
        if (offlineBanner != null) {
            offlineBanner.setVisibility(AppInstance.getInstance().canReachInternet() ? GONE : VISIBLE);
        }
    }

    public void onRefreshConnectivityClick(View view) {
        AppInstance.getInstance().checkForNetwork();
    }
}
