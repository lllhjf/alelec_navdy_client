package com.navdy.client.app.ui.bluetooth;

import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import com.alelec.navdyclient.R;
import com.navdy.client.debug.devicepicker.Device;
import java.util.ArrayList;
import java.util.List;

class BluetoothDialogRecyclerAdapter extends Adapter<BluetoothDeviceViewHolder> {
    private List<String> bluetoothDevices = new ArrayList();
    private OnClickListener listener;

    BluetoothDialogRecyclerAdapter() {
    }

    void setClickListener(OnClickListener customItemClickListener) {
        this.listener = customItemClickListener;
    }

    public int getItemCount() {
        return this.bluetoothDevices.size();
    }

    public BluetoothDeviceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new BluetoothDeviceViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.fl_bluetooth_recyclerview_item, parent, false));
    }

    public void onBindViewHolder(BluetoothDeviceViewHolder bluetoothDeviceViewHolder, int position) {
        if (!this.bluetoothDevices.isEmpty()) {
            bluetoothDeviceViewHolder.deviceMainTitle.setText((String) this.bluetoothDevices.get(position));
            bluetoothDeviceViewHolder.row.setOnClickListener(this.listener);
        }
    }

    void addDevice(Device newDevice) {
        this.bluetoothDevices.add(newDevice.getPrettyName());
        notifyItemInserted(this.bluetoothDevices.size() - 1);
    }

    public void clear() {
        this.bluetoothDevices.clear();
        notifyDataSetChanged();
    }

    public boolean contains(String name) {
        return this.bluetoothDevices.contains(name);
    }
}
