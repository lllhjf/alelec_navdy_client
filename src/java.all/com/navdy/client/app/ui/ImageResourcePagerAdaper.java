package com.navdy.client.app.ui;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.util.ImageCache;
import com.navdy.client.app.framework.util.ImageUtils;

public class ImageResourcePagerAdaper extends PagerAdapter {
    private ImageCache imageCache = new ImageCache();
    protected int[] imageResourceIds = new int[0];
    protected LayoutInflater mLayoutInflater;

    public ImageResourcePagerAdaper(Context context, int[] imageResourceIds, ImageCache imageCache) {
        this.imageResourceIds = (int[]) imageResourceIds.clone();
        this.imageCache = imageCache;
        this.mLayoutInflater = (LayoutInflater) context.getSystemService("layout_inflater");
    }

    public int getCount() {
        return this.imageResourceIds.length;
    }

    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    public Object instantiateItem(ViewGroup container, int position) {
        ImageView imageView = (ImageView) this.mLayoutInflater.inflate(R.layout.full_screen_image, container, false);
        if (imageView == null) {
            return null;
        }
        ImageUtils.loadImage(imageView, this.imageResourceIds[position], this.imageCache);
        container.addView(imageView);
        return imageView;
    }

    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((ImageView) object);
    }
}
