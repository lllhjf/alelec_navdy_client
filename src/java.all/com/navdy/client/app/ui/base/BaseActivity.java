package com.navdy.client.app.ui.base;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.PersistableBundle;
import android.provider.Settings.Secure;
import android.support.annotation.DrawableRes;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.ActionMode;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.location.NavdyLocationManager;
import com.navdy.client.app.framework.navigation.NavdyRouteHandler;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.client.app.framework.util.ImageCache;
import com.navdy.client.app.framework.util.ImageUtils;
import com.navdy.client.app.framework.util.SystemUtils;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Attributes;
import com.navdy.client.app.tracking.TrackerConstants.Event;
import com.navdy.client.app.ui.SimpleDialogFragment;
import com.navdy.client.app.ui.SimpleDialogFragment.DialogProvider;
import com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment;
import com.navdy.client.app.ui.firstlaunch.AppSetupActivity;
import com.navdy.client.app.ui.settings.BluetoothPairActivity;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.service.library.events.preferences.DriverProfilePreferences.ObdScanSetting;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicBoolean;

public class BaseActivity extends AppCompatActivity implements DialogProvider {
    private static final String DIALOG = "DIALOG";
    private static final int REQUEST_CALENDAR = 7;
    private static final int REQUEST_CAMERA = 8;
    private static final int REQUEST_CONTACTS = 3;
    private static final int REQUEST_LOCATION = 1;
    private static final int REQUEST_MICROPHONE = 2;
    private static final int REQUEST_PHONE = 4;
    private static final int REQUEST_SMS = 5;
    private static final int REQUEST_STORAGE = 6;
    protected static final boolean VERBOSE = false;
    protected static final Logger sLogger = new Logger(BaseActivity.class);
    private static Toast toast;
    private final Queue<Runnable> calendarPermissionDenialHandlers = new LinkedList();
    private final Queue<Runnable> cameraPermissionDenialHandlers = new LinkedList();
    private final Queue<Runnable> contactPermissionDenialHandlers = new LinkedList();
    private boolean destroying;
    protected final Handler handler = new Handler();
    public ImageCache imageCache = new ImageCache();
    AtomicBoolean instanceStateIsSaved = new AtomicBoolean(false);
    private final Queue<Runnable> locationPermissionDenialHandlers = new LinkedList();
    private final Object lock = new Object();
    protected final Logger logger = new Logger(getClass());
    private ActionMode mActionMode;
    private volatile boolean mIsInForeground = false;
    private final Queue<Runnable> microphonePermissionDenialHandlers = new LinkedList();
    private AlertDialog obdDialog = null;
    private final Queue<Runnable> phonePermissionDenialHandlers = new LinkedList();
    private ProgressDialog progressDialog = null;
    private AlertDialog questionDialog = null;
    private AlertDialog requestRouteDialog = null;
    private final Queue<Runnable> smsPermissionDenialHandlers = new LinkedList();
    private final Queue<Runnable> storagePermissionDenialHandlers = new LinkedList();
    private final Queue<Runnable> thingsThatRequireCalendarPermission = new LinkedList();
    private final Queue<Runnable> thingsThatRequireCameraPermission = new LinkedList();
    private final Queue<Runnable> thingsThatRequireContactPermission = new LinkedList();
    private final Queue<Runnable> thingsThatRequireLocationPermission = new LinkedList();
    private final Queue<Runnable> thingsThatRequireMicrophonePermission = new LinkedList();
    private final Queue<Runnable> thingsThatRequirePhonePermission = new LinkedList();
    private final Queue<Runnable> thingsThatRequireSmsPermission = new LinkedList();
    private final Queue<Runnable> thingsThatRequireStoragePermission = new LinkedList();

    protected void onCreate(Bundle savedInstanceState) {
        this.logger.v("::onCreate");
        this.instanceStateIsSaved.set(false);
        super.onCreate(savedInstanceState);
    }

    protected void onStart() {
        this.logger.v("::onStart");
        super.onStart();
    }

    protected void onResume() {
        super.onResume();
        this.logger.v("::onResume");
        this.mIsInForeground = true;
        if (requiresBus()) {
            BusProvider.getInstance().register(this);
        }
    }

    protected void onPause() {
        this.logger.v("::onPause");
        this.mIsInForeground = false;
        hideProgressDialog();
        endActionMode();
        if (requiresBus()) {
            BusProvider.getInstance().unregister(this);
        }
        super.onPause();
    }

    protected void onStop() {
        this.logger.v("::onStop");
        super.onStop();
    }

    protected void onDestroy() {
        this.logger.v("::onDestroy");
        this.destroying = true;
        this.thingsThatRequireLocationPermission.clear();
        this.locationPermissionDenialHandlers.clear();
        this.thingsThatRequireContactPermission.clear();
        this.contactPermissionDenialHandlers.clear();
        this.thingsThatRequireSmsPermission.clear();
        this.smsPermissionDenialHandlers.clear();
        this.thingsThatRequirePhonePermission.clear();
        this.phonePermissionDenialHandlers.clear();
        this.thingsThatRequireStoragePermission.clear();
        this.storagePermissionDenialHandlers.clear();
        dismissProgressDialog();
        dismissQuestionDialog();
        this.imageCache.clearCache();
        super.onDestroy();
    }

    public void onBackPressed() {
        try {
            if (!isEnding(this)) {
                super.onBackPressed();
            }
        } catch (IllegalStateException e) {
            this.logger.e("Unable to perform back pressed", e);
        }
    }

    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        this.instanceStateIsSaved.set(true);
        super.onSaveInstanceState(outState, outPersistentState);
    }

    public boolean isActivityDestroyed() {
        return this.destroying || isDestroyed() || isFinishing();
    }

    public Dialog createDialog(int id, Bundle arguments) {
        if (arguments == null) {
            return null;
        }
        if (arguments.get("title") == null && arguments.get("message") == null) {
            return null;
        }
        Builder builder = new Builder(this);
        builder.setCancelable(true);
        String title = arguments.getString("title");
        if (title != null) {
            builder.setTitle(title);
        }
        String message = arguments.getString("message");
        if (message != null) {
            builder.setMessage(message);
        }
        String positiveButtonTitle = arguments.getString(SimpleDialogFragment.EXTRA_POSITIVE_BUTTON_TITLE);
        if (positiveButtonTitle != null) {
            builder.setPositiveButton(positiveButtonTitle, null);
        } else {
            builder.setPositiveButton(getString(R.string.ok), null);
        }
        return builder.create();
    }

    public boolean isInForeground() {
        return this.mIsInForeground;
    }

    public void showSimpleDialog(int dialogId, @StringRes int titleId, @StringRes int messageId) {
        showSimpleDialog(dialogId, getString(titleId), getString(messageId));
    }

    public void showSimpleDialog(int dialogId, String title, String message) {
        FragmentManager manager = getFragmentManager();
        if (manager != null) {
            Bundle bundle = new Bundle();
            bundle.putString("title", title);
            bundle.putString("message", message);
            SimpleDialogFragment dialogFragment = SimpleDialogFragment.newInstance(dialogId, bundle);
            if (dialogFragment != null) {
                dialogFragment.show(manager, DIALOG);
            }
        }
    }

    public void showQuestionDialog(@StringRes int titleResId, @StringRes int messageResId, @StringRes int positiveResId, @StringRes int negativeResId, @NonNull OnClickListener dialogClickListener, @Nullable OnCancelListener onCancelListener) {
        final Builder builder = new Builder(this);
        builder.setTitle(titleResId).setMessage(messageResId).setPositiveButton(positiveResId, dialogClickListener).setNegativeButton(negativeResId, dialogClickListener);
        if (onCancelListener != null) {
            builder.setOnCancelListener(onCancelListener);
        }
        if (Looper.myLooper() == Looper.getMainLooper()) {
            createAndShowQuestionDialog(builder);
        } else {
            runOnUiThread(new Runnable() {
                public void run() {
                    BaseActivity.this.createAndShowQuestionDialog(builder);
                }
            });
        }
    }

    private void createAndShowQuestionDialog(Builder builder) {
        if (!isActivityDestroyed()) {
            try {
                this.questionDialog = builder.create();
                this.questionDialog.show();
            } catch (Throwable t) {
                this.logger.e("Error happened while trying to create question dialog: " + builder, t);
            }
        }
    }

    public void showRequestNewRouteDialog(final Runnable requestRouteRunnable) {
        if (isActivityDestroyed() || !NavdyRouteHandler.getInstance().isInOneOfTheActiveTripStates()) {
            requestRouteRunnable.run();
            return;
        }
        OnClickListener dialogClickListener = new OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                requestRouteRunnable.run();
            }
        };
        Builder builder = new Builder(this);
        builder.setTitle(R.string.request_new_route_dialog_title).setMessage(R.string.request_new_route_dialog_message).setPositiveButton(R.string.yes_button, dialogClickListener).setNegativeButton(R.string.cancel_button, new OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        if (this.requestRouteDialog != null) {
            this.requestRouteDialog.hide();
        }
        this.requestRouteDialog = builder.create();
        this.requestRouteDialog.show();
    }

    public void showExplicitObdSettingForBlacklistDialog() {
        final SharedPreferences sharedPrefs = SettingsUtils.getSharedPreferences();
        Builder builder = new Builder(this);
        builder.setTitle(R.string.settings_obd_warning_title).setMessage(R.string.settings_obd_warning_description).setPositiveButton(R.string.enable_button, new OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                TaskManager.getInstance().execute(new Runnable() {
                    public void run() {
                        BaseActivity.this.logger.d("user overrode blacklist");
                        sharedPrefs.edit().putString(SettingsConstants.HUD_OBD_ON, ObdScanSetting.SCAN_ON.toString()).putBoolean(SettingsConstants.HUD_BLACKLIST_OVERRIDDEN, false).apply();
                    }
                }, 1);
            }
        }).setNegativeButton(R.string.cancel, new OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                BaseActivity.this.logger.d("user turned obd data off");
                sharedPrefs.edit().putString(SettingsConstants.HUD_OBD_ON, ObdScanSetting.SCAN_OFF.toString()).apply();
            }
        });
        if (!isActivityDestroyed()) {
            if (this.obdDialog != null) {
                this.obdDialog.hide();
            }
            this.obdDialog = builder.create();
            this.obdDialog.show();
        }
    }

    public void showObdDialogIfCarHasBeenAddedToBlacklist() {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                SharedPreferences sharedPreferences = SettingsUtils.getSharedPreferences();
                if (SettingsUtils.obdScanIsOn()) {
                    boolean overrodeBlacklist = sharedPreferences.getBoolean(SettingsConstants.HUD_BLACKLIST_OVERRIDDEN, false);
                    if (SettingsUtils.carIsBlacklistedForObdData() && !overrodeBlacklist) {
                        BaseActivity.this.runOnUiThread(new Runnable() {
                            public void run() {
                                BaseActivity.this.showExplicitObdSettingForBlacklistDialog();
                            }
                        });
                    }
                }
            }
        }, 1);
    }

    private static String[] getLocationPermissions() {
        String[] permissions = new String[]{"android.permission.ACCESS_FINE_LOCATION"};
        if (VERSION.SDK_INT <= 25) {
            return permissions;
        }
        return new String[]{"android.permission.ACCESS_FINE_LOCATION", "android.permission.ACCESS_COARSE_LOCATION"};
    }

    private static String getMicrophonePermissions() {
        return "android.permission.RECORD_AUDIO";
    }

    @NonNull
    private static String getContactsPermission() {
        return "android.permission.READ_CONTACTS";
    }

    @NonNull
    private static String[] getSmsPermissions() {
        String[] permissions = new String[]{"android.permission.RECEIVE_SMS"};
        if (VERSION.SDK_INT <= 25) {
            return permissions;
        }
        return new String[]{"android.permission.RECEIVE_SMS", "android.permission.SEND_SMS"};
    }

    @NonNull
    private static String[] getPhonePermissions() {
        String[] permissions = new String[]{"android.permission.CALL_PHONE"};
        if (VERSION.SDK_INT <= 25) {
            return permissions;
        }
        return new String[]{"android.permission.CALL_PHONE", "android.permission.PROCESS_OUTGOING_CALLS", "android.permission.READ_PHONE_STATE", "android.permission.ANSWER_PHONE_CALLS", "android.permission.READ_PHONE_NUMBERS"};
    }

    @NonNull
    private static String[] getStoragePermissions() {
        String[] permissions = new String[]{"android.permission.WRITE_EXTERNAL_STORAGE"};
        if (VERSION.SDK_INT <= 25) {
            return permissions;
        }
        return new String[]{"android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE"};
    }

    @NonNull
    private static String getCalendarPermissions() {
        return "android.permission.READ_CALENDAR";
    }

    @NonNull
    private static String getCameraPermissions() {
        return "android.permission.CAMERA";
    }

    public static boolean weHaveAllPermissions() {
        return weHaveNotificationPermission() && weHaveContactsPermission() && weHaveLocationPermission() && weHaveMicrophonePermission() && weHaveSmsPermission() && weHavePhonePermission() && weHaveStoragePermission() && weHaveCalendarPermission();
    }

    public static boolean weHaveNotificationPermission() {
        Context context = NavdyApplication.getAppContext();
        String enabledNotificationListeners = Secure.getString(context.getContentResolver(), "enabled_notification_listeners");
        return enabledNotificationListeners != null && enabledNotificationListeners.contains(context.getPackageName());
    }

    public static boolean weHaveLocationPermission() {
        return weHaveThisPermission(NavdyApplication.getAppContext(), getLocationPermissions());
    }

    public static boolean weHaveMicrophonePermission() {
        return weHaveThisPermission(NavdyApplication.getAppContext(), getMicrophonePermissions());
    }

    public static boolean weHaveContactsPermission() {
        return weHaveThisPermission(NavdyApplication.getAppContext(), getContactsPermission());
    }

    public static boolean weHaveSmsPermission() {
        return weHaveThisPermission(NavdyApplication.getAppContext(), getSmsPermissions());
    }

    public static boolean weHavePhonePermission() {
        return weHaveThisPermission(NavdyApplication.getAppContext(), getPhonePermissions());
    }

    public static boolean weHaveStoragePermission() {
        return weHaveThisPermission(NavdyApplication.getAppContext(), getStoragePermissions());
    }

    public static boolean weHaveCalendarPermission() {
        return weHaveThisPermission(NavdyApplication.getAppContext(), getCalendarPermissions());
    }

    public static boolean weHaveCameraPermission() {
        return weHaveThisPermission(NavdyApplication.getAppContext(), getCameraPermissions());
    }

    public boolean weHaveThisPermission(String permission) {
        return weHaveThisPermission(getApplicationContext(), permission);
    }

    public static boolean weHaveThisPermission(Context context, @NonNull String[] permissions) {
        boolean hasPermissions = true;
        for (String permission : permissions) {
            if (hasPermissions && weHaveThisPermission(context, permission)) {
                hasPermissions = true;
            } else {
                hasPermissions = false;
            }
        }
        return hasPermissions;
    }

    public static boolean weHaveThisPermission(Context context, String permission) {
        if (context == null) {
            context = NavdyApplication.getAppContext();
        }
        if (context != null && ContextCompat.checkSelfPermission(context, permission) == 0) {
            return true;
        }
        return false;
    }

    private void requestThisPermission(String permission, Queue<Runnable> thingsThatRequireThisPermission, Queue<Runnable> thisPermissionDenialHandlers, Runnable doThingsThatRequireThisPermission, Runnable handleThisPermissionDenial, int requestCode) {
        requestThisPermission(new String[]{permission}, (Queue) thingsThatRequireThisPermission, (Queue) thisPermissionDenialHandlers, doThingsThatRequireThisPermission, handleThisPermissionDenial, requestCode);
    }

    private void requestThisPermission(String[] permissions, Queue<Runnable> thingsThatRequireThisPermission, Queue<Runnable> thisPermissionDenialHandlers, Runnable doThingsThatRequireThisPermission, Runnable handleThisPermissionDenial, int requestCode) {
        boolean weDontHavePermission = false;
        for (String permission : permissions) {
            weDontHavePermission = weDontHavePermission || ContextCompat.checkSelfPermission(getApplicationContext(), permission) != 0;
        }
        if (weDontHavePermission) {
            boolean showTheToast = false;
            for (String permission2 : permissions) {
                boolean thisIsTheFirstTimeWeAsk = isThisTheFirstTimeWeRequestedThisPermission(permission2);
                showTheToast = showTheToast || !(ActivityCompat.shouldShowRequestPermissionRationale(this, permission2) || thisIsTheFirstTimeWeAsk);
                if (thisIsTheFirstTimeWeAsk) {
                    SettingsUtils.getPermissionsSharedPreferences().edit().putBoolean(permission2, false).apply();
                }
            }
            if (showTheToast) {
                showLongToast(R.string.need_permissions, new Object[0]);
            }
            synchronized (this.lock) {
                if (!(doThingsThatRequireThisPermission == null || thingsThatRequireThisPermission == null)) {
                    thingsThatRequireThisPermission.add(doThingsThatRequireThisPermission);
                }
                if (!(handleThisPermissionDenial == null || thisPermissionDenialHandlers == null)) {
                    thisPermissionDenialHandlers.add(handleThisPermissionDenial);
                }
            }
            ActivityCompat.requestPermissions(this, permissions, requestCode);
        } else if (doThingsThatRequireThisPermission != null) {
            doThingsThatRequireThisPermission.run();
        }
    }

    public static boolean isThisTheFirstTimeWeRequestedThisPermission(String permission) {
        return SettingsUtils.getPermissionsSharedPreferences().getBoolean(permission, true);
    }

    public static boolean isThisTheFirstTimeWeRequestedThisPermission(String[] permissions) {
        SharedPreferences sp = SettingsUtils.getPermissionsSharedPreferences();
        boolean isFirstTime = true;
        for (String permission : permissions) {
            if (isFirstTime && sp.getBoolean(permission, true)) {
                isFirstTime = true;
            } else {
                isFirstTime = false;
            }
        }
        return isFirstTime;
    }

    public static boolean alreadyAskedForLocationPermission() {
        return !isThisTheFirstTimeWeRequestedThisPermission(getLocationPermissions());
    }

    public static boolean alreadyAskedForMicrophonePermission() {
        return !isThisTheFirstTimeWeRequestedThisPermission(getMicrophonePermissions());
    }

    public static boolean alreadyAskedForContactsPermission() {
        return !isThisTheFirstTimeWeRequestedThisPermission(getContactsPermission());
    }

    public static boolean alreadyAskedForSmsPermission() {
        return !isThisTheFirstTimeWeRequestedThisPermission(getSmsPermissions());
    }

    public static boolean alreadyAskedForPhonePermission() {
        return !isThisTheFirstTimeWeRequestedThisPermission(getPhonePermissions());
    }

    public static boolean alreadyAskedForStoragePermission() {
        return !isThisTheFirstTimeWeRequestedThisPermission(getStoragePermissions());
    }

    public static boolean alreadyAskedForCalendarPermission() {
        return !isThisTheFirstTimeWeRequestedThisPermission(getCalendarPermissions());
    }

    public static boolean alreadyAskedForCameraPermission() {
        return !isThisTheFirstTimeWeRequestedThisPermission(getCameraPermissions());
    }

    public static void requestLocationPermission(Runnable doThingsThatRequireLocationPermission, Runnable handleLocationPermissionDenial, Activity activity) {
        if (activity != null && (activity instanceof BaseActivity)) {
            ((BaseActivity) activity).requestLocationPermission(doThingsThatRequireLocationPermission, handleLocationPermissionDenial);
        } else if (weHaveLocationPermission()) {
            doThingsThatRequireLocationPermission.run();
        } else {
            handleLocationPermissionDenial.run();
        }
    }

    public void requestLocationPermission(final Runnable doThingsThatRequireLocationPermission, Runnable handleLocationPermissionDenial) {
        requestThisPermission(getLocationPermissions(), this.thingsThatRequireLocationPermission, this.locationPermissionDenialHandlers, new Runnable() {
            public void run() {
                NavdyLocationManager.getInstance().initLocationServices();
                if (doThingsThatRequireLocationPermission != null) {
                    doThingsThatRequireLocationPermission.run();
                }
            }
        }, handleLocationPermissionDenial, 1);
    }

    public void requestMicrophonePermission(Runnable doThingsThatRequireMicrophonePermission, Runnable handleMicrophonePermissionDenial) {
        requestThisPermission(getMicrophonePermissions(), this.thingsThatRequireMicrophonePermission, this.microphonePermissionDenialHandlers, doThingsThatRequireMicrophonePermission, handleMicrophonePermissionDenial, 2);
    }

    public void requestContactsPermission(Runnable doThingsThatRequireContactPermission, Runnable handleContactPermissionDenial) {
        requestThisPermission(getContactsPermission(), this.thingsThatRequireContactPermission, this.contactPermissionDenialHandlers, doThingsThatRequireContactPermission, handleContactPermissionDenial, 3);
    }

    public void requestSmsPermission(Runnable doThingsThatRequireSmsPermission, Runnable handleSmsPermissionDenial) {
        requestThisPermission(getSmsPermissions(), this.thingsThatRequireSmsPermission, this.smsPermissionDenialHandlers, doThingsThatRequireSmsPermission, handleSmsPermissionDenial, 5);
    }

    public void requestPhonePermission(Runnable doThingsThatRequirePhonePermission, Runnable handlePhonePermissionDenial) {
        requestThisPermission(getPhonePermissions(), this.thingsThatRequirePhonePermission, this.phonePermissionDenialHandlers, doThingsThatRequirePhonePermission, handlePhonePermissionDenial, 4);
    }

    public void requestStoragePermission(Runnable doThingsThatRequireStoragePermission, Runnable handleStoragePermissionDenial) {
        requestThisPermission(getStoragePermissions(), this.thingsThatRequireStoragePermission, this.storagePermissionDenialHandlers, doThingsThatRequireStoragePermission, handleStoragePermissionDenial, 6);
    }

    public void requestCalendarPermission(Runnable doThingsThatRequireCalendarPermission, Runnable handleCalendarPermissionDenial) {
        requestThisPermission(getCalendarPermissions(), this.thingsThatRequireCalendarPermission, this.calendarPermissionDenialHandlers, doThingsThatRequireCalendarPermission, handleCalendarPermissionDenial, 7);
    }

    public void requestCameraPermission(Runnable doThingsThatRequireCameraPermission, Runnable handleCameraPermissionDenial) {
        requestThisPermission(getCameraPermissions(), this.thingsThatRequireCameraPermission, this.cameraPermissionDenialHandlers, doThingsThatRequireCameraPermission, handleCameraPermissionDenial, 8);
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        HashMap<String, String> attributes;
        switch (requestCode) {
            case 1:
                synchronized (this.lock) {
                    if (grantResults.length <= 0 || grantResults[0] != 0) {
                        attributes = new HashMap();
                        attributes.put("Type", "Location");
                        Tracker.tagEvent(Event.PERMISSION_REJECTED, attributes);
                        for (Runnable r : this.locationPermissionDenialHandlers) {
                            r.run();
                        }
                    } else {
                        for (Runnable r2 : this.thingsThatRequireLocationPermission) {
                            r2.run();
                        }
                    }
                    this.thingsThatRequireLocationPermission.clear();
                    this.locationPermissionDenialHandlers.clear();
                }
                return;
            case 2:
                synchronized (this.lock) {
                    if (grantResults.length <= 0 || grantResults[0] != 0) {
                        attributes = new HashMap();
                        attributes.put("Type", Attributes.MICROPHONE);
                        Tracker.tagEvent(Event.PERMISSION_REJECTED, attributes);
                        for (Runnable r22 : this.microphonePermissionDenialHandlers) {
                            r22.run();
                        }
                    } else {
                        for (Runnable r222 : this.thingsThatRequireMicrophonePermission) {
                            r222.run();
                        }
                    }
                    this.thingsThatRequireMicrophonePermission.clear();
                    this.microphonePermissionDenialHandlers.clear();
                }
                return;
            case 3:
                synchronized (this.lock) {
                    if (grantResults.length <= 0 || grantResults[0] != 0) {
                        attributes = new HashMap();
                        attributes.put("Type", Attributes.CONTACTS);
                        Tracker.tagEvent(Event.PERMISSION_REJECTED, attributes);
                        for (Runnable r2222 : this.contactPermissionDenialHandlers) {
                            r2222.run();
                        }
                    } else {
                        for (Runnable r22222 : this.thingsThatRequireContactPermission) {
                            r22222.run();
                        }
                    }
                    this.thingsThatRequireContactPermission.clear();
                    this.contactPermissionDenialHandlers.clear();
                }
                return;
            case 4:
                synchronized (this.lock) {
                    if (grantResults.length <= 0 || grantResults[0] != 0) {
                        attributes = new HashMap();
                        attributes.put("Type", Attributes.PHONE);
                        Tracker.tagEvent(Event.PERMISSION_REJECTED, attributes);
                        for (Runnable r222222 : this.phonePermissionDenialHandlers) {
                            r222222.run();
                        }
                    } else {
                        for (Runnable r2222222 : this.thingsThatRequirePhonePermission) {
                            r2222222.run();
                        }
                    }
                    this.thingsThatRequirePhonePermission.clear();
                    this.phonePermissionDenialHandlers.clear();
                }
                return;
            case 5:
                synchronized (this.lock) {
                    if (grantResults.length <= 0 || grantResults[0] != 0) {
                        attributes = new HashMap();
                        attributes.put("Type", Attributes.SMS);
                        Tracker.tagEvent(Event.PERMISSION_REJECTED, attributes);
                        for (Runnable r22222222 : this.smsPermissionDenialHandlers) {
                            r22222222.run();
                        }
                    } else {
                        for (Runnable r222222222 : this.thingsThatRequireSmsPermission) {
                            r222222222.run();
                        }
                    }
                    this.thingsThatRequireSmsPermission.clear();
                    this.smsPermissionDenialHandlers.clear();
                }
                return;
            case 6:
                synchronized (this.lock) {
                    if (grantResults.length <= 0 || grantResults[0] != 0) {
                        attributes = new HashMap();
                        attributes.put("Type", Attributes.STORAGE);
                        Tracker.tagEvent(Event.PERMISSION_REJECTED, attributes);
                        for (Runnable r2222222222 : this.storagePermissionDenialHandlers) {
                            r2222222222.run();
                        }
                    } else {
                        for (Runnable r22222222222 : this.thingsThatRequireStoragePermission) {
                            r22222222222.run();
                        }
                    }
                    this.thingsThatRequireStoragePermission.clear();
                    this.storagePermissionDenialHandlers.clear();
                }
                return;
            case 7:
                synchronized (this.lock) {
                    if (grantResults.length <= 0 || grantResults[0] != 0) {
                        for (Runnable r222222222222 : this.calendarPermissionDenialHandlers) {
                            r222222222222.run();
                        }
                    } else {
                        for (Runnable r2222222222222 : this.thingsThatRequireCalendarPermission) {
                            r2222222222222.run();
                        }
                    }
                    this.thingsThatRequireCalendarPermission.clear();
                    this.calendarPermissionDenialHandlers.clear();
                }
                return;
            case 8:
                synchronized (this.lock) {
                    if (grantResults.length <= 0 || grantResults[0] != 0) {
                        for (Runnable r22222222222222 : this.cameraPermissionDenialHandlers) {
                            r22222222222222.run();
                        }
                    } else {
                        for (Runnable r222222222222222 : this.thingsThatRequireCameraPermission) {
                            r222222222222222.run();
                        }
                    }
                    this.thingsThatRequireCameraPermission.clear();
                    this.cameraPermissionDenialHandlers.clear();
                }
                return;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                return;
        }
    }

    public void requestNotificationPermission() {
        if (!weHaveNotificationPermission()) {
            startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
        }
    }

    public void openBtConnectionDialog() {
        this.logger.v("Opening BT connection dialog");
        boolean isFirstLaunch = false;
        if (this instanceof AppSetupActivity) {
            this.logger.v("instance of FirstLaunchActivity");
            isFirstLaunch = true;
        }
        Intent i = new Intent(getApplicationContext(), BluetoothPairActivity.class);
        i.putExtra(BluetoothFramelayoutFragment.EXTRA_KILL_ACTIVITY_ON_CANCEL, true);
        i.putExtra(BluetoothFramelayoutFragment.EXTRA_USER_WAS_IN_FIRST_LAUNCH_EXPERIENCE, isFirstLaunch);
        startActivity(i);
    }

    public void setStatusBarColor(int color) {
        if (VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.clearFlags(67108864);
            window.addFlags(Integer.MIN_VALUE);
            window.setStatusBarColor(color);
        }
    }

    public void showProgressDialog() {
        this.progressDialog = showProgressDialog(this, this.progressDialog);
    }

    public static ProgressDialog showProgressDialog(Activity activity, ProgressDialog progressDialog) {
        if (isEnding(activity)) {
            return progressDialog;
        }
        if (progressDialog == null) {
            try {
                progressDialog = new ProgressDialog(activity);
            } catch (Exception e) {
                sLogger.e("Unable to show progress dialog. " + progressDialog, e);
            }
        }
        progressDialog.setProgressStyle(0);
        progressDialog.setMessage(activity.getString(R.string.loading_please_wait));
        progressDialog.setIndeterminate(true);
        progressDialog.setCanceledOnTouchOutside(false);
        if (Looper.myLooper() == Looper.getMainLooper()) {
            progressDialog.show();
        } else {
            final ProgressDialog finalProgressDialog = progressDialog;
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    finalProgressDialog.show();
                }
            });
        }
        return progressDialog;
    }

    public void hideProgressDialog() {
        hideProgressDialog(this, this.progressDialog);
    }

    public static void hideProgressDialog(Activity activity, ProgressDialog progressDialog) {
        if (progressDialog != null && progressDialog.isShowing()) {
            dismissProgressDialog(activity, progressDialog);
        }
    }

    public void dismissProgressDialog() {
        dismissProgressDialog(this, this.progressDialog);
    }

    public static void dismissProgressDialog(Activity activity, final ProgressDialog progressDialog) {
        if (progressDialog != null) {
            if (Looper.myLooper() == Looper.getMainLooper()) {
                progressDialog.dismiss();
            } else {
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        progressDialog.dismiss();
                    }
                });
            }
        }
    }

    public void dismissQuestionDialog() {
        if (this.questionDialog != null) {
            if (Looper.myLooper() == Looper.getMainLooper()) {
                this.questionDialog.dismiss();
            } else {
                runOnUiThread(new Runnable() {
                    public void run() {
                        BaseActivity.this.questionDialog.dismiss();
                    }
                });
            }
        }
    }

    @NonNull
    public Point getScreenSize() {
        return getScreenSize(this);
    }

    @NonNull
    public static Point getScreenSize(FragmentActivity activity) {
        Display display;
        if (activity != null) {
            display = activity.getWindowManager().getDefaultDisplay();
        } else {
            display = ((WindowManager) NavdyApplication.getAppContext().getSystemService("window")).getDefaultDisplay();
        }
        Point size = new Point();
        display.getSize(size);
        return size;
    }

    public static boolean isEnding(Activity activity) {
        return activity == null || activity.isFinishing() || activity.isDestroyed() || ((activity instanceof BaseActivity) && ((BaseActivity) activity).instanceStateIsSaved.get());
    }

    public boolean requiresBus() {
        return true;
    }

    public void onActionModeStarted(ActionMode mode) {
        super.onActionModeStarted(mode);
        this.mActionMode = mode;
    }

    private void endActionMode() {
        if (this.mActionMode != null) {
            this.mActionMode.finish();
        }
    }

    public void onActionModeFinished(ActionMode mode) {
        super.onActionModeFinished(mode);
        this.mActionMode = null;
    }

    protected void openMarketAppFor(String appPackageName) {
        try {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + appPackageName)));
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    public static void showLongToast(@StringRes int messageResId, Object... args) {
        showToast(1, messageResId, args);
    }

    public static void showShortToast(@StringRes int messageResId, Object... args) {
        showToast(0, messageResId, args);
    }

    private static void showToast(final int length, @StringRes final int messageResId, final Object... args) {
        SystemUtils.runOnUiThread(new Runnable() {
            public void run() {
                if (BaseActivity.toast != null) {
                    BaseActivity.toast.cancel();
                }
                Context appContext = NavdyApplication.getAppContext();
                BaseActivity.toast = Toast.makeText(appContext, appContext.getString(messageResId, args), length);
                BaseActivity.toast.show();
            }
        });
    }

    public void hideSystemUI() {
        if (!isEnding(this)) {
            Window window = getWindow();
            if (window != null) {
                View decorView = window.getDecorView();
                if (decorView != null) {
                    decorView.setSystemUiVisibility(5380);
                }
            }
        }
    }

    public void showSystemUI() {
        if (!isEnding(this)) {
            Window window = getWindow();
            if (window != null) {
                View decorView = window.getDecorView();
                if (decorView != null) {
                    decorView.setSystemUiVisibility(256);
                }
            }
        }
    }

    public void hideSystemUiDependingOnOrientation() {
        if (!isEnding(this)) {
            Resources resources = getResources();
            if (resources != null) {
                Configuration configuration = resources.getConfiguration();
                if (configuration == null) {
                    return;
                }
                if (configuration.orientation == 2) {
                    hideSystemUI();
                } else {
                    showSystemUI();
                }
            }
        }
    }

    public void openBrowserFor(Uri uri) {
        try {
            startActivity(new Intent("android.intent.action.VIEW", uri));
        } catch (ActivityNotFoundException e) {
            Toast.makeText(NavdyApplication.getAppContext(), R.string.no_browser, 1).show();
        }
    }

    public void loadImage(@IdRes int imgId, @DrawableRes int drawableRes) {
        ImageView imgView = (ImageView) findViewById(imgId);
        if (imgView != null) {
            ImageUtils.loadImage(imgView, drawableRes, this.imageCache);
        }
    }
}
