package com.navdy.client.app.framework.search;

import android.support.annotation.Nullable;
import com.navdy.client.app.framework.models.Address;
import com.navdy.client.app.framework.models.ContactModel;
import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.framework.models.Destination.SearchType;
import com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.ui.search.SearchRecyclerAdapter;
import com.navdy.service.library.events.places.PlacesSearchResult;
import java.util.ArrayList;
import java.util.List;

public class SearchResults {
    private List<Destination> contactDestinations;
    private List<ContactModel> contacts;
    private ArrayList<Destination> destinationsFromDatabase;
    private List<GoogleTextSearchDestinationResult> googleResults;
    private List<PlacesSearchResult> hudSearchResults;
    private String query;

    SearchResults() {
    }

    public void setContacts(List<ContactModel> contacts) {
        this.contacts = contacts;
        this.contactDestinations = getDestinationListFromContactResults(contacts);
    }

    void setGoogleResults(List<GoogleTextSearchDestinationResult> googleResults) {
        this.googleResults = googleResults;
    }

    void setHudSearchResults(List<PlacesSearchResult> hudSearchResults) {
        this.hudSearchResults = hudSearchResults;
    }

    void setDestinationsFromDatabase(ArrayList<Destination> destinationsFromDatabase) {
        this.destinationsFromDatabase = destinationsFromDatabase;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public List<ContactModel> getContacts() {
        return this.contacts;
    }

    private List<Destination> getContactDestinations() {
        if (this.contactDestinations == null) {
            this.contactDestinations = getDestinationListFromContactResults(this.contacts);
        }
        return this.contactDestinations;
    }

    public List<GoogleTextSearchDestinationResult> getGoogleResults() {
        return this.googleResults;
    }

    public ArrayList<Destination> getDestinationsFromDatabase() {
        return this.destinationsFromDatabase;
    }

    public List<PlacesSearchResult> getHudSearchResults() {
        return this.hudSearchResults;
    }

    public String getQuery() {
        return this.query;
    }

    @Nullable
    private List<Destination> getDestinationListFromContactResults(@Nullable List<ContactModel> contacts) {
        if (contacts == null || contacts.isEmpty()) {
            return null;
        }
        List<Destination> destinations = new ArrayList(contacts.size());
        for (ContactModel contact : contacts) {
            if (contact != null) {
                for (Address address : contact.addresses) {
                    String fullAddress = SearchRecyclerAdapter.getCleanAddressString(address);
                    if (!StringUtils.isEmptyAfterTrim(fullAddress)) {
                        Destination destination = new Destination();
                        destination.name = contact.name;
                        destination.rawAddressNotForDisplay = fullAddress;
                        destination.searchResultType = SearchType.CONTACT.getValue();
                        destination.contactLookupKey = contact.lookupKey;
                        destination.lastKnownContactId = contact.id;
                        destination.lastContactLookup = contact.lookupTimestamp;
                        destinations.add(destination);
                    }
                }
            }
        }
        return destinations;
    }

    @Nullable
    static ArrayList<Destination> getDestinationListFromHudSearchResults(@Nullable List<PlacesSearchResult> placesSearchResults) {
        ArrayList<Destination> destinations = new ArrayList();
        if (placesSearchResults != null) {
            for (PlacesSearchResult placesSearchResult : placesSearchResults) {
                Destination destination = new Destination();
                Destination.placesSearchResultToDestinationObject(placesSearchResult, destination);
                destinations.add(destination);
            }
        }
        return destinations;
    }

    @Nullable
    public static ArrayList<Destination> getDestinationListFromGoogleTextSearchResults(@Nullable List<GoogleTextSearchDestinationResult> googleTextSearchResults) {
        ArrayList<Destination> destinations = new ArrayList();
        if (googleTextSearchResults != null) {
            for (GoogleTextSearchDestinationResult googleTextSearchDestinationResult : googleTextSearchResults) {
                Destination destination = new Destination();
                googleTextSearchDestinationResult.toModelDestinationObject(SearchType.TEXT_SEARCH, destination);
                destinations.add(destination);
            }
        }
        return destinations;
    }

    public static boolean isEmpty(SearchResults sr) {
        List<Destination> contactDestinations = sr.getContactDestinations();
        return (contactDestinations == null || contactDestinations.isEmpty()) && ((sr.googleResults == null || sr.googleResults.isEmpty()) && ((sr.hudSearchResults == null || sr.hudSearchResults.isEmpty()) && (sr.destinationsFromDatabase == null || sr.destinationsFromDatabase.isEmpty())));
    }

    public ArrayList<Destination> getDestinationsList() {
        return (ArrayList) Destination.mergeAndDeduplicateLists(Destination.mergeAndDeduplicateLists(Destination.mergeAndDeduplicateLists(Destination.mergeAndDeduplicateLists(new ArrayList(0), getContactDestinations()), getDestinationListFromGoogleTextSearchResults(this.googleResults)), getDestinationListFromHudSearchResults(this.hudSearchResults)), this.destinationsFromDatabase);
    }

    public Destination getFirstDestination() {
        List<Destination> destinations = getContactDestinations();
        if (destinations != null) {
            for (Destination contactDestination : destinations) {
                if (contactDestination != null) {
                    return contactDestination;
                }
            }
        }
        destinations = getDestinationListFromGoogleTextSearchResults(this.googleResults);
        if (destinations != null) {
            for (Destination contactDestination2 : destinations) {
                if (contactDestination2 != null) {
                    return contactDestination2;
                }
            }
        }
        destinations = getDestinationListFromHudSearchResults(this.hudSearchResults);
        if (destinations != null) {
            for (Destination contactDestination22 : destinations) {
                if (contactDestination22 != null) {
                    return contactDestination22;
                }
            }
        }
        destinations = this.destinationsFromDatabase;
        if (destinations != null) {
            for (Destination contactDestination222 : destinations) {
                if (contactDestination222 != null) {
                    return contactDestination222;
                }
            }
        }
        return null;
    }
}
