package com.navdy.client.app.framework.callcontrol;

import android.annotation.TargetApi;
import android.content.Context;
import android.telecom.TelecomManager;
import android.telephony.TelephonyManager;
import com.navdy.service.library.log.Logger;

public class TelephonySupport26 extends TelephonySupport21 {
    private static Logger sLogger = new Logger(TelephonySupport26.class);

    public TelephonySupport26(Context context) {
        super(context);
    }

    @TargetApi(26)
    public void acceptRingingCall() {
        TelecomManager telecomManager = (TelecomManager) this.context.getSystemService("telecom");
        if (telecomManager == null) {
            sLogger.e("Unable to handleCallAction. telecomManager is null");
            acceptRingingCallFallback();
            return;
        }
        try {
            telecomManager.getClass().getMethod("acceptRingingCall", new Class[0]).invoke(telecomManager, new Object[0]);
        } catch (Exception e) {
            sLogger.e("Unable to use the Telecom Manager directly.", e);
            acceptRingingCallFallback();
        }
    }

    private void acceptRingingCallFallback() {
        super.acceptRingingCall();
    }

    public void endCall() {
        if (!callTelephonyManagerMethod("endCall")) {
            super.endCall();
        }
    }

    private boolean callTelephonyManagerMethod(String methodName) {
        TelephonyManager tm = (TelephonyManager) this.context.getSystemService("phone");
        if (tm == null) {
            sLogger.e("Unable to get the Telephony Manager.");
            return false;
        }
        try {
            tm.getClass().getMethod(methodName, new Class[0]).invoke(tm, new Object[0]);
            return true;
        } catch (Exception e) {
            sLogger.e("Unable to use the Telephony Manager directly.", e);
            return false;
        }
    }
}
