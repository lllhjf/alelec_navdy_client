package com.navdy.client.app.framework.callcontrol;

import android.annotation.TargetApi;
import android.content.Context;
import android.media.session.MediaController;
import android.view.KeyEvent;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;

public class TelephonySupport21_SAMSUNG_G920I extends TelephonySupport21 {
    private static final Logger sLogger = new Logger(TelephonySupport21_SAMSUNG_G920I.class);

    public TelephonySupport21_SAMSUNG_G920I(Context context) {
        super(context);
    }

    @TargetApi(21)
    protected void accept(final MediaController m) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                try {
                    m.dispatchMediaButtonEvent(new KeyEvent(0, 79));
                    TelephonySupport21_SAMSUNG_G920I.sLogger.i("HEADSETHOOK keydown sent:" + m.getPackageName());
                    m.dispatchMediaButtonEvent(new KeyEvent(1, 79));
                    TelephonySupport21_SAMSUNG_G920I.sLogger.i("HEADSETHOOK keyup sent:" + m.getPackageName());
                } catch (Throwable t) {
                    TelephonySupport21_SAMSUNG_G920I.sLogger.e(t);
                }
            }
        }, 4);
    }

    @TargetApi(21)
    protected void reject(MediaController m) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                try {
                    TelephonySupport21_SAMSUNG_G920I.sLogger.i("reject call:using telephony interface");
                    TelephonySupport.endPhoneCall();
                } catch (Throwable t) {
                    TelephonySupport21_SAMSUNG_G920I.sLogger.e("endCall failed", t);
                }
            }
        }, 4);
    }

    protected void end(MediaController m) {
        accept(m);
    }
}
