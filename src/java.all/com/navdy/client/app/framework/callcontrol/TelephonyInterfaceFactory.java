package com.navdy.client.app.framework.callcontrol;

import android.content.Context;
import android.os.Build;
import android.os.Build.VERSION;

public class TelephonyInterfaceFactory {
    public static TelephonyInterface getTelephonyInterface(Context context) {
        if (VERSION.SDK_INT > 25) {
            return new TelephonySupport26(context);
        }
        if (VERSION.SDK_INT < 21) {
            return new TelephonySupport(context);
        }
        if (!Build.MANUFACTURER.equalsIgnoreCase("lge")) {
            if (Build.MANUFACTURER.equalsIgnoreCase("samsung")) {
                if (Build.MODEL.contains("SM-G920") || Build.MODEL.contains("SM-G935") || Build.MODEL.contains("SM-G930") || Build.MODEL.contains("SM-N900") || Build.MODEL.contains("SM-N910") || Build.MODEL.contains("SM-G900")) {
                    return new TelephonySupport21_SAMSUNG_G920I(context);
                }
                if (Build.MODEL.contains("SM-G950")) {
                    return new TelephonySupport21_SAMSUNG_G950(context);
                }
            }
            if (Build.MANUFACTURER.equalsIgnoreCase("htc") && Build.MODEL.contains("HTC6545LVW")) {
                return new TelephonySupport21HTC10(context);
            }
            return new TelephonySupport21(context);
        } else if (Build.MODEL.toLowerCase().contains("nexus")) {
            return new TelephonySupport21(context);
        } else {
            if (Build.DEVICE.equalsIgnoreCase("h1") || Build.DEVICE.equalsIgnoreCase("p1")) {
                return new TelephonySupport21(context);
            }
            return new TelephonySupport21_LG_G3(context);
        }
    }
}
