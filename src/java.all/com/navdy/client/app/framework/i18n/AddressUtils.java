package com.navdy.client.app.framework.i18n;

import android.support.annotation.CheckResult;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.JsonReader;
import android.util.Pair;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import org.droidparts.contract.SQL.DDL;

public class AddressUtils {
    public static final String ADDRESS_SEPARATOR = ",";
    private static final String CITY = "city";
    private static final String COUNTRIES = "countries";
    private static final String COUNTRY = "country";
    public static final String DEFAULT_COUNTRY_CODE = "US";
    private static final String FORMAT = "format";
    private static final String FORMATS = "formats";
    private static final String ISO_3166_1_ALPHA_3 = "ISO 3166-1-a3";
    private static final String ISO_3166_2 = "ISO_3166-2";
    private static final String NUMBER = "number";
    private static final String STATE = "state";
    private static final String STREET = "street";
    private static final String ZIP = "zip";
    private static final HashMap<String, AddressFormat> formats = new HashMap();
    private static final Map<String, String> iso3ToIso2 = new HashMap();
    private static final HashMap<String, String> isoToFormatString = new HashMap();
    private static final Logger logger = new Logger(AddressUtils.class);

    public static class AddressFormat {
        public ArrayList<String> foreignLines = new ArrayList();
        public ArrayList<String> localLines = new ArrayList();

        static AddressFormat getClone(AddressFormat original) {
            if (original == null) {
                return null;
            }
            AddressFormat af = new AddressFormat();
            af.localLines = new ArrayList(original.localLines);
            af.foreignLines = new ArrayList(original.foreignLines);
            return af;
        }
    }

    static void initAddressFormats() {
        Exception e;
        Throwable th;
        JsonReader reader = null;
        try {
            JsonReader reader2 = new JsonReader(new InputStreamReader(NavdyApplication.getAppContext().getResources().openRawResource(R.raw.address_formats)));
            try {
                reader2.beginObject();
                while (reader2.hasNext()) {
                    String name = reader2.nextName();
                    Object obj = -1;
                    switch (name.hashCode()) {
                        case -677443748:
                            if (name.equals(FORMATS)) {
                                obj = 1;
                                break;
                            }
                            break;
                        case 1352637108:
                            if (name.equals(COUNTRIES)) {
                                obj = null;
                                break;
                            }
                            break;
                    }
                    switch (obj) {
                        case null:
                            readCountries(reader2);
                            break;
                        case 1:
                            readFormats(reader2);
                            break;
                        default:
                            reader2.skipValue();
                            break;
                    }
                }
                reader2.endObject();
                IOUtils.closeStream(reader2);
            } catch (Exception e2) {
                e = e2;
                reader = reader2;
                try {
                    logger.e("Exception found: " + e);
                    throw new RuntimeException(e);
                } catch (Throwable th2) {
                    th = th2;
                    IOUtils.closeStream(reader);
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                reader = reader2;
                IOUtils.closeStream(reader);
                throw th;
            }
        } catch (Exception e3) {
            e = e3;
            logger.e("Exception found: " + e);
            throw new RuntimeException(e);
        }
    }

    private static void readCountries(JsonReader reader) throws IOException {
        reader.beginArray();
        while (reader.hasNext()) {
            reader.beginObject();
            String iso2 = null;
            String iso3 = null;
            String format = null;
            while (reader.hasNext()) {
                String name = reader.nextName();
                Object obj = -1;
                switch (name.hashCode()) {
                    case -1268779017:
                        if (name.equals(FORMAT)) {
                            obj = 2;
                            break;
                        }
                        break;
                    case -991880995:
                        if (name.equals(ISO_3166_2)) {
                            obj = null;
                            break;
                        }
                        break;
                    case -256260126:
                        if (name.equals(ISO_3166_1_ALPHA_3)) {
                            obj = 1;
                            break;
                        }
                        break;
                }
                switch (obj) {
                    case null:
                        iso2 = reader.nextString();
                        break;
                    case 1:
                        iso3 = reader.nextString();
                        break;
                    case 2:
                        format = reader.nextString();
                        break;
                    default:
                        reader.skipValue();
                        break;
                }
            }
            if (!StringUtils.isEmptyAfterTrim(format)) {
                if (!StringUtils.isEmptyAfterTrim(iso2)) {
                    isoToFormatString.put(iso2, format);
                }
                if (!StringUtils.isEmptyAfterTrim(iso3)) {
                    isoToFormatString.put(iso3, format);
                }
                if (!(StringUtils.isEmptyAfterTrim(iso2) || StringUtils.isEmptyAfterTrim(iso3))) {
                    iso3ToIso2.put(iso3, iso2);
                }
            }
            reader.endObject();
        }
        reader.endArray();
    }

    private static void readFormats(JsonReader reader) throws IOException {
        reader.beginArray();
        while (reader.hasNext()) {
            reader.beginObject();
            String formatName = reader.nextName();
            AddressFormat format = new AddressFormat();
            reader.beginObject();
            while (reader.hasNext()) {
                String lineName = reader.nextName();
                Object obj = -1;
                switch (lineName.hashCode()) {
                    case 199508948:
                        if (lineName.equals("foreign_lines")) {
                            obj = 1;
                            break;
                        }
                        break;
                    case 1753027883:
                        if (lineName.equals("local_lines")) {
                            obj = null;
                            break;
                        }
                        break;
                }
                switch (obj) {
                    case null:
                        reader.beginArray();
                        while (reader.hasNext()) {
                            String localLine = reader.nextString();
                            if (!StringUtils.isEmptyAfterTrim(localLine)) {
                                format.localLines.add(localLine);
                            }
                        }
                        reader.endArray();
                        break;
                    case 1:
                        reader.beginArray();
                        while (reader.hasNext()) {
                            String foreignLine = reader.nextString();
                            if (!StringUtils.isEmptyAfterTrim(foreignLine)) {
                                format.foreignLines.add(foreignLine);
                            }
                        }
                        reader.endArray();
                        break;
                    default:
                        reader.skipValue();
                        break;
                }
            }
            reader.endObject();
            reader.endObject();
            if (!StringUtils.isEmptyAfterTrim(formatName)) {
                formats.put(formatName, format);
            }
        }
        reader.endArray();
    }

    @Nullable
    @CheckResult
    private static String getFormatStringFor(@NonNull Destination destination) {
        if (!destination.hasDetailedAddress() || StringUtils.isEmptyAfterTrim(destination.getCountryCode())) {
            return null;
        }
        return (String) isoToFormatString.get(destination.getCountryCode());
    }

    @Nullable
    @CheckResult
    public static AddressFormat getAddressFormatFor(@NonNull Destination destination) {
        AddressFormat format = null;
        String formatString = getFormatStringFor(destination);
        if (!StringUtils.isEmptyAfterTrim(formatString)) {
            format = AddressFormat.getClone((AddressFormat) formats.get(formatString));
            if (format != null) {
            }
        }
        return format;
    }

    @CheckResult
    @NonNull
    public static String insertAddressPartsInFormat(@NonNull String line, @NonNull Destination d) {
        return StringUtils.replaceOrErase(StringUtils.replaceOrErase(StringUtils.replaceOrErase(StringUtils.replaceOrErase(StringUtils.replaceOrErase(StringUtils.replaceOrErase(line, NUMBER, d.getStreetNumber()), STREET, d.getStreetName()), ZIP, d.getZipCode()), "city", d.getCity()), "state", d.getState()), "country", d.getCountry());
    }

    @CheckResult
    @NonNull
    public static String joinRemainingLines(ArrayList<String> lines, int startAt) {
        if (lines.size() > startAt) {
            return StringUtils.replaceAllSafely(StringUtils.join(lines.subList(startAt, lines.size()), DDL.SEPARATOR), ", *, *", DDL.SEPARATOR).trim();
        }
        return "";
    }

    @CheckResult
    public static String sanitizeAddress(String address) {
        if (StringUtils.isEmptyAfterTrim(address)) {
            return address;
        }
        address = address.replaceAll("\n", DDL.SEPARATOR).trim();
        if (address.endsWith(",")) {
            return address.substring(0, address.length() - ",".length());
        }
        return address;
    }

    public static Pair<String, String> cleanUpDebugPrefix(Pair<String, String> addressPair) {
        return addressPair;
    }

    private static String cleanUpDebugPrefix(@Nullable String line) {
        return line;
    }

    public static boolean isTitleIsInTheAddress(@Nullable String destinationTitle, @Nullable String address) {
        if (StringUtils.isEmptyAfterTrim(destinationTitle) || StringUtils.isEmptyAfterTrim(address)) {
            return false;
        }
        return address.startsWith(destinationTitle);
    }

    @Nullable
    public static String convertIso3ToIso2(@NonNull String iso3) {
        return (String) iso3ToIso2.get(iso3);
    }

    @Nullable
    public static String convertIso2ToIso3(@NonNull String iso2) {
        for (Entry<String, String> entries : iso3ToIso2.entrySet()) {
            if (TextUtils.equals((String) entries.getValue(), iso2)) {
                return (String) entries.getKey();
            }
        }
        return iso2;
    }
}
