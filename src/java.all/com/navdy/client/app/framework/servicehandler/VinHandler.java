package com.navdy.client.app.framework.servicehandler;

import com.navdy.client.app.framework.LocalyticsManager;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.client.app.tracking.TrackerConstants.Attributes;
import com.navdy.service.library.events.obd.ObdStatusResponse;
import com.squareup.otto.Subscribe;

public class VinHandler {
    public VinHandler() {
        BusProvider.getInstance().register(this);
    }

    @Subscribe
    public void onObdStatusResponse(ObdStatusResponse odbStatusResponse) {
        LocalyticsManager.setProfileAttribute(Attributes.VIN, odbStatusResponse.vin);
    }
}
