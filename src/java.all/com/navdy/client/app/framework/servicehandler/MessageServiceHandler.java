package com.navdy.client.app.framework.servicehandler;

import android.telephony.SmsManager;
import com.navdy.client.app.framework.DeviceConnection;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.messaging.SmsMessageRequest;
import com.navdy.service.library.events.messaging.SmsMessageResponse;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Subscribe;

public class MessageServiceHandler {
    public static final Logger sLogger = new Logger(MessageServiceHandler.class);

    public MessageServiceHandler() {
        BusProvider.getInstance().register(this);
    }

    @Subscribe
    public void onSmsMessageRequest(SmsMessageRequest request) {
        try {
            sLogger.v("[message] sending sms");
            SmsManager.getDefault().sendTextMessage(request.number, null, request.message, null, null);
            sLogger.v("[message] sent sms");
            DeviceConnection.postEvent(new SmsMessageResponse(RequestStatus.REQUEST_SUCCESS, request.number, request.name, request.id));
        } catch (Throwable t) {
            sLogger.e(t);
            DeviceConnection.postEvent(new SmsMessageResponse(RequestStatus.REQUEST_SERVICE_ERROR, request != null ? request.number : null, request != null ? request.name : null, request != null ? request.id : null));
        }
    }
}
