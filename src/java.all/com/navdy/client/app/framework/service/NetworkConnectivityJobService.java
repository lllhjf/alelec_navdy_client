package com.navdy.client.app.framework.service;

import android.app.job.JobInfo;
import android.app.job.JobInfo.Builder;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build.VERSION;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.util.SupportTicketService;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.service.library.log.Logger;

public class NetworkConnectivityJobService extends JobService {
    public static final int NETWORK_SERVICE = 100;
    public static final Logger logger = new Logger(NetworkConnectivityJobService.class);

    public boolean onStartJob(JobParameters jobParameters) {
        logger.d("onStartJob");
        SupportTicketService.startSupportTicketService(NavdyApplication.getAppContext());
        return false;
    }

    public boolean onStopJob(JobParameters jobParameters) {
        boolean pendingTicketsExist = SettingsUtils.getSharedPreferences().getBoolean(SettingsConstants.PENDING_TICKETS_EXIST, false);
        logger.d("onStopJob - JobService will reschedule: " + pendingTicketsExist);
        return pendingTicketsExist;
    }

    public static void scheduleNetworkServiceForNougatOrAbove() {
        if (VERSION.SDK_INT >= 24) {
            Context context = NavdyApplication.getAppContext();
            JobScheduler jobScheduler = (JobScheduler) context.getSystemService(JobScheduler.class);
            if (jobScheduler == null || jobScheduler.getPendingJob(100) == null) {
                logger.d("Schedule network connectivity job service");
                JobInfo job = new Builder(100, new ComponentName(context, NetworkConnectivityJobService.class)).setRequiredNetworkType(1).build();
                if (jobScheduler != null) {
                    jobScheduler.schedule(job);
                    return;
                }
                return;
            }
            logger.d("NetworkConnectivityJobService already pending");
        }
    }
}
