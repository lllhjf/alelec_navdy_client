package com.navdy.client.app.framework.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import com.navdy.client.app.framework.map.HereMapsManager;
import com.navdy.service.library.log.Logger;

public class TaskRemovalService extends Service {
    public static final Logger logger = new Logger(TaskRemovalService.class);
    private boolean startedByAppInstance;

    public int onStartCommand(Intent intent, int flags, int startId) {
        logger.i("Service started");
        this.startedByAppInstance = true;
        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onTaskRemoved(Intent rootIntent) {
        logger.i("Task removed - started by app: " + this.startedByAppInstance);
        if (this.startedByAppInstance) {
            HereMapsManager.getInstance().killSelfAndMapEngine();
        }
        super.onTaskRemoved(rootIntent);
    }

    public static void startService(Context context) {
        context.startService(new Intent(context, TaskRemovalService.class));
    }
}
