package com.navdy.client.app.framework.models;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.service.library.log.Logger;

public class UserAccountInfo {
    private static Logger logger = new Logger(UserAccountInfo.class);
    public String email;
    public String fullName;
    public Bitmap photo;

    public UserAccountInfo(String fullName, String email, Bitmap photo) {
        this.fullName = fullName;
        this.email = email;
        this.photo = photo;
    }

    public static UserAccountInfo getUserAccountInfo() {
        SharedPreferences customerPrefs = SettingsUtils.getCustomerPreferences();
        return new UserAccountInfo(customerPrefs.getString(ProfilePreferences.FULL_NAME, ""), customerPrefs.getString("email", ""), Tracker.getUserProfilePhoto());
    }

    public boolean hasInfo() {
        return (StringUtils.isEmptyAfterTrim(this.fullName) && StringUtils.isEmptyAfterTrim(this.email) && this.photo == null) ? false : true;
    }
}
