package com.navdy.client.app.framework.models;

import android.graphics.Bitmap;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.framework.util.SystemUtils;
import com.navdy.service.library.log.Logger;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ContactModel {
    private static final Logger logger = new Logger(ContactModel.class);
    public List<Address> addresses = new ArrayList();
    public boolean favorite;
    public long id;
    public String lookupKey;
    public long lookupTimestamp = -1;
    public String name;
    public List<PhoneNumberModel> phoneNumbers = new ArrayList();
    public Bitmap photo;

    public String toString() {
        return "ContactModel{id=" + this.id + ", lookupKey='" + this.lookupKey + '\'' + ", lookupTimestamp=" + this.lookupTimestamp + ", name='" + this.name + '\'' + ", addresses=" + this.addresses + ", phoneNumbers=" + this.phoneNumbers + ", photo=" + this.photo + ", favorite=" + this.favorite + '}';
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof ContactModel)) {
            return false;
        }
        ContactModel otherContact = (ContactModel) obj;
        if (this.addresses.size() != otherContact.addresses.size()) {
            return false;
        }
        for (Address address : this.addresses) {
            boolean found = false;
            for (Address address2 : otherContact.addresses) {
                if (!address.equals(address2)) {
                    found = true;
                }
            }
            if (!found) {
                return false;
            }
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(this.name, otherContact.name) && this.id == otherContact.id) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return (int) this.id;
    }

    public void addAddress(Address address) {
        this.addresses.add(address);
    }

    public void addAddresses(List<Address> addresses) {
        this.addresses.addAll(addresses);
    }

    public void addPhoneNumber(PhoneNumberModel number) {
        this.phoneNumbers.add(number);
    }

    public void addPhoneNumbers(List<PhoneNumberModel> numbers) {
        if (numbers != null) {
            this.phoneNumbers.addAll(numbers);
        }
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setID(long id) {
        this.id = id;
    }

    public void setLookupKey(String lookupKey) {
        this.lookupKey = lookupKey;
    }

    public void setLookupTimestampToNow() {
        this.lookupTimestamp = new Date().getTime();
    }

    public void setPhoto(Bitmap photo) {
        this.photo = photo;
    }

    public void deduplicatePhoneNumbers() {
        if (this.phoneNumbers != null) {
            Map<Long, PhoneNumberModel> numbers = new LinkedHashMap();
            for (PhoneNumberModel phoneNumber : this.phoneNumbers) {
                if (!StringUtils.isEmptyAfterTrim(phoneNumber.number)) {
                    try {
                        numbers.put(Long.valueOf(SystemUtils.convertToNumber(phoneNumber.number)), phoneNumber);
                    } catch (Exception t) {
                        logger.v("Unable to convert this to a phone number: " + phoneNumber.number + " " + t.getMessage());
                    }
                }
            }
            this.phoneNumbers.clear();
            this.phoneNumbers.addAll(numbers.values());
        }
    }
}
