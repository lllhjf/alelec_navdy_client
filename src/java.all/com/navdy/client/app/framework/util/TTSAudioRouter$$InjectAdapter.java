package com.navdy.client.app.framework.util;

import android.content.SharedPreferences;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;
import javax.inject.Provider;

public final class TTSAudioRouter$$InjectAdapter extends Binding<TTSAudioRouter> implements Provider<TTSAudioRouter>, MembersInjector<TTSAudioRouter> {
    private Binding<SharedPreferences> preferences;

    public TTSAudioRouter$$InjectAdapter() {
        super("com.navdy.client.app.framework.util.TTSAudioRouter", "members/com.navdy.client.app.framework.util.TTSAudioRouter", false, TTSAudioRouter.class);
    }

    public void attach(Linker linker) {
        this.preferences = linker.requestBinding("android.content.SharedPreferences", TTSAudioRouter.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.preferences);
    }

    public TTSAudioRouter get() {
        TTSAudioRouter result = new TTSAudioRouter();
        injectMembers(result);
        return result;
    }

    public void injectMembers(TTSAudioRouter object) {
        object.preferences = (SharedPreferences) this.preferences.get();
    }
}
