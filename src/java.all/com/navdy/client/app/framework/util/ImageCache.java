package com.navdy.client.app.framework.util;

import android.graphics.Bitmap;
import com.navdy.service.library.log.Logger;

public class ImageCache {
    private static final int DEFAULT_CACHE_SIZE = 20971520;
    private static final int DEFAULT_IMAGE_MAX_SIZE = 4194304;
    private static final Logger sLogger = new Logger(ImageCache.class);
    private final BitmapCache cache;
    private final int cacheCriteriaSize;

    public ImageCache() {
        this(DEFAULT_CACHE_SIZE, 4194304);
    }

    public ImageCache(int cacheSize, int cacheCriteriaSize) {
        this.cache = new BitmapCache(cacheSize);
        this.cacheCriteriaSize = cacheCriteriaSize;
    }

    public Bitmap getBitmap(String key) {
        if (key == null) {
            return null;
        }
        Bitmap bitmap = (Bitmap) this.cache.get(key);
        if (bitmap == null) {
            return bitmap;
        }
        sLogger.v("cache hit [" + key + "] size[" + getBitmapSize(bitmap) + "]");
        return bitmap;
    }

    public void putBitmap(String key, Bitmap bitmap) {
        if (key != null && bitmap != null) {
            int size = getBitmapSize(bitmap);
            if (size >= this.cacheCriteriaSize) {
                sLogger.e("cache image size [" + size + "] > large [" + this.cacheCriteriaSize + "]");
                return;
            }
            this.cache.put(key, bitmap);
            sLogger.v("cache put [" + key + "] size[" + getBitmapSize(bitmap) + "]");
        }
    }

    public void clearCache() {
        this.cache.evictAll();
        System.gc();
        sLogger.v("clearCache-explicit gc");
    }

    public static int getBitmapSize(Bitmap bitmap) {
        if (bitmap == null) {
            return 0;
        }
        return bitmap.getAllocationByteCount();
    }
}
