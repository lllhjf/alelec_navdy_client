package com.navdy.client.app.framework.util;

import android.graphics.Bitmap;
import android.util.LruCache;

public class BitmapCache extends LruCache<String, Bitmap> {
    public BitmapCache(int maxSize) {
        super(maxSize);
    }

    protected int sizeOf(String key, Bitmap value) {
        return ImageCache.getBitmapSize(value);
    }
}
