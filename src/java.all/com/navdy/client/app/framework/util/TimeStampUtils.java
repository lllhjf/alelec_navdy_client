package com.navdy.client.app.framework.util;

import android.content.Context;
import android.text.format.DateFormat;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class TimeStampUtils {
    private static final double MILE_PER_METER = 6.21371E-4d;

    public static String getDurationStringInHoursAndMinutes(Integer eta) {
        String estimatedETA = "";
        if (eta == null || eta.intValue() < 0) {
            return estimatedETA;
        }
        long minutes = TimeUnit.SECONDS.toMinutes((long) eta.intValue());
        long hours = TimeUnit.MINUTES.toHours(minutes);
        long days = TimeUnit.HOURS.toDays(hours);
        Context context = NavdyApplication.getAppContext();
        if (days > 0) {
            if (hours - TimeUnit.DAYS.toHours(days) > 0) {
                estimatedETA = context.getString(R.string.days_and_hours_format, new Object[]{Long.valueOf(days), Long.valueOf(hours - TimeUnit.DAYS.toHours(days))});
            } else {
                estimatedETA = context.getString(R.string.days_format, new Object[]{Long.valueOf(days)});
            }
        } else if (hours > 0) {
            if (minutes - TimeUnit.HOURS.toMinutes(hours) > 0) {
                estimatedETA = context.getString(R.string.hours_and_minutes_format, new Object[]{Long.valueOf(hours), Long.valueOf(minutes - TimeUnit.HOURS.toMinutes(hours))});
            } else {
                estimatedETA = context.getString(R.string.hours_format, new Object[]{Long.valueOf(hours)});
            }
        } else {
            estimatedETA = context.getString(R.string.minutes_format, new Object[]{Long.valueOf(minutes)});
        }
        return estimatedETA;
    }

    public static String getDistanceStringInMiles(Integer distance) {
        String estimatedDistance = "";
        if (distance == null || distance.intValue() < 0) {
            return estimatedDistance;
        }
        Double milesDouble = Double.valueOf(((double) distance.intValue()) * 6.21371E-4d);
        return new DecimalFormat("#.#").format(milesDouble) + " mi";
    }

    public static String getArrivalTimeString(Integer eta) {
        if (eta == null || eta.intValue() < 0) {
            return "";
        }
        return new SimpleDateFormat(DateFormat.is24HourFormat(NavdyApplication.getAppContext()) ? "HH:mm" : "h:mm aa", Locale.getDefault()).format(new Date(Long.valueOf(new Date().getTime() + TimeUnit.SECONDS.toMillis((long) eta.intValue())).longValue()));
    }
}
