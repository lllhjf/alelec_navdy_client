package com.navdy.client.debug.navigation;

import com.navdy.client.app.framework.DeviceConnection;
import com.navdy.service.library.events.destination.Destination;
import com.navdy.service.library.events.destination.Destination.FavoriteType;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.events.navigation.NavigationRouteRequest.Builder;
import com.navdy.service.library.log.Logger;
import java.util.List;
import java.util.UUID;

public class HUDNavigationManager extends NavigationManager {
    private static final boolean VERBOSE = false;
    private static final Logger logger = new Logger(HUDNavigationManager.class);

    public boolean startRouteRequest(Coordinate coordinate, String label, String streetAddress, String destinationId, FavoriteType destinationType, Coordinate display) {
        return startRouteRequest(coordinate, label, null, streetAddress, destinationId, destinationType, display);
    }

    public boolean startRouteRequest(Coordinate coordinate, String label, List<Coordinate> waypoints, String streetAddress, String destinationId, FavoriteType destinationType, Coordinate display) {
        return startRouteRequest(coordinate, label, waypoints, streetAddress, destinationId, destinationType, display, UUID.randomUUID().toString());
    }

    public boolean startRouteRequest(Coordinate coordinate, String label, List<Coordinate> waypoints, String streetAddress, String destinationId, FavoriteType destinationType, Coordinate display, String requestId) {
        return startRouteRequest(coordinate, label, waypoints, streetAddress, destinationId, destinationType, display, requestId, false);
    }

    public boolean startRouteRequest(Coordinate coordinate, String label, List<Coordinate> waypoints, String streetAddress, String destinationId, FavoriteType destinationType, Coordinate display, String requestId, boolean useStreetAddress) {
        return startRouteRequest(coordinate, label, waypoints, streetAddress, destinationId, destinationType, display, requestId, useStreetAddress, false, null);
    }

    public boolean startRouteRequest(Coordinate coordinate, String label, List<Coordinate> waypoints, String streetAddress, String destinationId, FavoriteType destinationType, Coordinate display, String requestId, boolean useStreetAddress, boolean initiatedOnHud) {
        return startRouteRequest(coordinate, label, waypoints, streetAddress, destinationId, destinationType, display, requestId, useStreetAddress, false, null);
    }

    public boolean startRouteRequest(Coordinate coordinate, String label, List<Coordinate> waypoints, String streetAddress, String destinationId, FavoriteType destinationType, Coordinate display, String requestId, boolean useStreetAddress, boolean initiatedOnHud, Destination destination) {
        return DeviceConnection.postEvent(new Builder().destination(coordinate).destination_identifier(destinationId).destinationType(destinationType).label(label).waypoints(waypoints).streetAddress(streetAddress).geoCodeStreetAddress(Boolean.valueOf(useStreetAddress)).cancelCurrent(Boolean.valueOf(true)).destinationDisplay(display).requestId(requestId).requestDestination(destination).originDisplay(Boolean.valueOf(initiatedOnHud)).build());
    }
}
