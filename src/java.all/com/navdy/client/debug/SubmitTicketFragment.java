package com.navdy.client.debug;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.Injector;
import com.navdy.client.app.framework.PathManager;
import com.navdy.client.app.framework.util.CredentialsUtils;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.ui.base.BaseFragment;
import com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.client.debug.file.FileTransferManager.FileTransferListener;
import com.navdy.client.debug.file.RemoteFileTransferManager;
import com.navdy.client.ota.OTAUpdateService;
import com.navdy.service.library.events.file.FileTransferError;
import com.navdy.service.library.events.file.FileTransferResponse;
import com.navdy.service.library.events.file.FileTransferStatus;
import com.navdy.service.library.events.file.FileType;
import com.navdy.service.library.network.http.HttpUtils;
import com.navdy.service.library.network.http.IHttpManager;
import com.navdy.service.library.network.http.services.JiraClient;
import com.navdy.service.library.network.http.services.JiraClient.Attachment;
import com.navdy.service.library.network.http.services.JiraClient.ResultCallback;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import com.navdy.service.library.util.LogUtils;
import com.navdy.service.library.util.SystemUtils;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;

public class SubmitTicketFragment extends BaseFragment {
    public static final String BUG = "Bug";
    public static final int CONFIRMATION_DIALOG = 2;
    private static final int CONNECTION_TIMEOUT_IN_MINUTES = 2;
    public static final String DESCRIPTION = "description";
    public static final int ERROR_DIALOG = 1;
    public static final String FIELDS = "fields";
    public static final String ISSUE_TYPE = "issuetype";
    private static final String JIRA_ATTACHMENTS = "/attachments/";
    private static final String JIRA_ISSUE_API_URL = "https://navdyhud.atlassian.net/rest/api/2/issue/";
    public static final String KEY = "key";
    public static final String NAME = "name";
    public static final int PROGRESS_DIALOG = 3;
    public static final String PROJECT = "project";
    private static final String PROJECT_KEY = "NAND";
    public static final long PULL_LOGS_TIMEOUT = 15000;
    public static final String SUMMARY = "summary";
    private static final String TEMP_FILE_TIMESTAMP_FORMAT = "'device_log'_yyyy_dd_MM-hh_mm_ss_aa'.txt'";
    private static final SimpleDateFormat format = new SimpleDateFormat(TEMP_FILE_TIMESTAMP_FORMAT);
    private String ENCODED_CRED = "";
    @InjectView(2131756012)
    EditText mDescription;
    private String mDeviceLogFile;
    private String mDisplayLogFile;
    @Inject
    IHttpManager mHttpManager = null;
    private JiraClient mJiraClient;
    @InjectView(2131756011)
    EditText mTitle;
    @InjectView(2131756010)
    Spinner spinner;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.ENCODED_CRED = CredentialsUtils.getCredentials(NavdyApplication.getAppContext().getString(R.string.metadata_jira_credentials));
        Injector.inject(NavdyApplication.getAppContext(), this);
        this.mJiraClient = new JiraClient(this.mHttpManager.getClientCopy().readTimeout(2, TimeUnit.MINUTES).connectTimeout(2, TimeUnit.MINUTES).build());
        this.mJiraClient.setEncodedCredentials(this.ENCODED_CRED);
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.submit_ticket_layout, container, false);
        ButterKnife.inject((Object) this, view);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(view.getContext(), R.array.jira_projects, 17367048);
        adapter.setDropDownViewResource(17367049);
        this.spinner.setAdapter(adapter);
        this.spinner.setSelection(0);
        return view;
    }

    @OnClick({2131756013})
    void onSubmit(View view) {
        if (this.mTitle.getText().toString().equals("")) {
            showSimpleDialog(1, getString(R.string.error), getString(R.string.please_enter_title));
        } else if (SystemUtils.isConnectedToNetwork(NavdyApplication.getAppContext())) {
            showSimpleDialog(3, getString(R.string.collect_logs), getString(R.string.collecting_device_logs));
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    Context applicationContext = NavdyApplication.getAppContext();
                    final String logsFolder = PathManager.getInstance().getLogsFolder();
                    SubmitTicketFragment.this.mDeviceLogFile = logsFolder + File.separator + (SubmitTicketFragment.format.format(Long.valueOf(System.currentTimeMillis())) + ".txt");
                    try {
                        LogUtils.dumpLog(applicationContext, SubmitTicketFragment.this.mDeviceLogFile);
                        new RemoteFileTransferManager(applicationContext, FileType.FILE_TYPE_LOGS, logsFolder, 15000, new FileTransferListener() {
                            public void onFileTransferResponse(FileTransferResponse response) {
                                if (response.success.booleanValue()) {
                                    SubmitTicketFragment.this.mDisplayLogFile = logsFolder + File.separator + response.destinationFileName;
                                    return;
                                }
                                SubmitTicketFragment.this.removeDialog();
                                SubmitTicketFragment.this.showSimpleDialog(1, SubmitTicketFragment.this.getString(R.string.error), "Error collecting display logs");
                            }

                            public void onFileTransferStatus(FileTransferStatus status) {
                                if (status.success.booleanValue() && status.transferComplete.booleanValue()) {
                                    SubmitTicketFragment.this.removeDialog();
                                    SubmitTicketFragment.this.handler.post(new Runnable() {
                                        public void run() {
                                            Toast.makeText(SubmitTicketFragment.this.getActivity(), "Logs collected", 1).show();
                                        }
                                    });
                                    SubmitTicketFragment.this.showSimpleDialog(3, "", "Submitting ticket");
                                    SubmitTicketFragment.this.bSubmitTicket();
                                }
                            }

                            public void onError(FileTransferError errorCode, String error) {
                                SubmitTicketFragment.this.removeDialog();
                                SubmitTicketFragment.this.showSimpleDialog(1, SubmitTicketFragment.this.getString(R.string.error), "Error collecting display logs " + error);
                            }
                        }).pullFile();
                    } catch (Throwable th) {
                        SubmitTicketFragment.this.showSimpleDialog(1, SubmitTicketFragment.this.getString(R.string.error), "Error collecting device logs");
                    }
                }
            }, 1);
        } else {
            showSimpleDialog(1, getString(R.string.error), getString(R.string.no_connectivity));
        }
    }

    public Dialog createDialog(int id, Bundle arguments) {
        Context context = getActivity();
        if (context == null) {
            return null;
        }
        switch (id) {
            case 2:
                Dialog dialog = (AlertDialog) super.createDialog(id, arguments);
                dialog.setButton(-1, "Ok", new OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        SubmitTicketFragment.this.getFragmentManager().popBackStack();
                    }
                });
                return dialog;
            case 3:
                Dialog progressDialog = new ProgressDialog(context);
                String title = arguments.getString("title");
                if (title != null) {
                    progressDialog.setTitle(title);
                }
                String message = arguments.getString("message");
                if (message != null) {
                    progressDialog.setMessage(message);
                }
                progressDialog.setCancelable(false);
                return progressDialog;
            default:
                return super.createDialog(id, arguments);
        }
    }

    private void bSubmitTicket() {
        final Context appContext = NavdyApplication.getAppContext();
        if (appContext != null) {
            String summary = this.mTitle.getText().toString();
            String description = this.mDescription.getText().toString();
            StringBuilder descriptionBuilder = new StringBuilder();
            descriptionBuilder.append("Device : ").append(Build.MODEL).append(" : ").append(VERSION.RELEASE).append(" (API ").append(VERSION.SDK_INT).append(")\n\n");
            SharedPreferences preferences = OTAUpdateService.getSharedPreferences();
            String lastConnectedDeviceId = preferences.getString(OTAUpdateService.LAST_CONNECTED_DEVICE_ID, null);
            if (lastConnectedDeviceId != null) {
                descriptionBuilder.append("Display : ").append(preferences.getString(lastConnectedDeviceId + "_" + OTAUpdateService.SW_VERSION_NAME, null)).append(" , ID :").append(lastConnectedDeviceId).append("\n\n");
            }
            SharedPreferences customerPrefs = SettingsUtils.getCustomerPreferences();
            descriptionBuilder.append("fullName =    ").append(customerPrefs.getString(ProfilePreferences.FULL_NAME, "")).append("\n");
            descriptionBuilder.append("email =       ").append(customerPrefs.getString("email", "")).append("\n");
            descriptionBuilder.append("carYear =     ").append(customerPrefs.getString(ProfilePreferences.CAR_YEAR, "")).append("\n");
            descriptionBuilder.append("carMake =     ").append(customerPrefs.getString(ProfilePreferences.CAR_MAKE, "")).append("\n");
            descriptionBuilder.append("carModel =    ").append(customerPrefs.getString(ProfilePreferences.CAR_MODEL, "")).append("\n\n");
            descriptionBuilder.append(description);
            try {
                this.mJiraClient.submitTicket(this.spinner.getSelectedItem().toString(), "Bug", summary, description, new ResultCallback() {
                    public void onSuccess(Object object) {
                        Attachment attachment;
                        final String key = (String) object;
                        SubmitTicketFragment.this.showSimpleDialog(3, "Uploading", "Uploading files");
                        final ArrayList<Attachment> attachments = new ArrayList();
                        if (!StringUtils.isEmptyAfterTrim(SubmitTicketFragment.this.mDeviceLogFile)) {
                            attachment = new Attachment();
                            attachment.filePath = SubmitTicketFragment.this.mDeviceLogFile;
                            attachment.mimeType = "text/plain";
                            attachments.add(attachment);
                        }
                        if (!StringUtils.isEmptyAfterTrim(SubmitTicketFragment.this.mDisplayLogFile)) {
                            attachment = new Attachment();
                            attachment.filePath = SubmitTicketFragment.this.mDisplayLogFile;
                            attachment.mimeType = HttpUtils.MIME_TYPE_ZIP;
                            attachments.add(attachment);
                        }
                        if (attachments == null || attachments.size() <= 0) {
                            SubmitTicketFragment.this.showSimpleDialog(2, SubmitTicketFragment.this.getString(R.string.success), "Issue created " + key);
                        } else {
                            SubmitTicketFragment.this.mJiraClient.attachFilesToTicket(key, attachments, new ResultCallback() {
                                public void onSuccess(Object object) {
                                    SubmitTicketFragment.this.showSimpleDialog(2, SubmitTicketFragment.this.getString(R.string.success), "Issue created and log files are uploaded :" + key);
                                    Iterator it = attachments.iterator();
                                    while (it.hasNext()) {
                                        IOUtils.deleteFile(appContext, ((Attachment) it.next()).filePath);
                                    }
                                }

                                public void onError(Throwable t) {
                                    SubmitTicketFragment.this.showSimpleDialog(2, SubmitTicketFragment.this.getString(R.string.success), "Issue created " + key);
                                    Iterator it = attachments.iterator();
                                    while (it.hasNext()) {
                                        IOUtils.deleteFile(appContext, ((Attachment) it.next()).filePath);
                                    }
                                }
                            });
                        }
                    }

                    public void onError(Throwable t) {
                        SubmitTicketFragment.this.showSimpleDialog(1, SubmitTicketFragment.this.getString(R.string.error), SubmitTicketFragment.this.getString(R.string.error_creating_ticket));
                    }
                });
            } catch (IOException e) {
                showSimpleDialog(1, getString(R.string.error), getString(R.string.error_creating_ticket));
            }
        }
    }
}
