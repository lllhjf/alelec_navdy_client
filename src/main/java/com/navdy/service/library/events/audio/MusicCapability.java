package com.navdy.service.library.events.audio;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoEnum;
import com.squareup.wire.ProtoField;
import java.util.Collections;
import java.util.List;

public final class MusicCapability extends Message {
    public static final MusicAuthorizationStatus DEFAULT_AUTHORIZATIONSTATUS = MusicAuthorizationStatus.MUSIC_AUTHORIZATION_NOT_DETERMINED;
    public static final MusicCollectionSource DEFAULT_COLLECTIONSOURCE = MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN;
    public static final List<MusicCollectionType> DEFAULT_COLLECTIONTYPES = Collections.emptyList();
    public static final Long DEFAULT_SERIAL_NUMBER = Long.valueOf(0);
    public static final List<MusicRepeatMode> DEFAULT_SUPPORTEDREPEATMODES = Collections.emptyList();
    public static final List<MusicShuffleMode> DEFAULT_SUPPORTEDSHUFFLEMODES = Collections.emptyList();
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 3, type = Datatype.ENUM)
    public final MusicAuthorizationStatus authorizationStatus;
    @ProtoField(tag = 1, type = Datatype.ENUM)
    public final MusicCollectionSource collectionSource;
    @ProtoField(enumType = MusicCollectionType.class, label = Label.REPEATED, tag = 2, type = Datatype.ENUM)
    public final List<MusicCollectionType> collectionTypes;
    @ProtoField(tag = 6, type = Datatype.INT64)
    public final Long serial_number;
    @ProtoField(enumType = MusicRepeatMode.class, label = Label.REPEATED, tag = 5, type = Datatype.ENUM)
    public final List<MusicRepeatMode> supportedRepeatModes;
    @ProtoField(enumType = MusicShuffleMode.class, label = Label.REPEATED, tag = 4, type = Datatype.ENUM)
    public final List<MusicShuffleMode> supportedShuffleModes;

    public static final class Builder extends com.squareup.wire.Message.Builder<MusicCapability> {
        public MusicAuthorizationStatus authorizationStatus;
        public MusicCollectionSource collectionSource;
        public List<MusicCollectionType> collectionTypes;
        public Long serial_number;
        public List<MusicRepeatMode> supportedRepeatModes;
        public List<MusicShuffleMode> supportedShuffleModes;

        public Builder() {}

        public Builder(MusicCapability message) {
            super(message);
            if (message != null) {
                this.collectionSource = message.collectionSource;
                this.collectionTypes = Message.copyOf(message.collectionTypes);
                this.authorizationStatus = message.authorizationStatus;
                this.supportedShuffleModes = Message.copyOf(message.supportedShuffleModes);
                this.supportedRepeatModes = Message.copyOf(message.supportedRepeatModes);
                this.serial_number = message.serial_number;
            }
        }

        public Builder collectionSource(MusicCollectionSource collectionSource) {
            this.collectionSource = collectionSource;
            return this;
        }

        public Builder collectionTypes(List<MusicCollectionType> collectionTypes) {
            this.collectionTypes = com.squareup.wire.Message.Builder.checkForNulls(collectionTypes);
            return this;
        }

        public Builder authorizationStatus(MusicAuthorizationStatus authorizationStatus) {
            this.authorizationStatus = authorizationStatus;
            return this;
        }

        public Builder supportedShuffleModes(List<MusicShuffleMode> supportedShuffleModes) {
            this.supportedShuffleModes = com.squareup.wire.Message.Builder.checkForNulls(supportedShuffleModes);
            return this;
        }

        public Builder supportedRepeatModes(List<MusicRepeatMode> supportedRepeatModes) {
            this.supportedRepeatModes = com.squareup.wire.Message.Builder.checkForNulls(supportedRepeatModes);
            return this;
        }

        public Builder serial_number(Long serial_number) {
            this.serial_number = serial_number;
            return this;
        }

        public MusicCapability build() {
            return new MusicCapability(this);
        }
    }

    public enum MusicAuthorizationStatus implements ProtoEnum {
        MUSIC_AUTHORIZATION_NOT_DETERMINED(1),
        MUSIC_AUTHORIZATION_AUTHORIZED(2),
        MUSIC_AUTHORIZATION_DENIED(3);
        
        private final int value;

        private MusicAuthorizationStatus(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    public MusicCapability(MusicCollectionSource collectionSource, List<MusicCollectionType> collectionTypes, MusicAuthorizationStatus authorizationStatus, List<MusicShuffleMode> supportedShuffleModes, List<MusicRepeatMode> supportedRepeatModes, Long serial_number) {
        this.collectionSource = collectionSource;
        this.collectionTypes = Message.immutableCopyOf(collectionTypes);
        this.authorizationStatus = authorizationStatus;
        this.supportedShuffleModes = Message.immutableCopyOf(supportedShuffleModes);
        this.supportedRepeatModes = Message.immutableCopyOf(supportedRepeatModes);
        this.serial_number = serial_number;
    }

    private MusicCapability(Builder builder) {
        this(builder.collectionSource, builder.collectionTypes, builder.authorizationStatus, builder.supportedShuffleModes, builder.supportedRepeatModes, builder.serial_number);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof MusicCapability)) {
            return false;
        }
        MusicCapability o = (MusicCapability) other;
        if (equals((Object) this.collectionSource, (Object) o.collectionSource) && equals(this.collectionTypes, o.collectionTypes) && equals((Object) this.authorizationStatus, (Object) o.authorizationStatus) && equals(this.supportedShuffleModes, o.supportedShuffleModes) && equals(this.supportedRepeatModes, o.supportedRepeatModes) && equals((Object) this.serial_number, (Object) o.serial_number)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 1;
        int i2 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.collectionSource != null ? this.collectionSource.hashCode() : 0) * 37;
        if (this.collectionTypes != null) {
            hashCode = this.collectionTypes.hashCode();
        } else {
            hashCode = 1;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.authorizationStatus != null) {
            hashCode = this.authorizationStatus.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.supportedShuffleModes != null) {
            hashCode = this.supportedShuffleModes.hashCode();
        } else {
            hashCode = 1;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.supportedRepeatModes != null) {
            i = this.supportedRepeatModes.hashCode();
        }
        hashCode = (hashCode + i) * 37;
        if (this.serial_number != null) {
            i2 = this.serial_number.hashCode();
        }
        result = hashCode + i2;
        this.hashCode = result;
        return result;
    }
}
