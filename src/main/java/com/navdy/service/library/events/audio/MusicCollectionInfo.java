package com.navdy.service.library.events.audio;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.ProtoField;

public final class MusicCollectionInfo extends Message {
    public static final Boolean DEFAULT_CANSHUFFLE = Boolean.valueOf(false);
    public static final String DEFAULT_COLLECTIONID = "";
    public static final MusicCollectionSource DEFAULT_COLLECTIONSOURCE = MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN;
    public static final MusicCollectionType DEFAULT_COLLECTIONTYPE = MusicCollectionType.COLLECTION_TYPE_UNKNOWN;
    public static final Integer DEFAULT_INDEX = Integer.valueOf(0);
    public static final String DEFAULT_NAME = "";
    public static final Integer DEFAULT_SIZE = Integer.valueOf(0);
    public static final String DEFAULT_SUBTITLE = "";
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 8, type = Datatype.BOOL)
    public final Boolean canShuffle;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String collectionId;
    @ProtoField(tag = 1, type = Datatype.ENUM)
    public final MusicCollectionSource collectionSource;
    @ProtoField(tag = 2, type = Datatype.ENUM)
    public final MusicCollectionType collectionType;
    @ProtoField(tag = 6, type = Datatype.INT32)
    public final Integer index;
    @ProtoField(tag = 4, type = Datatype.STRING)
    public final String name;
    @ProtoField(tag = 5, type = Datatype.INT32)
    public final Integer size;
    @ProtoField(tag = 7, type = Datatype.STRING)
    public final String subtitle;

    public static final class Builder extends com.squareup.wire.Message.Builder<MusicCollectionInfo> {
        public Boolean canShuffle;
        public String collectionId;
        public MusicCollectionSource collectionSource;
        public MusicCollectionType collectionType;
        public Integer index;
        public String name;
        public Integer size;
        public String subtitle;

        public Builder() {}

        public Builder(MusicCollectionInfo message) {
            super(message);
            if (message != null) {
                this.collectionSource = message.collectionSource;
                this.collectionType = message.collectionType;
                this.collectionId = message.collectionId;
                this.name = message.name;
                this.size = message.size;
                this.index = message.index;
                this.subtitle = message.subtitle;
                this.canShuffle = message.canShuffle;
            }
        }

        public Builder collectionSource(MusicCollectionSource collectionSource) {
            this.collectionSource = collectionSource;
            return this;
        }

        public Builder collectionType(MusicCollectionType collectionType) {
            this.collectionType = collectionType;
            return this;
        }

        public Builder collectionId(String collectionId) {
            this.collectionId = collectionId;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder size(Integer size) {
            this.size = size;
            return this;
        }

        public Builder index(Integer index) {
            this.index = index;
            return this;
        }

        public Builder subtitle(String subtitle) {
            this.subtitle = subtitle;
            return this;
        }

        public Builder canShuffle(Boolean canShuffle) {
            this.canShuffle = canShuffle;
            return this;
        }

        public MusicCollectionInfo build() {
            return new MusicCollectionInfo(this);
        }
    }

    public MusicCollectionInfo(MusicCollectionSource collectionSource, MusicCollectionType collectionType, String collectionId, String name, Integer size, Integer index, String subtitle, Boolean canShuffle) {
        this.collectionSource = collectionSource;
        this.collectionType = collectionType;
        this.collectionId = collectionId;
        this.name = name;
        this.size = size;
        this.index = index;
        this.subtitle = subtitle;
        this.canShuffle = canShuffle;
    }

    private MusicCollectionInfo(Builder builder) {
        this(builder.collectionSource, builder.collectionType, builder.collectionId, builder.name, builder.size, builder.index, builder.subtitle, builder.canShuffle);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof MusicCollectionInfo)) {
            return false;
        }
        MusicCollectionInfo o = (MusicCollectionInfo) other;
        if (equals((Object) this.collectionSource, (Object) o.collectionSource) && equals((Object) this.collectionType, (Object) o.collectionType) && equals((Object) this.collectionId, (Object) o.collectionId) && equals((Object) this.name, (Object) o.name) && equals((Object) this.size, (Object) o.size) && equals((Object) this.index, (Object) o.index) && equals((Object) this.subtitle, (Object) o.subtitle) && equals((Object) this.canShuffle, (Object) o.canShuffle)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.collectionSource != null ? this.collectionSource.hashCode() : 0) * 37;
        if (this.collectionType != null) {
            hashCode = this.collectionType.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.collectionId != null) {
            hashCode = this.collectionId.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.name != null) {
            hashCode = this.name.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.size != null) {
            hashCode = this.size.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.index != null) {
            hashCode = this.index.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.subtitle != null) {
            hashCode = this.subtitle.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.canShuffle != null) {
            i = this.canShuffle.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
