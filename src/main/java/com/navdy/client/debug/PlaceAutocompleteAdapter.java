package com.navdy.client.debug;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Toast;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLngBounds;
import com.navdy.service.library.log.Logger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

public class PlaceAutocompleteAdapter extends ArrayAdapter<PlaceDebugAutocomplete> implements Filterable {
    private static final Logger sLogger = new Logger(PlaceAutocompleteAdapter.class);
    private LatLngBounds mBounds;
    private GoogleApiClient mGoogleApiClient;
    private AutocompleteFilter mPlaceFilter;
    private ArrayList<PlaceDebugAutocomplete> mResultList;

    public PlaceAutocompleteAdapter(Context context, int resource, LatLngBounds bounds, AutocompleteFilter filter) {
        super(context, resource);
        this.mBounds = bounds;
        this.mPlaceFilter = filter;
    }

    public void setGoogleApiClient(GoogleApiClient googleApiClient) {
        if (googleApiClient == null || !googleApiClient.isConnected()) {
            this.mGoogleApiClient = null;
        } else {
            this.mGoogleApiClient = googleApiClient;
        }
    }

    public void setBounds(LatLngBounds bounds) {
        this.mBounds = bounds;
    }

    public int getCount() {
        return this.mResultList == null ? 0 : this.mResultList.size();
    }

    public PlaceDebugAutocomplete getItem(int position) {
        return (PlaceDebugAutocomplete) this.mResultList.get(position);
    }

    @NonNull
    public Filter getFilter() {
        return new Filter() {
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                if (constraint != null) {
                    PlaceAutocompleteAdapter.this.mResultList = PlaceAutocompleteAdapter.this.getAutocomplete(constraint);
                    if (PlaceAutocompleteAdapter.this.mResultList != null) {
                        results.values = PlaceAutocompleteAdapter.this.mResultList;
                        results.count = PlaceAutocompleteAdapter.this.mResultList.size();
                    }
                }
                return results;
            }

            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results == null || results.count <= 0) {
                    PlaceAutocompleteAdapter.this.notifyDataSetInvalidated();
                } else {
                    PlaceAutocompleteAdapter.this.notifyDataSetChanged();
                }
            }
        };
    }

    private ArrayList<PlaceDebugAutocomplete> getAutocomplete(CharSequence constraint) {
        if (this.mGoogleApiClient != null) {
            sLogger.i("Starting autocomplete query for: " + constraint);
            AutocompletePredictionBuffer autocompletePredictions = Places.GeoDataApi.getAutocompletePredictions(this.mGoogleApiClient, constraint.toString(), this.mBounds, this.mPlaceFilter).await(60, TimeUnit.SECONDS);
            Status status = autocompletePredictions.getStatus();
            if (status.isSuccess()) {
                sLogger.i("Query completed. Received " + autocompletePredictions.getCount() + " predictions.");
                Iterator<AutocompletePrediction> iterator = autocompletePredictions.iterator();
                ArrayList<PlaceDebugAutocomplete> resultList = new ArrayList<>(autocompletePredictions.getCount());
                while (iterator.hasNext()) {
                    AutocompletePrediction prediction = iterator.next();
                    resultList.add(new PlaceDebugAutocomplete(prediction.getPlaceId(), prediction.getFullText(null)));
                }
                autocompletePredictions.release();
                return resultList;
            }
            Toast.makeText(getContext(), "Error contacting API: " + status.toString(), Toast.LENGTH_SHORT).show();
            sLogger.e("Error getting autocomplete prediction API call: " + status.toString());
            autocompletePredictions.release();
            return null;
        }
        sLogger.e("Google API client is not connected for autocomplete query.");
        return null;
    }
}
