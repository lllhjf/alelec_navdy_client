package com.navdy.client.debug;

import android.app.Fragment;
import android.app.ListFragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.servicehandler.RecordingServiceManager;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.client.app.framework.util.SupportTicketService;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.client.debug.navdebug.NavAddressPickerFragment;
import com.navdy.client.debug.util.FragmentHelper;
import com.navdy.client.debug.view.OTAS3BrowserFragment;
import com.navdy.client.debug.view.VideoS3BrowserFragment;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.events.audio.AudioStatus;
import com.navdy.service.library.events.connection.DisconnectRequest;
import com.navdy.service.library.events.navigation.NavigationManeuverEvent;
import com.navdy.service.library.events.navigation.NavigationTurn;
import com.navdy.service.library.events.notification.NotificationCategory;
import com.navdy.service.library.events.notification.NotificationEvent;
import com.navdy.service.library.events.preferences.DriverProfilePreferences.FeatureMode;
import com.navdy.service.library.events.ui.DismissScreen;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.events.ui.ShowScreen.Builder;
import com.navdy.service.library.log.Logger;
import com.squareup.wire.Message;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import org.droidparts.contract.SQL.DDL;

public class DebugActionsFragment extends ListFragment {
    static final String[] RANDOM_STREETS = new String[]{"Divisadero St.", "Embarcadero St.", "MacArthur Blvd", "El Camino Real", "Cesar Chavez St.", "287 east alviso / milpitas"};
    private static final long RETRY_INTERVAL_FOR_SUPPORT_TICKETS = TimeUnit.MINUTES.toMillis(1);
    static final Screen[] SCREENS = Screen.values();
    private static final int SIM_SPEED_CHANGE_STEP = 1;
    private static final Logger sLogger = new Logger("TestActivity");
    static final SparseIntArray screenMap = new SparseIntArray();
    private static int simulationSpeed = 18;
    protected AppInstance mAppInstance;
    protected ConnectionStatusUpdater mConnectionStatus;
    protected int mLastDirectionIdx = 0;
    protected ArrayList<ListItem> mListItems;
    @InjectView(R.id.test_textview_current_sim_speed)
    TextView mTextViewCurrentSimSpeed;

    private enum ListItem {
        DISCONNECT("Disconnect"),
        SCREEN_TESTING("Screen testing"),
        SEND_NAVIGATION_EVENT("Send navigation event"),
        BROWSE_VIDEOS("Browse Videos"),
        DUMMY_EVENT0(" "),
        HERE_NAVIGATION("Here Nav Test"),
        SEND_GOOGLE_NOW_EVENT("Send google now notification event"),
        DUMMY_EVENT1(" "),
        INCREASE_SIM_SPEED("Increase simulation speed +1"),
        DECREASE_SIM_SPEED("Decrease simulation speed -1"),
        MOCK_CURRENT_POSITION("Mock current GPS position"),
        RESET_CURRENT_POSITION("Reset current GPS position"),
        NOTIFICATION_TESTING("Notification Testing"),
        SHOW_OPTIONS_VIEW("Show Options View"),
        HIDE_OPTIONS_VIEW("Hide Options View"),
        SHOW_CONTEXT_MENU("Show Context Menu"),
        HIDE_CONTEXT_MENU("Hide Context Menu"),
        OTA_SEND_LARGE_FILE("Send Large File (OTA test)"),
        DRIVE_PLAYBACK("Drive Playback"),
        STOP_DRIVE_PLAYBACK("Stop Drive Playback"),
        FILE_TRANSFER_PERF_TEST("File Transfer Performance Test"),
        ENABLE_EXPERIMENTAL_FEATURE_MODE("Enable Experimental Feature Mode"),
        SEND_HEAD_UNIT_CONNECT("Send head unit connection event"),
        SEND_HEAD_UNIT_DISCONNECT("Send head unit disconnect event"),
        SET_RETRY_TIME_FOR_SUPPORT_TICKETS("Reschedule Support Ticket Alarm To 1 Min"),
        ENABLE_ZENDESK_LOGGING("Enables Zendesk SDK Logger"),
        NAV_CORD_TEST_SUITE("Navigation Coord Test Suite");
        
        private final String mText;

        private ListItem(String item) {
            this.mText = item;
        }

        public String toString() {
            return this.mText;
        }
    }

    static {
        screenMap.put(R.id.dash_screen, Screen.SCREEN_DASHBOARD.ordinal());
        screenMap.put(R.id.hybrid_map_screen, Screen.SCREEN_HYBRID_MAP.ordinal());
        screenMap.put(R.id.context_menu_screen, Screen.SCREEN_CONTEXT_MENU.ordinal());
        screenMap.put(R.id.menu_screen, Screen.SCREEN_MENU.ordinal());
    }

    public void onCreate(Bundle savedState) {
        super.onCreate(savedState);
        this.mAppInstance = AppInstance.getInstance();
        this.mListItems = new ArrayList<>(Arrays.asList(ListItem.values()));
        setListAdapter(new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_activated_1, android.R.id.text1, ListItem.values()));
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_debug_actions, container, false);
        ButterKnife.inject((Object) this, rootView);
        this.mConnectionStatus = new ConnectionStatusUpdater(rootView);
        return rootView;
    }

    public void onPause() {
        this.mConnectionStatus.onPause();
        super.onPause();
    }

    public void onResume() {
        super.onResume();
        getListView().setChoiceMode(2);
        this.mConnectionStatus.onResume();
        refreshUI();
    }

    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        l.setItemChecked(position, false);
        switch (this.mListItems.get(position)) {
            case DISCONNECT:
                sendEvent(new DisconnectRequest(Boolean.TRUE));
                return;
            case SEND_NAVIGATION_EVENT:
                sendNavigationEvent();
                return;
            case HERE_NAVIGATION:
                onHereNavigationClick();
                return;
            case SCREEN_TESTING:
                onScreenTestingClick();
                return;
            case BROWSE_VIDEOS:
                onBrowseVideosClicked();
                return;
            case INCREASE_SIM_SPEED:
                onSimSpeedChangeClick(1);
                return;
            case DECREASE_SIM_SPEED:
                onSimSpeedChangeClick(-1);
                return;
            case SEND_GOOGLE_NOW_EVENT:
                sendGoogleNowEvent();
                return;
            case NOTIFICATION_TESTING:
                onCustomNotificationClick();
                return;
            case SHOW_OPTIONS_VIEW:
                onShowOpionsView();
                return;
            case HIDE_OPTIONS_VIEW:
                onHideOptionsView();
                return;
            case SHOW_CONTEXT_MENU:
                onShowContextMenu();
                return;
            case HIDE_CONTEXT_MENU:
                onHideContextMenu();
                return;
            case OTA_SEND_LARGE_FILE:
                onOTASendLargeFile();
                return;
            case MOCK_CURRENT_POSITION:
                FragmentHelper.pushFullScreenFragment(getFragmentManager(), MockedAddressPickerFragment.class);
                return;
            case RESET_CURRENT_POSITION:
                return;
            case DRIVE_PLAYBACK:
                onShowDrivePlayback();
                return;
            case STOP_DRIVE_PLAYBACK:
                onStopDrivePlayback();
                return;
            case FILE_TRANSFER_PERF_TEST:
                onFileTransferTest();
                return;
            case ENABLE_EXPERIMENTAL_FEATURE_MODE:
                SettingsUtils.setFeatureMode(FeatureMode.FEATURE_MODE_EXPERIMENTAL);
                return;
            case SEND_HEAD_UNIT_CONNECT:
                sendAudioStatusEvent(true);
                return;
            case SEND_HEAD_UNIT_DISCONNECT:
                sendAudioStatusEvent(false);
                return;
            case SET_RETRY_TIME_FOR_SUPPORT_TICKETS:
                SupportTicketService.scheduleAlarmForSubmittingPendingTickets(RETRY_INTERVAL_FOR_SUPPORT_TICKETS);
                Toast.makeText(NavdyApplication.getAppContext(), "Scheduling alarm to submit tickets in 1 minutes", Toast.LENGTH_SHORT).show();
                return;
            case ENABLE_ZENDESK_LOGGING:
                com.zendesk.logger.Logger.setLoggable(true);
                Toast.makeText(NavdyApplication.getAppContext(), "Zendesk SDK Logger enabled", Toast.LENGTH_SHORT).show();
                return;
            case NAV_CORD_TEST_SUITE:
                startActivity(new Intent(NavdyApplication.getAppContext(), NavCoordTestSuiteActivity.class));
                return;
            default:
                sLogger.d("unhandled: " + id);
                return;
        }
    }

    protected void sendNotificationEvent() {
        sendEvent(getRandomNotificationEvent());
    }

    protected void sendNavigationEvent() {
        sendEvent(getRandomNavigationEvent());
    }

    protected void sendGoogleNowEvent() {
        sendEvent(getRandomGoogleNowEvent());
    }

    public void onSendDismissClick() {
        sendEvent(new DismissScreen(Screen.SCREEN_NOTIFICATION));
    }

    public void onCustomNotificationClick() {
        sLogger.v("onCustomNotificationClick");
        pushFullScreenFragment(CustomNotificationFragment.class);
    }

    public void onScreenTestingClick() {
        sLogger.v("onScreenTestingClick");
        pushFullScreenFragment(ScreenTestingFragment.class);
    }

    public void onShowOpionsView() {
        sLogger.v("onShowOptionsView");
        sendEvent(new Builder().screen(Screen.SCREEN_OPTIONS).build());
    }

    public void onHideOptionsView() {
        sLogger.v("onHideOptionsView");
        sendEvent(new DismissScreen(Screen.SCREEN_OPTIONS));
    }

    public void onShowContextMenu() {
        sLogger.v("onShowContextMenu");
        sendEvent(new Builder().screen(Screen.SCREEN_CONTEXT_MENU).build());
    }

    public void onHideContextMenu() {
        sLogger.v("onHideContextMenu");
        sendEvent(new DismissScreen(Screen.SCREEN_CONTEXT_MENU));
    }

    public void sendAudioStatusEvent(boolean connected) {
        sendEvent(new AudioStatus.Builder(null).isConnected(connected).build());
    }

    private void onOTASendLargeFile() {
        pushFullScreenFragment(OTAS3BrowserFragment.class);
    }

    public void onBrowseVideosClicked() {
        pushFullScreenFragment(VideoS3BrowserFragment.class);
    }

    public void onHereNavigationClick() {
        pushFullScreenFragment(NavAddressPickerFragment.class);
    }

    public void onSimSpeedChangeClick(int change) {
        changeSimulationSpeed(change);
        updateSimSpeed();
    }

    public void onShowDrivePlayback() {
        sLogger.v("onShowDrivePlayback");
        pushFullScreenFragment(DrivePlaybackFragment.class);
    }

    public void onStopDrivePlayback() {
        RecordingServiceManager.getInstance().sendStopDrivePlaybackEvent();
    }

    public void onFileTransferTest() {
        sLogger.v("onFileTransferTest");
        pushFullScreenFragment(FileTransferTestFragment.class);
    }

    protected void pushFullScreenFragment(Class<? extends Fragment> fragmentClass) {
        FragmentHelper.pushFullScreenFragment(getFragmentManager(), fragmentClass);
    }

    private Screen lookupScreen(int id) {
        return SCREENS[screenMap.get(id)];
    }

    @OnClick({R.id.dash_screen, R.id.menu_screen, R.id.context_menu_screen, R.id.hybrid_map_screen})
    public void onShowScreenClicked(View button) {
        sendEvent(new Builder().screen(lookupScreen(button.getId())).build());
    }

    protected void refreshUI() {
        updateSimSpeed();
    }

    private void updateSimSpeed() {
        this.mTextViewCurrentSimSpeed.setText("Simulation speed: " + getSimulationSpeed() + " m/s " + DDL.SEPARATOR + ((int) (((float) getSimulationSpeed()) * 2.2369f)) + " MPH");
    }

    protected NotificationEvent getRandomNotificationEvent() {
        String phoneNumber = "408-393-6782";
        return new NotificationEvent(1, NotificationCategory.CATEGORY_INCOMING_CALL, phoneNumber, null, "Are you on the way?", null, null, null, null, null, phoneNumber, Boolean.TRUE, null);
    }

    protected NotificationEvent getRandomGoogleNowEvent() {
        return new NotificationEvent(1, NotificationCategory.CATEGORY_OTHER, null, null, "AskNavdy", null, null, null, null, null, null, Boolean.TRUE, null);
    }

    private void sendEvent(Message event) {
        RemoteDevice remoteDevice = this.mAppInstance.getRemoteDevice();
        if (remoteDevice != null) {
            remoteDevice.postEvent(event);
        }
    }

    protected NavigationManeuverEvent getRandomNavigationEvent() {
        int randomStreetIndex = new Random().nextInt(RANDOM_STREETS.length);
        this.mLastDirectionIdx = (this.mLastDirectionIdx + 1) % NavigationTurn.values().length;
        return new NavigationManeuverEvent("current road", NavigationTurn.values()[this.mLastDirectionIdx], "1 mi", RANDOM_STREETS[randomStreetIndex], "1:10", "10 mph", null, 0L);
    }

    public static int getSimulationSpeed() {
        return simulationSpeed;
    }

    private static void changeSimulationSpeed(int diff) {
        simulationSpeed += diff;
        if (simulationSpeed <= 0) {
            sLogger.w("Trying to set simulation speed to value less than or equal to 0 (reset to 1): " + simulationSpeed);
            simulationSpeed = 1;
        }
    }
}
