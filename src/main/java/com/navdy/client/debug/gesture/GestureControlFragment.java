package com.navdy.client.debug.gesture;

import android.app.Fragment;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.events.Notification;
import com.navdy.service.library.events.input.DialSimulationEvent;
import com.navdy.service.library.events.input.DialSimulationEvent.DialInputAction;
import com.navdy.service.library.events.input.Gesture;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.events.ui.ShowScreen.Builder;
import com.navdy.service.library.log.Logger;
import com.squareup.wire.Message;
import java.util.HashMap;
import java.util.Map;

public class GestureControlFragment extends Fragment {
    private static final Map<Integer, DialInputAction> DIAL_BUTTONS = new HashMap<Integer, DialInputAction>() {
        {
            put(Integer.valueOf(R.id.dial_click), DialInputAction.DIAL_CLICK);
            put(Integer.valueOf(R.id.dial_long_click), DialInputAction.DIAL_LONG_CLICK);
            put(Integer.valueOf(R.id.dial_left), DialInputAction.DIAL_LEFT_TURN);
            put(Integer.valueOf(R.id.dial_right), DialInputAction.DIAL_RIGHT_TURN);
        }
    };
    private static final Logger sLogger = new Logger(GestureControlFragment.class);
    private OnKeyListener keyListener = new OnKeyListener() {
        public boolean onKey(View v, int keyCode, KeyEvent keyEvent) {
            Message message = null;
            switch (keyCode) {
                case 8:
                case 9:
                case 10:
                case 11:
                case 12:
                    message = new Notification(Integer.valueOf(-1), "", "Menu select", "", Integer.toString((keyCode - 8) + 1), "sms");
                    break;
                case 19:
                    message = new GestureEvent(Gesture.GESTURE_FINGER_UP, null, null);
                    break;
                case 20:
                case 66:
                    message = new GestureEvent(Gesture.GESTURE_FINGER_TO_PINCH, null, null);
                    break;
                case 21:
                    message = new GestureEvent(Gesture.GESTURE_SWIPE_LEFT, null, null);
                    break;
                case 22:
                    message = new GestureEvent(Gesture.GESTURE_SWIPE_RIGHT, null, null);
                    break;
                case 29:
                    message = new Notification(Integer.valueOf(0), "", "+1 415-867-5309", "", "ANSWER", "sms");
                    break;
                case 37:
                    message = new Notification(Integer.valueOf(0), "", "+1 800-555-1212", "", "IGNORE", "sms");
                    break;
                case 41:
                    message = new Builder().screen(Screen.SCREEN_MENU).build();
                    break;
                default:
                    GestureControlFragment.sLogger.e("Unknown keycode: " + keyCode);
                    break;
            }
            if (keyEvent.getAction() == 0 && message != null) {
                GestureControlFragment.this.sendEvent(message);
            }
            if (message != null) {
                return true;
            }
            return false;
        }
    };
    private Gesture lastGesture = Gesture.GESTURE_FINGER_UP;
    protected AppInstance mAppInstance;
    private OnSeekBarChangeListener seekBarChangeListener = new OnSeekBarChangeListener() {
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (GestureControlFragment.this.lastGesture == Gesture.GESTURE_FINGER_TO_PINCH) {
                GestureControlFragment.this.lastGesture = Gesture.GESTURE_PINCH;
            }
            if (GestureControlFragment.this.lastGesture == Gesture.GESTURE_FINGER_UP) {
                GestureControlFragment.this.lastGesture = Gesture.GESTURE_FINGER;
            }
            if (GestureControlFragment.this.lastGesture == Gesture.GESTURE_PINCH_TO_FINGER) {
                GestureControlFragment.this.lastGesture = Gesture.GESTURE_FINGER;
            }
            GestureControlFragment.this.sendEvent(new GestureEvent(GestureControlFragment.this.lastGesture, Integer.valueOf(progress), Integer.valueOf(0)));
        }

        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        public void onStopTrackingTouch(SeekBar seekBar) {
        }
    };
    @InjectView(R.id.x_slider)
    SeekBar slider;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mAppInstance = AppInstance.getInstance();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_gesture_control, container, false);
        ButterKnife.inject((Object) this, rootView);
        this.slider.setMax(600);
        this.slider.setProgress(300);
        this.slider.setOnSeekBarChangeListener(this.seekBarChangeListener);
        rootView.setOnKeyListener(this.keyListener);
        return rootView;
    }

    @OnClick({R.id.swipe_right, R.id.swipe_left})
    public void onSwipeClicked(View button) {
        sendEvent(new GestureEvent(button.getId() == R.id.swipe_left ? Gesture.GESTURE_SWIPE_LEFT : Gesture.GESTURE_SWIPE_RIGHT, Integer.valueOf(-1), Integer.valueOf(-1)));
    }

    @OnClick({R.id.fist, R.id.fist_with_index, R.id.fist_offscreen})
    public void onFistClicked(View button) {
        Gesture gesture;
        int i = 0;
        switch (button.getId()) {
            case R.id.fist_with_index /*2131755586*/:
                gesture = Gesture.GESTURE_FINGER_UP;
                break;
            case R.id.fist /*2131755587*/:
                Gesture[] gestures = new Gesture[]{Gesture.GESTURE_FINGER_TO_PINCH, Gesture.GESTURE_PINCH, Gesture.GESTURE_PINCH_TO_FINGER};
                int length = gestures.length;
                while (i < length) {
                    sendEvent(new GestureEvent(gestures[i], null, null));
                    i++;
                }
                gesture = Gesture.GESTURE_FINGER_TO_PINCH;
                break;
            case R.id.fist_offscreen /*2131755588*/:
                gesture = Gesture.GESTURE_FINGER_DOWN;
                break;
            default:
                throw new IllegalArgumentException("Unknown button id " + button.getId());
        }
        this.lastGesture = gesture;
        sendEvent(new GestureEvent(gesture, null, null));
    }

    @OnClick({R.id.dial_click, R.id.dial_long_click, R.id.dial_left, R.id.dial_right})
    public void onDialSimulationClicked(View button) {
        sendEvent(new DialSimulationEvent((DialInputAction) DIAL_BUTTONS.get(button.getId())));
    }

    private void sendEvent(Message event) {
        RemoteDevice remoteDevice = this.mAppInstance.getRemoteDevice();
        if (remoteDevice != null) {
            remoteDevice.postEvent(event);
        }
    }
}
