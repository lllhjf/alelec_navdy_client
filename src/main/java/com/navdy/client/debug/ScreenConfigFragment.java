package com.navdy.client.debug;

import android.app.Fragment;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.DeviceConnection;
import com.navdy.client.app.framework.DeviceConnection.DeviceConnectedEvent;
import com.navdy.client.app.framework.DeviceConnection.DeviceDisconnectedEvent;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.service.library.events.settings.ReadSettingsRequest;
import com.navdy.service.library.events.settings.ReadSettingsResponse;
import com.navdy.service.library.events.settings.ScreenConfiguration;
import com.navdy.service.library.events.settings.UpdateSettings;
import com.squareup.otto.Subscribe;
import com.squareup.wire.Message;

public class ScreenConfigFragment extends Fragment {
    private static int MAX_SCALE = 10;
    private static final int REPEAT_INTERVAL_MS = 250;
    private static final String TAG = "ScreenConfigFragment";
    private Rect margins;
    private View pressedButton;
    private Runnable repeatAction = new Runnable() {
        public void run() {
            ScreenConfigFragment.this.adjustMargins(ScreenConfigFragment.this.pressedButton, ScreenConfigFragment.this.scale);
            if (ScreenConfigFragment.this.scale < ScreenConfigFragment.MAX_SCALE) {
                ScreenConfigFragment.this.scale = ScreenConfigFragment.this.scale + 1;
            }
            ScreenConfigFragment.this.repeatHandler.postDelayed(this, 250);
        }
    };
    private Handler repeatHandler;
    private int scale = 1;
    private float scaleX;
    private float scaleY;
    private ScreenConfiguration screenConfiguration;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.repeatHandler = new Handler();
    }

    public void onPause() {
        BusProvider.getInstance().unregister(this);
        super.onPause();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_screen_config, container, false);
        ButterKnife.inject((Object) this, rootView);
        return rootView;
    }

    public void onResume() {
        super.onResume();
        BusProvider.getInstance().register(this);
        if (this.screenConfiguration == null) {
            sendEvent(new ReadSettingsRequest(null));
        }
    }

    private void adjustMargins(int leftOffset, int topOffset, int rightOffset, int bottomOffset, int scale) {
        if (this.margins != null) {
            this.margins.set(Math.max(this.margins.left + (leftOffset * scale), 0), this.margins.top + (topOffset * scale), Math.max(this.margins.right + (rightOffset * scale), 0), Math.max(this.margins.bottom + (bottomOffset * scale), 0));
            updateSettings();
        }
    }

    private void adjustMargins(View button, int scale) {
        if (button != null) {
            switch (button.getId()) {
                case R.id.slide_down /*2131755647*/:
                    adjustMargins(0, 1, 0, -1, scale);
                    return;
                default:
                    adjustMargins(0, -1, 0, 1, scale);
                    return;
            }
        }
    }

    @OnTouch({2131755647, 2131755648})
    boolean onTouchButton(View v, MotionEvent event) {
        int action = event.getAction();
        switch (action) {
            case 0:
                this.pressedButton = v;
                this.scale = 1;
                this.repeatHandler.post(this.repeatAction);
                break;
            case 1:
            case 3:
                this.repeatHandler.removeCallbacks(this.repeatAction);
                break;
            default:
                Log.d(TAG, "onTouchButton: unknown action: " + action);
                break;
        }
        return false;
    }

    @OnClick({R.id.reset_scale})
    public void onResetScale() {
        this.margins = new Rect(0, 0, 0, 0);
        this.scaleY = 1.0f;
        this.scaleX = 1.0f;
        updateSettings();
    }

    private void sendEvent(Message event) {
        DeviceConnection.postEvent(event);
    }

    private void updateSettings() {
        if (this.margins != null) {
            sendEvent(new UpdateSettings(new ScreenConfiguration(Integer.valueOf(this.margins.left), Integer.valueOf(this.margins.top), Integer.valueOf(this.margins.right), Integer.valueOf(this.margins.bottom), Float.valueOf(this.scaleX), Float.valueOf(this.scaleY)), null));
        }
    }

    @Subscribe
    public void onDeviceConnectedEvent(DeviceConnectedEvent event) {
        sendEvent(new ReadSettingsRequest(null));
    }

    @Subscribe
    public void onDeviceDisconnectedEvent(DeviceDisconnectedEvent event) {
        this.screenConfiguration = null;
    }

    @Subscribe
    public void onReadSettingsResponse(ReadSettingsResponse response) {
        this.screenConfiguration = response.screenConfiguration;
        this.margins = new Rect(this.screenConfiguration.marginLeft.intValue(), this.screenConfiguration.marginTop.intValue(), this.screenConfiguration.marginRight.intValue(), this.screenConfiguration.marginBottom.intValue());
        this.scaleX = this.screenConfiguration.scaleX.floatValue();
        this.scaleY = this.screenConfiguration.scaleY.floatValue();
    }
}
