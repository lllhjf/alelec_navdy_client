package com.navdy.client.app.framework.util;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.PathManager;
import com.navdy.client.app.framework.service.NetworkConnectivityJobService;
import com.navdy.client.app.framework.util.SubmitTicketWorker.OnFinishSubmittingTickets;
import com.navdy.client.app.providers.NavdyContentProviderConstants;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.client.app.ui.settings.ZendeskConstants;
import com.navdy.client.debug.file.FileTransferManager.FileTransferListener;
import com.navdy.client.debug.file.RemoteFileTransferManager;
import com.navdy.service.library.events.file.FileTransferError;
import com.navdy.service.library.events.file.FileTransferResponse;
import com.navdy.service.library.events.file.FileTransferStatus;
import com.navdy.service.library.events.file.FileType;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import com.navdy.service.library.util.SystemUtils;
import com.zendesk.sdk.model.access.AnonymousIdentity.Builder;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import org.json.JSONObject;

public class SupportTicketService extends IntentService {
    public static final String EXTRA_CREATE_INDETERMINATE_NOTIFICATION = "create_indeterminate_notification";
    public static final String EXTRA_EMAIL = "extra_email";
    public static final String EXTRA_RESET_COUNTER = "reset_retry_counter";
    public static final String EXTRA_TICKET_RANDOM_ID = "random_id";
    private static final String INTENT_SERVICE_NAME = "SUBMIT_TICKET";
    private static final long MAX_FILE_SIZE = 2097152;
    public static final String NEW_FOLDER_FORMAT = "yyyy-MM-dd'_'HH:mm:ss.SSS";
    private static final long PULL_LOGS_TIMEOUT = 15000;
    private static final long RETRY_INTERVAL_MILLIS = TimeUnit.MINUTES.toMillis(45);
    private static final int RETRY_REQUEST_CODE = 1;
    private static final String SHARED_PREF_TICKET_LAST_RETRY_DATE = "ticket_last_retry_date";
    private static final String SHARED_PREF_TICKET_RETRY_COUNT = "ticket_retry_count";
    private static final int SHARED_PREF_TICKET_RETRY_COUNT_DEFAULT = 0;
    private static final long SHARED_PREF_TICKET_RETRY_DATE_DEFAULT = 0;
    private static final int SUBMIT_AFTER_INIT_DELAY = 10000;
    private static final String SUPPORT_TICKET_SHARED_PREFS = "support_ticket_shared_prefs";
    private static final String TEMP_FILE_TIMESTAMP_FORMAT = "yyyy_dd_MM-hh_mm_ss_aa";
    public static final String TICKET_ATTACHMENTS_FILENAME = "attachments.zip";
    public static final String TICKET_FILTER_FOR_READY_FOLDERS = "_READY";
    public static final String TICKET_FOLDER_README_FILENAME = "readme.txt";
    private static final int TIMER_REQUEST_CODE = 0;
    private static final long TRY_MAX_COUNT = 4;
    public static final Boolean VERBOSE = Boolean.valueOf(true);
    private static String email;
    public static final FilenameFilter filterOnReadyFolders = new FilenameFilter() {
        public boolean accept(File file, String string) {
            return string.contains(SupportTicketService.TICKET_FILTER_FOR_READY_FOLDERS);
        }
    };
    private static final SimpleDateFormat format = new SimpleDateFormat(TEMP_FILE_TIMESTAMP_FORMAT, Locale.US);
    public static final Logger logger = new Logger(SupportTicketService.class);
    private String lastTicketId = "";
    private SubmitTicketWorker submitTicketWorker;

    public interface OnLogCollectedInterface {
        void onLogCollectionFailed();

        void onLogCollectionSucceeded(ArrayList<File> arrayList);
    }

    public static class SubmitTicketFinishedEvent {
        private boolean conditionsMet;
        private String ticketId = "";

        SubmitTicketFinishedEvent(boolean conditionsMet, @NonNull String ticketId) {
            this.conditionsMet = conditionsMet;
            this.ticketId = ticketId;
        }

        public boolean getConditionsMet() {
            return this.conditionsMet;
        }

        @NonNull
        public String getTicketId() {
            return this.ticketId;
        }
    }

    public static void writeJsonObjectToFile(JSONObject jsonObject, String directory, Logger logger) {
        Exception e;
        Throwable th;
        FileWriter fileWriter = null;
        try {
            FileWriter fileWriter2 = new FileWriter(directory + File.separator + ZendeskConstants.TICKET_JSON_FILENAME);
            try {
                fileWriter2.write(jsonObject.toString());
                fileWriter2.flush();
                IOUtils.closeStream(fileWriter2);
                fileWriter = fileWriter2;
            } catch (Exception e2) {
                e = e2;
                fileWriter = fileWriter2;
                try {
                    logger.e("Failed to write JSON to file: " + e.toString());
                    IOUtils.closeStream(fileWriter);
                } catch (Throwable th2) {
                    th = th2;
                    IOUtils.closeStream(fileWriter);
                    th.printStackTrace();
                }
            } catch (Throwable th3) {
                th = th3;
                fileWriter = fileWriter2;
                IOUtils.closeStream(fileWriter);
                th.printStackTrace();
            }
        } catch (Exception e3) {
            e = e3;
            logger.e("Failed to write JSON to file: " + e.toString());
            IOUtils.closeStream(fileWriter);
        }
    }

    public SupportTicketService() {
        super(INTENT_SERVICE_NAME);
        BusProvider.getInstance().register(this);
        this.submitTicketWorker = new SubmitTicketWorker();
        if (StringUtils.isEmptyAfterTrim(email)) {
            setEmail(SettingsUtils.getCustomerPreferences().getString("email", ""));
        }
    }

    private synchronized void authenticateZendesk() {
        if (VERBOSE.booleanValue()) {
            logger.v("authenticateZendesk");
        }
        String zendeskAppId = CredentialsUtils.getCredentials(NavdyApplication.getAppContext().getString(R.string.metadata_zendesk_appid));
        String zendeskOAuth = CredentialsUtils.getCredentials(NavdyApplication.getAppContext().getString(R.string.metadata_zendesk_oauth));
        Context context = NavdyApplication.getAppContext();
        ZendeskConfig.INSTANCE.init(context, context.getString(R.string.zendesk_url), zendeskAppId, zendeskOAuth);
        if (ZendeskConfig.INSTANCE.isInitialized()) {
            logger.d("Zendesk isInitialized == true");
            ZendeskConfig.INSTANCE.setIdentity(new Builder().withNameIdentifier(SettingsUtils.getCustomerPreferences().getString(ProfilePreferences.FULL_NAME, "")).withEmailIdentifier(email).build());
            scheduleAlarmForSubmittingPendingTickets(10000);
        } else {
            logger.d("Zendesk initialization failed");
            finishService(false);
        }
    }

    protected synchronized void onHandleIntent(Intent intent) {
        logger.d("SupportTicketService handling intent");
        if (!BaseActivity.weHaveStoragePermission()) {
            logger.e("We do not have storage permission");
            finishService(false);
        }
        if (!SystemUtils.isConnectedToNetwork(NavdyApplication.getAppContext())) {
            if (VERBOSE.booleanValue()) {
                logger.d("Network is not accessible. Scheduling another alarm for the future...");
            }
            if (VERSION.SDK_INT >= 24 && !SettingsUtils.isLimitingCellularData()) {
                NetworkConnectivityJobService.scheduleNetworkServiceForNougatOrAbove();
            }
            buildNotificationForRetry();
            finishService(false);
        }
        if (!SettingsUtils.getSharedPreferences().getBoolean(SettingsConstants.PENDING_TICKETS_EXIST, false)) {
            logger.d("pending tickets currently do not exist");
            finishService(false);
        }
        if (intent.getBooleanExtra(EXTRA_RESET_COUNTER, false)) {
            if (VERBOSE.booleanValue()) {
                logger.d("reset boolean extra found");
            }
            resetRetryCount();
        }
        if (intent.getBooleanExtra(EXTRA_CREATE_INDETERMINATE_NOTIFICATION, false)) {
            buildNotificationForIndeterminateProgress();
        }
        if (intent.hasExtra(EXTRA_TICKET_RANDOM_ID)) {
            this.lastTicketId = intent.getStringExtra(EXTRA_TICKET_RANDOM_ID);
        }
        String extraEmail = intent.getStringExtra(EXTRA_EMAIL);
        if (!StringUtils.isEmptyAfterTrim(extraEmail)) {
            if (VERBOSE.booleanValue()) {
                logger.d("extra intent email found: " + extraEmail);
            }
            setEmail(extraEmail);
        }
        logger.v("onHandleIntent: " + intent);
        if (this.submitTicketWorker.isRunning()) {
            logger.d("SupportTicketService is already running. Avoiding a duplicate call to intent service.");
        } else if (ZendeskConfig.INSTANCE.isInitialized()) {
            submitPendingTickets();
        } else {
            authenticateZendesk();
        }
    }

    private synchronized void submitPendingTickets() {
        try {
            logger.d("submitPendingTickets");
            final File ticketsDirectory = new File(PathManager.getInstance().getTicketFolderPath());
            File[] readyFolders = ticketsDirectory.listFiles(filterOnReadyFolders);
            if (readyFolders == null || readyFolders.length <= 0) {
                logger.v("There are no pending tickets found");
                endTicketNotifications();
                finishService(false);
            }
            this.submitTicketWorker.start(new OnFinishSubmittingTickets() {
                public void onFinish(int failCount, boolean showSuccessNotification) {
                    File[] remainingFolders = ticketsDirectory.listFiles(SupportTicketService.filterOnReadyFolders);
                    Editor editor = SettingsUtils.getSharedPreferences().edit();
                    if (remainingFolders == null || remainingFolders.length != 0) {
                        SupportTicketService.logger.d("pending tickets awaiting hud logs exist");
                        editor.putBoolean(SettingsConstants.PENDING_TICKETS_EXIST, true);
                        if (SupportTicketService.isBelowMaxRetryCount()) {
                            SupportTicketService.scheduleAlarmForSubmittingPendingTickets(SupportTicketService.RETRY_INTERVAL_MILLIS);
                        }
                    } else {
                        SupportTicketService.logger.d("no pending tickets remaining");
                        editor.putBoolean(SettingsConstants.PENDING_TICKETS_EXIST, false);
                        editor.putBoolean(SettingsConstants.TICKETS_AWAITING_HUD_LOG_EXIST, false);
                    }
                    editor.apply();
                    if (failCount == 0) {
                        SupportTicketService.logger.d("Folders successfully submitted.");
                        SupportTicketService.resetRetryCount();
                        SupportTicketService.this.endTicketNotifications();
                        if (showSuccessNotification) {
                            SupportTicketService.this.buildNotificationForSuccess();
                        }
                    } else {
                        SupportTicketService.this.buildNotificationForRetry();
                        if (SupportTicketService.isBelowMaxRetryCount()) {
                            SupportTicketService.scheduleAlarmForSubmittingPendingTickets(SupportTicketService.RETRY_INTERVAL_MILLIS);
                        }
                        SupportTicketService.this.incrementTryCount();
                    }
                    SupportTicketService.this.incrementUpdateDate();
                    SupportTicketService.this.finishService(true);
                }
            }, readyFolders);
        } catch (Exception e) {
            logger.e("There was a problem submitting pending tickets: " + e);
        }
        return;
    }

    public static SharedPreferences getSupportTicketPreferences() {
        return NavdyApplication.getAppContext().getSharedPreferences(SUPPORT_TICKET_SHARED_PREFS, 0);
    }

    public static void scheduleAlarmForSubmittingPendingTickets(long interval) {
        if (SettingsUtils.isLimitingCellularData()) {
            logger.v("app is limiting cellular data, won't schedule alarm for submitting pending tickets");
            return;
        }
        if (VERBOSE.booleanValue()) {
            logger.v("scheduleAlarmForSubmittingPendingTickets retry coming in " + TimeUnit.MILLISECONDS.toMinutes(interval) + " minutes");
        }
        Context context = NavdyApplication.getAppContext();
        ((AlarmManager) context.getSystemService("alarm")).set(1, System.currentTimeMillis() + interval, PendingIntent.getService(NavdyApplication.getAppContext(), 0, new Intent(context, SupportTicketService.class), 268435456));
    }

    public void buildNotificationForRetry() {
        if (VERBOSE.booleanValue()) {
            logger.v("buildNotificationForRetry");
        }
        int failureCount = 0;
        Context context = NavdyApplication.getAppContext();
        Intent intent = new Intent(context, SupportTicketService.class);
        intent.putExtra(EXTRA_RESET_COUNTER, true);
        intent.putExtra(EXTRA_CREATE_INDETERMINATE_NOTIFICATION, true);
        PendingIntent pendingIntent = PendingIntent.getService(NavdyApplication.getAppContext(), 1, intent, 134217728);
        File[] folders = new File(PathManager.getInstance().getTicketFolderPath()).listFiles(filterOnReadyFolders);
        if (folders != null) {
            for (File ticketFolder : folders) {
                File content = new File(ticketFolder.getAbsolutePath() + File.separator + ZendeskConstants.TICKET_JSON_FILENAME);
                if (VERBOSE.booleanValue()) {
                    logger.v("content filepath: " + ticketFolder.getAbsolutePath() + File.separator + ZendeskConstants.TICKET_JSON_FILENAME);
                }
                JSONObject jsonObject = SubmitTicketWorker.getJSONObjectFromFile(content);
                if (!(jsonObject == null || jsonObject.optString(ZendeskConstants.TICKET_ACTION, "").equals(ZendeskConstants.ACTION_FETCH_HUD_LOG_UPLOAD_DELETE))) {
                    failureCount++;
                }
            }
        }
        if (failureCount > 0) {
            if (VERBOSE.booleanValue()) {
                logger.v("failureCount: " + failureCount);
            }
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context).setSmallIcon(R.drawable.ic_navdy_logo).setContentTitle(context.getString(R.string.zendesk_unable_to_submit_x_tickets, new Object[]{Integer.valueOf(failureCount)})).setContentText(context.getString(R.string.zendesk_notification_failure_message)).setContentIntent(pendingIntent);
            NotificationManager notificationManager = (NotificationManager) getSystemService("notification");
            if (notificationManager != null) {
                notificationManager.notify(2, notificationBuilder.build());
            }
        } else if (VERBOSE.booleanValue()) {
            logger.v("no failures found. Not building notification for retry");
        }
    }

    public void buildNotificationForSuccess() {
        if (VERBOSE.booleanValue()) {
            logger.v("buildNotificationForSuccess");
        }
        Context context = NavdyApplication.getAppContext();
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context).setSmallIcon(R.drawable.ic_navdy_logo).setContentTitle(context.getString(R.string.ticket_submission_success_title));
        NotificationManager notificationManager = (NotificationManager) getSystemService("notification");
        if (notificationManager != null) {
            notificationManager.notify(4, notificationBuilder.build());
        }
    }

    public void buildNotificationForIndeterminateProgress() {
        if (VERBOSE.booleanValue()) {
            logger.v("buildNotificationForIndeterminateProgress");
        }
        Context context = NavdyApplication.getAppContext();
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context).setSmallIcon(R.drawable.ic_navdy_logo).setContentTitle(context.getString(R.string.zendesk_upload_support_request)).setProgress(100, 0, true);
        NotificationManager notificationManager = (NotificationManager) getSystemService("notification");
        if (notificationManager != null) {
            notificationManager.notify(3, notificationBuilder.build());
        }
    }

    public void endTicketNotifications() {
        if (VERBOSE.booleanValue()) {
            logger.v("endTicketNotifications");
        }
        NotificationManager notificationManager = (NotificationManager) getSystemService("notification");
        if (notificationManager != null) {
            notificationManager.cancel(3);
            notificationManager.cancel(2);
        }
    }

    public static void resetRetryCount() {
        if (VERBOSE.booleanValue()) {
            logger.v("resetRetryCount");
        }
        getSupportTicketPreferences().edit().putInt(SHARED_PREF_TICKET_RETRY_COUNT, 0).putLong(SHARED_PREF_TICKET_LAST_RETRY_DATE, 0).apply();
    }

    public static boolean isBelowMaxRetryCount() {
        if (VERBOSE.booleanValue()) {
            logger.v("isBelowMaxRetryCount");
        }
        int retryCount = getSupportTicketPreferences().getInt(SHARED_PREF_TICKET_RETRY_COUNT, 0);
        logger.v("retryCount: " + retryCount);
        if (((long) retryCount) <= 4) {
            return true;
        }
        return false;
    }

    public void incrementTryCount() {
        SharedPreferences sharedPreferences = getSupportTicketPreferences();
        int retryCount = sharedPreferences.getInt(SHARED_PREF_TICKET_RETRY_COUNT, 0);
        logger.d("incrementTryCount retryCount: " + retryCount);
        sharedPreferences.edit().putInt(SHARED_PREF_TICKET_RETRY_COUNT, retryCount + 1).apply();
    }

    public void incrementUpdateDate() {
        getSupportTicketPreferences().edit().putLong(SHARED_PREF_TICKET_LAST_RETRY_DATE, new Date().getTime()).apply();
    }

    public static boolean lastRetryDateIsOldEnough() {
        boolean lastRetryDateIsOldEnough = System.currentTimeMillis() - RETRY_INTERVAL_MILLIS > getSupportTicketPreferences().getLong(SHARED_PREF_TICKET_LAST_RETRY_DATE, 0);
        if (VERBOSE.booleanValue()) {
            logger.d("lastRetryDateIsOldEnough: " + lastRetryDateIsOldEnough);
        }
        return lastRetryDateIsOldEnough;
    }

    public static void startSupportTicketService(Context context) {
        if (SettingsUtils.getSharedPreferences().getBoolean(SettingsConstants.PENDING_TICKETS_EXIST, false)) {
            context.startService(new Intent(context, SupportTicketService.class));
        }
    }

    public static synchronized String getEmail() {
        String str;
        synchronized (SupportTicketService.class) {
            str = email;
        }
        return str;
    }

    static synchronized void setEmail(String email) {
        synchronized (SupportTicketService.class) {
            email = email;
        }
    }

    public static boolean createFolderForTicket(ArrayList<File> attachments, JSONObject jsonObjectForTicketContents) {
        logger.i("jsonObject: " + jsonObjectForTicketContents);
        if (jsonObjectForTicketContents == null) {
            logger.e("jsonObject unsuccessfully created");
            return false;
        }
        String folderName = new SimpleDateFormat(NEW_FOLDER_FORMAT, Locale.US).format(new Date(System.currentTimeMillis()));
        if (VERBOSE.booleanValue()) {
            logger.v("new foldername: " + folderName);
        }
        File unfininishedDirectory = new File(PathManager.getInstance().getTicketFolderPath() + File.separator + folderName);
        boolean folderSuccessfullyCreated = unfininishedDirectory.mkdirs();
        if (folderSuccessfullyCreated) {
            File[] basicAttachmentArray = (File[]) attachments.toArray(new File[attachments.size()]);
            if (!attachments.isEmpty()) {
                IOUtils.compressFilesToZip(NavdyApplication.getAppContext(), basicAttachmentArray, new File(unfininishedDirectory.getAbsolutePath() + File.separator + "attachments.zip").getAbsolutePath());
            }
            writeJsonObjectToFile(jsonObjectForTicketContents, unfininishedDirectory.getAbsolutePath(), logger);
            if (!unfininishedDirectory.renameTo(new File(unfininishedDirectory.getAbsolutePath() + TICKET_FILTER_FOR_READY_FOLDERS))) {
                return folderSuccessfullyCreated;
            }
            logger.v("rename worked: " + unfininishedDirectory.getAbsolutePath());
            return folderSuccessfullyCreated;
        }
        logger.e("folder failed");
        return folderSuccessfullyCreated;
    }

    public static void collectLogs(final OnLogCollectedInterface onLogCollectedInterface) {
        if (VERBOSE.booleanValue()) {
            logger.v("collectLogsAndThen");
        }
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                Context applicationContext = NavdyApplication.getAppContext();
                final String logsFolder = PathManager.getInstance().getLogsFolder();
                String timestamp = SupportTicketService.format.format(Long.valueOf(System.currentTimeMillis()));
                String deviceDbsFilePath = logsFolder + File.separator + "Database_" + timestamp + ".db";
                File file = new File(deviceDbsFilePath);
                String deviceSharedPrefsFolderPath = logsFolder + File.separator + timestamp + "-SharedPrefs";
                file = new File(deviceSharedPrefsFolderPath);
                File tracesFile = null;
                try {
                    tracesFile = new File(NavdyApplication.getAppContext().getString(R.string.trace_file_path));
                } catch (Exception e) {
                }
                final ArrayList<File> attachments = new ArrayList();
                try {
                    SupportTicketService.dumpDbs(applicationContext, deviceDbsFilePath);
                    if (file.exists()) {
                        SupportTicketService.logger.v("mobile client Dbs successfully dumped");
                        attachments.add(file);
                    }
                    if (file.mkdirs()) {
                        SupportTicketService.dumpSharedPrefs(applicationContext, deviceSharedPrefsFolderPath);
                        SupportTicketService.logger.v("mobile client SharedPrefs successfully dumped");
                        File[] listOfSharedPrefsFiles = file.listFiles();
                        if (listOfSharedPrefsFiles != null) {
                            for (File sharedPrefsFile : listOfSharedPrefsFiles) {
                                if (sharedPrefsFile != null && sharedPrefsFile.exists() && sharedPrefsFile.isFile()) {
                                    attachments.add(sharedPrefsFile);
                                }
                            }
                        }
                    }
                    Iterator it = NavdyApplication.getLogFiles().iterator();
                    while (it.hasNext()) {
                        File logFile = (File) it.next();
                        if (logFile != null && logFile.length() > 0) {
                            attachments.add(logFile);
                        }
                    }
                    if (tracesFile != null) {
                        if (tracesFile.length() > 0) {
                            attachments.add(tracesFile);
                        }
                    }
                    new RemoteFileTransferManager(applicationContext, FileType.FILE_TYPE_LOGS, logsFolder, 15000, new FileTransferListener() {
                        public void onFileTransferResponse(FileTransferResponse response) {
                            if (SupportTicketService.VERBOSE.booleanValue()) {
                                SupportTicketService.logger.v("onFileTransferResponse: " + response.success);
                            }
                            if (response.success.booleanValue()) {
                                SupportTicketService.logger.v("HUD file name saved");
                                attachments.add(new File(logsFolder, response.destinationFileName));
                                return;
                            }
                            onError(response.error, "Error collecting display logs");
                        }

                        public void onFileTransferStatus(FileTransferStatus status) {
                            if (SupportTicketService.VERBOSE.booleanValue()) {
                                SupportTicketService.logger.v("onFileTransferStatus: " + status.success);
                            }
                            if (status.success.booleanValue() && status.transferComplete.booleanValue()) {
                                SupportTicketService.logger.v("Logs collected");
                                if (attachments.size() > 0) {
                                    onLogCollectedInterface.onLogCollectionSucceeded(attachments);
                                } else {
                                    onLogCollectedInterface.onLogCollectionFailed();
                                }
                            }
                        }

                        public void onError(FileTransferError errorCode, String error) {
                            SupportTicketService.logger.v("Error collecting display logs " + error);
                            if (attachments.size() > 0) {
                                onLogCollectedInterface.onLogCollectionSucceeded(attachments);
                            } else {
                                onLogCollectedInterface.onLogCollectionFailed();
                            }
                        }
                    }).pullFile();
                } catch (Throwable t) {
                    SupportTicketService.logger.v("Error collecting device logs: " + t);
                    if (attachments.size() > 0) {
                        onLogCollectedInterface.onLogCollectionSucceeded(attachments);
                    } else {
                        onLogCollectedInterface.onLogCollectionFailed();
                    }
                }
            }
        }, 1);
    }

    public static void collectHudLog(final OnLogCollectedInterface onLogCollectedInterface) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                Context applicationContext = NavdyApplication.getAppContext();
                final String logsFolder = PathManager.getInstance().getLogsFolder();
                final ArrayList<File> attachments = new ArrayList();
                try {
                    new RemoteFileTransferManager(applicationContext, FileType.FILE_TYPE_LOGS, logsFolder, 15000, new FileTransferListener() {
                        public void onFileTransferResponse(FileTransferResponse response) {
                            if (SupportTicketService.VERBOSE.booleanValue()) {
                                SupportTicketService.logger.v("onFileTransferResponse: " + response.success);
                            }
                            if (response.success.booleanValue()) {
                                SupportTicketService.logger.v("HUD log saved");
                                attachments.add(new File(logsFolder, response.destinationFileName));
                                return;
                            }
                            onError(response.error, "Error collecting display logs");
                        }

                        public void onFileTransferStatus(FileTransferStatus status) {
                            if (SupportTicketService.VERBOSE.booleanValue()) {
                                SupportTicketService.logger.v("onFileTransferStatus: " + status.success);
                            }
                            if (status.success.booleanValue() && status.transferComplete.booleanValue()) {
                                SupportTicketService.logger.v("Logs collected");
                                if (attachments.size() > 0) {
                                    onLogCollectedInterface.onLogCollectionSucceeded(attachments);
                                } else {
                                    onLogCollectedInterface.onLogCollectionFailed();
                                }
                            }
                        }

                        public void onError(FileTransferError errorCode, String error) {
                            SupportTicketService.logger.v("Error collecting display logs " + error);
                            if (attachments.size() > 0) {
                                onLogCollectedInterface.onLogCollectionSucceeded(attachments);
                            } else {
                                onLogCollectedInterface.onLogCollectionFailed();
                            }
                        }
                    }).pullFile();
                } catch (Throwable t) {
                    SupportTicketService.logger.v("Error collecting hud logs " + t);
                    if (attachments.size() > 0) {
                        onLogCollectedInterface.onLogCollectionSucceeded(attachments);
                    } else {
                        onLogCollectedInterface.onLogCollectionFailed();
                    }
                }
            }
        }, 1);
    }

    private static synchronized void dumpDbs(Context context, String fileName) throws Throwable {
        synchronized (SupportTicketService.class) {
            File backupDB = new File(fileName);
            if (backupDB.exists()) {
                IOUtils.deleteFile(context, fileName);
            }
            FileChannel src = null;
            FileChannel dst = null;
            try {
                File currentDB = context.getDatabasePath(NavdyContentProviderConstants.DB_NAME);
                if (currentDB.length() > MAX_FILE_SIZE) {
                    logger.e("DB too big: " + currentDB.length() + " bytes");
                } else if (currentDB.exists()) {
                    src = new FileInputStream(currentDB).getChannel();
                    dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                } else {
                    logger.e("invalid DB path: " + currentDB);
                }
            } catch (Exception e) {
                logger.e("Unable to pull the DB from the phone: ", e);
            } finally {
                IOUtils.closeStream(src);
                IOUtils.closeStream(dst);
            }
        }
    }

    private static synchronized void dumpSharedPrefs(Context context, String destinationFolderPath) throws Throwable {
        synchronized (SupportTicketService.class) {
            File destinationFolder = new File(destinationFolderPath);
            if (destinationFolder.exists() || destinationFolder.mkdirs()) {
                FileChannel src = null;
                FileChannel dst = null;
                try {
                    File[] listOfSharedPrefsFiles = new File(context.getFilesDir().getParent() + "/shared_prefs/").listFiles();
                    if (listOfSharedPrefsFiles != null) {
                        int length = listOfSharedPrefsFiles.length;
                        int i = 0;
                        while (true) {
                            int i2 = i;
                            if (i2 >= length) {
                                break;
                            }
                            File sharedPrefsFile = listOfSharedPrefsFiles[i2];
                            if (sharedPrefsFile != null && sharedPrefsFile.exists() && sharedPrefsFile.isFile()) {
                                File backupFile = new File(destinationFolder, "SharedPref_" + sharedPrefsFile.getName());
                                src = null;
                                dst = null;
                                src = new FileInputStream(sharedPrefsFile).getChannel();
                                dst = new FileOutputStream(backupFile).getChannel();
                                dst.transferFrom(src, 0, src.size());
                                IOUtils.closeStream(src);
                                IOUtils.closeStream(dst);
                            }
                            i = i2 + 1;
                        }
                    }
                } catch (Exception e) {
                    logger.e("Unable to pull the shared prefs files from the phone: ", e);
                } catch (Throwable th) {
                    IOUtils.closeStream(src);
                    IOUtils.closeStream(dst);
                }
            } else {
                logger.e("Unable to create the destination folder: " + destinationFolderPath);
            }
        }
        return;
    }

    public static void submitTicketsIfConditionsAreMet() {
        boolean isReachable = SystemUtils.isConnectedToNetwork(NavdyApplication.getAppContext());
        boolean pendingTicketsExist = SettingsUtils.getSharedPreferences().getBoolean(SettingsConstants.PENDING_TICKETS_EXIST, false);
        boolean lastRetryDateIsOldEnough = lastRetryDateIsOldEnough();
        boolean isBelowMaxRetryCount = isBelowMaxRetryCount();
        if (VERBOSE.booleanValue()) {
            logger.d("isReachable: " + isReachable + "\n pendingTicketExists: " + pendingTicketsExist + "\n lastRetryDateIsOldEnough: " + lastRetryDateIsOldEnough + "\n isBelowMaxRetryCount: " + isBelowMaxRetryCount);
        }
        if (isReachable && lastRetryDateIsOldEnough && isBelowMaxRetryCount && pendingTicketsExist) {
            if (VERBOSE.booleanValue()) {
                logger.d("Should submit tickets");
            }
            try {
                startSupportTicketService(NavdyApplication.getAppContext());
            } catch (Exception e) {
                logger.e("Exception found: " + e);
            }
        }
    }

    public void finishService(boolean isSuccess) {
        BusProvider.getInstance().post(new SubmitTicketFinishedEvent(isSuccess, this.lastTicketId));
        stopSelf();
    }
}
