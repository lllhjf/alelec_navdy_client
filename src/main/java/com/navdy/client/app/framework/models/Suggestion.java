package com.navdy.client.app.framework.models;

import com.alelec.navdyclient.R;
import com.navdy.service.library.events.destination.Destination.Builder;

public class Suggestion {
    private static final int NO_RESOURCE = -1;
    public boolean calculateSuggestedRoute = false;
    public Destination destination;
    public CalendarEvent event;
    private SuggestionType type;

    public enum SuggestionType {
        LOADING,
        HERE_HEADER,
        GOOGLE_HEADER,
        SECTION_HEADER,
        ACTIVE_TRIP,
        PENDING_TRIP,
        DEMO_VIDEO,
        ENABLE_GLANCES,
        ENABLE_MICROPHONE,
        GOOGLE_NOW,
        HUD_LOCAL_MUSIC_BROWSER,
        VOICE_SEARCH,
        ENABLE_GESTURES,
        TRY_GESTURES,
        ADD_HOME,
        ADD_WORK,
        OTA,
        ADD_FAVORITE,
        CALENDAR,
        RECOMMENDATION,
        RECENT,
        GOOGLE_FOOTER,
        HERE_FOOTER
    }

    public Suggestion(Destination destination, SuggestionType type) {
        this.destination = destination;
        this.type = type;
    }

    public Suggestion(Destination destination, SuggestionType type, CalendarEvent event) {
        this.destination = destination;
        this.type = type;
        this.event = event;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof Suggestion)) {
            return false;
        }
        Suggestion other = (Suggestion) obj;
        if (other.type == this.type && Destination.equals(other.destination, this.destination)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int result;
        int hashCode;
        int i = 0;
        if (this.destination != null) {
            result = this.destination.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 31;
        if (this.event != null) {
            hashCode = this.event.hashCode();
        } else {
            hashCode = 0;
        }
        i2 = (i2 + hashCode) * 31;
        if (this.calculateSuggestedRoute) {
            hashCode = 1;
        } else {
            hashCode = 0;
        }
        hashCode = (i2 + hashCode) * 31;
        if (this.type != null) {
            i = this.type.hashCode();
        }
        return hashCode + i;
    }

    public SuggestionType getType() {
        return this.type;
    }

    public boolean shouldSendToHud() {
        return this.destination.hasOneValidSetOfCoordinates() && (isCalendar() || this.type == SuggestionType.RECENT || isRecommendation());
    }

    public boolean canBeRoutedTo() {
        return this.type == SuggestionType.CALENDAR || this.type == SuggestionType.RECENT || this.type == SuggestionType.RECOMMENDATION || this.type == SuggestionType.PENDING_TRIP || this.type == SuggestionType.ACTIVE_TRIP;
    }

    public boolean hasInfoButton() {
        return (this.type == SuggestionType.OTA || isTip()) ? false : true;
    }

    public boolean isTip() {
        return this.type == SuggestionType.DEMO_VIDEO || this.type == SuggestionType.ENABLE_GLANCES || this.type == SuggestionType.ENABLE_MICROPHONE || this.type == SuggestionType.VOICE_SEARCH || this.type == SuggestionType.GOOGLE_NOW || this.type == SuggestionType.HUD_LOCAL_MUSIC_BROWSER || this.type == SuggestionType.ENABLE_GESTURES || this.type == SuggestionType.TRY_GESTURES || this.type == SuggestionType.ADD_HOME || this.type == SuggestionType.ADD_WORK || this.type == SuggestionType.OTA || this.type == SuggestionType.ADD_FAVORITE;
    }

    public boolean isRecommendation() {
        return this.type == SuggestionType.RECOMMENDATION;
    }

    public boolean isCalendar() {
        return this.type == SuggestionType.CALENDAR;
    }

    public boolean isActiveTrip() {
        return this.type == SuggestionType.ACTIVE_TRIP;
    }

    public boolean isPendingTrip() {
        return this.type == SuggestionType.PENDING_TRIP;
    }

    public boolean shouldShowRightChevron() {
        return this.type == SuggestionType.ENABLE_GLANCES || this.type == SuggestionType.ENABLE_MICROPHONE || this.type == SuggestionType.VOICE_SEARCH || this.type == SuggestionType.GOOGLE_NOW || this.type == SuggestionType.HUD_LOCAL_MUSIC_BROWSER || this.type == SuggestionType.ENABLE_GESTURES || this.type == SuggestionType.TRY_GESTURES || this.type == SuggestionType.ADD_HOME || this.type == SuggestionType.ADD_WORK || this.type == SuggestionType.ADD_FAVORITE;
    }

    private boolean isColorful() {
        return isTip() || isCalendar() || isRecommendation();
    }

    public int getBadgeAsset(boolean isSelected) {
        if (isSelected) {
            return R.drawable.icon_suggested_edit;
        }
        switch (this.type) {
            case LOADING:
                return R.drawable.content_loading_loop;
            case PENDING_TRIP:
            case ACTIVE_TRIP:
                if (this.type == SuggestionType.PENDING_TRIP) {
                    return this.destination.getBadgeAssetForPendingTrip();
                }
                return this.destination.getBadgeAssetForSuggestion();
            case DEMO_VIDEO:
                return R.drawable.icon_badge_video;
            case ENABLE_MICROPHONE:
            case VOICE_SEARCH:
                return R.drawable.icon_badge_voice_search;
            case ENABLE_GESTURES:
            case TRY_GESTURES:
                return R.drawable.icon_settings_gestures;
            case GOOGLE_NOW:
                return R.drawable.icon_googlenow;
            case HUD_LOCAL_MUSIC_BROWSER:
                return R.drawable.icon_music;
            case ENABLE_GLANCES:
                return R.drawable.icon_badge_glances;
            case CALENDAR:
                return R.drawable.icon_badge_calendar;
            case OTA:
                return R.drawable.icon_badge_softwareupdate;
            case ADD_FAVORITE:
                return isColorful() ? R.drawable.icon_badge_favorite : R.drawable.icon_badge_favorite_light;
            case ADD_HOME:
                return isColorful() ? R.drawable.icon_badge_home : R.drawable.icon_badge_home_light;
            case ADD_WORK:
                return isColorful() ? R.drawable.icon_badge_work : R.drawable.icon_badge_work_light;
            case RECOMMENDATION:
                return this.destination.getBadgeAsset();
            case RECENT:
                return this.destination.getBadgeAssetForRecent();
            default:
                return -1;
        }
    }

    public com.navdy.service.library.events.destination.Destination toProtobufDestinationObject() {
        return new Builder(this.destination.toProtobufDestinationObject()).suggestion_type(getSuggestionType()).is_recommendation(isRecommendation()).build();
    }

    private com.navdy.service.library.events.destination.Destination.SuggestionType getSuggestionType() {
        if (isCalendar()) {
            return com.navdy.service.library.events.destination.Destination.SuggestionType.SUGGESTION_CALENDAR;
        }
        return com.navdy.service.library.events.destination.Destination.SuggestionType.SUGGESTION_RECENT;
    }

    public String toString() {
        return "Suggestion{type=" + this.type + ", calculateSuggestedRoute=" + this.calculateSuggestedRoute + ", destination=" + this.destination + '}';
    }
}
