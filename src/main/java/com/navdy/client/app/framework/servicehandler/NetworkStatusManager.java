package com.navdy.client.app.framework.servicehandler;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;
import com.here.odnp.config.OdnpConfigStatic;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.Injector;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.events.settings.NetworkStateChange;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.network.http.IHttpManager;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.SystemUtils;
import com.squareup.wire.Message;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import okhttp3.Request.Builder;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class NetworkStatusManager {
    public static final boolean ENABLE_REACHABILITY_CHECK = false;
    private static final int ENDPOINT_CONNECTION_TIMEOUT = 30000;
    private static final int FAIL_PING_COUNT = 2;
    private static final int FAIL_PING_TIMEOUT = 30000;
    private static final int FAST_FAIL_PING_TIMEOUT = 2000;
    public static final int NETWORK_CHECK_RETRY_INTERVAL = 15000;
    private static final String PING_URL = "http://www.google.com";
    public static final int REACHABILITY_CHECK_INITIAL_DELAY = 5000;
    private static final int SUCCESS_PING_TIMEOUT = 120000;
    private static final boolean VERBOSE = false;
    private static final Logger sLogger = new Logger(NetworkStatusManager.class);
    private Context context = NavdyApplication.getAppContext();
    private Handler handler = new Handler(Looper.getMainLooper());
    private boolean isNetworkReachable;
    private NetworkStateChange lastState;
    @Inject
    IHttpManager mHttpManager;
    private Runnable makeNetworkCall = new Runnable() {
        public void run() {
            Response response = null;
            ResponseBody body;
            try {
                if (SystemUtils.isConnectedToNetwork(NetworkStatusManager.this.context)) {
                    NetworkStatusManager.access$404(NetworkStatusManager.this);
                    NetworkStatusManager.sLogger.d("Ping request start:" + NetworkStatusManager.this.retryCount + " reachable:" + NetworkStatusManager.this.isNetworkReachable);
                    response = NetworkStatusManager.this.mHttpManager.getClientCopy().connectTimeout(30000, TimeUnit.MILLISECONDS).build().newCall(new Builder().head().url(NetworkStatusManager.PING_URL).get().build()).execute();
                    NetworkStatusManager.sLogger.d("Ping response succeeded :" + NetworkStatusManager.this.retryCount);
                    if (NetworkStatusManager.this.isNetworkReachable) {
                        NetworkStatusManager.sLogger.v("n/w still reachable");
                        NetworkStatusManager.this.sendEvent();
                    } else {
                        NetworkStatusManager.this.isNetworkReachable = true;
                        NetworkStatusManager.sLogger.v("n/w is reachable now");
                        NetworkStatusManager.this.sendEvent();
                        BusProvider.getInstance().post(new ReachabilityEvent(NetworkStatusManager.this.isNetworkReachable));
                    }
                    NetworkStatusManager.this.retryCount = 0;
                    NetworkStatusManager.sLogger.v("run reachable check in 120000");
                    NetworkStatusManager.this.handler.removeCallbacks(NetworkStatusManager.this.reachableCheck);
                    NetworkStatusManager.this.handler.postDelayed(NetworkStatusManager.this.reachableCheck, OdnpConfigStatic.OEM_MAX_HIGH_POWER_INTERVAL);
                    if (response != null) {
                        try {
                            body = response.body();
                            if (body != null) {
                                body.close();
                                return;
                            }
                            return;
                        } catch (Throwable t) {
                            NetworkStatusManager.sLogger.e(t);
                            return;
                        }
                    }
                    return;
                }
                NetworkStatusManager.this.sendEvent();
                NetworkStatusManager.sLogger.d("Ping request bailing out: no n/w, stop reachable");
                if (response != null) {
                    try {
                        body = response.body();
                        if (body != null) {
                            body.close();
                        }
                    } catch (Throwable t2) {
                        NetworkStatusManager.sLogger.e(t2);
                    }
                }
            } catch (Throwable t22) {
                NetworkStatusManager.sLogger.e(t22);
            }
        }
    };
    private Runnable networkCheck = new Runnable() {
        public void run() {
            NetworkStatusManager.sLogger.v("networkCheck");
            NetworkStatusManager.this.checkForNetwork();
        }
    };
    private Runnable reachableCheck = new Runnable() {
        public void run() {
            TaskManager.getInstance().execute(NetworkStatusManager.this.makeNetworkCall, 5);
        }
    };
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if ("android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction())) {
                NetworkStatusManager.this.checkForNetwork();
            }
        }
    };
    private boolean registered;
    private RemoteDevice remoteDevice;
    private int retryCount;

    public static class ReachabilityEvent {
        public boolean isReachable;

        ReachabilityEvent(boolean reachability) {
            this.isReachable = reachability;
        }
    }

    static /* synthetic */ int access$404(NetworkStatusManager x0) {
        int i = x0.retryCount + 1;
        x0.retryCount = i;
        return i;
    }

    public NetworkStatusManager() {
        Injector.inject(NavdyApplication.getAppContext(), this);
    }

    public boolean checkForNetwork() {
        if (SystemUtils.isConnectedToNetwork(this.context)) {
            sLogger.d("checkForNetwork: Connectivity changed, n/w available, Connected to internet");
            sendEvent();
            BusProvider.getInstance().post(new ReachabilityEvent(true));
        } else {
            sLogger.d("checkForNetwork: Connectivity changed, no n/w, stop reachable");
            cleanState();
            sendEvent();
            BusProvider.getInstance().post(new ReachabilityEvent(false));
            this.handler.removeCallbacks(this.networkCheck);
            this.handler.postDelayed(this.networkCheck, 15000);
        }
        return false;
    }

    public synchronized void register() {
        cleanState();
        if (!this.registered) {
            IntentFilter filter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
            this.registered = true;
            sLogger.v("stop reachable check");
            this.handler.removeCallbacks(this.reachableCheck);
            if (SystemUtils.isConnectedToNetwork(this.context)) {
                sLogger.v("launch reachable check");
                this.isNetworkReachable = true;
            } else {
                sLogger.v("Not connected to the n/w, Check after some time");
                this.handler.postDelayed(this.networkCheck, 15000);
            }
            this.context.registerReceiver(this.receiver, filter);
        }
    }

    public synchronized void unregister() {
        cleanState();
        if (this.registered) {
            NavdyApplication.getAppContext().unregisterReceiver(this.receiver);
            this.registered = false;
        }
    }

    public void onRemoteDeviceConnected(RemoteDevice remoteDevice) {
        this.lastState = null;
        this.remoteDevice = remoteDevice;
        sendEvent();
    }

    public void onRemoteDeviceDisconnected() {
        this.remoteDevice = null;
        this.lastState = null;
    }

    private void sendEvent() {
        try {
            if (this.remoteDevice == null) {
                sLogger.v("[NetworkStateChange] hud not connected");
                return;
            }
            NetworkStateChange status = getNetworkStateChange();
            if (isNetworkStateEquals(status, this.lastState)) {
                sLogger.v("[NetworkStateChange] already sent lastState[" + this.lastState.networkAvailable + "]");
                return;
            }
            this.lastState = status;
            sLogger.v("NetworkStateChange available[" + status.networkAvailable + "] cell[" + status.cellNetwork + "] wifi[" + status.wifiNetwork + "] reach[" + status.reachability + "]");
            this.remoteDevice.postEvent(status);
            sLogger.v("[NetworkStateChange] status sent");
        } catch (Throwable t) {
            sLogger.e("[NetworkStateChange]", t);
        }
    }

    private NetworkStateChange getNetworkStateChange() {
        boolean cell;
        boolean wifi = true;
        boolean connected = SystemUtils.isConnectedToNetwork(this.context);
        if (connected && SystemUtils.isConnectedToCellNetwork(this.context)) {
            cell = true;
        } else {
            cell = false;
        }
        if (!(connected && SystemUtils.isConnectedToWifi(this.context))) {
            wifi = false;
        }
        boolean isReachable = canReachInternet();
        if (connected && !isReachable) {
            sLogger.v("connected but not reachable");
            connected = false;
        }
        return new NetworkStateChange(Boolean.valueOf(connected), Boolean.valueOf(cell), Boolean.valueOf(wifi), Boolean.valueOf(isReachable));
    }

    private boolean isNetworkStateEquals(NetworkStateChange left, NetworkStateChange right) {
        return left != null && right != null && isBooleanEquals(left.networkAvailable, right.networkAvailable) && isBooleanEquals(left.cellNetwork, right.cellNetwork) && isBooleanEquals(left.wifiNetwork, right.wifiNetwork) && isBooleanEquals(left.reachability, right.reachability);
    }

    private boolean isBooleanEquals(Boolean left, Boolean right) {
        return Boolean.TRUE.equals(left) == Boolean.TRUE.equals(right);
    }

    public boolean canReachInternet() {
        return SystemUtils.isConnectedToNetwork(this.context);
    }

    private void cleanState() {
        this.retryCount = 0;
        sLogger.v("stop reachable check");
        this.handler.removeCallbacks(this.reachableCheck);
        this.isNetworkReachable = false;
        this.lastState = null;
    }
}
