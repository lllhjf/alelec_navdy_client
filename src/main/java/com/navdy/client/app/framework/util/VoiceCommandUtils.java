package com.navdy.client.app.framework.util;

import android.content.Context;
import android.support.annotation.CheckResult;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RawRes;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.providers.NavdyContentProvider;
import com.navdy.service.library.util.IOUtils;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VoiceCommandUtils {
    @Nullable
    @CheckResult
    public static VoiceCommand getMatchingVoiceCommand(@RawRes int rawRes, @Nullable String query) {
        Throwable th;
        VoiceCommand vc = null;
        if (!StringUtils.isEmptyAfterTrim(query)) {
            InputStream inputStream = null;
            Scanner sc = null;
            Context context = NavdyApplication.getAppContext();
            String lowercaseQuery = query.toLowerCase().trim();
            try {
                inputStream = context.getResources().openRawResource(rawRes);
                Scanner sc2 = new Scanner(inputStream, "UTF-8");
                while (sc2.hasNextLine()) {
                    try {
                        String line = sc2.nextLine();
                        if (!StringUtils.isEmptyAfterTrim(line)) {
                            line = line.toLowerCase().replaceAll("\\{.*?\\}", "(.*?)");
                            Matcher matcher = Pattern.compile(line).matcher(lowercaseQuery);
                            if (matcher.matches() && matcher.groupCount() == 1) {
                                vc = new VoiceCommand();
                                vc.prefix = line;
                                vc.query = matcher.group(1);
                                IOUtils.closeStream(inputStream);
                                IOUtils.closeStream(sc2);
                                break;
                            }
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        sc = sc2;
                        IOUtils.closeStream(inputStream);
                        IOUtils.closeStream(sc);
                        throw th;
                    }
                }
                IOUtils.closeStream(inputStream);
                IOUtils.closeStream(sc2);
            } catch (Throwable th3) {
                th = th3;
                IOUtils.closeStream(inputStream);
                IOUtils.closeStream(sc);
                th.printStackTrace();
            }
        }
        return vc;
    }

    @CheckResult
    public static boolean isOneOfTheseVoiceCommands(@RawRes int CommandListRawRes, @Nullable String query) {
        Throwable th;
        if (StringUtils.isEmptyAfterTrim(query)) {
            return false;
        }
        InputStream inputStream = null;
        Scanner sc = null;
        Context context = NavdyApplication.getAppContext();
        String lowercaseQuery = query.toLowerCase().trim();
        try {
            inputStream = context.getResources().openRawResource(CommandListRawRes);
            Scanner sc2 = new Scanner(inputStream, "UTF-8");
            while (sc2.hasNextLine()) {
                try {
                    String line = sc2.nextLine();
                    if (!StringUtils.isEmptyAfterTrim(line) && StringUtils.equalsOrBothEmptyAfterTrim(line.toLowerCase(), lowercaseQuery)) {
                        IOUtils.closeStream(inputStream);
                        IOUtils.closeStream(sc2);
                        return true;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    sc = sc2;
                    IOUtils.closeStream(inputStream);
                    IOUtils.closeStream(sc);
                    throw th;
                }
            }
            IOUtils.closeStream(inputStream);
            IOUtils.closeStream(sc2);
            return false;
        } catch (Throwable th3) {
            th = th3;
            IOUtils.closeStream(inputStream);
            IOUtils.closeStream(sc);
            th.printStackTrace();
        }
        return false;
    }

    public static void addHomeAndWorkIfMatch(@NonNull ArrayList<Destination> destinationsFromDatabase, @Nullable String query) {
        if (isHome(query)) {
            Destination homeDestination = NavdyContentProvider.getHome();
            if (homeDestination != null) {
                destinationsFromDatabase.add(0, homeDestination);
            }
        } else if (isWork(query)) {
            Destination workDestination = NavdyContentProvider.getWork();
            if (workDestination != null) {
                destinationsFromDatabase.add(0, workDestination);
            }
        }
    }

    public static boolean isHome(@Nullable String query) {
        return isOneOfTheseVoiceCommands(R.raw.home_strings, query);
    }

    public static boolean isWork(@Nullable String query) {
        return isOneOfTheseVoiceCommands(R.raw.work_strings, query);
    }
}
