package com.navdy.client.app.framework.search;

import android.location.Location;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.alelec.navdyclient.R;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBufferResponse;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.maps.android.SphericalUtil;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.location.NavdyLocationManager;
import com.navdy.client.app.framework.map.MapUtils;
import com.navdy.client.app.framework.map.MapsForWorkUtils;
import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult;
import com.navdy.client.app.framework.util.CredentialsUtils;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.providers.NavdyContentProvider;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import com.navdy.service.library.util.SystemUtils;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.PlaceDetectionClient;


public final class GooglePlacesSearch {
    public static final int DEFAULT_RADIUS_VALUE = 1609;
    private static final String DESTINATION_PLACE_ID_SYNTAX = "destination=place_id:";
    private static final String DESTINATION_SYNTAX = "destination=";
    private static final String INPUT_SYNTAX = "input=";
    private static final String KEYWORD_SYNTAX = "keyword=";
    private static final String KEY_SYNTAX = "key=";
    private static final String LOCATION_SYNTAX = "location=";
    private static final String OPENNOW_SYNTAX = "opennow=";
    private static final String OPENNOW_VALUE = "true";
    private static final String ORIGIN_SYNTAX = "origin=";
    private static final String PLACE_ID_SYNTAX = "placeid=";
    private static final String QUERY_SYNTAX = "query=";
    private static final String RADIUS_SYNTAX = "radius=";
    private static final String RANKBY_SYNTAX = "rankby=";
    private static final int SERVICE_RADIUS_VALUE = 8046;
    private static final int SEARCH_RADIUS_METERS = 50000;
    private static final String TYPE_SYNTAX = "type=";
    private static final boolean VERBOSE = false;
    public static final Logger logger = new Logger(GooglePlacesSearch.class);
    private String apiKEY = "";
    private Query currentQueryType = Query.NEARBY;
    private Destination destination;
    private GoogleSearchListener googleSearchListener;
    private Handler handler;
    private Location location;
    private String location_value;
    private Runnable onSuccess = new Runnable() {
        public void run() {
            GooglePlacesSearch.this.sendDestinationsToListener(GooglePlacesSearch.this.results, Error.NONE);
        }
    };
    private String placeId;
    private List<GoogleTextSearchDestinationResult> results;

    protected GeoDataClient mGeoDataClient;

    public interface GoogleSearchListener {
        void onGoogleSearchResult(List<GoogleTextSearchDestinationResult> list, Query query, Error error);
    }

    public enum Error {
        NONE,
        SERVICE_ERROR
    }

    public enum Query {
        QUERY_AUTOCOMPLETE,
        TEXTSEARCH,
        DETAILS,
        NEARBY,
        DIRECTIONS;

        public static String getBaseUrl(Query query) {
            switch (query) {
                case QUERY_AUTOCOMPLETE:
                    return "https://maps.googleapis.com/maps/api/place/queryautocomplete/json?";
                case TEXTSEARCH:
                    return "https://maps.googleapis.com/maps/api/place/textsearch/json?";
                case DETAILS:
                    return "https://maps.googleapis.com/maps/api/place/details/json?";
                case DIRECTIONS:
                    return "https://maps.googleapis.com/maps/api/directions/json?";
                default:
                    return "https://maps.googleapis.com/maps/api/place/nearbysearch/json?";
            }
        }
    }

    private enum RankByTypes {
        PROMINENCE,
        DISTANCE;

        public static String getBaseUrl(RankByTypes type) {
            switch (type) {
                case PROMINENCE:
                    return "prominence";
                default:
                    return "distance";
            }
        }
    }

    public enum ServiceTypes {
        GAS("gas_station", "Gas"),
        PARKING("parking", "Parking"),
        FOOD("restaurant", "Food"),
        COFFEE("cafe", "Coffee"),
        ATM("atm", "ATMs"),
        HOSPITAL("hospital", "Hospitals"),
        STORE("convenience_store", "Grocery Stores");
        
        String baseUrl;
        String hudServiceType;

        private ServiceTypes(String baseUrl, String hudServiceType) {
            this.baseUrl = baseUrl;
            this.hudServiceType = hudServiceType;
        }

        public String getBaseUrl() {
            return this.baseUrl;
        }

        public String getHudServiceType() {
            return this.hudServiceType;
        }
    }

    public GooglePlacesSearch(GoogleSearchListener listener, Handler handler) {
        this.handler = handler;
        init(listener);
    }

    public GooglePlacesSearch(GoogleSearchListener listener) {
        init(listener);
    }

    private void init(GoogleSearchListener listener) {
        this.apiKEY = KEY_SYNTAX + CredentialsUtils.getCredentials(NavdyApplication.getAppContext().getString(R.string.metadata_google_web_service));
        this.googleSearchListener = listener;
        location = NavdyLocationManager.getInstance().getSmartStartLocation();
        if (location != null) {
            this.location_value = location.getLatitude() + "," + location.getLongitude();
        }
        // Construct a GeoDataClient.
        mGeoDataClient = Places.getGeoDataClient(NavdyApplication.getAppContext());

        // Construct a PlaceDetectionClient.
//        mPlaceDetectionClient = Places.getPlaceDetectionClient(this, null);

    }

    public void runQueryAutoCompleteWebApi(String query) {
        logger.d("executing Query Auto Complete");
        this.currentQueryType = Query.QUERY_AUTOCOMPLETE;
        runSearchWebApiInTaskManager(query);
    }

    public void runTextSearchWebApi(String query) {
        logger.d("executing Text Search");
        this.currentQueryType = Query.TEXTSEARCH;
        runSearchWebApiInTaskManager(query);
    }

    public void runServiceSearchWebApi(ServiceTypes type) {
        runServiceSearchWebApi(type, SERVICE_RADIUS_VALUE);
    }

    public void runServiceSearchWebApi(ServiceTypes type, int searchRadius) {
        if (type == null) {
            logger.e("Trying to call runServiceSearchWebApi with a null service type. You crazy?");
            return;
        }
        logger.d("executing Service Search");
        if (StringUtils.isEmptyAfterTrim(this.location_value)) {
            this.currentQueryType = Query.TEXTSEARCH;
        } else {
            this.currentQueryType = Query.NEARBY;
        }
        runSearchWebApiInTaskManager(null, type, searchRadius);
    }

    public void runNearbySearchWebApi(String query) {
        logger.d("executing Nearby Search");
        this.currentQueryType = Query.NEARBY;
        runSearchWebApiInTaskManager(query);
    }

    public void runDetailsSearchWebApi(String placeId) {
        this.placeId = placeId;
        this.currentQueryType = Query.DETAILS;
        runSearchWebApiInTaskManager(null);
    }

    public void runDirectionsSearchWebApi(Destination d) {
        this.destination = d;
        this.placeId = d.placeId;
        this.currentQueryType = Query.DIRECTIONS;
        runSearchWebApiInTaskManager(null);
    }

    private void runSearchWebApiInTaskManager(String searchText) {
        runSearchWebApiInTaskManager(searchText, null, DEFAULT_RADIUS_VALUE);
    }

    private void runSearchWebApiInTaskManager(@Nullable final String searchText, @Nullable final ServiceTypes type, final int searchRadius) {
        if (!SystemUtils.isConnectedToNetwork(NavdyApplication.getAppContext())) {
            sendDestinationsToListener(null, Error.SERVICE_ERROR);
        }
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                if (GooglePlacesSearch.this.currentQueryType == Query.DETAILS && !StringUtils.isEmptyAfterTrim(GooglePlacesSearch.this.placeId)) {
                    String placeDetailJson = NavdyContentProvider.getNonStalePlaceDetailInfoFor(GooglePlacesSearch.this.placeId);
                    if (!StringUtils.isEmptyAfterTrim(placeDetailJson)) {
                        GooglePlacesSearch.logger.v("Place detail data is not stale so using it.");
                        GoogleTextSearchDestinationResult destinationResult = GoogleTextSearchDestinationResult.createDestinationObject(placeDetailJson);
                        GooglePlacesSearch.this.results = new ArrayList();
                        GooglePlacesSearch.this.results.add(destinationResult);
                        if (GooglePlacesSearch.this.handler != null) {
                            GooglePlacesSearch.this.handler.post(GooglePlacesSearch.this.onSuccess);
                            return;
                        } else {
                            GooglePlacesSearch.this.onSuccess.run();
                            return;
                        }
                    }
                }

                // TODO add handling for getting details with Task<PlaceBufferResponse> GeoDataClient.getPlaceById
                // TODO this would be good too: this.currentQueryType == Query.TEXTSEARCH

                if ((type == null) && (searchText != null) && (GooglePlacesSearch.this.currentQueryType == Query.QUERY_AUTOCOMPLETE)) {
                    GooglePlacesSearch.this.runSearchSdk(searchText, type, searchRadius);
                } else {
                    GooglePlacesSearch.this.runSearchWebApi(searchText, type, searchRadius);
                }
            }
        }, 3);
    }

    private LatLngBounds searchAreaFor(Location loc, double radiusInMeters) {
        LatLng latLng = new LatLng(loc.getLatitude(), loc.getLongitude());
        return new LatLngBounds.Builder()
                .include(SphericalUtil.computeOffset(MapsForWorkUtils.m4bToGms(latLng), radiusInMeters, 0.0d))
                .include(SphericalUtil.computeOffset(MapsForWorkUtils.m4bToGms(latLng), radiusInMeters, 90.0d))
                .include(SphericalUtil.computeOffset(MapsForWorkUtils.m4bToGms(latLng), radiusInMeters, 180.0d))
                .include(SphericalUtil.computeOffset(MapsForWorkUtils.m4bToGms(latLng), radiusInMeters, 270.0d))
                .build();
    }

    private void runSearchSdk(String searchText, @Nullable ServiceTypes type, int searchRadius) {
//        AutocompleteFilter mPlaceFilter = new AutocompleteFilter.Builder()
//                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_ADDRESS)
//                .build();

        logger.i("Running sdk search with following parameters: " + searchText);

        AutocompleteFilter mPlaceFilter = null;

        Task<AutocompletePredictionBufferResponse> results =
                mGeoDataClient.getAutocompletePredictions(searchText, searchAreaFor(location, searchRadius),
                        mPlaceFilter);

        results.addOnSuccessListener(new OnSuccessListener<AutocompletePredictionBufferResponse>() {

            @Override
            public void onSuccess(AutocompletePredictionBufferResponse autocompletePredictions) {
                Iterator<AutocompletePrediction> iterator = autocompletePredictions.iterator();
                ArrayList<GoogleTextSearchDestinationResult> results = new ArrayList<>(autocompletePredictions.getCount());
                if (iterator.hasNext()) {
                    while (iterator.hasNext()) {
                        AutocompletePrediction prediction = iterator.next();
                        //do your stuff with prediction here

                        GoogleTextSearchDestinationResult dest = new GoogleTextSearchDestinationResult();
                        dest.name = String.valueOf(prediction.getPrimaryText(null));
                        dest.address = String.valueOf(prediction.getFullText(null));

                        results.add(dest);
                    }
                }else {
                    //Nothing matches...
                }
                GooglePlacesSearch.this.sendDestinationsToListener(results, Error.NONE);
                autocompletePredictions.release();
            }

        });
        results.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(NavdyApplication.getAppContext(),"Error while fetching suggessions : "+e.getLocalizedMessage(),Toast.LENGTH_SHORT).show();
            }

        });
    }


    private void runSearchWebApi(String searchText, @Nullable ServiceTypes type, int searchRadius) {
        String typeString = "";
        if (type != null) {
            typeString = type.getBaseUrl();
        }
        logger.v("Running web search with following parameters: " + searchText + " type: " + type);
        String encodedSearchText = Uri.encode(searchText);
        StringBuilder url = new StringBuilder();
        url.append(Query.getBaseUrl(this.currentQueryType));
        url.append(this.apiKEY);
        switch (this.currentQueryType) {
            case QUERY_AUTOCOMPLETE:
                if (!StringUtils.isEmptyAfterTrim(this.location_value)) {
                    url.append("&");
                    url.append(LOCATION_SYNTAX);
                    url.append(this.location_value);
                }
                url.append("&");
                url.append(RADIUS_SYNTAX);
                url.append(DEFAULT_RADIUS_VALUE);
                if (!StringUtils.isEmptyAfterTrim(encodedSearchText)) {
                    url.append("&");
                    url.append(INPUT_SYNTAX);
                    url.append(encodedSearchText);
                    break;
                }
                break;
            case TEXTSEARCH:
                if (MapUtils.parseLatLng(searchText) == null) {
                    if (!StringUtils.isEmptyAfterTrim(this.location_value)) {
                        url.append("&");
                        url.append(LOCATION_SYNTAX);
                        url.append(this.location_value);
                    }
                    url.append("&");
                    url.append(RADIUS_SYNTAX);
                    url.append(DEFAULT_RADIUS_VALUE);
                }
                if (!StringUtils.isEmptyAfterTrim(encodedSearchText)) {
                    url.append("&");
                    url.append(QUERY_SYNTAX);
                    url.append(encodedSearchText);
                    break;
                }
                break;
            case DETAILS:
                url.append("&");
                url.append(PLACE_ID_SYNTAX);
                url.append(this.placeId);
                break;
            case DIRECTIONS:
                if (!StringUtils.isEmptyAfterTrim(this.location_value)) {
                    url.append("&");
                    url.append(ORIGIN_SYNTAX);
                    url.append(this.location_value);
                }
                url.append("&");
                if (StringUtils.isEmptyAfterTrim(this.placeId)) {
                    if (this.destination != null && !StringUtils.isEmptyAfterTrim(this.destination.rawAddressNotForDisplay)) {
                        url.append(DESTINATION_SYNTAX);
                        url.append(Uri.encode(this.destination.rawAddressNotForDisplay));
                        break;
                    }
                    logger.e("Trying to call Google Directions API with a no placeId or address.");
                    break;
                }
                url.append(DESTINATION_PLACE_ID_SYNTAX);
                url.append(this.placeId);
                break;
            case NEARBY:
                if (!StringUtils.isEmptyAfterTrim(this.location_value)) {
                    url.append("&");
                    url.append(LOCATION_SYNTAX);
                    url.append(this.location_value);
                }
                if (type != null) {
                    url.append("&");
                    url.append(RANKBY_SYNTAX);
                    switch (type) {
                        case GAS:
                        case PARKING:
                            url.append(RankByTypes.getBaseUrl(RankByTypes.DISTANCE));
                            break;
                        default:
                            url.append(RankByTypes.getBaseUrl(RankByTypes.PROMINENCE));
                            url.append("&");
                            url.append(RADIUS_SYNTAX);
                            if (searchRadius <= 0) {
                                url.append(SERVICE_RADIUS_VALUE);
                                break;
                            } else {
                                url.append(searchRadius);
                                break;
                            }
                    }
                }
                url.append("&");
                url.append(KEYWORD_SYNTAX);
                url.append(encodedSearchText);
                url.append("&");
                url.append(OPENNOW_SYNTAX);
                url.append(OPENNOW_VALUE);
                break;
        }
        if (!StringUtils.isEmptyAfterTrim(typeString)) {
            url.append("&");
            url.append(TYPE_SYNTAX);
            url.append(typeString);
        }
        String urlString = url.toString();
        logger.d("Calling Google with URL: " + urlString.replaceAll(this.apiKEY, "key=API_KEY"));
        this.results = getSearchResults(urlString);
        if (this.handler != null) {
            this.handler.post(this.onSuccess);
        } else {
            this.onSuccess.run();
        }
    }

    private void sendDestinationsToListener(List<GoogleTextSearchDestinationResult> destinations, Error error) {
        this.googleSearchListener.onGoogleSearchResult(destinations, this.currentQueryType, error);
    }

    private List<GoogleTextSearchDestinationResult> getSearchResults(String url) {
        InputStream inputStream = null;
        ArrayList<GoogleTextSearchDestinationResult> resultDestinations = new ArrayList();
        try {
            inputStream = ((HttpURLConnection) new URL(url).openConnection()).getInputStream();
            JSONObject jsonObject = new JSONObject(IOUtils.convertInputStreamToString(inputStream, "UTF-8"));
            logger.v("jsonObject received: " + jsonObject.toString());
            GoogleTextSearchDestinationResult destination;
            if (this.currentQueryType == Query.DIRECTIONS) {
                destination = GoogleTextSearchDestinationResult.createDestinationObject(jsonObject);
                if (destination != null) {
                    resultDestinations.add(destination);
                }
            } else {
                int i;
                if (this.currentQueryType == Query.QUERY_AUTOCOMPLETE) {
                    JSONArray predictions = (JSONArray) jsonObject.get("predictions");
                    int predictionsLength = predictions.length();
                    for (i = 0; i < predictionsLength; i++) {
                        destination = GoogleTextSearchDestinationResult.createDestinationObject(predictions.getJSONObject(i));
                        if (destination != null) {
                            resultDestinations.add(destination);
                        }
                    }
                } else {
                    if (this.currentQueryType != Query.DETAILS) {
                        JSONArray results = (JSONArray) jsonObject.get("results");
                        int resultsLength = results.length();
                        for (i = 0; i < resultsLength; i++) {
                            destination = GoogleTextSearchDestinationResult.createDestinationObject(results.getJSONObject(i));
                            if (destination != null) {
                                resultDestinations.add(destination);
                            }
                        }
                    } else {
                        destination = GoogleTextSearchDestinationResult.createDestinationObject((JSONObject) jsonObject.get("result"));
                        if (destination != null) {
                            resultDestinations.add(destination);
                        }
                    }
                }
            }
            logger.v("resultDestinations size after parsing response: " + resultDestinations.size());
        } catch (Throwable e) {
            logger.v("throwable e: " + e);
            resultDestinations = null;
        } finally {
            IOUtils.closeStream(inputStream);
        }
        return resultDestinations;
    }

    public void setListener(GoogleSearchListener listener) {
        this.googleSearchListener = listener;
    }
}
