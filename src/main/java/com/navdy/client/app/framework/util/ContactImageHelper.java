package com.navdy.client.app.framework.util;

import android.support.v4.content.ContextCompat;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import java.util.HashMap;
import java.util.Map;

public class ContactImageHelper {
    public static final int DEFAULT_IMAGE_INDEX = -1;
    public static final int NO_CONTACT_COLOR = ContextCompat.getColor(NavdyApplication.getAppContext(), R.color.grey);
    public static final int NO_CONTACT_IMAGE = 2130837759;
    private static final Map<Integer, Integer> sContactColorMap = new HashMap();
    private static final Map<Integer, Integer> sContactImageMap = new HashMap();
    private static final ContactImageHelper sInstance = new ContactImageHelper();

    static {
        sContactColorMap.put(Integer.valueOf(0), Integer.valueOf(ContextCompat.getColor(NavdyApplication.getAppContext(), R.color.icon_user_bg_0)));
        int counter = 0 + 1;
        sContactImageMap.put(Integer.valueOf(0), Integer.valueOf(R.drawable.icon_user_bg_0));
        sContactColorMap.put(Integer.valueOf(counter), Integer.valueOf(ContextCompat.getColor(NavdyApplication.getAppContext(), R.color.icon_user_bg_1)));
        int counter2 = counter + 1;
        sContactImageMap.put(Integer.valueOf(counter), Integer.valueOf(R.drawable.icon_user_bg_1));
        sContactColorMap.put(Integer.valueOf(counter2), Integer.valueOf(ContextCompat.getColor(NavdyApplication.getAppContext(), R.color.icon_user_bg_2)));
        counter = counter2 + 1;
        sContactImageMap.put(Integer.valueOf(counter2), Integer.valueOf(R.drawable.icon_user_bg_2));
        sContactColorMap.put(Integer.valueOf(counter), Integer.valueOf(ContextCompat.getColor(NavdyApplication.getAppContext(), R.color.icon_user_bg_3)));
        counter2 = counter + 1;
        sContactImageMap.put(Integer.valueOf(counter), Integer.valueOf(R.drawable.icon_user_bg_3));
        sContactColorMap.put(Integer.valueOf(counter2), Integer.valueOf(ContextCompat.getColor(NavdyApplication.getAppContext(), R.color.icon_user_bg_4)));
        counter = counter2 + 1;
        sContactImageMap.put(Integer.valueOf(counter2), Integer.valueOf(R.drawable.icon_user_bg_4));
        sContactColorMap.put(Integer.valueOf(counter), Integer.valueOf(ContextCompat.getColor(NavdyApplication.getAppContext(), R.color.icon_user_bg_5)));
        counter2 = counter + 1;
        sContactImageMap.put(Integer.valueOf(counter), Integer.valueOf(R.drawable.icon_user_bg_5));
        sContactColorMap.put(Integer.valueOf(counter2), Integer.valueOf(ContextCompat.getColor(NavdyApplication.getAppContext(), R.color.icon_user_bg_6)));
        counter = counter2 + 1;
        sContactImageMap.put(Integer.valueOf(counter2), Integer.valueOf(R.drawable.icon_user_bg_6));
        sContactColorMap.put(Integer.valueOf(counter), Integer.valueOf(ContextCompat.getColor(NavdyApplication.getAppContext(), R.color.icon_user_bg_7)));
        counter2 = counter + 1;
        sContactImageMap.put(Integer.valueOf(counter), Integer.valueOf(R.drawable.icon_user_bg_7));
        sContactColorMap.put(Integer.valueOf(counter2), Integer.valueOf(ContextCompat.getColor(NavdyApplication.getAppContext(), R.color.icon_user_bg_8)));
        counter = counter2 + 1;
        sContactImageMap.put(Integer.valueOf(counter2), Integer.valueOf(R.drawable.icon_user_bg_8));
    }

    public static ContactImageHelper getInstance() {
        return sInstance;
    }

    public int getContactImageIndex(String contactId) {
        if (contactId == null) {
            return -1;
        }
        return Math.abs(normalizeToFilename(contactId).hashCode() % sContactImageMap.size());
    }

    public int getResourceId(int index) {
        if (index < 0 || index >= sContactImageMap.size()) {
            return R.drawable.ic_search_no_profile;
        }
        return ((Integer) sContactImageMap.get(Integer.valueOf(index))).intValue();
    }

    public int getResourceColor(int index) {
        if (index < 0 || index >= sContactColorMap.size()) {
            return NO_CONTACT_COLOR;
        }
        return ((Integer) sContactColorMap.get(Integer.valueOf(index))).intValue();
    }

    public int getDriverImageResId(String deviceName) {
        return getResourceId(Math.abs(normalizeToFilename(deviceName).hashCode() % sContactImageMap.size()));
    }

    public static String normalizeToFilename(String str) {
        return StringUtils.isEmptyAfterTrim(str) ? str : str.replaceAll("[^a-zA-Z0-9\\._]+", "");
    }
}
