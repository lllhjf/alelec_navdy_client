package com.navdy.client.app.framework.models;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.PointF;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.CheckResult;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.util.Pair;
import com.amazonaws.services.s3.internal.Constants;
import com.google.android.gms.maps.model.LatLng;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.Image;
import com.here.android.mpa.mapping.MapMarker;
import com.here.android.mpa.search.Address;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.i18n.AddressUtils;
import com.navdy.client.app.framework.i18n.AddressUtils.AddressFormat;
import com.navdy.client.app.framework.location.NavdyLocationManager;
import com.navdy.client.app.framework.map.MapUtils;
import com.navdy.client.app.framework.search.GooglePlacesSearch;
import com.navdy.client.app.framework.search.GooglePlacesSearch.Error;
import com.navdy.client.app.framework.search.GooglePlacesSearch.GoogleSearchListener;
import com.navdy.client.app.framework.search.GooglePlacesSearch.Query;
import com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler.DestinationType;
import com.navdy.client.app.framework.util.ContactsManager;
import com.navdy.client.app.framework.util.NetworkUtils;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.framework.util.SuggestionManager;
import com.navdy.client.app.framework.util.SystemUtils;
import com.navdy.client.app.framework.util.VersioningUtils;
import com.navdy.client.app.providers.NavdyContentProvider;
import com.navdy.client.app.providers.NavdyContentProvider.QueryResultCallback;
import com.navdy.client.app.providers.NavdyContentProviderConstants;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Event;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.homescreen.CalendarUtils;
import com.navdy.client.app.ui.search.SearchRecyclerAdapter.ImageEnum;
import com.navdy.service.library.events.contacts.Contact;
import com.navdy.service.library.events.contacts.PhoneNumber;
import com.navdy.service.library.events.destination.Destination.FavoriteType;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.events.location.Coordinate.Builder;
import com.navdy.service.library.events.location.LatLong;
import com.navdy.service.library.events.navigation.NavigationRouteResult;
import com.navdy.service.library.events.places.PlaceType;
import com.navdy.service.library.events.places.PlacesSearchResult;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.task.TaskManager.TaskPriority;
import com.navdy.service.library.util.IOUtils;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import org.droidparts.contract.SQL;
import org.json.JSONException;
import org.json.JSONObject;

public class Destination implements Parcelable, Serializable {
    public static final Creator<Destination> CREATOR = new Creator<Destination>() {
        public Destination createFromParcel(Parcel in) {
            return new Destination(in);
        }

        public Destination[] newArray(int size) {
            return new Destination[size];
        }
    };
    private static final int DESTINATION_NOT_FOUND = -1;
    private static final float DISTANCE_LIMIT = 5.0f;
    public static final String IDENTIFIER_CONTACT_ID_SEPARATOR = "|";
    public static final String IDENTIFIER_NAMESPACE_CONTACT = "contact:";
    public static final String IDENTIFIER_NAMESPACE_DB_ID = "db-id:";
    public static final String IDENTIFIER_NAMESPACE_PLACE_ID = "google-places-id:";
    private static final long PLACE_ID_REFRESH_LIMIT = 2592000000L;
    private static final boolean VERBOSE = false;
    private static Logger logger = new Logger(Destination.class);
    String city;
    public String contactLookupKey;
    public String country;
    private String countryCode;
    public double displayLat;
    public double displayLong;
    public boolean doNotSuggest;
    private String favoriteLabel;
    private int favoriteOrder;
    public int favoriteType;
    private boolean hasBeenProcessed;
    public int id;
    private boolean isCalendarEvent;
    public long lastContactLookup;
    public long lastKnownContactId;
    long lastPlaceIdRefresh;
    public long lastRoutedDate;
    public String name;
    public double navigationLat;
    public double navigationLong;
    public String placeDetailJson;
    public String placeId;
    public Precision precisionLevel;
    public String rawAddressNotForDisplay;
    public ArrayList<String> rawAddressVariations;
    public int searchResultType;
    public String state;
    public String streetName;
    public String streetNumber;
    public Type type;
    String zipCode;
    public double distance;
    public String price_level;

    public static class MultilineAddress {
        public String addressFirstLine = "";
        public String addressSecondLine = "";
        public String title = "";

        MultilineAddress(Destination destination) {
            String destinationTitle = destination.getTitle();
            if (!StringUtils.isEmptyAfterTrim(destinationTitle)) {
                Pair<String, String> splitAddress = destination.getSplitAddress();
                if (!AddressUtils.isTitleIsInTheAddress(destinationTitle, (String) splitAddress.first)) {
                    this.title = destinationTitle;
                    this.addressFirstLine = AddressUtils.sanitizeAddress((String) splitAddress.first);
                    this.addressSecondLine = AddressUtils.sanitizeAddress((String) splitAddress.second);
                    return;
                }
            }
            ArrayList<String> lines = destination.getAddressLines();
            if (lines.size() > 0) {
                this.title = (String) lines.get(0);
            }
            if (lines.size() > 1) {
                this.addressFirstLine = (String) lines.get(1);
            }
            this.addressSecondLine = AddressUtils.joinRemainingLines(lines, 2);
        }
    }

    public enum Precision {
        UNKNOWN(0),
        IMPRECISE(1),
        PRECISE(2);
        
        int level;

        private Precision(int level) {
            this.level = level;
        }

        public int getValue() {
            return this.level;
        }

        public static Precision get(int level) {
            return values()[level];
        }

        public static Precision get(boolean isImprecise) {
            return isImprecise ? IMPRECISE : PRECISE;
        }

        public boolean isPrecise() {
            return this != IMPRECISE;
        }
    }

    public enum SearchType {
        SEARCH_POWERED_BY_GOOGLE_HEADER,
        SERVICES_SEARCH,
        SEARCH_HISTORY,
        AUTOCOMPLETE,
        CONTACT,
        CONTACT_ADDRESS_SELECTION,
        TEXT_SEARCH,
        RECENT_PLACES,
        DETAILS,
        MORE,
        NO_RESULTS,
        UNKNOWN,
        FOOTER;

        public int getValue() {
            return ordinal();
        }
    }

    public enum Type {
        UNKNOWN,
        CONTACT,
        CALENDAR_EVENT,
        PLACE,
        GAS_STATION,
        AIRPORT,
        PARKING,
        TRANSIT,
        ATM,
        BANK,
        SCHOOL,
        STORE,
        COFFEE,
        RESTAURANT,
        GYM,
        PARK,
        HOSPITAL,
        ENTERTAINMENT,
        BAR;

        public int getValue() {
            return ordinal();
        }

        public static Type get(int value) {
            return values()[value];
        }

        public PlaceType toProtobufPlaceType() {
            switch (this) {
                case CONTACT:
                    return PlaceType.PLACE_TYPE_CONTACT;
                case CALENDAR_EVENT:
                    return PlaceType.PLACE_TYPE_CALENDAR_EVENT;
                case GAS_STATION:
                    return PlaceType.PLACE_TYPE_GAS;
                case AIRPORT:
                    return PlaceType.PLACE_TYPE_AIRPORT;
                case PARKING:
                    return PlaceType.PLACE_TYPE_PARKING;
                case TRANSIT:
                    return PlaceType.PLACE_TYPE_TRANSIT;
                case ATM:
                    return PlaceType.PLACE_TYPE_ATM;
                case BANK:
                    return PlaceType.PLACE_TYPE_BANK;
                case SCHOOL:
                    return PlaceType.PLACE_TYPE_SCHOOL;
                case STORE:
                    return PlaceType.PLACE_TYPE_STORE;
                case COFFEE:
                    return PlaceType.PLACE_TYPE_COFFEE;
                case RESTAURANT:
                    return PlaceType.PLACE_TYPE_RESTAURANT;
                case GYM:
                    return PlaceType.PLACE_TYPE_GYM;
                case PARK:
                    return PlaceType.PLACE_TYPE_PARK;
                case HOSPITAL:
                    return PlaceType.PLACE_TYPE_HOSPITAL;
                case ENTERTAINMENT:
                    return PlaceType.PLACE_TYPE_ENTERTAINMENT;
                case BAR:
                    return PlaceType.PLACE_TYPE_BAR;
                default:
                    return PlaceType.PLACE_TYPE_UNKNOWN;
            }
        }
    }

    public Destination() {
        this.name = "";
        this.rawAddressNotForDisplay = "";
        this.streetNumber = "";
        this.streetName = "";
        this.city = "";
        this.state = "";
        this.zipCode = "";
        this.country = "";
        this.countryCode = AddressUtils.DEFAULT_COUNTRY_CODE;
        this.placeDetailJson = "";
        this.isCalendarEvent = false;
        this.precisionLevel = Precision.UNKNOWN;
        this.type = Type.UNKNOWN;
        this.searchResultType = SearchType.UNKNOWN.getValue();
        this.hasBeenProcessed = false;
        this.rawAddressVariations = new ArrayList();
    }

    protected Destination(Parcel in) {
        boolean z = true;
        this.name = "";
        this.rawAddressNotForDisplay = "";
        this.streetNumber = "";
        this.streetName = "";
        this.city = "";
        this.state = "";
        this.zipCode = "";
        this.country = "";
        this.countryCode = AddressUtils.DEFAULT_COUNTRY_CODE;
        this.placeDetailJson = "";
        this.isCalendarEvent = false;
        this.precisionLevel = Precision.UNKNOWN;
        this.type = Type.UNKNOWN;
        this.searchResultType = SearchType.UNKNOWN.getValue();
        this.hasBeenProcessed = false;
        this.rawAddressVariations = new ArrayList();
        this.id = in.readInt();
        this.placeId = in.readString();
        this.lastPlaceIdRefresh = in.readLong();
        this.displayLat = in.readDouble();
        this.displayLong = in.readDouble();
        this.navigationLat = in.readDouble();
        this.navigationLong = in.readDouble();
        this.name = in.readString();
        this.rawAddressNotForDisplay = in.readString();
        this.streetNumber = in.readString();
        this.streetName = in.readString();
        this.city = in.readString();
        this.state = in.readString();
        this.zipCode = in.readString();
        this.country = in.readString();
        this.countryCode = in.readString();
        this.doNotSuggest = in.readInt() != 0;
        this.lastRoutedDate = in.readLong();
        this.favoriteLabel = in.readString();
        this.favoriteOrder = in.readInt();
        this.favoriteType = in.readInt();
        this.searchResultType = in.readInt();
        this.placeDetailJson = in.readString();
        this.precisionLevel = Precision.get(in.readInt());
        this.type = Type.get(in.readInt());
        if (in.readInt() == 0) {
            z = false;
        }
        this.hasBeenProcessed = z;
        this.contactLookupKey = in.readString();
        this.lastKnownContactId = in.readLong();
        this.lastContactLookup = in.readLong();
        in.readStringList(this.rawAddressVariations);
    }

    public void writeToParcel(Parcel dest, int flags) {
        int i = 1;
        dest.writeInt(this.id);
        dest.writeString(this.placeId);
        dest.writeLong(this.lastPlaceIdRefresh);
        dest.writeDouble(this.displayLat);
        dest.writeDouble(this.displayLong);
        dest.writeDouble(this.navigationLat);
        dest.writeDouble(this.navigationLong);
        dest.writeString(this.name);
        dest.writeString(this.rawAddressNotForDisplay);
        dest.writeString(this.streetNumber);
        dest.writeString(this.streetName);
        dest.writeString(this.city);
        dest.writeString(this.state);
        dest.writeString(this.zipCode);
        dest.writeString(this.country);
        dest.writeString(this.countryCode);
        dest.writeInt(this.doNotSuggest ? 1 : 0);
        dest.writeLong(this.lastRoutedDate);
        dest.writeString(this.favoriteLabel);
        dest.writeInt(this.favoriteOrder);
        dest.writeInt(this.favoriteType);
        dest.writeInt(this.searchResultType);
        dest.writeString(this.placeDetailJson);
        dest.writeInt(this.precisionLevel.getValue());
        dest.writeInt(this.type.getValue());
        if (!this.hasBeenProcessed) {
            i = 0;
        }
        dest.writeInt(i);
        dest.writeString(this.contactLookupKey);
        dest.writeLong(this.lastKnownContactId);
        dest.writeLong(this.lastContactLookup);
        dest.writeStringList(this.rawAddressVariations);
    }

    public Destination(String name, String rawAddressNotForDisplay) {
        this(0, null, 0, 0.0d, 0.0d, 0.0d, 0.0d, name, rawAddressNotForDisplay, "", "", "", "", "", "", "", false, 0, null, 0, 0, "", Precision.UNKNOWN, Type.UNKNOWN, null, -1, -1);
    }

    public Destination(int id, String placeId, double displayLat, double displayLong, double navigationLat, double navigationLong, String name, String rawAddressNotForDisplay) {
        this(id, placeId, 0, displayLat, displayLong, navigationLat, navigationLong, name, rawAddressNotForDisplay, "", "", "", "", "", "", "", false, 0, null, 0, 0, "", Precision.UNKNOWN, Type.UNKNOWN, null, -1, -1);
    }

    public Destination(int id, String placeId, long lastPlaceIdRefresh, double displayLat, double displayLong, double navigationLat, double navigationLong, String name, String rawAddressNotForDisplay, String streetNumber, String streetName, String city, String state, String zipCode, String country, String countryCode) {
        this(id, placeId, lastPlaceIdRefresh, displayLat, displayLong, navigationLat, navigationLong, name, rawAddressNotForDisplay, streetNumber, streetName, city, state, zipCode, country, countryCode, false, 0, null, 0, 0, "", Precision.UNKNOWN, Type.UNKNOWN, null, -1, -1);
    }

    public Destination(int id, String placeId, double displayLat, double displayLong, double navigationLat, double navigationLong, String name, String rawAddressNotForDisplay, long lastRoutedDate) {
        this(id, placeId, 0, displayLat, displayLong, navigationLat, navigationLong, name, rawAddressNotForDisplay, "", "", "", "", "", "", "", false, lastRoutedDate, null, 0, 0, "", Precision.UNKNOWN, Type.UNKNOWN, null, -1, -1);
    }

    public Destination(int id, String placeId, double displayLat, double displayLong, double navigationLat, double navigationLong, String name, String rawAddressNotForDisplay, long lastRoutedDate, Type type) {
        this(id, placeId, 0, displayLat, displayLong, navigationLat, navigationLong, name, rawAddressNotForDisplay, "", "", "", "", "", "", "", false, lastRoutedDate, null, 0, 0, "", Precision.UNKNOWN, type, null, -1, -1);
    }

    public Destination(int id, String placeId, long lastPlaceIdRefresh, double displayLat, double displayLong, double navigationLat, double navigationLong, String name, String rawAddressNotForDisplay, String streetNumber, String streetName, String city, String state, String zipCode, String country, String countryCode, long lastRoutedDate) {
        this(id, placeId, lastPlaceIdRefresh, displayLat, displayLong, navigationLat, navigationLong, name, rawAddressNotForDisplay, streetNumber, streetName, city, state, zipCode, country, countryCode, false, lastRoutedDate, null, 0, 0, "", Precision.UNKNOWN, Type.UNKNOWN, null, -1, -1);
    }

    public Destination(int id, String placeId, double displayLat, double displayLong, double navigationLat, double navigationLong, String name, String rawAddressNotForDisplay, String favoriteLabel, int favoriteOrder, int favoriteType) {
        this(id, placeId, 0, displayLat, displayLong, navigationLat, navigationLong, name, rawAddressNotForDisplay, "", "", "", "", "", "", "", false, 0, favoriteLabel, favoriteOrder, favoriteType, "", Precision.UNKNOWN, Type.UNKNOWN, null, -1, -1);
    }

    public Destination(int id, String placeId, long lastPlaceIdRefresh, double displayLat, double displayLong, double navigationLat, double navigationLong, String name, String rawAddressNotForDisplay, String streetNumber, String streetName, String city, String state, String zipCode, String country, String countryCode, String favoriteLabel, int favoriteOrder, int favoriteType) {
        this(id, placeId, lastPlaceIdRefresh, displayLat, displayLong, navigationLat, navigationLong, name, rawAddressNotForDisplay, streetNumber, streetName, city, state, zipCode, country, countryCode, false, 0, favoriteLabel, favoriteOrder, favoriteType, "", Precision.UNKNOWN, Type.UNKNOWN, null, -1, -1);
    }

    public Destination(@NonNull NavigationRouteResult navigationRouteResult) {
        this(0, null, getLatitude(navigationRouteResult), getLongitude(navigationRouteResult), getLatitude(navigationRouteResult), getLongitude(navigationRouteResult), navigationRouteResult.label, navigationRouteResult.address);
    }

    public Destination(@NonNull com.navdy.service.library.events.destination.Destination destination) {
        this(0, destination.place_id, destination.display_position.latitude.doubleValue(), destination.display_position.longitude.doubleValue(), destination.navigation_position.latitude.doubleValue(), destination.navigation_position.longitude.doubleValue(), destination.destination_title, destination.full_address, 0, fromProtobufPlaceType(destination.place_type));
        if (fromProtobufPlaceType(destination.place_type) == Type.CALENDAR_EVENT) {
            this.isCalendarEvent = true;
        }
    }

    public Destination(@NonNull Destination destination) {
        this();
        mergeWith(destination);
    }

    public Destination(int id, String placeIdentifier, long lastPlaceIdRefresh, double displayLat, double displayLong, double navigationLat, double navigationLong, String name, String rawAddressNotForDisplay, String streetNumber, String streetName, String city, String state, String zipCode, String country, String countryCode, boolean doNotSuggest, long lastRoutedDate, String favoriteLabel, int favoriteOrder, int favoriteType, String placeDetailJson, Precision precisionLevel, Type type, String contactLookupKey, long lastKnownContatId, long lastContactLookup) {
        this.name = "";
        this.rawAddressNotForDisplay = "";
        this.streetNumber = "";
        this.streetName = "";
        this.city = "";
        this.state = "";
        this.zipCode = "";
        this.country = "";
        this.countryCode = AddressUtils.DEFAULT_COUNTRY_CODE;
        this.placeDetailJson = "";
        this.isCalendarEvent = false;
        this.precisionLevel = Precision.UNKNOWN;
        this.type = Type.UNKNOWN;
        this.searchResultType = SearchType.UNKNOWN.getValue();
        this.hasBeenProcessed = false;
        this.rawAddressVariations = new ArrayList();
        this.id = id;
        this.lastPlaceIdRefresh = lastPlaceIdRefresh;
        this.displayLat = displayLat;
        this.displayLong = displayLong;
        this.navigationLat = navigationLat;
        this.navigationLong = navigationLong;
        this.name = name;
        this.rawAddressNotForDisplay = rawAddressNotForDisplay;
        this.streetNumber = streetNumber;
        this.streetName = streetName;
        this.city = city;
        this.state = state;
        this.zipCode = zipCode;
        this.country = country;
        this.countryCode = countryCode;
        this.doNotSuggest = doNotSuggest;
        this.lastRoutedDate = lastRoutedDate;
        this.favoriteLabel = favoriteLabel;
        this.favoriteOrder = favoriteOrder;
        this.favoriteType = favoriteType;
        this.placeDetailJson = placeDetailJson;
        this.precisionLevel = precisionLevel;
        this.type = type;
        this.contactLookupKey = contactLookupKey;
        this.lastKnownContactId = lastKnownContatId;
        this.lastContactLookup = lastContactLookup;
        setIdentifier(placeIdentifier);
    }

    public String toString() {
        String lastPlaceIdRfrshTime = CalendarUtils.getDateAndTime(this.lastPlaceIdRefresh);
        String lastRoutedDateTime = CalendarUtils.getDateAndTime(this.lastRoutedDate);
        return String.format(Locale.US, "Destination{id=%d,\tplaceId=%s,\tlastPlaceIdRefresh=%s,\tname=%s,\tType=%s,\tprecisionLevel=%s\t,address=%s,\tdisplayLat=%s,\tdisplayLong=%s,\tnavigationLat=%s,\tnavigationLong=%s,\tstreetNumber=%s,\tstreetName=%s,\tcity=%s,\tstate=%s,\tzipCode=%s,\tcountry=%s (%s),\tdoNotSuggest=%s,\tlastRoutedDate=%s,\tfavoriteLabel=%s,\tfavoriteOrder=%d,\tfavoriteType=%d,\tisCalendarEvent=%s,\tsearchResultType=%d,\tcontactLookupKey=%s,\tlastKnownContactId=%d,\tlastContactLookup=%d}", new Object[]{Integer.valueOf(this.id), this.placeId, lastPlaceIdRfrshTime, this.name, this.type.name(), this.precisionLevel.name(), this.rawAddressNotForDisplay, Double.valueOf(this.displayLat), Double.valueOf(this.displayLong), Double.valueOf(this.navigationLat), Double.valueOf(this.navigationLong), this.streetNumber, this.streetName, this.city, this.state, this.zipCode, this.country, this.countryCode, Boolean.valueOf(this.doNotSuggest), lastRoutedDateTime, this.favoriteLabel, Integer.valueOf(this.favoriteOrder), Integer.valueOf(this.favoriteType), Boolean.valueOf(this.isCalendarEvent), Integer.valueOf(this.searchResultType), this.contactLookupKey, Long.valueOf(this.lastKnownContactId), Long.valueOf(this.lastContactLookup)});
    }

    public String toShortString() {
        String lastPlaceIdRfrshTime = CalendarUtils.getDateAndTime(this.lastPlaceIdRefresh);
        String lastRoutedDateTime = CalendarUtils.getDateAndTime(this.lastRoutedDate);
        return String.format(Locale.US, "Destination{id=%d,\tplaceId=%s,\tlastPlaceIdRefresh=%s,\tname=%s,\tType=%s,\tprecisionLevel=%s\t,address=%s,\tdisplayLat=%s,\tdisplayLong=%s,\tnavigationLat=%s,\tnavigationLong=%s,\thasDetailedAddress=%s,\tdoNotSuggest=%s,\tlastRoutedDate=%s,\tfavoriteLabel=%s,\tfavoriteType=%d,\tisCalendarEvent=%s,\tsearchResultType=%d}", new Object[]{Integer.valueOf(this.id), this.placeId, lastPlaceIdRfrshTime, this.name, this.type.name(), this.precisionLevel.name(), this.rawAddressNotForDisplay, Double.valueOf(this.displayLat), Double.valueOf(this.displayLong), Double.valueOf(this.navigationLat), Double.valueOf(this.navigationLong), Boolean.valueOf(hasDetailedAddress()), Boolean.valueOf(this.doNotSuggest), lastRoutedDateTime, this.favoriteLabel, Integer.valueOf(this.favoriteType), Boolean.valueOf(this.isCalendarEvent), Integer.valueOf(this.searchResultType)});
    }

    public String toJsonString() {
        return "{\"id\":\"" + this.id + "\"," + " \"placeId\":\"" + this.placeId + "\"," + " \"name\":\"" + this.name + "\"," + " \"Type\":\"" + this.type.name() + "\"," + " \"precisionLevel\":\"" + this.precisionLevel.name() + "\"," + " \"address\":\"" + this.rawAddressNotForDisplay + "\"," + " \"displayLat\":\"" + this.displayLat + "\"," + " \"displayLong\":\"" + this.displayLong + "\"," + " \"navigationLat\":\"" + this.navigationLat + "\"," + " \"navigationLong\":\"" + this.navigationLong + "\"," + " \"streetNumber\":\"" + this.streetNumber + "\"," + " \"streetName\":\"" + this.streetName + "\"," + " \"city\":\"" + this.city + "\"," + " \"state\":\"" + this.state + "\"," + " \"zipCode\":\"" + this.zipCode + "\"," + " \"country\":\"" + this.country + "\" " + "(" + this.countryCode + ")," + " \"doNotSuggest\":\"" + this.doNotSuggest + "\"," + " \"lastRoutedDate\":\"" + CalendarUtils.getDateAndTime(this.lastRoutedDate) + "\"," + " \"favoriteLabel\":\"" + this.favoriteLabel + "\"," + " \"favoriteOrder\":\"" + this.favoriteOrder + "\"," + " \"favoriteType\":\"" + this.favoriteType + "\"," + " \"searchResultType\":\"" + this.searchResultType + "\"," + " \"lastPlaceIdRefresh\":\"" + CalendarUtils.getDateAndTime(this.lastPlaceIdRefresh) + "\"," + " \"placeDetailJson\":" + this.placeDetailJson + "\"," + " \"contactLookupKey\":\"" + this.contactLookupKey + "\"," + " \"lastKnownContactId\":\"" + this.lastKnownContactId + "\"," + " \"lastContactLookup\":\"" + this.lastContactLookup + "\"," + "}";
    }

    private static double getLatitude(@NonNull NavigationRouteResult navigationRouteResult) {
        List<Float> routeLatLongs = navigationRouteResult.routeLatLongs;
        if (routeLatLongs == null || routeLatLongs.size() <= 1) {
            return 0.0d;
        }
        return (double) ((Float) routeLatLongs.get(routeLatLongs.size() - 2)).floatValue();
    }

    private static double getLongitude(@NonNull NavigationRouteResult navigationRouteResult) {
        List<Float> routeLatLongs = navigationRouteResult.routeLatLongs;
        if (routeLatLongs == null || routeLatLongs.size() <= 1) {
            return 0.0d;
        }
        return (double) ((Float) routeLatLongs.get(routeLatLongs.size() - 1)).floatValue();
    }

    public int describeContents() {
        return 0;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPlaceId() {
        return this.placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public double getDisplayLat() {
        return this.displayLat;
    }

    public void setDisplayLat(double displayLat) {
        this.displayLat = displayLat;
    }

    public double getDisplayLng() {
        return this.displayLong;
    }

    public void setDisplayLong(double displayLong) {
        this.displayLong = displayLong;
    }

    public Coordinate getDisplayCoordinate() {
        return new Builder(null).latitude(Double.valueOf(this.displayLat)).longitude(Double.valueOf(this.displayLong)).build();
    }

    public double getNavigationLat() {
        return this.navigationLat;
    }

    public void setNavigationLat(double navigationLat) {
        this.navigationLat = navigationLat;
    }

    public double getNavigationLong() {
        return this.navigationLong;
    }

    public void setNavigationLong(double navigationLong) {
        this.navigationLong = navigationLong;
    }

    public Coordinate getNavigationCoordinate() {
        return new Builder(null).latitude(Double.valueOf(this.navigationLat)).longitude(Double.valueOf(this.navigationLong)).build();
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String getRawAddressNotForDisplay() {
        return this.rawAddressNotForDisplay;
    }

    public void setRawAddressNotForDisplay(String address) {
        this.rawAddressNotForDisplay = AddressUtils.sanitizeAddress(address);
    }

    public String getStreetNumber() {
        return this.streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getStreetName() {
        return this.streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return this.state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipCode() {
        return this.zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCountry() {
        return this.country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountryCode() {
        return this.countryCode;
    }

    public void setCountryCode(String countryCode) {
        if (countryCode != null && countryCode.length() > 2) {
            String iso2 = AddressUtils.convertIso3ToIso2(countryCode);
            if (!StringUtils.isEmptyAfterTrim(iso2)) {
                countryCode = iso2;
            }
        }
        this.countryCode = countryCode;
    }

    public boolean isDoNotSuggest() {
        return this.doNotSuggest;
    }

    public void setDoNotSuggest(boolean doNotSuggest) {
        this.doNotSuggest = doNotSuggest;
    }

    public long getLastRoutedDate() {
        return this.lastRoutedDate;
    }

    public void setLastRoutedDate(long lastRoutedDate) {
        this.lastRoutedDate = lastRoutedDate;
    }

    public int getSearchResultType() {
        return this.searchResultType;
    }

    public void setSearchResultType(int searchResultType) {
        this.searchResultType = searchResultType;
    }

    public String getFavoriteLabel() {
        if (isHome()) {
            return NavdyApplication.getAppContext().getString(R.string.home);
        }
        if (isWork()) {
            return NavdyApplication.getAppContext().getString(R.string.work);
        }
        return this.favoriteLabel;
    }

    public void setFavoriteLabel(String favoriteLabel) {
        this.favoriteLabel = favoriteLabel;
    }

    public int getFavoriteOrder() {
        return this.favoriteOrder;
    }

    public void setFavoriteOrder(int favoriteOrder) {
        this.favoriteOrder = favoriteOrder;
    }

    public int getFavoriteType() {
        return this.favoriteType;
    }

    public void setFavoriteType(int favoriteType) {
        this.favoriteType = favoriteType;
    }

    public FavoriteType getFavoriteTypeForProto() {
        switch (this.favoriteType) {
            case NavdyContentProviderConstants.FAVORITE_CONTACT /*-4*/:
                return FavoriteType.FAVORITE_CONTACT;
            case -3:
                return FavoriteType.FAVORITE_HOME;
            case -2:
                return FavoriteType.FAVORITE_WORK;
            default:
                if (isFavoriteDestination()) {
                    return FavoriteType.FAVORITE_CUSTOM;
                }
                return FavoriteType.FAVORITE_NONE;
        }
    }

    public ImageEnum getImageEnum() {
        if (getFavoriteTypeForProto() == FavoriteType.FAVORITE_CONTACT) {
            return ImageEnum.CONTACT;
        }
        return ImageEnum.PLACE;
    }

    public boolean hasAlreadyBeenProcessed() {
        return this.hasBeenProcessed;
    }

    public static boolean equals(Destination d1, Destination d2) {
        return ((d1 == null && d2 == null) || !(d1 == null || d2 == null)) && (d1 == d2 || ((d1.id > 0 && d1.id == d2.id) || ((!StringUtils.isEmptyAfterTrim(d1.placeId) && StringUtils.equalsOrBothEmptyAfterTrim(d1.placeId, d2.placeId)) || ((StringUtils.isEmptyAfterTrim(d1.placeId) || StringUtils.isEmptyAfterTrim(d2.placeId)) && haveMatchingAddresses(d1, d2)))));
    }

    private static boolean haveMatchingAddresses(Destination d1, Destination d2) {
        if (StringUtils.equalsOrBothEmptyAfterTrim(d1.rawAddressNotForDisplay, d2.rawAddressNotForDisplay)) {
            return true;
        }
        Iterator it;
        if (d1.rawAddressVariations != null) {
            it = d1.rawAddressVariations.iterator();
            while (it.hasNext()) {
                if (StringUtils.equalsOrBothEmptyAfterTrim((String) it.next(), d2.rawAddressNotForDisplay)) {
                    return true;
                }
            }
        }
        if (d2.rawAddressVariations != null) {
            it = d2.rawAddressVariations.iterator();
            while (it.hasNext()) {
                if (StringUtils.equalsOrBothEmptyAfterTrim(d1.rawAddressNotForDisplay, (String) it.next())) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean equals(Object o) {
        if (o instanceof Destination) {
            return equals(this, (Destination) o);
        }
        return super.equals(o);
    }

    public int hashCode() {
        return this.id;
    }

    public boolean isPersisted() {
        return this.id > 0;
    }

    @NonNull
    public static List<Destination> mergeAndDeduplicateLists(@Nullable List<Destination> list1, @Nullable List<Destination> list2) {
        if (list1 == null) {
            list1 = new ArrayList(0);
        }
        if (list2 == null) {
            list2 = new ArrayList(0);
        }
        int initialCapacity = list1.size() + list2.size();
        if (initialCapacity == 0) {
            return new ArrayList(0);
        }
        List<Destination> destinations = new ArrayList(initialCapacity);
        destinations.addAll(list1);
        for (Destination destination2 : list2) {
            boolean merged = false;
            for (Destination destination : list1) {
                Destination destination1 = new Destination(destination);
                if (destination1.equals(destination2)) {
                    destination1.mergeWith(destination2);
                    merged = true;
                    break;
                }
            }
            if (!merged) {
                destinations.add(destination2);
            }
        }
        return destinations;
    }

    private void mergeWith(Destination destination) {
        boolean z;
        boolean z2 = false;
        if (this.id <= 0 && destination.id > 0) {
            this.id = destination.id;
        }
        if (StringUtils.isEmptyAfterTrim(this.placeId) && !StringUtils.isEmptyAfterTrim(destination.placeId)) {
            this.placeId = destination.placeId;
        }
        if (destination.lastPlaceIdRefresh > this.lastPlaceIdRefresh) {
            this.lastPlaceIdRefresh = destination.lastPlaceIdRefresh;
        }
        if (!hasValidDisplayCoordinates() && destination.hasValidDisplayCoordinates()) {
            this.displayLat = destination.displayLat;
            this.displayLong = destination.displayLong;
        }
        if (!hasValidNavCoordinates() && destination.hasValidNavCoordinates()) {
            this.navigationLat = destination.navigationLat;
            this.navigationLong = destination.navigationLong;
        }
        if (StringUtils.isEmptyAfterTrim(this.name) && !StringUtils.isEmptyAfterTrim(destination.name)) {
            this.name = destination.name;
        }
        if (StringUtils.isEmptyAfterTrim(this.rawAddressNotForDisplay) && !StringUtils.isEmptyAfterTrim(destination.rawAddressNotForDisplay)) {
            this.rawAddressNotForDisplay = destination.rawAddressNotForDisplay;
        }
        if (StringUtils.isEmptyAfterTrim(this.streetNumber) && !StringUtils.isEmptyAfterTrim(destination.streetNumber)) {
            this.streetNumber = destination.streetNumber;
        }
        if (StringUtils.isEmptyAfterTrim(this.streetName) && !StringUtils.isEmptyAfterTrim(destination.streetName)) {
            this.streetName = destination.streetName;
        }
        if (StringUtils.isEmptyAfterTrim(this.city) && !StringUtils.isEmptyAfterTrim(destination.city)) {
            this.city = destination.city;
        }
        if (StringUtils.isEmptyAfterTrim(this.state) && !StringUtils.isEmptyAfterTrim(destination.state)) {
            this.state = destination.state;
        }
        if (StringUtils.isEmptyAfterTrim(this.zipCode) && !StringUtils.isEmptyAfterTrim(destination.zipCode)) {
            this.zipCode = destination.zipCode;
        }
        if (StringUtils.isEmptyAfterTrim(this.country) && !StringUtils.isEmptyAfterTrim(destination.country)) {
            this.country = destination.country;
        }
        if (StringUtils.isEmptyAfterTrim(this.countryCode) && !StringUtils.isEmptyAfterTrim(destination.countryCode)) {
            this.countryCode = destination.countryCode;
        }
        if (!this.doNotSuggest) {
            this.doNotSuggest = destination.doNotSuggest;
        }
        if (destination.lastRoutedDate > this.lastRoutedDate) {
            this.lastRoutedDate = destination.lastRoutedDate;
        }
        if (StringUtils.isEmptyAfterTrim(this.favoriteLabel) && !StringUtils.isEmptyAfterTrim(destination.favoriteLabel)) {
            this.favoriteLabel = destination.favoriteLabel;
        }
        if (this.favoriteOrder <= 0 && destination.favoriteOrder > 0) {
            this.favoriteOrder = destination.favoriteOrder;
        }
        if (this.favoriteType == 0 && destination.favoriteType != 0) {
            this.favoriteType = destination.favoriteType;
        }
        if (!StringUtils.isEmptyAfterTrim(destination.placeDetailJson) && (StringUtils.isEmptyAfterTrim(this.placeDetailJson) || this.lastPlaceIdRefresh < destination.lastPlaceIdRefresh)) {
            this.placeDetailJson = destination.placeDetailJson;
        }
        if (this.precisionLevel == Precision.UNKNOWN && destination.precisionLevel != Precision.UNKNOWN) {
            this.precisionLevel = destination.precisionLevel;
        }
        if (this.type == Type.UNKNOWN && destination.type != Type.UNKNOWN) {
            this.type = destination.type;
        }
        if (this.hasBeenProcessed || destination.hasBeenProcessed) {
            z = true;
        } else {
            z = false;
        }
        this.hasBeenProcessed = z;
        if (this.isCalendarEvent || destination.isCalendarEvent) {
            z2 = true;
        }
        this.isCalendarEvent = z2;
        if ((destination.lastContactLookup > 0 && destination.lastContactLookup > this.lastContactLookup) || (StringUtils.isEmptyAfterTrim(this.contactLookupKey) && this.lastKnownContactId <= 0)) {
            this.contactLookupKey = destination.contactLookupKey;
            this.lastKnownContactId = destination.lastKnownContactId;
            this.lastContactLookup = destination.lastContactLookup;
        }
        if (destination.rawAddressVariations != null) {
            Iterator it = destination.rawAddressVariations.iterator();
            while (it.hasNext()) {
                String rawAddressVariation = (String) it.next();
                boolean found = false;
                if (this.rawAddressVariations != null) {
                    Iterator it2 = this.rawAddressVariations.iterator();
                    while (it2.hasNext()) {
                        if (StringUtils.equalsOrBothEmptyAfterTrim((String) it2.next(), rawAddressVariation)) {
                            found = true;
                            break;
                        }
                    }
                }
                this.rawAddressVariations = new ArrayList();
                if (!found) {
                    this.rawAddressVariations.add(rawAddressVariation);
                }
            }
        }
    }

    @NonNull
    public ArrayList<String> getAddressLines() {
        ArrayList<String> lines = new ArrayList();
        if (hasDetailedAddress()) {
            AddressFormat addressFormat = AddressUtils.getAddressFormatFor(this);
            if (addressFormat != null) {
                if (StringUtils.equalsOrBothEmptyAfterTrim(this.countryCode, NavdyLocationManager.getLastKnownCountryCode())) {
                    lines = addressFormat.localLines;
                } else {
                    lines = addressFormat.foreignLines;
                }
                for (int i = 0; i < lines.size(); i++) {
                    lines.set(i, AddressUtils.insertAddressPartsInFormat((String) lines.get(i), this));
                }
                return lines;
            }
        }
        if (!StringUtils.isEmptyAfterTrim(this.rawAddressNotForDisplay)) {
            this.rawAddressNotForDisplay = AddressUtils.sanitizeAddress(this.rawAddressNotForDisplay);
            if (this.rawAddressNotForDisplay.contains(",")) {
                String[] splitAddress = this.rawAddressNotForDisplay.split(", *");
                if (splitAddress.length > 0) {
                    return new ArrayList(Arrays.asList(splitAddress));
                }
            }
            int nbChars = NavdyApplication.getAppContext().getResources().getInteger(R.integer.nb_first_line_address_char);
            if (this.rawAddressNotForDisplay.length() < nbChars) {
                lines.add(this.rawAddressNotForDisplay);
            } else {
                String[] addressParts = this.rawAddressNotForDisplay.split(" ");
                StringBuilder firstLineSb = new StringBuilder(addressParts[0]);
                StringBuilder secondLineSb = new StringBuilder();
                int index = 1;
                while (firstLineSb.length() < nbChars && index < addressParts.length) {
                    firstLineSb.append(" ");
                    firstLineSb.append(addressParts[index]);
                    index++;
                }
                while (index < addressParts.length) {
                    secondLineSb.append(addressParts[index]);
                    index++;
                    if (index < addressParts.length) {
                        secondLineSb.append(" ");
                    }
                }
                lines.add(firstLineSb.toString());
                if (secondLineSb.length() > 0) {
                    lines.add(secondLineSb.toString());
                }
            }
        } else if (hasValidDisplayCoordinates()) {
            Context context = NavdyApplication.getAppContext();
            lines.add(context.getString(R.string.latitude_is, new Object[]{Double.valueOf(this.displayLat)}));
            lines.add(context.getString(R.string.longitude_is, new Object[]{Double.valueOf(this.displayLong)}));
        }
        return lines;
    }

    public String getAddressForDisplay() {
        ArrayList<String> lines = getAddressLines();
        if (lines.size() > 0) {
            return AddressUtils.joinRemainingLines(lines, 0);
        }
        return this.rawAddressNotForDisplay;
    }

    public Pair<String, String> getSplitAddress() {
        String firstLine;
        String secondLine;
        ArrayList<String> addressLines = getAddressLines();
        if (addressLines.size() <= 0) {
            firstLine = this.rawAddressNotForDisplay;
            secondLine = "";
        } else {
            firstLine = (String) addressLines.get(0);
            secondLine = AddressUtils.joinRemainingLines(addressLines, 1);
        }
        return new Pair(firstLine.trim(), secondLine.trim());
    }

    public Pair<String, String> getTitleAndSubtitle() {
        String destinationSubtitle;
        String destinationTitle = getTitle();
        ArrayList<String> lines = getAddressLines();
        if (lines.size() > 0 && StringUtils.isEmptyAfterTrim(destinationTitle)) {
            destinationTitle = (String) lines.get(0);
            destinationSubtitle = AddressUtils.joinRemainingLines(lines, 1);
        } else if ((lines.size() > 0 && AddressUtils.isTitleIsInTheAddress(destinationTitle, AddressUtils.joinRemainingLines(lines, 0))) || AddressUtils.isTitleIsInTheAddress(destinationTitle, this.rawAddressNotForDisplay)) {
            destinationSubtitle = AddressUtils.joinRemainingLines(lines, 1);
        } else if (lines.size() > 0) {
            destinationSubtitle = AddressUtils.joinRemainingLines(lines, 0);
        } else {
            destinationSubtitle = this.rawAddressNotForDisplay;
        }
        return new Pair(AddressUtils.sanitizeAddress(destinationTitle), AddressUtils.sanitizeAddress(destinationSubtitle));
    }

    @Nullable
    public String getTitle() {
        String destinationTitle = null;
        if (isFavoriteDestination()) {
            if (this.favoriteType == -3) {
                destinationTitle = NavdyApplication.getAppContext().getString(R.string.home);
            } else if (this.favoriteType == -2) {
                destinationTitle = NavdyApplication.getAppContext().getString(R.string.work);
            } else if (!StringUtils.isEmptyAfterTrim(this.favoriteLabel)) {
                destinationTitle = this.favoriteLabel;
            }
        }
        if (!StringUtils.isEmptyAfterTrim(destinationTitle) || StringUtils.isEmptyAfterTrim(this.name)) {
            return destinationTitle;
        }
        return this.name;
    }

    @NonNull
    public MultilineAddress getMultilineAddress() {
        return new MultilineAddress(this);
    }

    public boolean hasDetailedAddress() {
        return (StringUtils.isEmptyAfterTrim(this.streetName) || StringUtils.isEmptyAfterTrim(this.city) || StringUtils.isEmptyAfterTrim(this.countryCode)) ? false : true;
    }

    public boolean hasStreetNumber() {
        if (!StringUtils.isEmptyAfterTrim(this.streetNumber)) {
            return true;
        }
        if (this.rawAddressNotForDisplay != null) {
            String streetAddress = getSplitAddress().first;
            if (!StringUtils.isEmptyAfterTrim(streetAddress) && streetAddress.matches("^[0-9]+[\\u0009\\u000A\\u000B\\u000C\\u000D\\u0020\\u0085\\u00A0\\u1680\\u180E\\u2000\\u2001\\u2002\\u2003\\u2004\\u2005\\u2006\\u2007\\u2008\\u2009\\u200A\\u2028\\u2029\\u202F\\u205F\\u3000]")) {
                return true;
            }
        }
        return false;
    }

    private void setAddressFieldsFromHereAddress(Address hereAddress) {
        if (hereAddress == null) {
            logger.e("address is null");
            return;
        }
        this.country = hereAddress.getCountryName();
        if (!StringUtils.isEmptyAfterTrim(hereAddress.getCountryCode())) {
            this.countryCode = AddressUtils.convertIso3ToIso2(hereAddress.getCountryCode());
        }
        this.city = hereAddress.getCity();
        this.zipCode = hereAddress.getPostalCode();
        this.streetName = hereAddress.getStreet();
        this.state = hereAddress.getState();
        String hereAddressText = StringUtils.fromHtml(hereAddress.getText()).toString();
        if (!StringUtils.isEmptyAfterTrim(hereAddressText)) {
            hereAddressText = hereAddressText.replaceAll("\n\n", " ").replaceAll("\n", " ");
        }
        this.rawAddressNotForDisplay = hereAddressText;
        this.streetNumber = hereAddress.getHouseNumber();
    }

    public com.navdy.service.library.events.destination.Destination toProtobufDestinationObject() {
        FavoriteType favoriteType = getFavoriteTypeForProto();
        Pair<String, String> titleAndSubtitle = AddressUtils.cleanUpDebugPrefix(getTitleAndSubtitle());
        String firstLine = titleAndSubtitle.first;
        String secondLine = titleAndSubtitle.second;
        ArrayList<PhoneNumber> phoneNumbers = getPhoneNumbers();
        return new com.navdy.service.library.events.destination.Destination.Builder(null).navigation_position(new LatLong(Double.valueOf(this.navigationLat), Double.valueOf(this.navigationLong))).display_position(new LatLong(Double.valueOf(this.displayLat), Double.valueOf(this.displayLong))).full_address(this.rawAddressNotForDisplay).destination_title(firstLine).destination_subtitle(secondLine).place_type(this.type.toProtobufPlaceType()).favorite_type(favoriteType).identifier(Integer.toString(this.id)).place_id(getPlaceIndentifier()).last_navigated_to(Long.valueOf(this.lastRoutedDate)).phoneNumbers(phoneNumbers).contacts(getContacts()).build();
    }

    public String getPlaceIndentifier() {
        if (!StringUtils.isEmptyAfterTrim(this.placeId)) {
            return IDENTIFIER_NAMESPACE_PLACE_ID + this.placeId;
        }
        if (this.lastKnownContactId > 0 || !StringUtils.isEmptyAfterTrim(this.contactLookupKey)) {
            return IDENTIFIER_NAMESPACE_CONTACT + this.lastKnownContactId + IDENTIFIER_CONTACT_ID_SEPARATOR + this.contactLookupKey;
        }
        return IDENTIFIER_NAMESPACE_DB_ID + this.id;
    }

    public void setIdentifier(String identifier) {
        if (!StringUtils.isEmptyAfterTrim(identifier)) {
            if (identifier.startsWith(IDENTIFIER_NAMESPACE_PLACE_ID)) {
                this.placeId = identifier.substring(IDENTIFIER_NAMESPACE_PLACE_ID.length());
            } else if (identifier.startsWith(IDENTIFIER_NAMESPACE_CONTACT) && identifier.contains(IDENTIFIER_CONTACT_ID_SEPARATOR)) {
                String tmp = identifier.substring(IDENTIFIER_NAMESPACE_CONTACT.length());
                int i = tmp.indexOf(IDENTIFIER_CONTACT_ID_SEPARATOR);
                if (i > 0 && i < tmp.length()) {
                    try {
                        this.lastKnownContactId = (long) Integer.parseInt(tmp.substring(0, i));
                    } catch (NumberFormatException e) {
                    }
                }
                String lookupString = tmp.substring(i + 1);
                if (!StringUtils.equalsOrBothEmptyAfterTrim(lookupString, Constants.NULL_VERSION_ID)) {
                    this.contactLookupKey = lookupString;
                }
                this.lastContactLookup = new Date().getTime();
            } else {
                if (identifier.startsWith(IDENTIFIER_NAMESPACE_DB_ID)) {
                    identifier = identifier.substring(IDENTIFIER_NAMESPACE_DB_ID.length());
                }
                try {
                    this.id = Integer.parseInt(identifier);
                } catch (NumberFormatException e2) {
                    this.placeId = identifier;
                }
            }
        }
    }

    private ArrayList<PhoneNumber> getPhoneNumbers() {
        ArrayList<PhoneNumber> phoneNumbers = new ArrayList();
        if (StringUtils.isEmptyAfterTrim(this.placeDetailJson) && StringUtils.isEmptyAfterTrim(this.contactLookupKey)) {
            logger.d("No phone number because no placeDetailJson: " + this.placeDetailJson + " and no contactLookupKey: " + this.contactLookupKey + " for destination: " + this);
        } else if (!StringUtils.isEmptyAfterTrim(this.placeDetailJson)) {
            try {
                JSONObject jsonObject = new JSONObject(this.placeDetailJson);
                String number = jsonObject.optString("formatted_phone_number");
                logger.d("formatted_phone_number = " + number);
                if (StringUtils.isEmptyAfterTrim(number)) {
                    number = jsonObject.optString("formatted_phone_number");
                    logger.d("formatted_phone_number = " + number);
                }
                if (!StringUtils.isEmptyAfterTrim(number)) {
                    phoneNumbers.add(new PhoneNumber.Builder(null).number(number).build());
                }
            } catch (JSONException e) {
                logger.e("Unable to parse placeDetailJson: " + this.placeDetailJson, e);
            }
        }
        return phoneNumbers;
    }

    public ArrayList<Contact> getContacts() {
        ArrayList<Contact> contacts = new ArrayList();
        logger.d("contactLookupKey = " + this.contactLookupKey + ", lastKnownContactId = " + this.lastKnownContactId);
        if (StringUtils.isEmptyAfterTrim(this.contactLookupKey)) {
            return contacts;
        }
        ContentResolver contentResolver = NavdyApplication.getAppContext().getContentResolver();
        long contactId = ContactsManager.getContactIdFromLookupKey(contentResolver, this.contactLookupKey, this.lastKnownContactId);
        if (contactId < 0) {
            contactId = this.lastKnownContactId;
        }
        List<PhoneNumberModel> numbers = ContactsManager.getPhoneNumbers(contentResolver, contactId);
        if (numbers.size() > 0) {
            String contactName = ContactsManager.getContactName(contentResolver, contactId);
            ArrayList<Contact> primary = new ArrayList();
            ArrayList<Contact> mobile = new ArrayList();
            ArrayList<Contact> home = new ArrayList();
            ArrayList<Contact> work = new ArrayList();
            ArrayList<Contact> other = new ArrayList();
            for (PhoneNumberModel num : numbers) {
                logger.d("number = " + num + " for destination: " + this);
                if (!num.isFax()) {
                    Contact.Builder builder = new Contact.Builder(null).name(contactName).number(num.number).numberType(num.getPhoneNumberProtobufType()).label(num.customType);
                    if (!num.isPrimary) {
                        switch (num.type) {
                            case 1:
                                home.add(builder.build());
                                break;
                            case 2:
                                mobile.add(builder.build());
                                break;
                            case 3:
                                work.add(builder.build());
                                break;
                            default:
                                other.add(0, builder.build());
                                break;
                        }
                    }
                    primary.add(builder.build());
                }
            }
            contacts.addAll(primary);
            contacts.addAll(mobile);
            contacts.addAll(home);
            contacts.addAll(work);
            contacts.addAll(other);
            return deDuplicatePhoneNumbersWhileKeepingOrder(contacts);
        }
        logger.d("No phone numbers for contact lookup key: " + this.contactLookupKey);
        return contacts;
    }

    @Nullable
    @CheckResult
    private <T> ArrayList<T> deDuplicatePhoneNumbersWhileKeepingOrder(@Nullable ArrayList<T> list) {
        if (list == null || list.size() <= 0) {
            return list;
        }
        ArrayList<T> dedupped = new ArrayList(list.size());
        for (int i = 0; i < list.size(); i++) {
            boolean foundDuplicate = false;
            T number = list.get(i);
            String num = getComparablePhoneNumber(number);
            if (!StringUtils.isEmptyAfterTrim(num)) {
                for (int j = 0; j < i; j++) {
                    if (num.equals(getComparablePhoneNumber(list.get(j)))) {
                        foundDuplicate = true;
                        break;
                    }
                }
                if (!foundDuplicate) {
                    dedupped.add(number);
                }
            }
        }
        return dedupped;
    }

    private String getComparablePhoneNumber(Object number) {
        if (number == null) {
            return null;
        }
        if (!(number instanceof PhoneNumber) && !(number instanceof Contact)) {
            return null;
        }
        String num;
        if (number instanceof PhoneNumber) {
            num = ((PhoneNumber) number).number;
        } else {
            num = ((Contact) number).number;
        }
        if (StringUtils.isEmptyAfterTrim(num)) {
            return null;
        }
        return num.replaceAll("[^0-9+]", "");
    }

    @Nullable
    public static ArrayList<com.navdy.service.library.events.destination.Destination> convertDestinationsToProto(List<Destination> destinations) {
        if (destinations == null || destinations.isEmpty()) {
            return null;
        }
        ArrayList<com.navdy.service.library.events.destination.Destination> protobufDestinations = new ArrayList(destinations.size());
        for (Destination destination : destinations) {
            if (destination != null) {
                protobufDestinations.add(destination.toProtobufDestinationObject());
            }
        }
        return protobufDestinations;
    }

    public boolean isPlaceDetailsInfoStale() {
        return this.lastPlaceIdRefresh < new Date().getTime() - PLACE_ID_REFRESH_LIMIT || StringUtils.isEmptyAfterTrim(this.placeDetailJson);
    }

    public void refreshPlaceIdDataAndUpdateListsAsync() {
        refreshPlaceIdDataAndUpdateListsAsync(null);
    }

    public void refreshPlaceIdDataAndUpdateListsAsync(final Runnable callback) {
        if (!NetworkUtils.isNetworkAvailable()) {
            logger.i("Unable to refresh google place details due to lack of internet access.");
            if (callback != null) {
                callback.run();
            }
        } else if (StringUtils.isEmptyAfterTrim(this.placeId)) {
            logger.i("No place ID so no refresh of google place details.");
            if (callback != null) {
                callback.run();
            }
        } else if (isPlaceDetailsInfoStale()) {
            logger.d("Refresh google place details for: " + this);
            new GooglePlacesSearch(new GoogleSearchListener() {
                public void onGoogleSearchResult(final List<GoogleTextSearchDestinationResult> destinations, Query queryType, final Error error) {
                    TaskManager.getInstance().execute(new Runnable() {
                        public void run() {
                            if (error != Error.NONE) {
                                Destination.logger.e("received error while refreshing google place details: " + error.name());
                                if (callback != null) {
                                    callback.run();
                                }
                            } else if (destinations != null && !destinations.isEmpty()) {
                                ((GoogleTextSearchDestinationResult) destinations.get(0)).toModelDestinationObject(SearchType.DETAILS, Destination.this);
                                if (callback != null) {
                                    callback.run();
                                }
                                Destination.this.persistPlaceDetailInfoAndUpdateLists();
                            }
                        }
                    }, 1, TaskPriority.LOW);
                }
            }).runDetailsSearchWebApi(this.placeId);
        } else {
            logger.i("No need to refresh google place details at this point.");
            if (callback != null) {
                callback.run();
            }
        }
    }

    public void persistPlaceDetailInfoAndUpdateListsAsync() {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                Destination.this.persistPlaceDetailInfoAndUpdateLists();
            }
        }, 1, TaskPriority.LOW);
    }

    private void persistPlaceDetailInfoAndUpdateLists() {
        updateDestinationLists(persistPlaceDetailInfo());
    }

    public void updateDestinationListsAsync() {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                Destination.this.persistPlaceDetailInfoAndUpdateLists();
            }
        }, 1, TaskPriority.LOW);
    }

    private void updateDestinationLists(int nbRows) {
        if (nbRows > 0) {
            boolean isFavoriteDestination = isFavoriteDestination();
            if (!isFavoriteDestination && (nbRows > 1 || this.id <= 0)) {
                isFavoriteDestination = isFavoriteDestination(this.placeId);
            }
            if (isFavoriteDestination) {
                VersioningUtils.increaseVersionAndSendToDisplay(DestinationType.FAVORITES);
            }
            boolean isSuggestion = false;
            Iterator it = SuggestionManager.getSuggestions().iterator();
            while (it.hasNext()) {
                Suggestion suggestion = (Suggestion) it.next();
                if (!(suggestion == null || suggestion.destination == null || !StringUtils.equalsOrBothEmptyAfterTrim(suggestion.destination.placeId, this.placeId))) {
                    isSuggestion = true;
                }
            }
            if (isSuggestion) {
                SuggestionManager.buildSuggestionList(true);
            }
        }
    }

    public int persistPlaceDetailInfo() {
        if (StringUtils.isEmptyAfterTrim(this.placeId)) {
            return -1;
        }
        ContentResolver contentResolver = NavdyApplication.getAppContext().getContentResolver();
        if (contentResolver != null) {
            return contentResolver.update(NavdyContentProviderConstants.DESTINATIONS_CONTENT_URI, getPlaceDetailInfoContentValues(), "place_id=?", new String[]{this.placeId});
        }
        logger.e("Unable to get contentResolver !");
        return -1;
    }

    public void reloadSelfFromDatabase() {
        Destination dbDestination = NavdyContentProvider.getThisDestination(this);
        if (dbDestination == null) {
            logger.d("reloadSelfFromDatabase: destination not found in DB");
        } else {
            mergeWith(dbDestination);
        }
    }

    public void reloadSelfFromDatabaseAsync(final Runnable onFinish) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                Destination.this.reloadSelfFromDatabase();
                onFinish.run();
            }
        }, 1);
    }

    public int findInDb() {
        int id = -1;
        try {
            ContentResolver contentResolver = NavdyApplication.getAppContext().getContentResolver();
            Pair<String, String[]> comparisonSelectionClause = getComparisonSelectionClause();
            if (comparisonSelectionClause == null) {
                return -1;
            }
            Cursor c = contentResolver.query(NavdyContentProviderConstants.DESTINATIONS_CONTENT_URI, new String[]{"_id"}, (String) comparisonSelectionClause.first, (String[]) comparisonSelectionClause.second, null);
            if (c != null && c.moveToFirst()) {
                id = c.getInt(0);
                logger.d("Found this destination in the db under id: " + id);
            }
            IOUtils.closeStream(c);
            return id;
        } finally {
            IOUtils.closeStream(null);
        }
    }

    private void makeSureWeHaveDestinationInDb() {
        if (!isPersisted()) {
            this.id = findInDb();
            if (this.id > 0) {
                logger.v("Found this ID: " + this.id);
                return;
            }
            logger.e("Couldn't find a matching destination in the db");
            Uri uri = saveToDb();
            if (uri != null) {
                try {
                    this.id = Integer.parseInt(uri.getLastPathSegment());
                } catch (NumberFormatException e) {
                    logger.e("Unable to parse destination id from the URI path segment", e);
                }
            }
        }
    }

    @Nullable
    public Pair<String, String[]> getComparisonSelectionClause() {
        String selection;
        String[] selectionArgs;
        if (!StringUtils.isEmptyAfterTrim(this.placeId) && StringUtils.isEmptyAfterTrim(this.rawAddressNotForDisplay)) {
            selection = "place_id=?";
            selectionArgs = new String[]{this.placeId};
        } else if (!StringUtils.isEmptyAfterTrim(this.placeId) && !StringUtils.isEmptyAfterTrim(this.rawAddressNotForDisplay)) {
            selection = "(place_id is not null AND place_id=?) OR (place_id is null AND address=?)";
            selectionArgs = new String[]{this.placeId, this.rawAddressNotForDisplay};
        } else if (!StringUtils.isEmptyAfterTrim(this.placeId) || StringUtils.isEmptyAfterTrim(this.rawAddressNotForDisplay)) {
            return null;
        } else {
            StringBuilder selectionBuilder = new StringBuilder("address=?");
            ArrayList<String> args = new ArrayList();
            args.add(this.rawAddressNotForDisplay);
            if (this.rawAddressVariations != null) {
                Iterator it = this.rawAddressVariations.iterator();
                while (it.hasNext()) {
                    selectionBuilder.append(SQL.OR).append((String) it.next()).append("=?");
                }
            }
            selection = selectionBuilder.toString();
            selectionArgs = (String[]) args.toArray(new String[args.size()]);
        }
        return new Pair(selection, selectionArgs);
    }

    private int updateDb(ContentValues cv) {
        makeSureWeHaveDestinationInDb();
        ContentResolver cr = NavdyApplication.getAppContext().getContentResolver();
        if (cr != null) {
            int nbRows = cr.update(NavdyContentProviderConstants.DESTINATIONS_CONTENT_URI, cv, String.format("%s=?", new Object[]{"_id"}), new String[]{String.valueOf(this.id)});
            logger.d("update returned " + nbRows + " rows.");
            return nbRows;
        }
        logger.e("Unable to get content resolver !");
        return 0;
    }

    private void updateAllColumnsOrInsertNewEntryInDbAsync() {
        updateAllColumnsOrInsertNewEntryInDbAsync(null);
    }

    private void updateAllColumnsOrInsertNewEntryInDbAsync(final QueryResultCallback callback) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                Uri uri = Destination.this.updateAllColumnsOrInsertNewEntryInDb();
                if (callback != null) {
                    callback.onQueryCompleted(uri != null ? 1 : 0, uri);
                }
            }
        }, 1);
    }

    @WorkerThread
    public Uri updateAllColumnsOrInsertNewEntryInDb() {
        logger.v("Saving this destination to the DB: " + toString());
        SystemUtils.ensureNotOnMainThread();
        if (((long) this.id) <= 0) {
            this.id = findInDb();
            if (((long) this.id) < 0) {
                return saveToDb();
            }
        }
        updateDb(getAllContentValues());
        return Uri.withAppendedPath(NavdyContentProviderConstants.DESTINATIONS_CONTENT_URI, String.valueOf(this.id));
    }

    public void updateDoNotSuggestAsync() {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                Destination.this.updateDoNotSuggest();
            }
        }, 1);
    }

    private void updateDoNotSuggest() {
        if (this.id <= 0) {
            logger.e("Trying to updateDoNotSuggest on an object that has no ID!");
            return;
        }
        ContentResolver contentResolver = NavdyApplication.getAppContext().getContentResolver();
        if (contentResolver == null) {
            logger.e("Unable to get a content resolver");
            return;
        }
        ContentValues contentValues = new ContentValues(1);
        contentValues.put(NavdyContentProviderConstants.DESTINATIONS_DO_NOT_SUGGEST, Boolean.valueOf(this.doNotSuggest));
        contentResolver.update(NavdyContentProviderConstants.DESTINATIONS_CONTENT_URI, contentValues, "_id=?", new String[]{String.valueOf(this.id)});
    }

    private void saveToDbAsync() {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                Destination.this.saveToDb();
            }
        }, 1);
    }

    private Uri saveToDb() {
        ContentResolver contentResolver = NavdyApplication.getAppContext().getContentResolver();
        if (contentResolver != null) {
            Uri uri = contentResolver.insert(NavdyContentProviderConstants.DESTINATIONS_CONTENT_URI, getAllContentValues());
            if (uri != null) {
                this.id = Integer.parseInt(uri.getLastPathSegment());
            }
            if (isRecentDestination()) {
                VersioningUtils.increaseVersionAndSendToDisplay(DestinationType.SUGGESTIONS);
            }
            if (!isFavoriteDestination()) {
                return uri;
            }
            Tracker.tagEvent(Event.SAVING_A_FAVORITE);
            VersioningUtils.increaseVersionAndSendToDisplay(DestinationType.FAVORITES);
            return uri;
        }
        logger.e("Unable to get contentResolver !");
        return null;
    }

    @NonNull
    private ContentValues getPlaceDetailInfoContentValues() {
        return getContentValues(false);
    }

    @NonNull
    private ContentValues getAllContentValues() {
        return getContentValues(true);
    }

    @NonNull
    private ContentValues getContentValues(boolean includeExtras) {
        ContentValues destinationValues = new ContentValues();
        destinationValues.put(NavdyContentProviderConstants.DESTINATIONS_PLACE_ID, this.placeId);
        destinationValues.put(NavdyContentProviderConstants.DESTINATIONS_LAST_PLACE_ID_REFRESH, Long.valueOf(this.lastPlaceIdRefresh));
        destinationValues.put(NavdyContentProviderConstants.DESTINATIONS_DISPLAY_LAT, Double.valueOf(this.displayLat));
        destinationValues.put(NavdyContentProviderConstants.DESTINATIONS_DISPLAY_LONG, Double.valueOf(this.displayLong));
        destinationValues.put(NavdyContentProviderConstants.DESTINATIONS_NAVIGATION_LAT, Double.valueOf(this.navigationLat));
        destinationValues.put(NavdyContentProviderConstants.DESTINATIONS_NAVIGATION_LONG, Double.valueOf(this.navigationLong));
        if (this.name == null) {
            this.name = "";
        }
        destinationValues.put(NavdyContentProviderConstants.DESTINATIONS_NAME, this.name);
        this.rawAddressNotForDisplay = AddressUtils.sanitizeAddress(this.rawAddressNotForDisplay);
        destinationValues.put(NavdyContentProviderConstants.DESTINATIONS_ADDRESS, this.rawAddressNotForDisplay);
        if (!StringUtils.isEmptyAfterTrim(this.streetNumber)) {
            destinationValues.put(NavdyContentProviderConstants.DESTINATIONS_STREET_NUMBER, this.streetNumber);
        }
        if (!StringUtils.isEmptyAfterTrim(this.streetName)) {
            destinationValues.put(NavdyContentProviderConstants.DESTINATIONS_STREET_NAME, this.streetName);
        }
        if (!StringUtils.isEmptyAfterTrim(this.city)) {
            destinationValues.put(NavdyContentProviderConstants.DESTINATIONS_CITY, this.city);
        }
        if (!StringUtils.isEmptyAfterTrim(this.state)) {
            destinationValues.put("state", this.state);
        }
        if (!StringUtils.isEmptyAfterTrim(this.zipCode)) {
            destinationValues.put(NavdyContentProviderConstants.DESTINATIONS_ZIP_CODE, this.zipCode);
        }
        if (!StringUtils.isEmptyAfterTrim(this.country)) {
            destinationValues.put(NavdyContentProviderConstants.DESTINATIONS_COUNTRY, this.country);
        }
        if (!StringUtils.isEmptyAfterTrim(this.countryCode)) {
            destinationValues.put(NavdyContentProviderConstants.DESTINATIONS_COUNTRY_CODE, this.countryCode);
        }
        if (includeExtras) {
            destinationValues.put(NavdyContentProviderConstants.DESTINATIONS_DO_NOT_SUGGEST, Integer.valueOf(this.doNotSuggest ? 1 : 0));
            destinationValues.put(NavdyContentProviderConstants.DESTINATIONS_LAST_ROUTED_DATE, Long.valueOf(this.lastRoutedDate));
            destinationValues.put(NavdyContentProviderConstants.DESTINATIONS_LABEL, this.favoriteLabel);
            destinationValues.put(NavdyContentProviderConstants.DESTINATIONS_ORDER, Integer.valueOf(this.favoriteOrder));
            destinationValues.put(NavdyContentProviderConstants.DESTINATIONS_FAVORITE_TYPE, Integer.valueOf(this.favoriteType));
        }
        if (!StringUtils.isEmptyAfterTrim(this.placeDetailJson)) {
            destinationValues.put(NavdyContentProviderConstants.DESTINATIONS_PLACE_DETAIL_JSON, this.placeDetailJson);
        }
        destinationValues.put(NavdyContentProviderConstants.DESTINATIONS_PRECISION_LEVEL, Integer.valueOf(this.precisionLevel.getValue()));
        destinationValues.put("type", Integer.valueOf(this.type.getValue()));
        if (!StringUtils.isEmptyAfterTrim(this.contactLookupKey)) {
            destinationValues.put(NavdyContentProviderConstants.DESTINATIONS_CONTACT_LOOKUP_KEY, this.contactLookupKey);
        }
        if (this.lastKnownContactId > 0) {
            destinationValues.put(NavdyContentProviderConstants.DESTINATIONS_LAST_KNOWN_CONTACT_ID, Long.valueOf(this.lastKnownContactId));
        }
        if (this.lastContactLookup > 0) {
            destinationValues.put(NavdyContentProviderConstants.DESTINATIONS_LAST_CONTACT_LOOKUP, Long.valueOf(this.lastContactLookup));
        }
        return destinationValues;
    }

    private boolean shouldDeleteDestinationEntryAsWell() {
        return (isFavoriteDestination() || isRecentDestination() || isInferredDestination() || this.doNotSuggest) ? false : true;
    }

    public void deleteFromDbAsync() {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                Destination.this.deleteFromDb();
            }
        }, 1);
    }

    private int deleteFromDb() {
        boolean success = false;
        ContentResolver cr = NavdyApplication.getAppContext().getContentResolver();
        int nbDeleted = 0;
        if (cr != null) {
            nbDeleted = cr.delete(NavdyContentProviderConstants.DESTINATIONS_CONTENT_URI, String.format("%s=?", new Object[]{"_id"}), new String[]{String.valueOf(this.id)});
            success = nbDeleted > 0;
        } else {
            logger.e("Unable to get content resolver !");
        }
        if (success) {
            if (isFavoriteDestination()) {
                Tracker.tagEvent(Event.DELETING_A_FAVORITE);
                VersioningUtils.increaseVersionAndSendToDisplay(DestinationType.FAVORITES);
            }
            if (isRecentDestination()) {
                VersioningUtils.increaseVersionAndSendToDisplay(DestinationType.SUGGESTIONS);
            }
        } else if (isFavoriteDestination()) {
            BaseActivity.showLongToast(R.string.toast_favorite_delete_error, new Object[0]);
        }
        return nbDeleted;
    }

    public boolean hasOneValidSetOfCoordinates() {
        return hasValidNavCoordinates() || hasValidDisplayCoordinates();
    }

    public boolean hasValidDisplayCoordinates() {
        return MapUtils.isValidSetOfCoordinates(this.displayLat, this.displayLong);
    }

    public boolean hasValidNavCoordinates() {
        return MapUtils.isValidSetOfCoordinates(this.navigationLat, this.navigationLong);
    }

    public void setCoordsToSame(LatLng latlng) {
        this.displayLat = latlng.latitude;
        this.displayLong = latlng.longitude;
        this.navigationLat = latlng.latitude;
        this.navigationLong = latlng.longitude;
        this.precisionLevel = Precision.PRECISE;
        this.hasBeenProcessed = true;
    }

    @WorkerThread
    public void handleNewCoordsAndAddress(double displayLat, double displayLong, double navigationLat, double navigationLong, Address address, Precision precision) {
        SystemUtils.ensureNotOnMainThread();
        boolean newNavIsValid = MapUtils.isValidSetOfCoordinates(navigationLat, navigationLong);
        if (address != null && (!hasDetailedAddress() || newNavIsValid)) {
            setAddressFieldsFromHereAddress(address);
        }
        if (newNavIsValid) {
            this.displayLat = displayLat;
            this.displayLong = displayLong;
            this.navigationLat = navigationLat;
            this.navigationLong = navigationLong;
            if (precision != Precision.UNKNOWN) {
                this.precisionLevel = precision;
            }
            logger.d("Navigation latLng from Here: " + navigationLat + "," + navigationLong + "; precision=" + precision);
        }
        this.hasBeenProcessed = true;
        if (saveContentValuesToDb(getContentValuesForCoords(), getContentValuesForAddress()) <= 0) {
            logger.e("Error while handling new coords and address from here. Unable to update the DB with values: coordValues: " + getContentValuesForCoords() + " and addrValues:" + getContentValuesForAddress());
        }
    }

    private int saveContentValuesToDb(ContentValues... contentValues) {
        makeSureWeHaveDestinationInDb();
        ContentValues allContentValues = new ContentValues();
        for (ContentValues contentValueGroup : contentValues) {
            if (contentValueGroup != null && contentValueGroup.size() > 0) {
                allContentValues.putAll(contentValueGroup);
            }
        }
        if (allContentValues.size() < 1) {
            logger.d("No content values were found for updating the DB");
            return -1;
        }
        ContentResolver contentResolver = NavdyApplication.getAppContext().getContentResolver();
        if (contentResolver != null) {
            ContentValues destinationValues = new ContentValues();
            destinationValues.put(NavdyContentProviderConstants.DESTINATIONS_NAVIGATION_LAT, Double.valueOf(this.navigationLat));
            destinationValues.put(NavdyContentProviderConstants.DESTINATIONS_NAVIGATION_LONG, Double.valueOf(this.navigationLong));
            destinationValues.put(NavdyContentProviderConstants.DESTINATIONS_DISPLAY_LAT, Double.valueOf(this.displayLat));
            destinationValues.put(NavdyContentProviderConstants.DESTINATIONS_DISPLAY_LONG, Double.valueOf(this.displayLong));
            return contentResolver.update(NavdyContentProviderConstants.DESTINATIONS_CONTENT_URI, allContentValues, "_id=?", new String[]{String.valueOf(this.id)});
        }
        logger.e("Unable to get contentResolver !");
        return -1;
    }

    private ContentValues getContentValuesForCoords() {
        ContentValues coordValues = new ContentValues();
        if (hasValidNavCoordinates()) {
            coordValues.put(NavdyContentProviderConstants.DESTINATIONS_NAVIGATION_LAT, Double.valueOf(this.navigationLat));
            coordValues.put(NavdyContentProviderConstants.DESTINATIONS_NAVIGATION_LONG, Double.valueOf(this.navigationLong));
            coordValues.put(NavdyContentProviderConstants.DESTINATIONS_PRECISION_LEVEL, Integer.valueOf(this.precisionLevel.getValue()));
        }
        if (hasValidDisplayCoordinates()) {
            coordValues.put(NavdyContentProviderConstants.DESTINATIONS_DISPLAY_LAT, Double.valueOf(this.displayLat));
            coordValues.put(NavdyContentProviderConstants.DESTINATIONS_DISPLAY_LONG, Double.valueOf(this.displayLong));
        }
        return coordValues;
    }

    private ContentValues getContentValuesForAddress() {
        ContentValues addressValues = new ContentValues();
        if (!StringUtils.isEmptyAfterTrim(this.streetNumber)) {
            addressValues.put(NavdyContentProviderConstants.DESTINATIONS_STREET_NUMBER, this.streetNumber);
        }
        if (!StringUtils.isEmptyAfterTrim(this.streetName)) {
            addressValues.put(NavdyContentProviderConstants.DESTINATIONS_STREET_NAME, this.streetName);
        }
        if (!StringUtils.isEmptyAfterTrim(this.city)) {
            addressValues.put(NavdyContentProviderConstants.DESTINATIONS_CITY, this.city);
        }
        if (!StringUtils.isEmptyAfterTrim(this.state)) {
            addressValues.put("state", this.state);
        }
        if (!StringUtils.isEmptyAfterTrim(this.zipCode)) {
            addressValues.put(NavdyContentProviderConstants.DESTINATIONS_ZIP_CODE, this.zipCode);
        }
        if (!StringUtils.isEmptyAfterTrim(this.country)) {
            addressValues.put(NavdyContentProviderConstants.DESTINATIONS_COUNTRY, this.country);
        }
        if (!StringUtils.isEmptyAfterTrim(this.countryCode)) {
            addressValues.put(NavdyContentProviderConstants.DESTINATIONS_COUNTRY_CODE, this.countryCode);
        }
        return addressValues;
    }

    public static Pair<String, String[]> getCoordinateBoundingBoxSelectionClause(double latitude, double longitude) {
        Location[] boundingBox = MapUtils.getBoundingBox(latitude, longitude, 5.0d);
        if (boundingBox.length != 2 ) {
            return new Pair(null, null);
        }
        return new Pair(String.format(
                    "((%s >= ? AND %s <= ?) AND (%s >= ? AND %s <= ?)) OR ((%s >= ? AND %s <= ?) AND (%s >= ? AND %s <= ?))",
                    NavdyContentProviderConstants.DESTINATIONS_DISPLAY_LAT,
                    NavdyContentProviderConstants.DESTINATIONS_DISPLAY_LAT,
                    NavdyContentProviderConstants.DESTINATIONS_DISPLAY_LONG,
                    NavdyContentProviderConstants.DESTINATIONS_DISPLAY_LONG,
                    NavdyContentProviderConstants.DESTINATIONS_NAVIGATION_LAT,
                    NavdyContentProviderConstants.DESTINATIONS_NAVIGATION_LAT,
                    NavdyContentProviderConstants.DESTINATIONS_NAVIGATION_LONG,
                    NavdyContentProviderConstants.DESTINATIONS_NAVIGATION_LONG
            ),
            new String[]{String.valueOf(boundingBox[0].getLatitude()),
                        String.valueOf(boundingBox[1].getLatitude()),
                        String.valueOf(boundingBox[0].getLongitude()),
                        String.valueOf(boundingBox[1].getLongitude()),
                        String.valueOf(boundingBox[0].getLatitude()),
                        String.valueOf(boundingBox[1].getLatitude()),
                        String.valueOf(boundingBox[0].getLongitude()),
                        String.valueOf(boundingBox[1].getLongitude())
            }
        );
    }

    public int getPinAsset() {
        return getPinAsset(true, false, false);
    }

    public int getUnselectedPinAsset() {
        return getPinAsset(false, false, false);
    }

    public int getRecentPinAsset() {
        return getPinAsset(true, true, false);
    }

    private int getActiveTripPinAsset() {
        return getPinAsset(true, false, true);
    }

    private int getPinAsset(boolean isSelected, boolean recentTakesPrecedence, boolean isActiveTrip) {
        if (isHome()) {
            if (isSelected) {
                return R.drawable.icon_pin_home;
            }
            return R.drawable.icon_pin_home_unselected;
        } else if (isWork()) {
            return isSelected ? R.drawable.icon_pin_work : R.drawable.icon_pin_work_unselected;
        } else {
            if (isContact()) {
                return isSelected ? R.drawable.icon_pin_user_blue : R.drawable.icon_pin_user_grey;
            } else {
                if (isFavoriteDestination()) {
                    return isSelected ? R.drawable.icon_pin_favorite : R.drawable.icon_pin_favorite_unselected;
                } else {
                    if (recentTakesPrecedence && !isActiveTrip && isRecentDestination()) {
                        return !isSelected ? R.drawable.icon_pin_recent_unselected : R.drawable.icon_pin_recent;
                    } else {
                        switch (this.type) {
                            case CALENDAR_EVENT:
                                return isSelected ? R.drawable.icon_pin_calendar : R.drawable.icon_pin_calendar_unselected;
                            case GAS_STATION:
                                return isSelected ? R.drawable.icon_pin_gas : R.drawable.icon_pin_gas_unselected;
                            case AIRPORT:
                                return isSelected ? R.drawable.icon_pin_airport : R.drawable.icon_pin_airport_unselected;
                            case PARKING:
                                return isSelected ? R.drawable.icon_pin_parking : R.drawable.icon_pin_parking_unselected;
                            case TRANSIT:
                                return isSelected ? R.drawable.icon_pin_transit : R.drawable.icon_pin_transit_unselected;
                            case ATM:
                                return isSelected ? R.drawable.icon_pin_atm : R.drawable.icon_pin_atm_unselected;
                            case BANK:
                                return isSelected ? R.drawable.icon_pin_bank : R.drawable.icon_pin_bank_unselected;
                            case SCHOOL:
                                return isSelected ? R.drawable.icon_pin_school : R.drawable.icon_pin_school_unselected;
                            case STORE:
                                return isSelected ? R.drawable.icon_pin_store : R.drawable.icon_pin_store_unselected;
                            case COFFEE:
                                return isSelected ? R.drawable.icon_pin_coffee : R.drawable.icon_pin_coffee_unselected;
                            case RESTAURANT:
                                return isSelected ? R.drawable.icon_pin_restaurant : R.drawable.icon_pin_restaurant_unselected;
                            case GYM:
                                return isSelected ? R.drawable.icon_pin_gym : R.drawable.icon_pin_gym_unselected;
                            case PARK:
                                return isSelected ? R.drawable.icon_pin_park : R.drawable.icon_pin_park_unselected;
                            case HOSPITAL:
                                return isSelected ? R.drawable.icon_pin_hospital : R.drawable.icon_pin_hospital_unselected;
                            case ENTERTAINMENT:
                                return isSelected ? R.drawable.icon_pin_entertainment : R.drawable.icon_pin_entertainment_unselected;
                            case BAR:
                                return isSelected ? R.drawable.icon_pin_bar : R.drawable.icon_pin_bar_unselected;
                            default:
                                return (isActiveTrip || !isRecentDestination()) ? isSelected ? R.drawable.icon_pin_place : R.drawable.icon_pin_place_unselected : !isSelected ? R.drawable.icon_pin_recent_unselected : R.drawable.icon_pin_recent;
                        }
                    }
                }
            }
        }
    }

    public int getBadgeAsset() {
        return getBadgeAsset(true, false, false, false);
    }

    public int getBadgeAssetForActiveTrip() {
        return getBadgeAsset(false, false, false, true);
    }

    public int getBadgeAssetForPendingTrip() {
        return getBadgeAsset(true, false, false, true);
    }

    public int getBadgeAssetForSuggestion() {
        return getBadgeAsset(false, false, true, false);
    }

    int getBadgeAssetForRecent() {
        return getBadgeAsset(true, true, false, false);
    }

    public int getBadgeAssetForAutoComplete() {
        return getBadgeAsset(true, true, true, false);
    }

    public int getBadgeAssetForSearchResults() {
        return getBadgeAsset(true, false, false, false);
    }

    private int getBadgeAsset(boolean isOverWhiteBg, boolean isGrey, boolean recentTakesPrecedence, boolean isActiveTrip) {
        int i = R.drawable.icon_badge_recent_light;
        if (isHome()) {
            if (isGrey) {
                return R.drawable.icon_badge_home_light;
            }
            return R.drawable.icon_badge_home;
        } else if (isWork()) {
            return isGrey ? R.drawable.icon_badge_work_light : R.drawable.icon_badge_work;
        } else {
            if (isContact()) {
                return !isGrey ? R.drawable.icon_badge_user_blue : R.drawable.icon_badge_user_grey;
            } else {
                if (isFavoriteDestination()) {
                    return isGrey ? R.drawable.icon_badge_favorite_light : R.drawable.icon_badge_favorite;
                } else {
                    if (recentTakesPrecedence && !isActiveTrip && isRecentDestination()) {
                        return isGrey ? R.drawable.icon_badge_recent_light : R.drawable.icon_badge_recent;
                    } else {
                        switch (this.type) {
                            case CONTACT:
                                return !isGrey ? R.drawable.icon_badge_user_blue : R.drawable.icon_badge_user_grey;
                            case CALENDAR_EVENT:
                                return isGrey ? R.drawable.icon_badge_calendar_light : R.drawable.icon_badge_calendar;
                            case GAS_STATION:
                                return isGrey ? R.drawable.icon_badge_gas_light : R.drawable.icon_badge_gas;
                            case AIRPORT:
                                return isGrey ? R.drawable.icon_badge_airport_light : R.drawable.icon_badge_airport;
                            case PARKING:
                                return isGrey ? R.drawable.icon_badge_parking_light : R.drawable.icon_badge_parking;
                            case TRANSIT:
                                return isGrey ? R.drawable.icon_badge_transit_light : R.drawable.icon_badge_transit;
                            case ATM:
                                return isGrey ? R.drawable.icon_badge_atm_light : R.drawable.icon_badge_atm;
                            case BANK:
                                return isGrey ? R.drawable.icon_badge_bank_light : R.drawable.icon_badge_bank;
                            case SCHOOL:
                                return isGrey ? R.drawable.icon_badge_school_light : R.drawable.icon_badge_school;
                            case STORE:
                                return isGrey ? R.drawable.icon_badge_store_light : R.drawable.icon_badge_store;
                            case COFFEE:
                                return isGrey ? R.drawable.icon_badge_coffee_light : R.drawable.icon_badge_coffee;
                            case RESTAURANT:
                                return isGrey ? R.drawable.icon_badge_restaurant_light : R.drawable.icon_badge_restaurant;
                            case GYM:
                                return isGrey ? R.drawable.icon_badge_gym_light : R.drawable.icon_badge_gym;
                            case PARK:
                                return isGrey ? R.drawable.icon_badge_park_light : R.drawable.icon_badge_park;
                            case HOSPITAL:
                                return isGrey ? R.drawable.icon_badge_hospital_light : R.drawable.icon_badge_hospital;
                            case ENTERTAINMENT:
                                return isGrey ? R.drawable.icon_badge_entertainment_light : R.drawable.icon_badge_entertainment;
                            case BAR:
                                return isGrey ? R.drawable.icon_badge_bar_light : R.drawable.icon_badge_bar;
                            default:
                                if (isActiveTrip || !isRecentDestination()) {
                                    return getDefaultBadgeAsset(isOverWhiteBg, isGrey);
                                }
                                if (!isGrey) {
                                    i = R.drawable.icon_badge_recent;
                                }
                                return i;
                        }
                    }
                }
            }
        }
    }

    private static int getDefaultBadgeAsset(boolean isOverWhiteBg, boolean isLight) {
        if (isOverWhiteBg) {
            return isLight ? R.drawable.icon_badge_place_light : R.drawable.icon_badge_place;
        } else {
            return R.drawable.icon_badge_active_trip;
        }
    }

    @Nullable
    public MapMarker getHereMapMarker() {
        Image image = new Image();
        try {
            double latitude;
            double longitude;
            image.setImageResource(getActiveTripPinAsset());
            if (hasValidDisplayCoordinates()) {
                latitude = this.displayLat;
                longitude = this.displayLong;
            } else {
                latitude = this.navigationLat;
                longitude = this.navigationLong;
            }
            MapMarker mapMarker = new MapMarker(new GeoCoordinate(latitude, longitude), image);
            mapMarker.setZIndex(NavdyApplication.getAppContext().getResources().getInteger(R.integer.map_marker_z_index));
            mapMarker.setAnchorPoint(new PointF(((float) image.getWidth()) / 2.0f, (float) image.getHeight()));
            return mapMarker;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean isRecentDestination() {
        return this.lastRoutedDate > 0;
    }

    public void updateLastRoutedDateInDbAsync() {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                Destination.this.updateLastRoutedDateInDb();
            }
        }, 1);
    }

    public int updateLastRoutedDateInDb() {
        if (this.lastRoutedDate <= 0) {
            this.lastRoutedDate = new Date().getTime();
        }
        makeSureWeHaveDestinationInDb();
        ContentValues recentValues = new ContentValues();
        recentValues.put(NavdyContentProviderConstants.DESTINATIONS_LAST_ROUTED_DATE, Long.valueOf(new Date().getTime()));
        int nbUpdatedRows = NavdyApplication.getAppContext().getContentResolver().update(NavdyContentProviderConstants.DESTINATIONS_CONTENT_URI, recentValues, String.format("%s=?", new Object[]{"_id"}), new String[]{String.valueOf(this.id)});
        logger.v("Updated " + nbUpdatedRows + " entries");
        if (nbUpdatedRows > 0) {
            SuggestionManager.forceSuggestionFullRefresh();
        }
        return nbUpdatedRows;
    }

    public void setIsCalendarEvent(boolean is) {
        this.isCalendarEvent = is;
    }

    public boolean isCalendarEvent() {
        return this.isCalendarEvent || this.type == Type.CALENDAR_EVENT;
    }

    public boolean isContact() {
        return this.type == Type.CONTACT || this.favoriteType == -4;
    }

    public boolean isHome() {
        return this.favoriteType == -3;
    }

    public boolean isWork() {
        return this.favoriteType == -2;
    }

    public boolean isFavoriteDestination() {
        return isFavoriteDestination(this.favoriteType);
    }

    public static boolean isFavoriteDestination(int favoriteType) {
        return favoriteType != 0;
    }

    private static boolean isFavoriteDestination(String placeId) {
        boolean isFavoriteDestination = false;
        try {
            Cursor cursor = NavdyApplication.getAppContext().getContentResolver().query(NavdyContentProviderConstants.DESTINATIONS_CONTENT_URI, new String[]{NavdyContentProviderConstants.DESTINATIONS_FAVORITE_TYPE}, "place_id=?", new String[]{placeId}, null);
            if (cursor != null && cursor.moveToFirst()) {
                isFavoriteDestination = isFavoriteDestination(cursor.getInt(0));
            }
            IOUtils.closeStream(cursor);
            return isFavoriteDestination;
        } catch (Throwable th) {
            IOUtils.closeStream(null);
        }
        return false;
    }

    public void saveDestinationAsFavoritesAsync(final int specialType, final QueryResultCallback callback) {
        new AsyncTask<Object, Object, Integer>() {
            protected Integer doInBackground(Object... params) {
                return Integer.valueOf(Destination.this.saveDestinationAsFavorite(specialType));
            }

            protected void onPostExecute(Integer nbRows) {
                if (callback != null) {
                    callback.onQueryCompleted(nbRows.intValue(), null);
                }
            }
        }.execute(new Object[0]);
    }

    private int saveDestinationAsFavorite(int specialType) {
        int nbRows = -1;
        Cursor favoritesCursor = null;
        try {
            int oldOrder;
            int newOrder;
            favoritesCursor = NavdyContentProvider.getFavoritesCursor();
            int count = favoritesCursor.getCount();
            if (isPersisted() && isFavoriteDestination()) {
                oldOrder = this.favoriteOrder;
            } else {
                oldOrder = count;
            }
            this.favoriteType = specialType;
            if (specialType == -1 && isContact()) {
                this.favoriteType = -4;
            }
            if (specialType == -3) {
                newOrder = 0;
            } else if (specialType == -2) {
                Destination home = NavdyContentProvider.getHome();
                if (home == null || home.favoriteOrder != 0) {
                    newOrder = 0;
                } else {
                    newOrder = 1;
                }
            } else {
                newOrder = count;
            }
            logger.v("Moving favorite from position " + oldOrder + " to " + newOrder);
            this.favoriteOrder = newOrder;
            shiftListForMove(oldOrder, newOrder);
            if (StringUtils.isEmptyAfterTrim(this.favoriteLabel)) {
                if (StringUtils.isEmptyAfterTrim(this.name)) {
                    this.favoriteLabel = this.rawAddressNotForDisplay;
                } else {
                    this.favoriteLabel = this.name;
                }
            }
            if (!isPersisted() && findInDb() > 0) {
                reloadSelfFromDatabase();
            }
            if (isPersisted()) {
                nbRows = updateOnlyFavoriteFieldsInDb();
            } else if (saveToDb() != null) {
                nbRows = 1;
            }
            VersioningUtils.increaseVersionAndSendToDisplay(DestinationType.FAVORITES);
            return nbRows;
        } finally {
            IOUtils.closeObject(favoritesCursor);
        }
    }

    public int updateOnlyFavoriteFieldsInDb() {
        return updateOnlyFavoriteFieldsInDb(true);
    }

    public int updateOnlyFavoriteFieldsInDb(boolean sendToDisplay) {
        ContentValues cv = new ContentValues();
        cv.put(NavdyContentProviderConstants.DESTINATIONS_NAME, this.name);
        cv.put(NavdyContentProviderConstants.DESTINATIONS_LABEL, this.favoriteLabel);
        cv.put(NavdyContentProviderConstants.DESTINATIONS_ORDER, Integer.valueOf(this.favoriteOrder));
        cv.put(NavdyContentProviderConstants.DESTINATIONS_FAVORITE_TYPE, Integer.valueOf(this.favoriteType));
        int nbRows = updateDb(cv);
        if (nbRows <= 0) {
            logger.e(NavdyApplication.getAppContext().getString(R.string.toast_favorite_edit_error));
        } else if (sendToDisplay) {
            VersioningUtils.increaseVersionAndSendToDisplay(DestinationType.FAVORITES);
        }
        return nbRows;
    }

    public void deleteFavoriteFromDbAsync(final QueryResultCallback callback) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                int nbRows = Destination.this.deleteFavoriteFromDb();
                if (callback != null) {
                    callback.onQueryCompleted(nbRows, null);
                }
            }
        }, 1);
    }

    private int deleteFavoriteFromDb() {
        Cursor favoritesCursor = null;
        try {
            favoritesCursor = NavdyContentProvider.getFavoritesCursor();
            int newOrder = favoritesCursor.getCount();
            int oldOrder = this.favoriteOrder;
            logger.v("Moving favorite from position " + oldOrder + " to " + newOrder);
            this.favoriteOrder = newOrder;
            shiftListForMove(oldOrder, newOrder);
            if (shouldDeleteDestinationEntryAsWell()) {
                return deleteFromDb();
            }
            this.favoriteLabel = "";
            this.favoriteOrder = 0;
            this.favoriteType = 0;
            return updateOnlyFavoriteFieldsInDb();
        } finally {
            IOUtils.closeObject(favoritesCursor);
        }
    }

    private static void shiftListForMove(int oldOrder, int newOrder) {
        int startOrder;
        int endOrder;
        int shift;
        if (newOrder < oldOrder) {
            startOrder = newOrder;
            endOrder = oldOrder - 1;
            shift = 1;
        } else if (newOrder > oldOrder) {
            startOrder = oldOrder + 1;
            endOrder = newOrder;
            shift = -1;
        } else {
            logger.v("No need to shift list");
            return;
        }
        try {
            Cursor cursor = NavdyContentProvider.getFavoritesCursor(selectionForShift(startOrder, endOrder));
            ContentValues contentValues = new ContentValues();
            int i = 0;
            while (cursor != null && i < cursor.getCount()) {
                Destination destination = NavdyContentProvider.getDestinationItemAt(cursor, i);
                contentValues.put(NavdyContentProviderConstants.DESTINATIONS_ORDER, Integer.valueOf(destination.favoriteOrder + shift));
                destination.updateDb(contentValues);
                i++;
            }
            IOUtils.closeObject(cursor);
        } catch (Throwable th) {
            IOUtils.closeObject(null);
        }
    }

    @NonNull
    private static Pair<String, String[]> selectionForShift(int startOrder, int endOrder) {
        return new Pair("favorite_listing_order >= ? AND favorite_listing_order <= ?", new String[]{String.valueOf(startOrder), String.valueOf(endOrder)});
    }

    private boolean isInferredDestination() {
        try {
            Cursor cursor = NavdyContentProvider.getTripsCursor(new Pair("destination_id = " + this.id, null));
            boolean z = cursor != null && cursor.getCount() > 0;
            IOUtils.closeObject(cursor);
            return z;
        } catch (Throwable th) {
            IOUtils.closeObject(null);
        }
        return false;
    }

    public static void placesSearchResultToDestinationObject(PlacesSearchResult placesSearchResult, Destination destination) {
        if (!StringUtils.isEmptyAfterTrim(placesSearchResult.label)) {
            destination.name = placesSearchResult.label;
        }
        if (!StringUtils.isEmptyAfterTrim(placesSearchResult.address)) {
            destination.rawAddressNotForDisplay = AddressUtils.sanitizeAddress(placesSearchResult.address);
        }
        if (placesSearchResult.destinationLocation != null) {
            destination.displayLat = placesSearchResult.destinationLocation.latitude;
            destination.displayLong = placesSearchResult.destinationLocation.longitude;
        }
        if (placesSearchResult.navigationPosition != null) {
            destination.navigationLat = placesSearchResult.navigationPosition.latitude;
            destination.navigationLong = placesSearchResult.navigationPosition.longitude;
        }
        destination.searchResultType = SearchType.TEXT_SEARCH.getValue();
    }

    public String getNameForDisplay() {
        String name = "";
        if (isFavoriteDestination()) {
            name = this.favoriteLabel;
        }
        if (StringUtils.isEmptyAfterTrim(name)) {
            return this.name;
        }
        return name;
    }

    private static Type fromProtobufPlaceType(PlaceType placeType) {
        if (placeType == null) {
            return Type.UNKNOWN;
        }
        switch (placeType) {
            case PLACE_TYPE_AIRPORT:
                return Type.AIRPORT;
            case PLACE_TYPE_ATM:
                return Type.ATM;
            case PLACE_TYPE_BANK:
                return Type.BANK;
            case PLACE_TYPE_BAR:
                return Type.BAR;
            case PLACE_TYPE_CALENDAR_EVENT:
                return Type.CALENDAR_EVENT;
            case PLACE_TYPE_COFFEE:
                return Type.COFFEE;
            case PLACE_TYPE_CONTACT:
                return Type.CONTACT;
            case PLACE_TYPE_ENTERTAINMENT:
                return Type.ENTERTAINMENT;
            case PLACE_TYPE_GAS:
                return Type.GAS_STATION;
            case PLACE_TYPE_GYM:
                return Type.GYM;
            case PLACE_TYPE_HOSPITAL:
                return Type.HOSPITAL;
            case PLACE_TYPE_PARK:
                return Type.PARK;
            case PLACE_TYPE_PARKING:
                return Type.PARKING;
            case PLACE_TYPE_RESTAURANT:
                return Type.RESTAURANT;
            case PLACE_TYPE_SCHOOL:
                return Type.SCHOOL;
            case PLACE_TYPE_STORE:
                return Type.STORE;
            case PLACE_TYPE_TRANSIT:
                return Type.TRANSIT;
            default:
                return Type.UNKNOWN;
        }
    }

    public Address getHereAddress() {
        Address hereAddress = new Address();
        if (this.rawAddressNotForDisplay != null) {
            hereAddress.setText(this.rawAddressNotForDisplay);
        }
        if (this.country != null) {
            hereAddress.setCountryName(this.country);
        }
        if (this.countryCode != null) {
            if (this.countryCode.length() == 2) {
                hereAddress.setCountryCode(AddressUtils.convertIso2ToIso3(this.countryCode));
            } else {
                hereAddress.setCountryCode(this.countryCode);
            }
        }
        if (this.state != null) {
            hereAddress.setState(this.state);
        }
        if (this.city != null) {
            hereAddress.setCity(this.city);
        }
        if (this.zipCode != null) {
            hereAddress.setPostalCode(this.zipCode);
        }
        return hereAddress;
    }
}
