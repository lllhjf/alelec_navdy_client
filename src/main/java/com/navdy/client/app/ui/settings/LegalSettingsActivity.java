package com.navdy.client.app.ui.settings;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.alelec.navdyclient.R;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Screen.Settings;
import com.navdy.client.app.ui.WebViewActivity;
import com.navdy.client.app.ui.base.BaseToolbarActivity;
import com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder;

public class LegalSettingsActivity extends BaseToolbarActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.settings_legal);
        new ToolbarBuilder().title((int) R.string.menu_legal).build();
    }

    protected void onResume() {
        super.onResume();
        Tracker.tagScreen(Settings.LEGAL);
    }

    public void onAttributionClick(View view) {
        Intent browserIntent = new Intent(getApplicationContext(), WebViewActivity.class);
        browserIntent.putExtra("type", WebViewActivity.ATTRIBUTION);
        startActivity(browserIntent);
    }

    public void onPrivacyPolicyClick(View view) {
        Intent browserIntent = new Intent(getApplicationContext(), WebViewActivity.class);
        browserIntent.putExtra("type", WebViewActivity.PRIVACY);
        startActivity(browserIntent);
    }

    public void onTermsClick(View view) {
        Intent browserIntent = new Intent(getApplicationContext(), WebViewActivity.class);
        browserIntent.putExtra("type", WebViewActivity.TERMS);
        startActivity(browserIntent);
    }
}
