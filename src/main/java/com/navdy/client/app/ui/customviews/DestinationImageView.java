package com.navdy.client.app.ui.customviews;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.util.ContactImageHelper;
import com.navdy.client.app.framework.util.StringUtils;

public class DestinationImageView extends ImageView {
    private static final String EMPTY = "";
    private static final String SPACE = " ";
    private static int greyColor;
    private static int largeStyleTextSize;
    private static int smallStyleTextSize;
    private static Typeface typefaceLarge;
    private static Typeface typefaceSmall;
    private static boolean valuesSet;
    private static int whiteColor;
    private ContactImageHelper contactImageHelper;
    private String initials;
    private Paint paint;
    private Style style;

    public enum Style {
        INITIALS,
        DEFAULT
    }

    public DestinationImageView(Context context) {
        this(context, null, 0);
    }

    public DestinationImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DestinationImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.style = Style.DEFAULT;
        init();
    }

    private void init() {
        this.contactImageHelper = ContactImageHelper.getInstance();
        if (!valuesSet) {
            valuesSet = true;
            Resources resources = getResources();
            greyColor = ContextCompat.getColor(NavdyApplication.getAppContext(), R.color.material_grey_800);
            whiteColor = ContextCompat.getColor(NavdyApplication.getAppContext(), R.color.white);
            smallStyleTextSize = (int) resources.getDimension(R.dimen.contact_image_small_text);
            largeStyleTextSize = (int) resources.getDimension(R.dimen.contact_image_large_text);
            typefaceSmall = Typeface.create("sans-serif-medium", 0);
            typefaceLarge = Typeface.create("sans-serif", 1);
        }
        this.paint = new Paint();
        this.paint.setStrokeWidth(0.0f);
        this.paint.setAntiAlias(true);
    }

    public void setInitials(String initials, Style style) {
        this.initials = initials;
        this.style = style;
        invalidate();
    }

    public void setImage(int resourceId, String initials, Style style) {
        setImageResource(resourceId);
        setInitials(initials, style);
    }

    public void setImage(Bitmap bitmap) {
        clearInitials();
        setImageBitmap(createCircleBitmap(bitmap));
    }

    public void setImage(String name) {
        setImage(this.contactImageHelper.getResourceId(this.contactImageHelper.getContactImageIndex(name)), name);
    }

    public void setImage(int resourceId, String name) {
        setImage(resourceId, getInitials(name), Style.INITIALS);
    }

    public static synchronized String getInitials(String name) {
        String str;
        synchronized (DestinationImageView.class) {
            StringBuilder builder = new StringBuilder();
            if (StringUtils.isEmptyAfterTrim(name)) {
                str = "";
            } else {
                name = name.trim();
                int index = name.indexOf(SPACE);
                if (index > 0) {
                    String first = name.substring(0, index).trim();
                    String last = name.substring(index + 1).trim();
                    builder.setLength(0);
                    if (!StringUtils.isEmptyAfterTrim(first)) {
                        builder.append(first.charAt(0));
                    }
                    if (!StringUtils.isEmptyAfterTrim(last)) {
                        builder.append(last.charAt(0));
                    }
                    str = builder.toString().toUpperCase();
                } else if (name.length() > 0) {
                    str = String.valueOf(name.charAt(0)).toUpperCase();
                } else {
                    str = "";
                }
            }
        }
        return str;
    }

    public void clearInitials() {
        this.initials = "";
        this.style = Style.DEFAULT;
    }

    public static Bitmap createCircleBitmap(Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        Context context = NavdyApplication.getAppContext();
        int width = context.getResources().getDimensionPixelSize(R.dimen.contact_image_width);
        int height = context.getResources().getDimensionPixelSize(R.dimen.contact_image_height);
        bitmap = Bitmap.createScaledBitmap(bitmap, width, height, false);
        Bitmap newBitmap = Bitmap.createBitmap(width, height, Config.ARGB_8888);
        Path path = new Path();
        path.addCircle(((float) width) / 2.0f, ((float) height) / 2.0f, Math.min((float) width, ((float) height) / 2.0f), Direction.CCW);
        Canvas canvas = new Canvas(newBitmap);
        canvas.clipPath(path);
        canvas.drawBitmap(bitmap, 0.0f, 0.0f, null);
        return newBitmap;
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.style != Style.DEFAULT) {
            int width = getWidth();
            int height = getHeight();
            if (this.initials != null) {
                this.paint.setColor(whiteColor);
                this.paint.setTextSize((float) largeStyleTextSize);
                this.paint.setTypeface(typefaceLarge);
                this.paint.setTextAlign(Align.CENTER);
                canvas.drawText(this.initials, ((float) width) / 2.0f, (((float) height) / 2.0f) - ((this.paint.descent() + this.paint.ascent()) / 2.0f), this.paint);
            }
        }
    }
}
