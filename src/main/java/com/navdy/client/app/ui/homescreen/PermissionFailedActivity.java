package com.navdy.client.app.ui.homescreen;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.framework.util.SystemUtils;
import com.navdy.client.app.ui.base.BaseToolbarActivity;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class PermissionFailedActivity extends BaseToolbarActivity {
    public static final String EXTRA_BUTTON_RES = "extra_button_res";
    public static final String EXTRA_DESCRIPTION_RES = "extra_description_res";
    public static final String EXTRA_TITLE_RES = "extra_title_res";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.mic_permission_failed);
        TextView title = (TextView) findViewById(R.id.title);
        TextView description = (TextView) findViewById(R.id.desc);
        TextView blueText = (TextView) findViewById(R.id.blue_text);
        Button button = (Button) findViewById(R.id.next_step);
        Intent intent = getIntent();
        int titleRes = intent.getIntExtra(EXTRA_TITLE_RES, R.string.fle_app_setup_default_title_fail);
        int descriptionRes = intent.getIntExtra(EXTRA_DESCRIPTION_RES, R.string.fle_app_setup_default_desc_fail);
        int buttonTextRes = intent.getIntExtra(EXTRA_BUTTON_RES, R.string.fle_app_setup_default_button_fail);
        if (title != null) {
            title.setText(titleRes);
            title.setVisibility(VISIBLE);
        }
        if (description != null) {
            description.setText(StringUtils.fromHtml(descriptionRes));
            description.setVisibility(VISIBLE);
        }
        if (blueText != null) {
            blueText.setVisibility(GONE);
        }
        if (button != null) {
            button.setText(buttonTextRes);
            button.setVisibility(VISIBLE);
        }
    }

    public void onDescriptionClick(View view) {
        SystemUtils.goToSystemSettingsAppInfoForOurApp(this);
    }

    public void onButtonClick(View v) {
        finish();
    }
}
