package com.navdy.client.app.ui.settings;

import android.app.FragmentManager;
import android.app.FragmentManager.OnBackStackChangedListener;
import android.os.Bundle;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Screen.Settings;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment;

public class BluetoothPairActivity extends BaseActivity {
    public static final String bluetoothDialogTag = "BLUETOOTH_DIALOG_FRAGMENT";

    public void onCreate(Bundle savedInstanceState) {
        this.logger.v("::onCreate");
        super.onCreate(savedInstanceState);
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().addToBackStack(null).add(new BluetoothFramelayoutFragment(), bluetoothDialogTag).commit();
        fragmentManager.addOnBackStackChangedListener(new OnBackStackChangedListener() {
            public void onBackStackChanged() {
                if (BluetoothPairActivity.this.getFragmentManager().getBackStackEntryCount() == 0) {
                    BluetoothPairActivity.this.finish();
                }
            }
        });
    }

    protected void onResume() {
        super.onResume();
        Tracker.tagScreen(Settings.BLUETOOTH);
    }

    public boolean requiresBus() {
        return false;
    }
}
