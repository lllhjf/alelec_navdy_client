package com.navdy.client.app.ui.firstlaunch;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnInfoListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnTimedTextListener;
import android.media.MediaPlayer.TrackInfo;
import android.media.TimedText;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.i18n.I18nManager;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.ui.base.BaseActivity;
import java.util.Locale;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class VideoPlayerActivity extends BaseActivity {
    public static final String EXTRA_VIDEO_URL = "video_url";
    private int currentPosition;
    private VideoView myVideo;
    private TextView subtitlesTextView;
    private String videoUrl;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.fle_install_video);
        this.subtitlesTextView = (TextView) findViewById(R.id.subtitles);
        showProgressDialog();
        MediaController mediaController = new MediaController(this) {
            public boolean dispatchKeyEvent(KeyEvent event) {
                if (event.getKeyCode() == 4) {
                    VideoPlayerActivity.this.finish();
                }
                return super.dispatchKeyEvent(event);
            }
        };
        this.myVideo = (VideoView) findViewById(R.id.install_video);
        this.videoUrl = getIntent().getStringExtra(EXTRA_VIDEO_URL);
        if (StringUtils.isEmptyAfterTrim(this.videoUrl)) {
            this.logger.e("Invalid video URL! [" + this.videoUrl + "]");
        }
        if (this.myVideo != null) {
            this.myVideo.setMediaController(mediaController);
            this.myVideo.setVideoPath(this.videoUrl);
            mediaController.setAnchorView(this.myVideo);
            mediaController.setMediaPlayer(this.myVideo);
            this.myVideo.setOnCompletionListener(new OnCompletionListener() {
                public void onCompletion(MediaPlayer mediaPlayer) {
                    VideoPlayerActivity.this.finish();
                }
            });
            this.myVideo.setOnPreparedListener(new OnPreparedListener() {
                public void onPrepared(MediaPlayer player) {
                    VideoPlayerActivity.this.hideProgressDialog();
                    player.setOnInfoListener(new OnInfoListener() {
                        public boolean onInfo(MediaPlayer mp, int what, int extra) {
                            if (what == 701) {
                                VideoPlayerActivity.this.showProgressDialog();
                            }
                            if (what == 702) {
                                VideoPlayerActivity.this.hideProgressDialog();
                            }
                            return false;
                        }
                    });
                    try {
                        Locale currentLocale = I18nManager.getInstance().getCurrentLocale();
                        if (!isEnglish(currentLocale)) {
                            int textTrackIndex = VideoPlayerActivity.this.findTrackIndexFor(player.getTrackInfo(), 3, currentLocale.getISO3Language());
                            if (textTrackIndex >= 0) {
                                player.selectTrack(textTrackIndex);
                            } else {
                                VideoPlayerActivity.this.logger.w("Cannot find text track for language: " + currentLocale.getISO3Language());
                            }
                        }
                        player.setOnTimedTextListener(new OnTimedTextListener() {
                            public void onTimedText(MediaPlayer mp, final TimedText text) {
                                if (text != null) {
                                    VideoPlayerActivity.this.handler.post(new Runnable() {
                                        public void run() {
                                            String subtitleLine = text.getText();
                                            if (StringUtils.isEmptyAfterTrim(subtitleLine)) {
                                                VideoPlayerActivity.this.subtitlesTextView.setVisibility(GONE);
                                                return;
                                            }
                                            if (subtitleLine.endsWith("\n")) {
                                                subtitleLine = subtitleLine.substring(0, subtitleLine.length() - 1);
                                            }
                                            if (subtitleLine.endsWith("\n")) {
                                                subtitleLine = subtitleLine.substring(0, subtitleLine.length() - 1);
                                            }
                                            VideoPlayerActivity.this.subtitlesTextView.setText(subtitleLine);
                                            VideoPlayerActivity.this.subtitlesTextView.setVisibility(VISIBLE);
                                        }
                                    });
                                }
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                private boolean isEnglish(Locale currentLocale) {
                    return currentLocale == null || StringUtils.equalsOrBothEmptyAfterTrim(currentLocale.getLanguage(), "en");
                }
            });
            this.myVideo.start();
        }
    }

    private int findTrackIndexFor(TrackInfo[] trackInfo, int mediaTrackType, String language) {
        int i = 0;
        while (i < trackInfo.length) {
            if (trackInfo[i].getTrackType() == mediaTrackType && StringUtils.equalsOrBothEmptyAfterTrim(trackInfo[i].getLanguage(), language)) {
                return i;
            }
            i++;
        }
        return -1;
    }

    protected void onRestart() {
        this.myVideo.start();
        this.myVideo.seekTo(this.currentPosition);
        if (!this.myVideo.isPlaying()) {
            this.myVideo.resume();
        }
        super.onRestart();
    }

    protected void onPause() {
        this.currentPosition = this.myVideo.getCurrentPosition();
        this.myVideo.pause();
        super.onPause();
    }

    protected void onResume() {
        super.onResume();
        Tracker.tagScreen("First_Launch_Install_Video: " + this.videoUrl);
    }

    protected void onDestroy() {
        if (!(this.myVideo == null || this.myVideo.getHolder() == null || this.myVideo.getHolder().getSurface() == null)) {
            this.myVideo.getHolder().getSurface().release();
        }
        super.onDestroy();
    }
}
