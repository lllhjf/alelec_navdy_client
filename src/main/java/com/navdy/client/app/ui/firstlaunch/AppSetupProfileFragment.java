package com.navdy.client.app.ui.firstlaunch;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import com.alelec.navdyclient.R;
import com.navdy.client.app.ui.base.BaseSupportFragment;
import com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences;
import com.navdy.client.app.ui.settings.SettingsUtils;

public class AppSetupProfileFragment extends BaseSupportFragment {
    private SharedPreferences customerPrefs;
    private EditText emailInput;
    private EditText nameInput;
    private View rootView;

    @Nullable
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.rootView = inflater.inflate(R.layout.fle_app_setup_bottom_card_profile, null);
        this.customerPrefs = SettingsUtils.getCustomerPreferences();
        this.nameInput = (EditText) this.rootView.findViewById(R.id.enter_name);
        this.emailInput = (EditText) this.rootView.findViewById(R.id.enter_email);
        FragmentActivity activity = getActivity();
        if (activity instanceof AppSetupActivity) {
            ((AppSetupActivity) activity).doProfileOnCreate(this);
        }
        if (this.nameInput == null || this.emailInput == null) {
            return this.rootView;
        }
        return this.rootView;
    }

    public View getRootView() {
        return this.rootView;
    }

    public void onResume() {
        super.onResume();
        String fullName = this.customerPrefs.getString(ProfilePreferences.FULL_NAME, "");
        String email = this.customerPrefs.getString("email", "");
        if (this.nameInput != null) {
            this.nameInput.setText(fullName);
        }
        if (this.emailInput != null) {
            this.emailInput.setText(email);
        }
    }

    public String getNameText() {
        if (this.nameInput != null) {
            return String.valueOf(this.nameInput.getText());
        }
        return null;
    }

    public boolean requiresBus() {
        return false;
    }
}
