package com.navdy.client.app.ui.settings;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AlertDialog.Builder;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.util.ImageUtils;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.framework.util.SystemUtils;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Screen.Settings;
import com.navdy.client.app.ui.PhotoUtils;
import com.navdy.client.app.ui.WebViewActivity;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.base.BaseEditActivity;
import com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder;
import com.navdy.client.app.ui.customviews.DestinationImageView;
import com.navdy.client.app.ui.firstlaunch.AppSetupActivity;
import com.navdy.client.app.ui.firstlaunch.AppSetupProfileFragment;
import com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import com.soundcloud.android.crop.Crop;
import java.io.File;

public class ProfileSettingsActivity extends BaseEditActivity {
    public static final int PICK_AN_IMAGE = 0;
    public static final int REMOVE_PICTURE = 3;
    public static final int TAKE_A_PICTURE = 1;
    protected final Context appContext = NavdyApplication.getAppContext();
    protected final SharedPreferences customerPrefs = SettingsUtils.getCustomerPreferences();
    protected String email = null;
    protected EditText emailInput;
    protected TextInputLayout emailLabel;
    protected final ColorStateList greyColorStateList = ColorStateList.valueOf(ContextCompat.getColor(this.appContext, R.color.grey));
    protected String name = null;
    protected EditText nameInput;
    protected TextInputLayout nameLabel;
    protected DestinationImageView photo;
    protected LinearLayout photoHint;
    protected AppSetupProfileFragment profileFragment;
    protected final ColorStateList redColorStateList = ColorStateList.valueOf(ContextCompat.getColor(this.appContext, R.color.red));
    protected Bitmap userPhoto = null;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!(this instanceof AppSetupActivity)) {
            setContentView((int) R.layout.settings_profile);
            new ToolbarBuilder().title((int) R.string.menu_profile).build();
            doProfileOnCreate((AppSetupProfileFragment) getSupportFragmentManager().findFragmentById(R.id.profile_card));
        }
    }

    public void doProfileOnCreate(AppSetupProfileFragment profileFragment) {
        this.profileFragment = profileFragment;
        if (profileFragment != null) {
            View rootView = profileFragment.getRootView();
            this.nameInput = (EditText) rootView.findViewById(R.id.enter_name);
            this.emailInput = (EditText) rootView.findViewById(R.id.enter_email);
            this.nameLabel = (TextInputLayout) rootView.findViewById(R.id.enter_name_til);
            this.emailLabel = (TextInputLayout) rootView.findViewById(R.id.enter_email_til);
            if (this.nameInput != null) {
                ViewCompat.setBackgroundTintList(this.nameInput, this.greyColorStateList);
                this.nameInput.addTextChangedListener(new TextWatcher() {
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }

                    public void afterTextChanged(Editable text) {
                        ProfileSettingsActivity.this.updatePhoto(text.toString());
                    }
                });
                this.nameInput.setOnFocusChangeListener(new OnFocusChangeListener() {
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus) {
                            ProfileSettingsActivity.this.nameInput.setSelection(0, ProfileSettingsActivity.this.nameInput.getText().length());
                        } else {
                            ProfileSettingsActivity.this.validateName();
                        }
                    }
                });
            }
            if (this.emailInput != null) {
                ViewCompat.setBackgroundTintList(this.emailInput, this.greyColorStateList);
                this.emailInput.setOnFocusChangeListener(new OnFocusChangeListener() {
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus) {
                            ProfileSettingsActivity.this.emailInput.setSelection(0, ProfileSettingsActivity.this.emailInput.getText().length());
                        } else {
                            ProfileSettingsActivity.this.validateEmail();
                        }
                    }
                });
            }
        }
    }

    protected void onPause() {
        validateAndSaveName(true);
        validateAndSaveEmail(true);
        super.onPause();
    }

    public void onResume() {
        super.onResume();
        this.photo = (DestinationImageView) findViewById(R.id.photo);
        this.photoHint = (LinearLayout) findViewById(R.id.photo_hint);
        if (this.userPhoto == null) {
            this.userPhoto = Tracker.getUserProfilePhoto();
        }
        this.name = this.customerPrefs.getString(ProfilePreferences.FULL_NAME, "");
        this.email = this.customerPrefs.getString("email", "");
        updatePhoto(this.userPhoto, this.name);
        if (!(this.nameInput == null || this.name == null)) {
            this.nameInput.setText(this.name);
            this.nameInput.setSelection(this.name.length());
            validateName();
        }
        if (!(this.emailInput == null || this.email == null)) {
            this.emailInput.setText(this.email);
            this.emailInput.setSelection(this.email.length());
            validateEmail();
        }
        this.somethingChanged = false;
        SystemUtils.dismissKeyboard(this);
        Tracker.tagScreen(Settings.PROFILE);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    public void onActivityResult(int requestCode, int resultCode, final Intent responseIntent) {
        if (resultCode == -1) {
            if (requestCode == 1 || requestCode == Crop.REQUEST_PICK) {
                final Uri source;
                final Uri destination = Uri.fromFile(IOUtils.getTempFile(this.logger, PhotoUtils.TEMP_PHOTO_FILE));
                if (requestCode == Crop.REQUEST_PICK) {
                    source = responseIntent.getData();
                } else {
                    source = destination;
                }
                requestStoragePermission(new Runnable() {
                    public void run() {
                        Crop.of(source, destination).asSquare().start(ProfileSettingsActivity.this);
                    }
                }, new Runnable() {
                    public void run() {
                        BaseActivity.showLongToast(R.string.storage_permission_not_granted, new Object[0]);
                    }
                });
            } else if (requestCode == Crop.REQUEST_CROP) {
                new AsyncTask<Void, Void, Void>() {
                    protected void onPreExecute() {
                        super.onPreExecute();
                        ProfileSettingsActivity.this.showProgressDialog();
                    }

                    protected Void doInBackground(Void[] params) {
                        Bitmap photoBitmap;
                        Uri output = Crop.getOutput(responseIntent);
                        Resources resources = NavdyApplication.getAppContext().getResources();
                        int width = resources.getDimensionPixelSize(R.dimen.contact_image_width);
                        int height = resources.getDimensionPixelSize(R.dimen.contact_image_height);
                        if (output != null) {
                            photoBitmap = ImageUtils.getScaledBitmap(new File(output.getPath()), width, height);
                        } else {
                            photoBitmap = null;
                        }
                        if (photoBitmap != null) {
                            ProfileSettingsActivity.this.userPhoto = photoBitmap;
                            ProfileSettingsActivity.this.somethingChanged = true;
                            Tracker.saveUserPhotoToInternalStorage(photoBitmap);
                            SettingsUtils.incrementDriverProfileSerial();
                            SettingsUtils.sendDriverProfileToTheHudBasedOnSharedPrefValue();
                            if (!new File(output.getPath()).delete()) {
                                ProfileSettingsActivity.this.logger.w("Unable to delete temporary photo file.");
                            }
                        }
                        return null;
                    }

                    protected void onPostExecute(Void aVoid) {
                        String fullName;
                        super.onPostExecute(aVoid);
                        ProfileSettingsActivity.this.hideProgressDialog();
                        if (ProfileSettingsActivity.this.nameInput != null) {
                            fullName = ProfileSettingsActivity.this.nameInput.getText().toString();
                        } else {
                            fullName = "";
                        }
                        ProfileSettingsActivity.this.updatePhoto(ProfileSettingsActivity.this.userPhoto, fullName);
                    }
                }.execute(new Void[0]);
            }
        }
        super.onActivityResult(requestCode, resultCode, responseIntent);
    }

    public void onChangePhoto(View view) {
        CharSequence[] options = getResources().getStringArray(R.array.settings_profile_edit_picture);
        Builder builder = new Builder(this);
        builder.setTitle((int) R.string.settings_profile_change_photo_btn);
        builder.setItems(options, new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        ProfileSettingsActivity.this.requestStoragePermission(new Runnable() {
                            public void run() {
                                Crop.pickImage(ProfileSettingsActivity.this);
                            }
                        }, new Runnable() {
                            public void run() {
                                BaseActivity.showLongToast(R.string.storage_permission_not_granted, new Object[0]);
                            }
                        });
                        return;
                    case 1:
                        PhotoUtils.takePhoto(ProfileSettingsActivity.this, ProfileSettingsActivity.this.logger, PhotoUtils.TEMP_PHOTO_FILE);
                        return;
                    default:
                        Tracker.eraseUserProfilePhoto();
                        if (ProfileSettingsActivity.this.profileFragment != null) {
                            ProfileSettingsActivity.this.photo.setImage(R.drawable.icon_user_bg_4, ProfileSettingsActivity.this.profileFragment.getNameText());
                            ProfileSettingsActivity.this.somethingChanged = true;
                            return;
                        }
                        return;
                }
            }
        });
        builder.show();
    }

    public void onPrivacyPolicyClick(View view) {
        Intent browserIntent = new Intent(getApplicationContext(), WebViewActivity.class);
        browserIntent.putExtra("type", WebViewActivity.PRIVACY);
        startActivity(browserIntent);
    }

    public void onNextProfileStepClick(View view) {
        if (saveProfileInfo()) {
            onProfileInfoSet();
        }
    }

    public void onProfileInfoSet() {
        finish();
    }

    private void updatePhoto(String fullName) {
        if (this.photo != null && this.userPhoto == null) {
            this.photo.setImage(R.drawable.icon_user_bg_4, fullName);
        }
    }

    private void updatePhoto(Bitmap userPhoto, String fullName) {
        if (this.photo != null) {
            TextView hintText = (TextView) findViewById(R.id.photo_hint_text);
            if (userPhoto != null) {
                this.photo.setImage(userPhoto);
                if (hintText != null) {
                    hintText.setText(R.string.edit_photo);
                    return;
                }
                return;
            }
            this.photo.setImage(R.drawable.icon_user_bg_4, fullName);
            if (hintText != null) {
                hintText.setText(R.string.add_photo);
            }
        }
    }

    protected void saveChanges() {
        saveProfileInfo();
    }

    private boolean saveProfileInfo() {
        boolean isOnMainThread = false;
        boolean success = validateAndSaveEmail(validateAndSaveName(true));
        if (success) {
            BaseActivity.showShortToast(R.string.settings_profile_edit_succeeded, new Object[0]);
            SystemUtils.dismissKeyboard(this);
            Tracker.registerUser(this.email, this.name);
            if (Looper.myLooper() == Looper.getMainLooper()) {
                isOnMainThread = true;
            }
            if (isOnMainThread) {
                TaskManager.getInstance().execute(new Runnable() {
                    public void run() {
                        SettingsUtils.sendDriverProfileToTheHudBasedOnSharedPrefValue();
                    }
                }, 1);
            } else {
                SettingsUtils.sendDriverProfileToTheHudBasedOnSharedPrefValue();
            }
        }
        return success;
    }

    private boolean validateName() {
        if (StringUtils.isValidName(this.nameInput.getText().toString())) {
            this.nameLabel.setHintTextAppearance(R.style.blue_text);
            ViewCompat.setBackgroundTintList(this.nameInput, this.greyColorStateList);
            return true;
        }
        this.nameLabel.setHintTextAppearance(R.style.red_text_button);
        ViewCompat.setBackgroundTintList(this.nameInput, this.redColorStateList);
        return false;
    }

    private boolean validateEmail() {
        String newEmail = this.emailInput.getText().toString();
        if (!StringUtils.isEmptyAfterTrim(newEmail) && newEmail.matches(".*[\\u0009\\u000A\\u000B\\u000C\\u000D\\u0020\\u0085\\u00A0\\u1680\\u180E\\u2000\\u2001\\u2002\\u2003\\u2004\\u2005\\u2006\\u2007\\u2008\\u2009\\u200A\\u2028\\u2029\\u202F\\u205F\\u3000].*")) {
            newEmail = newEmail.replaceAll(StringUtils.WHITESPACE_REGEXP, "");
            this.emailInput.setText(newEmail);
        }
        if (StringUtils.isValidEmail(newEmail)) {
            this.emailLabel.setHintTextAppearance(R.style.blue_text);
            ViewCompat.setBackgroundTintList(this.emailInput, this.greyColorStateList);
            return true;
        }
        this.emailLabel.setHintTextAppearance(R.style.red_text_button);
        ViewCompat.setBackgroundTintList(this.emailInput, this.redColorStateList);
        return false;
    }

    protected boolean validateAndSaveName(boolean success) {
        if (this.nameInput == null) {
            return success;
        }
        String newName = this.nameInput.getText().toString();
        boolean differentFromBefore = isDifferentFromBefore(newName, this.name);
        if (differentFromBefore) {
            this.somethingChanged = true;
        }
        if (validateName()) {
            this.name = newName;
            if (this.customerPrefs == null) {
                return success;
            }
            this.customerPrefs.edit().putString(ProfilePreferences.FULL_NAME, this.name).apply();
            return success;
        }
        if (differentFromBefore) {
            BaseActivity.showLongToast(R.string.settings_profile_edit_invalid_name, new Object[0]);
        }
        return false;
    }

    protected boolean validateAndSaveEmail(boolean success) {
        if (this.emailInput == null) {
            return success;
        }
        String newEmail = this.emailInput.getText().toString();
        if (!StringUtils.isEmptyAfterTrim(newEmail) && newEmail.matches(".*[\\u0009\\u000A\\u000B\\u000C\\u000D\\u0020\\u0085\\u00A0\\u1680\\u180E\\u2000\\u2001\\u2002\\u2003\\u2004\\u2005\\u2006\\u2007\\u2008\\u2009\\u200A\\u2028\\u2029\\u202F\\u205F\\u3000].*")) {
            newEmail = newEmail.replaceAll(StringUtils.WHITESPACE_REGEXP, "");
            this.emailInput.setText(newEmail);
        }
        boolean differentFromBefore = isDifferentFromBefore(newEmail, this.email);
        if (differentFromBefore) {
            this.somethingChanged = true;
        }
        if (validateEmail()) {
            this.email = newEmail;
            if (this.customerPrefs == null) {
                return success;
            }
            this.customerPrefs.edit().putString("email", this.email).apply();
            return success;
        }
        if (differentFromBefore) {
            BaseActivity.showLongToast(R.string.settings_profile_edit_invalid_email, new Object[0]);
        }
        return false;
    }

    public boolean isDifferentFromBefore(String newString, String oldString) {
        return (StringUtils.isEmptyAfterTrim(newString) == StringUtils.isEmptyAfterTrim(oldString) && StringUtils.equalsOrBothEmptyAfterTrim(newString, oldString)) ? false : true;
    }
}
