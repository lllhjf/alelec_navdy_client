package com.navdy.client.app.ui.firstlaunch;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.models.MountInfo.MountType;
import com.navdy.client.app.framework.util.ImageCache;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.ui.ImageResourcePagerAdaper;
import com.navdy.client.app.ui.VerticalViewPager;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.service.library.log.Logger;
import java.util.concurrent.atomic.AtomicBoolean;

public class InstallLensCheckFragment extends InstallCardFragment {
    public static final int ANIMATION_DURATION = 2500;
    private static final int[] MEDIUM_TALL_MOUNT_IMAGES = new int[]{R.drawable.img_installation_lens_medium_tall_high, R.drawable.img_installation_lens_medium_tall_success, R.drawable.img_installation_lens_medium_tall_low};
    private static final int[] SHORT_MOUNT_2016_IMAGES = new int[]{R.drawable.img_installation_lens_2016_short_high, R.drawable.img_installation_lens_2016_short_success, R.drawable.img_installation_lens_2016_short_low};
    private static final int[] SHORT_MOUNT_2017_IMAGES = new int[]{R.drawable.img_installation_lens_2017_short_high, R.drawable.img_installation_lens_2017_short_success, R.drawable.img_installation_lens_2017_short_low};
    protected static final boolean VERBOSE = false;
    protected static final Logger logger = new Logger(InstallLensCheckFragment.class);
    ImageCache imageCache = new ImageCache();
    private AtomicBoolean isAnimating = new AtomicBoolean(false);
    private MountType mountType;
    VerticalViewPager mountViewPager;
    private ImageResourcePagerAdaper mountViewPagerAdapter;

    private class LensCheckPageListener implements OnPageChangeListener {
        final View[] dots;

        private LensCheckPageListener() {
            this.dots = new View[]{InstallLensCheckFragment.this.rootView.findViewById(R.id.lens_too_high_dot), InstallLensCheckFragment.this.rootView.findViewById(R.id.lens_ok_dot), InstallLensCheckFragment.this.rootView.findViewById(R.id.lens_too_low_dot)};
        }

//        /* synthetic */ LensCheckPageListener(InstallLensCheckFragment x0, AnonymousClass1 x1) {
//            this();
//        }

        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        public void onPageSelected(int position) {
            int i = 0;
            while (i < this.dots.length) {
                if (this.dots[i] != null) {
                    this.dots[i].setVisibility(position == i ? View.GONE : View.INVISIBLE);
                }
                i++;
            }
        }

        public void onPageScrollStateChanged(int state) {
        }
    }

    public InstallLensCheckFragment() {
        setLayoutId(R.layout.fle_install_lens_check);
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.rootView = inflater.inflate(R.layout.fle_install_lens_check, container, false);
        this.mountViewPager = (VerticalViewPager) this.rootView.findViewById(R.id.mount_illustration_view_pager);
        updateMountType();
        return this.rootView;
    }

    public void setMountType(MountType type) {
        this.mountType = type;
    }

    public void updateMountType() {
        if (this.rootView != null) {
            TextView textOk = (TextView) this.rootView.findViewById(R.id.lens_ok_title);
            TextView textTooLow = (TextView) this.rootView.findViewById(R.id.lens_too_low_title);
            ((TextView) this.rootView.findViewById(R.id.lens_too_high_title)).setText(StringUtils.fromHtml((int) R.string.lens_too_high));
            textOk.setText(StringUtils.fromHtml((int) R.string.lens_ok));
            textTooLow.setText(StringUtils.fromHtml((int) R.string.lens_too_low));
            if (this.mountViewPager != null) {
                if (this.mountType != MountType.SHORT) {
                    this.mountViewPagerAdapter = new ImageResourcePagerAdaper(getContext(), MEDIUM_TALL_MOUNT_IMAGES, this.imageCache);
                } else if (StringUtils.equalsOrBothEmptyAfterTrim(SettingsUtils.getSharedPreferences().getString(SettingsConstants.BOX, "Old_Box"), "Old_Box")) {
                    this.mountViewPagerAdapter = new ImageResourcePagerAdaper(getContext(), SHORT_MOUNT_2016_IMAGES, this.imageCache);
                } else {
                    this.mountViewPagerAdapter = new ImageResourcePagerAdaper(getContext(), SHORT_MOUNT_2017_IMAGES, this.imageCache);
                }
                this.mountViewPager.setAdapter(this.mountViewPagerAdapter);
                this.mountViewPager.addOnPageChangeListener(new LensCheckPageListener());
                this.mountViewPager.setCurrentItem(1);
            }
            animate();
        }
    }

    private void animate() {
        if (this.mountViewPager != null && this.mountViewPagerAdapter != null && !this.isAnimating.get()) {
            this.isAnimating.set(true);
            this.handler.postDelayed(new Runnable() {
                public void run() {
                    int position = InstallLensCheckFragment.this.mountViewPager.getCurrentItem() + 1;
                    if (position >= InstallLensCheckFragment.this.mountViewPagerAdapter.getCount()) {
                        position = 0;
                    }
                    InstallLensCheckFragment.this.mountViewPager.setCurrentItem(position);
                    InstallLensCheckFragment.this.handler.postDelayed(this, 2500);
                }
            }, 2500);
        }
    }

    public void onPause() {
        this.handler.removeCallbacksAndMessages(null);
        this.isAnimating.set(false);
        super.onPause();
    }

    public void onResume() {
        super.onResume();
        animate();
    }

    public void onDestroy() {
        this.imageCache.clearCache();
        super.onDestroy();
    }
}
