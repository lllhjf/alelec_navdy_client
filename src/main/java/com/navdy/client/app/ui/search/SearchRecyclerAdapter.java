package com.navdy.client.app.ui.search;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView.Adapter;
import android.text.Spannable;
import android.text.Spannable.Factory;
import android.text.style.ForegroundColorSpan;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.LatLngBounds.Builder;
import com.google.android.gms.maps.model.MarkerOptions;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.location.NavdyLocationManager;
import com.navdy.client.app.framework.map.MapUtils;
import com.navdy.client.app.framework.models.Address;
import com.navdy.client.app.framework.models.ContactModel;
import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.framework.models.Destination.SearchType;
import com.navdy.client.app.framework.models.Destination.Type;
import com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.framework.util.VoiceCommandUtils;
import com.navdy.client.app.providers.NavdyContentProvider;
import com.navdy.client.app.ui.base.BaseGoogleMapFragment;
import com.navdy.client.app.ui.homescreen.CalendarUtils;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.events.places.PlacesSearchResult;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;
import com.navdy.service.library.util.SystemUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import static android.view.View.GONE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

public class SearchRecyclerAdapter extends Adapter<SearchViewHolder> {
    private static final int CONTACT_PHOTO_RES_VALUE = 0;
    private static final double INVALID_DISTANCE = -1.0d;
    private static final int NO_FLAGS = 0;
    private static final boolean VERBOSE = false;
    private static final Logger logger = new Logger(SearchRecyclerAdapter.class);
    private RelativeLayout card;
    private HashMap<String, Bitmap> contactPhotos;
    private Context context;
    Mode currentMode = Mode.EMPTY;
    private BaseGoogleMapFragment googleMapFragment;
    private String lastQuery;
    private OnClickListener listener;
    private HashMap<String, Integer> markers = new HashMap();
    private final NavdyLocationManager navdyLocationManager = NavdyLocationManager.getInstance();
    private List<SearchItem> searchItems;
    private final int searchType;

    public enum ImageEnum {
        PLACE,
        CONTACT,
        CONTACT_PHOTO,
        SEARCH,
        SEARCH_HISTORY,
        NONE
    }

    enum Mode {
        EMPTY,
        AUTO_COMPLETE,
        FULL_SEARCH
    }

    static class SearchItem {
        public ContactModel contact;
        public Destination destination;
        public Double distance;
        ImageEnum imageEnum;
        String price;

        SearchItem(Destination destination, Double distance, String price, ImageEnum imageEnum, ContactModel contact) {
            this.destination = destination;
            this.distance = distance;
            this.price = price;
            this.imageEnum = imageEnum;
            this.contact = contact;
        }

        int getRes(boolean isAutocomplete) {
            switch (this.imageEnum) {
                case PLACE:
                    if (isAutocomplete) {
                        return this.destination.getBadgeAssetForAutoComplete();
                    }
                    return this.destination.getBadgeAssetForSearchResults();
                case CONTACT:
                    return R.drawable.ic_person_black;
                case SEARCH:
                case SEARCH_HISTORY:
                    return R.drawable.recent_search_icon;
                default:
                    return 0;
            }
        }
    }

    SearchRecyclerAdapter(int searchType, BaseGoogleMapFragment googleMapFragment, RelativeLayout card) {
        this.searchType = searchType;
        this.searchItems = new ArrayList();
        this.contactPhotos = new HashMap();
        this.context = NavdyApplication.getAppContext();
        this.googleMapFragment = googleMapFragment;
        this.card = card;
        logger.v("bus registered");
    }

    void setUpServicesAndHistory(boolean showService) {
        if (showService && (SystemUtils.isConnectedToNetwork(this.context) || AppInstance.getInstance().isDeviceConnected())) {
            addSearchPoweredByGoogleHeader();
            Location lastLocation = this.navdyLocationManager.getSmartStartLocation();
            if (!(lastLocation == null || lastLocation.getLongitude() == 0.0d || lastLocation.getLatitude() == 0.0d)) {
                Destination servicesDestination = new Destination();
                servicesDestination.name = "";
                servicesDestination.searchResultType = SearchType.SERVICES_SEARCH.getValue();
                this.searchItems.add(new SearchItem(servicesDestination, Double.valueOf(-1.0d), "", ImageEnum.NONE, null));
            }
        }
        try {
            Cursor cursor = NavdyContentProvider.getSearchHistoryCursor();
            while (cursor != null && cursor.moveToNext()) {
                int idIndex = cursor.getColumnIndex("_id");
                int queryIndex = cursor.getColumnIndex("query");
                int id = cursor.getInt(idIndex);
                String query = cursor.getString(queryIndex);
                Destination destination = new Destination();
                destination.id = id;
                destination.name = query;
                destination.searchResultType = SearchType.SEARCH_HISTORY.getValue();
                this.searchItems.add(new SearchItem(destination, Double.valueOf(-1.0d), "", ImageEnum.SEARCH_HISTORY, null));
            }
            IOUtils.closeStream(cursor);
        } catch (Throwable th) {
            IOUtils.closeStream(null);
        }
    }

    void setClickListener(OnClickListener onClickListener) {
        logger.v("setClickListener: " + onClickListener);
        this.listener = onClickListener;
    }

    public int getItemCount() {
        return (this.currentMode == Mode.FULL_SEARCH ? 1 : 0) + this.searchItems.size();
    }

    @Nullable
    SearchItem getItemAt(int position) {
        if (position < 0 || position >= this.searchItems.size()) {
            return null;
        }
        return (SearchItem) this.searchItems.get(position);
    }

    @Nullable
    Destination getDestinationAt(int position) {
        SearchItem item = getItemAt(position);
        if (item != null) {
            return item.destination;
        }
        return null;
    }

    public int getItemViewType(int position) {
        if (position >= this.searchItems.size()) {
            return SearchType.FOOTER.getValue();
        }
        SearchItem searchItem = (SearchItem) this.searchItems.get(position);
        Destination destination = null;
        if (searchItem != null) {
            destination = searchItem.destination;
        }
        if (destination != null) {
            return destination.searchResultType;
        }
        return SearchType.UNKNOWN.getValue();
    }

    public SearchViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        if (isHeader(viewType)) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_header_powered_by_google, parent, false);
            setTitleForHeader(v, viewType);
        } else if (viewType == SearchType.SERVICES_SEARCH.getValue()) {
            return new SearchViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.search_list_item_services, parent, false));
        } else {
            if (viewType == SearchType.NO_RESULTS.getValue()) {
                return new SearchViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.search_list_item_no_results, parent, false));
            }
            if (viewType == SearchType.FOOTER.getValue()) {
                return new SearchViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.padding_footer_layout, parent, false));
            }
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_list_item, parent, false);
        }
        return new SearchViewHolder(v);
    }

    private void setTitleForHeader(View v, int viewType) {
        TextView leftTitle = (TextView) v.findViewById(R.id.left_title);
        ImageView rightSideImage = (ImageView) v.findViewById(R.id.right_side_image);
        if (leftTitle == null) {
            logger.e("The Search Header view doesn't exist");
        } else if (viewType == SearchType.SEARCH_POWERED_BY_GOOGLE_HEADER.getValue()) {
            leftTitle.setText(R.string.search_powered_by);
            if (SystemUtils.isConnectedToNetwork(this.context) && rightSideImage != null) {
                rightSideImage.setImageResource(R.drawable.google_2015_logo);
                rightSideImage.setVisibility(VISIBLE);
            }
        }
    }

    public void onBindViewHolder(SearchViewHolder searchViewHolder, int position) {
        setCardData(searchViewHolder, position);
    }

    void setCardData(SearchViewHolder searchViewHolder, final int position) {
        int itemViewType = getItemViewType(position);
        if (itemViewType != SearchType.FOOTER.getValue()) {
            SearchItem searchItem = (SearchItem) this.searchItems.get(position);
            if (searchItem == null) {
                logger.e("Unable to get the searchItem at position: " + position);
                return;
            }
            Destination destination = searchItem.destination;
            if (destination == null) {
                logger.e("Unable to get the destination at position: " + position);
            } else if (searchViewHolder == null) {
                logger.e("Unable to get the searchViewHolder at position: " + position);
            } else if (isHeader(itemViewType)) {
                View v = searchViewHolder.itemView;
                setTitleForHeader(v, itemViewType);
                ImageView rightSideImage = (ImageView) v.findViewById(R.id.right_side_image);
                if (SystemUtils.isConnectedToNetwork(this.context)) {
                    if (itemViewType == SearchType.SEARCH_POWERED_BY_GOOGLE_HEADER.getValue()) {
                        if (this.currentMode == Mode.AUTO_COMPLETE) {
                            v.setBackgroundResource(R.color.white);
                        } else {
                            v.setBackgroundResource(R.color.search_header_online_background);
                        }
                        if (rightSideImage != null) {
                            rightSideImage.setImageResource(R.drawable.google_2015_logo);
                            rightSideImage.setVisibility(VISIBLE);
                        }
                    }
                } else if (rightSideImage != null) {
                    rightSideImage.setVisibility(INVISIBLE);
                }
                if (position == getItemCount() - 1 || this.currentMode == Mode.AUTO_COMPLETE) {
                    searchViewHolder.hideBottomDivider();
                } else {
                    searchViewHolder.showLongDivider();
                }
            } else {
                if (position == getItemCount() - 1) {
                    searchViewHolder.hideBottomDivider();
                } else if (this.currentMode == Mode.FULL_SEARCH) {
                    searchViewHolder.showLongDivider();
                } else {
                    searchViewHolder.showShortDivider();
                }
                if (itemViewType != SearchType.NO_RESULTS.getValue() && itemViewType != SearchType.SERVICES_SEARCH.getValue() && position < this.searchItems.size() && !this.searchItems.isEmpty()) {
                    Spannable highlightedText;
                    Pair<String, String> titleAndSubtitle = destination.getTitleAndSubtitle();
                    String title = titleAndSubtitle.first;
                    if (destination.searchResultType == SearchType.MORE.getValue()) {
                        searchViewHolder.setTitle(title);
                    } else if (this.currentMode == Mode.AUTO_COMPLETE) {
                        highlightedText = highlightSearchedText(title, this.lastQuery);
                        if (highlightedText != null) {
                            searchViewHolder.setTitle(highlightedText);
                        } else {
                            searchViewHolder.setTitle(title, R.color.grey);
                        }
                    } else {
                        searchViewHolder.setTitle(title, R.color.black);
                    }
                    String subTitle = titleAndSubtitle.second;
                    if (this.currentMode == Mode.AUTO_COMPLETE) {
                        highlightedText = highlightSearchedText(subTitle, this.lastQuery);
                        if (highlightedText != null) {
                            searchViewHolder.setDetails(highlightedText);
                        } else {
                            searchViewHolder.setDetails(subTitle, R.color.grey);
                        }
                    } else {
                        searchViewHolder.setDetails(subTitle);
                    }
                    if (this.currentMode == Mode.FULL_SEARCH) {
                        setRowHeightTo(searchViewHolder.row, R.dimen.card_height);
                    } else {
                        setRowHeightTo(searchViewHolder.row, R.dimen.card_height_small);
                    }
                    boolean isAutocomplete = this.currentMode == Mode.AUTO_COMPLETE;
                    switch (searchItem.imageEnum) {
                        case PLACE:
                            searchViewHolder.setImage(searchItem, isAutocomplete);
                            searchViewHolder.showNavButton();
                            break;
                        case CONTACT:
                            searchViewHolder.setImage(title);
                            searchViewHolder.showNavButton();
                            break;
                        case CONTACT_PHOTO:
                            searchViewHolder.setImage((Bitmap) this.contactPhotos.get(title));
                            searchViewHolder.showNavButton();
                            break;
                        case SEARCH_HISTORY:
                            searchViewHolder.setImage(searchItem, isAutocomplete);
                            searchViewHolder.showDeleteButton();
                            break;
                        default:
                            searchViewHolder.setImage(searchItem, isAutocomplete);
                            searchViewHolder.hideNavButton();
                            break;
                    }
                    if (itemViewType == SearchType.CONTACT_ADDRESS_SELECTION.getValue() || (SearchActivity.isFavoriteSearch(this.searchType) && destination.searchResultType != SearchType.SEARCH_HISTORY.getValue())) {
                        searchViewHolder.hideNavButton();
                    }
                    searchViewHolder.setDistance(searchItem.distance);
                    searchViewHolder.setPrice(searchItem.price);
                    final int i;
                    searchViewHolder.row.setOnClickListener(new OnClickListener() {
                        public void onClick(View v) {
                            v.setTag(position);
                            SearchRecyclerAdapter.this.listener.onClick(v);
                            SearchRecyclerAdapter.logger.v("click on item at position " + position);
                        }
                    });
                    searchViewHolder.navButton.setOnClickListener(new OnClickListener() {
                        public void onClick(View v) {
                            v.setTag(position);
                            SearchRecyclerAdapter.this.listener.onClick(v);
                        }
                    });
                }
            }
        }
    }

    private void setRowHeightTo(View row, int dimenRes) {
        LayoutParams params = (LayoutParams) row.getLayoutParams();
        params.height = (int) ((float) this.context.getResources().getDimensionPixelSize(dimenRes));
        row.setLayoutParams(params);
    }

    private static boolean isHeader(int itemViewType) {
        return itemViewType == SearchType.SEARCH_POWERED_BY_GOOGLE_HEADER.getValue();
    }

    private static Spannable highlightSearchedText(String autocompleteName, String query) {
        if (StringUtils.isEmptyAfterTrim(autocompleteName) || StringUtils.isEmptyAfterTrim(query)) {
            return null;
        }
        String autocompleteNameLowercase = autocompleteName.toLowerCase();
        String queryLowercase = query.toLowerCase();
        Spannable queryWithHighlightedSelection = Factory.getInstance().newSpannable(autocompleteName);
        int startIndex = 0;
        int length = autocompleteName.length();
        int queryLength = queryLowercase.length();
        do {
            int lastEndIndex = startIndex;
            startIndex = autocompleteNameLowercase.indexOf(queryLowercase, lastEndIndex);
            if (startIndex < 0) {
                startIndex = lastEndIndex;
                break;
            }
            int endIndex = startIndex + queryLength;
            if (endIndex > length) {
                endIndex = length;
            }
            if (startIndex > lastEndIndex) {
                queryWithHighlightedSelection.setSpan(new ForegroundColorSpan(-7829368), lastEndIndex, startIndex, 0);
            }
            queryWithHighlightedSelection.setSpan(new ForegroundColorSpan(-16777216), startIndex, endIndex, 0);
            startIndex = endIndex;
        } while (startIndex < length);
        if (startIndex >= length) {
            return queryWithHighlightedSelection;
        }
        queryWithHighlightedSelection.setSpan(new ForegroundColorSpan(-7829368), startIndex, length, 0);
        return queryWithHighlightedSelection;
    }

    void updateRecyclerViewWithAutocomplete(@Nullable List<GoogleTextSearchDestinationResult> newAddresses, @Nullable ArrayList<String> previousSearches, @Nullable CharSequence constraint) {
        if (newAddresses == null) {
            newAddresses = new ArrayList(0);
        }
        if (previousSearches == null) {
            previousSearches = new ArrayList(0);
        }
        logger.v("updateRecyclerViewWithAutocomplete");
        for (GoogleTextSearchDestinationResult newAddress : newAddresses) {
            if (!StringUtils.isEmptyAfterTrim(newAddress.place_id)) {
                insertAutoCompleteItem(newAddress, true);
            }
        }
        if (constraint != null) {
            createSearchForMoreFooter(constraint.toString());
        } else {
            constraint = "";
        }
        ArrayList<String> googleSearchSuggestions = new ArrayList(newAddresses.size());
        for (GoogleTextSearchDestinationResult newAddress2 : newAddresses) {
            if (StringUtils.isEmptyAfterTrim(newAddress2.place_id) && !StringUtils.equalsOrBothEmptyAfterTrim(newAddress2.name, constraint)) {
                insertAutoCompleteItem(newAddress2);
                googleSearchSuggestions.add(newAddress2.name.toLowerCase());
            }
        }
        Iterator it = previousSearches.iterator();
        while (it.hasNext()) {
            String previousSearch = (String) it.next();
            if (!(googleSearchSuggestions.contains(previousSearch.toLowerCase()) || previousSearch.equalsIgnoreCase(constraint.toString()))) {
                insertPreviousSearch(previousSearch);
                googleSearchSuggestions.add(previousSearch.toLowerCase());
            }
        }
    }

    private void insertAutoCompleteItem(GoogleTextSearchDestinationResult newAddress) {
        insertAutoCompleteItem(newAddress, false);
    }

    private void insertAutoCompleteItem(GoogleTextSearchDestinationResult newAddress, boolean checkForDuplicates) {
        Destination destination = new Destination();
        destination.name = newAddress.name;
        destination.rawAddressNotForDisplay = newAddress.address;
        destination.placeId = newAddress.place_id;
        destination.searchResultType = SearchType.AUTOCOMPLETE.getValue();
        if (checkForDuplicates) {
            for (SearchItem searchItem : this.searchItems) {
                if (destination.equals(searchItem.destination)) {
                    return;
                }
            }
        }
        ImageEnum imageEnum = ImageEnum.PLACE;
        if (StringUtils.isEmptyAfterTrim(destination.placeId)) {
            imageEnum = ImageEnum.SEARCH;
        }
        this.searchItems.add(new SearchItem(destination, Double.valueOf(-1.0d), "", imageEnum, null));
        notifyItemInserted(this.searchItems.size() - 1);
    }

    private void insertPreviousSearch(String previousSearch) {
        Destination destination = new Destination();
        destination.name = previousSearch;
        destination.searchResultType = SearchType.AUTOCOMPLETE.getValue();
        this.searchItems.add(new SearchItem(destination, Double.valueOf(-1.0d), "", ImageEnum.SEARCH, null));
        notifyItemInserted(this.searchItems.size() - 1);
    }

    void updateRecyclerViewWithDatabaseData(@Nullable ArrayList<Destination> destinationsFromDatabase, String query) {
        if (destinationsFromDatabase != null && !destinationsFromDatabase.isEmpty()) {
            Destination destination;
            logger.v("updateRecyclerViewWithDatabaseData");
            for (SearchItem searchItem : this.searchItems) {
                Iterator<Destination> iterator = destinationsFromDatabase.iterator();
                while (iterator.hasNext()) {
                    destination = (Destination) iterator.next();
                    if (destination.equals(searchItem.destination)) {
                        searchItem.destination = destination;
                        setLastRoutedIfRecent(searchItem);
                        iterator.remove();
                    }
                }
            }
            VoiceCommandUtils.addHomeAndWorkIfMatch(destinationsFromDatabase, query);
            Iterator it = destinationsFromDatabase.iterator();
            while (it.hasNext()) {
                destination = (Destination) it.next();
                ImageEnum imageEnum = destination.getImageEnum();
                destination.searchResultType = SearchType.RECENT_PLACES.getValue();
                String lastRoutedOn = "";
                if (this.currentMode == Mode.FULL_SEARCH && destination.isRecentDestination()) {
                    lastRoutedOn = this.context.getString(R.string.last_routed_on, new Object[]{CalendarUtils.getDateAndTime(destination.lastRoutedDate)});
                }
                this.searchItems.add(new SearchItem(destination, Double.valueOf(-1.0d), lastRoutedOn, imageEnum, null));
                addMarker(this.searchItems.size() - 1);
            }
            notifyDataSetChanged();
        }
    }

    private void setLastRoutedIfRecent(SearchItem searchItem) {
        if (this.currentMode == Mode.FULL_SEARCH && searchItem.destination.isRecentDestination()) {
            searchItem.destination.searchResultType = SearchType.RECENT_PLACES.getValue();
            String dateAndTime = CalendarUtils.getDateAndTime(searchItem.destination.lastRoutedDate);
            searchItem.price = this.context.getString(R.string.last_routed_on, new Object[]{dateAndTime});
            searchItem.distance = Double.valueOf(0.0d);
        }
    }

    void updateRecyclerViewWithPlaceSearchResults(List<PlacesSearchResult> placesSearchResults) {
        if (placesSearchResults != null && !placesSearchResults.isEmpty()) {
            logger.v("updateRecyclerViewWithPlaceSearchResults");
            addSearchPoweredByGoogleHeader();
            for (PlacesSearchResult placesSearchResult : placesSearchResults) {
                Double distance;
                Destination destination = new Destination();
                Destination.placesSearchResultToDestinationObject(placesSearchResult, destination);
                if (placesSearchResult.distance != null) {
                    distance = placesSearchResult.distance;
                } else {
                    distance = Double.valueOf(-1.0d);
                }
                this.searchItems.add(new SearchItem(destination, distance, "", ImageEnum.PLACE, null));
                addMarker(this.searchItems.size() - 1);
            }
            notifyDataSetChanged();
        }
    }

    void updateRecyclerViewWithTextSearchResults(@Nullable List<GoogleTextSearchDestinationResult> textSearchResults) {
        if (textSearchResults != null && !textSearchResults.isEmpty()) {
            logger.v("updateRecyclerViewWithTextSearchResults");
            addSearchPoweredByGoogleHeader();
            for (GoogleTextSearchDestinationResult result : textSearchResults) {
                Destination destination = new Destination();
                result.toModelDestinationObject(SearchType.TEXT_SEARCH, destination);
                this.searchItems.add(new SearchItem(destination, Double.valueOf(result.distance), getPriceString(result.price_level), destination.getImageEnum(), null));
                addMarker(this.searchItems.size() - 1);
            }
            notifyDataSetChanged();
        }
    }

    void updateRecyclerViewWithDestination(Destination destination) {
        logger.v("updateRecyclerViewWithDestination");
        addSearchPoweredByGoogleHeader();
        this.searchItems.add(new SearchItem(destination, destination.distance, getPriceString(destination.price_level), destination.getImageEnum(), null));
        addMarker(this.searchItems.size() - 1);
        notifyDataSetChanged();
    }

    void updateNearbySearchResultsWithKnownDestinationData() {
        final List<SearchItem> items = this.searchItems;
        new AsyncTask<Void, Void, Void>() {
            protected Void doInBackground(Void... params) {
                for (SearchItem searchItem : items) {
                    searchItem.destination.reloadSelfFromDatabase();
                    SearchRecyclerAdapter.this.setLastRoutedIfRecent(searchItem);
                }
                return null;
            }

            protected void onPostExecute(Void aVoid) {
                SearchRecyclerAdapter.this.notifyDataSetChanged();
            }
        }.execute(new Void[0]);
    }

    void updateRecyclerViewWithContactsForTextSearch(@Nullable List<ContactModel> contacts) {
        if (contacts != null && contacts.size() > 0) {
            logger.v("updateRecyclerViewWithContactsForTextSearch");
            for (ContactModel contact : contacts) {
                ArrayList<Address> addresses = getDeduplicatedAddressesForContact(contact);
                if (addresses.size() > 0) {
                    Iterator it = addresses.iterator();
                    while (it.hasNext()) {
                        Address address = (Address) it.next();
                        if (address != null) {
                            addContactWithOneAddress(contact, address);
                        }
                    }
                }
            }
            notifyDataSetChanged();
        }
    }

    void updateRecyclerViewWithContactsForAutoComplete(@Nullable List<ContactModel> contacts) {
        if (contacts != null && contacts.size() > 0) {
            logger.v("updateRecyclerViewWithContactsForAutoComplete");
            for (ContactModel contact : contacts) {
                ArrayList<Address> addresses = getDeduplicatedAddressesForContact(contact);
                if (addresses.size() > 0) {
                    if (addresses.size() == 1) {
                        addContactWithOneAddress(contact, (Address) addresses.get(0));
                    } else {
                        addContactWithMultipleAddress(contact, addresses);
                    }
                }
            }
            notifyDataSetChanged();
        }
    }

    @NonNull
    private ArrayList<Address> getDeduplicatedAddressesForContact(@Nullable ContactModel contact) {
        if (contact == null || contact.addresses == null || contact.addresses.isEmpty()) {
            return new ArrayList();
        }
        ArrayList<String> addedAddresses = new ArrayList(contact.addresses.size());
        ArrayList<Address> validAddresses = new ArrayList(contact.addresses.size());
        for (Address address : contact.addresses) {
            if (address != null) {
                String fullAddress = getCleanAddressString(address);
                if (!(StringUtils.isEmptyAfterTrim(fullAddress) || addedAddresses.contains(fullAddress))) {
                    addedAddresses.add(fullAddress);
                    validAddresses.add(address);
                }
            }
        }
        return validAddresses;
    }

    public static String getCleanAddressString(@Nullable Address address) {
        if (address == null) {
            return "";
        }
        String fullAddress = address.getFullAddress();
        if (StringUtils.isEmptyAfterTrim(fullAddress)) {
            return "";
        }
        return fullAddress.replace("\n", " ");
    }

    private void addContactWithOneAddress(@Nullable ContactModel contact, @Nullable Address address) {
        if (contact != null && address != null) {
            String fullAddress = getCleanAddressString(address);
            if (!StringUtils.isEmptyAfterTrim(fullAddress)) {
                String addressType;
                ImageEnum imageEnum;
                switch (address.type) {
                    case 0:
                        addressType = address.label;
                        break;
                    case 1:
                        addressType = this.context.getString(R.string.home_address_type);
                        break;
                    case 2:
                        addressType = this.context.getString(R.string.work_address_type);
                        break;
                    default:
                        addressType = this.context.getString(R.string.other_address_type);
                        break;
                }
                Destination destination = new Destination();
                destination.name = contact.name;
                destination.rawAddressNotForDisplay = fullAddress;
                destination.searchResultType = SearchType.CONTACT.getValue();
                destination.type = Type.CONTACT;
                destination.contactLookupKey = contact.lookupKey;
                destination.lastKnownContactId = contact.id;
                destination.lastContactLookup = contact.lookupTimestamp;
                if (contact.photo != null) {
                    imageEnum = ImageEnum.CONTACT_PHOTO;
                    this.contactPhotos.put(contact.name, contact.photo);
                } else {
                    imageEnum = ImageEnum.CONTACT;
                }
                this.searchItems.add(new SearchItem(destination, Double.valueOf(-1.0d), addressType, imageEnum, null));
            }
        }
    }

    private void addContactWithMultipleAddress(@Nullable ContactModel contact, @Nullable ArrayList<Address> preDeduplicatedAddresses) {
        if (contact != null && preDeduplicatedAddresses != null) {
            ImageEnum imageEnum;
            Destination destination = new Destination();
            destination.name = contact.name;
            destination.rawAddressNotForDisplay = this.context.getResources().getQuantityString(R.plurals.this_many_addresses, preDeduplicatedAddresses.size(), new Object[]{Integer.valueOf(preDeduplicatedAddresses.size())});
            destination.searchResultType = SearchType.CONTACT_ADDRESS_SELECTION.getValue();
            if (contact.photo != null) {
                imageEnum = ImageEnum.CONTACT_PHOTO;
                this.contactPhotos.put(contact.name, contact.photo);
            } else {
                imageEnum = ImageEnum.CONTACT;
            }
            contact.addresses = preDeduplicatedAddresses;
            this.searchItems.add(new SearchItem(destination, Double.valueOf(-1.0d), "", imageEnum, contact));
        }
    }

    void updateRecyclerViewWithContact(ContactModel contact) {
        if (contact == null || contact.addresses == null) {
            addNoResultsItem();
            return;
        }
        logger.v("updateRecyclerViewWithContact");
        for (Address address : contact.addresses) {
            addContactWithOneAddress(contact, address);
        }
    }

    void createSearchForMoreFooter(String autocompleteQuery) {
        Destination moreDestination = new Destination();
        moreDestination.name = autocompleteQuery;
        moreDestination.searchResultType = SearchType.MORE.getValue();
        this.searchItems.add(new SearchItem(moreDestination, Double.valueOf(-1.0d), "", ImageEnum.SEARCH, null));
        notifyDataSetChanged();
    }

    private boolean firstItemIsPoweredByGoogleHeader() {
        if (this.searchItems == null || this.searchItems.size() <= 0) {
            return false;
        }
        SearchItem searchItem = (SearchItem) this.searchItems.get(0);
        if (searchItem == null || searchItem.destination == null || searchItem.destination.searchResultType != SearchType.SEARCH_POWERED_BY_GOOGLE_HEADER.getValue()) {
            return false;
        }
        return true;
    }

    void addSearchPoweredByGoogleHeader() {
        if (AppInstance.getInstance().canReachInternet() && !firstItemIsPoweredByGoogleHeader()) {
            Destination header = new Destination();
            header.searchResultType = SearchType.SEARCH_POWERED_BY_GOOGLE_HEADER.getValue();
            this.searchItems.add(0, new SearchItem(header, Double.valueOf(-1.0d), "", ImageEnum.NONE, null));
        }
    }

    void removeSearchPoweredByGoogleHeader() {
        if (firstItemIsPoweredByGoogleHeader()) {
            this.searchItems.remove(0);
        }
    }

    void addNoResultsItemIfEmpty(List... lists) {
        if (lists == null || lists.length <= 0) {
            addNoResultsItem();
            return;
        }
        int length = lists.length;
        int i = 0;
        while (i < length) {
            List list = lists[i];
            if (list == null || list.isEmpty()) {
                i++;
            } else {
                return;
            }
        }
        addNoResultsItem();
    }

    void addNoResultsItem() {
        Destination noResultItem = new Destination();
        noResultItem.searchResultType = SearchType.NO_RESULTS.getValue();
        this.searchItems.add(new SearchItem(noResultItem, Double.valueOf(-1.0d), "", ImageEnum.NONE, null));
    }

    public static String getPriceString(String level) {
        if (level == null) {
            return "";
        }
        StringBuilder stringPrice = new StringBuilder();
        try {
            int price = Integer.parseInt(level);
            for (int i = 0; i < price; i++) {
                stringPrice.append("$");
            }
        } catch (Exception e) {
            logger.w("Unable to parse a price string out of " + level);
        }
        return stringPrice.toString();
    }

    void clear() {
        logger.v("clear");
        this.searchItems.clear();
        notifyDataSetChanged();
        this.contactPhotos.clear();
        if (this.googleMapFragment != null) {
            this.googleMapFragment.getMapAsync(new OnMapReadyCallback() {
                public void onMapReady(GoogleMap googleMap) {
                    if (googleMap != null) {
                        try {
                            googleMap.clear();
                        } catch (IllegalArgumentException e) {
                            SearchRecyclerAdapter.logger.e("Unable to clear the google map due to IllegalArgumentException.", e);
                        }
                    }
                }
            });
        }
        if (this.markers != null) {
            this.markers.clear();
        }
        if (this.card != null) {
            this.card.setVisibility(GONE);
        }
    }

    private void addMarker(final int destinationIndex) {
        if (this.googleMapFragment != null) {
            try {
                this.googleMapFragment.getMapAsync(new OnMapReadyCallback() {
                    public void onMapReady(GoogleMap googleMap) {
                        Destination destination = SearchRecyclerAdapter.this.getDestinationAt(destinationIndex);
                        if (destination == null) {
                            SearchRecyclerAdapter.logger.e("Unable to retrieve a destination from position: " + destinationIndex);
                        } else if (destination.hasOneValidSetOfCoordinates()) {
                            LatLng latlong;
                            MarkerOptions mo = new MarkerOptions();
                            if (destination.hasValidDisplayCoordinates()) {
                                latlong = new LatLng(destination.displayLat, destination.displayLong);
                            } else {
                                latlong = new LatLng(destination.navigationLat, destination.navigationLong);
                            }
                            mo.position(latlong);
                            mo.draggable(false);
                            mo.icon(BitmapDescriptorFactory.fromResource(destination.getUnselectedPinAsset()));
                            SearchRecyclerAdapter.this.markers.put(googleMap.addMarker(mo).getId(), Integer.valueOf(destinationIndex));
                        } else {
                            SearchRecyclerAdapter.logger.e("Unable to retrieve valid coordinates for the destination at position: " + destinationIndex);
                        }
                    }
                });
            } catch (Exception e) {
                logger.e("Unable to add marker: " + e);
            }
        }
    }

    Integer getDestinationIndexForMarker(String markerId) {
        return (Integer) this.markers.get(markerId);
    }

    void centerMapOnMarkers(boolean animate) {
        if (this.googleMapFragment != null) {
            List<LatLng> sanitizedLocations = new ArrayList();
            Coordinate myLocation = this.navdyLocationManager.getSmartStartCoordinates();
            if (MapUtils.isValidSetOfCoordinates(myLocation)) {
                sanitizedLocations.add(new LatLng(myLocation.latitude.doubleValue(), myLocation.longitude.doubleValue()));
            }
            if (this.searchItems != null) {
                for (SearchItem searchItem : this.searchItems) {
                    Destination destination = searchItem.destination;
                    if (destination != null) {
                        LatLng position = null;
                        if (MapUtils.isValidSetOfCoordinates(destination.displayLat, destination.displayLong)) {
                            position = new LatLng(destination.displayLat, destination.displayLong);
                        } else if (MapUtils.isValidSetOfCoordinates(destination.navigationLat, destination.navigationLong)) {
                            position = new LatLng(destination.navigationLat, destination.navigationLong);
                        }
                        if (position != null) {
                            sanitizedLocations.add(position);
                        }
                    }
                }
            }
            if (sanitizedLocations.size() > 1) {
                Builder builder = LatLngBounds.builder();
                for (LatLng latLng : sanitizedLocations) {
                    builder.include(latLng);
                }
                this.googleMapFragment.zoomTo(builder.build(), animate);
            } else if (sanitizedLocations.size() > 0) {
                this.googleMapFragment.moveMap((LatLng) sanitizedLocations.get(0), 14.0f, animate);
            }
        }
    }

    void setLastQuery(String query) {
        this.lastQuery = query;
    }
}
