package com.navdy.client.app.ui.settings;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.alelec.navdyclient.R;

public class ReplyViewHolder extends ViewHolder {
    public final ImageView more;
    public final TextView text;

    ReplyViewHolder(View itemView) {
        super(itemView);
        this.text = (TextView) itemView.findViewById(R.id.text);
        this.more = (ImageView) itemView.findViewById(R.id.more);
    }
}
