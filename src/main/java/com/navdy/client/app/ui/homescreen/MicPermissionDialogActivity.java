package com.navdy.client.app.ui.homescreen;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import com.alelec.navdyclient.R;
import com.navdy.client.app.ui.base.BaseActivity;

public class MicPermissionDialogActivity extends BaseActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.dialog_mic_permission);
    }

    public void onButtonClick(final View v) {
        requestMicrophonePermission(new Runnable() {
            public void run() {
                MicPermissionDialogActivity.this.onCloseClick(v);
            }
        }, new Runnable() {
            public void run() {
                Intent intent = new Intent(MicPermissionDialogActivity.this.getApplicationContext(), PermissionFailedActivity.class);
                intent.putExtra(PermissionFailedActivity.EXTRA_TITLE_RES, R.string.fle_app_setup_microphone_title_fail);
                intent.putExtra(PermissionFailedActivity.EXTRA_DESCRIPTION_RES, R.string.fle_app_setup_microphone_desc_fail);
                intent.putExtra(PermissionFailedActivity.EXTRA_BUTTON_RES, R.string.fle_app_setup_microphone_button_fail);
                MicPermissionDialogActivity.this.startActivity(intent);
                MicPermissionDialogActivity.this.finish();
            }
        });
        RelativeLayout dialog = (RelativeLayout) findViewById(R.id.dialog);
        if (dialog != null) {
            dialog.setAlpha(0.0f);
        }
    }

    public void onCloseClick(View view) {
        if (!isActivityDestroyed()) {
            finish();
        }
    }
}
