package com.navdy.client.app.ui.firstlaunch;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.Marketing;
import com.navdy.client.app.ui.UiUtils;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.service.library.task.TaskManager;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class MarketingFlowActivity extends BaseActivity {
    @DrawableRes
    private static final int[] BG_IMAGES = new int[]{R.drawable.img_meet_navdy_road, R.drawable.img_never_miss_turn_road, R.drawable.img_stay_connected, R.drawable.img_be_in_control_road};
    @StringRes
    private static final int[] DESC_TEXTS = new int[]{R.string.marketing_meet_navdy_desc, R.string.marketing_never_miss_turn_desc, R.string.marketing_stay_connected_desc, R.string.marketing_effortless_control_desc, R.string.marketing_get_started_desc};
    public static final int HAND_APPEARANCE_INDEX = 3;
    @DrawableRes
    private static final int[] HUD_UI_IMAGES = new int[]{R.drawable.img_fle_marketing_meet_navdy_ui, R.drawable.img_fle_marketing_never_miss_ui, R.drawable.img_fle_marketing_stay_connected_ui, R.drawable.img_fle_marketing_effortless_control_ui};
    @DrawableRes
    private static final int[] PAGINATIONS = new int[]{R.drawable.pagination_1, R.drawable.pagination_2, R.drawable.pagination_3, R.drawable.pagination_4, R.drawable.pagination_5};
    @StringRes
    private static final int[] TITLE_TEXTS = new int[]{R.string.marketing_meet_navdy, R.string.marketing_never_miss_turn, R.string.marketing_stay_connected, R.string.marketing_effortless_control, R.string.marketing_get_started};
    private int hiddenButtonClickCount = 0;

    protected void onStart() {
        super.onStart();
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                CarMdUtils.buildCarList();
            }
        }, 1);
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.fle_marketing_flow);
        if (userHasFinishedMarketing()) {
            callInstallActivity(true);
        }
        final ViewPager bgPager = (ViewPager) findViewById(R.id.bg_view_pager);
        final ViewPager hudUiPager = (ViewPager) findViewById(R.id.hud_ui_view_pager);
        final ViewPager textPager = (ViewPager) findViewById(R.id.text_view_pager);
        final ImageView pagination = (ImageView) findViewById(R.id.pagination);
        final ImageView hand = (ImageView) findViewById(R.id.hand);
        final TextView buyOne = (TextView) findViewById(R.id.buy_one);
        final ImageView logo = (ImageView) findViewById(R.id.navdy_logo);
        final Button start = (Button) findViewById(R.id.start_install);
        if (bgPager != null && hudUiPager != null && textPager != null && pagination != null && hand != null && buyOne != null && logo != null && start != null) {
            hand.setTranslationX(((float) hand.getWidth()) / 2.0f);
            hand.setTranslationY(((float) hand.getHeight()) * 0.75f);
            final float totalTranslationY = (float) UiUtils.convertDpToPx(135.0f);
            buyOne.setTranslationY(-totalTranslationY);
            Context appContext = getApplicationContext();
            bgPager.setAdapter(new MarketingFlowImagePagerAdaper(appContext, BG_IMAGES, this.imageCache));
            hudUiPager.setAdapter(new MarketingFlowImagePagerAdaper(appContext, HUD_UI_IMAGES, this.imageCache));
            MarketingFlowTextPagerAdaper textPagerAdapter = new MarketingFlowTextPagerAdaper(appContext, TITLE_TEXTS, DESC_TEXTS, new OnClickListener() {
                public void onClick(View v) {
                    MarketingFlowActivity.this.onTextViewPagerClick(v);
                }
            });
            buyOne.setText(StringUtils.fromHtml((int) R.string.buy_navdy_at_navdycom));
            textPager.setAdapter(textPagerAdapter);
            textPager.addOnPageChangeListener(new OnPageChangeListener() {
                boolean gotHandY = false;
                float handPivotY = 0.0f;

                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    if (!this.gotHandY) {
                        this.gotHandY = true;
                        this.handPivotY = hand.getPivotY();
                    }
                    hand.setPivotY(this.handPivotY + ((float) hand.getHeight()));
                    int scrollX = textPager.getScrollX();
                    bgPager.setScrollX(scrollX);
                    if (position >= 2 && position <= 4) {
                        hand.setRotation((-90.0f * (((((float) scrollX) - ((float) (textPager.getWidth() * 3))) * 100.0f) / ((float) textPager.getWidth()))) / 100.0f);
                    }
                    if (position >= MarketingFlowActivity.TITLE_TEXTS.length - 2) {
                        float percentage = ((((float) scrollX) - ((float) (textPager.getWidth() * (MarketingFlowActivity.TITLE_TEXTS.length - 2)))) * 100.0f) / ((float) textPager.getWidth());
                        float translation = (totalTranslationY * percentage) / 100.0f;
                        logo.setTranslationY(translation);
                        buyOne.setTranslationY((-totalTranslationY) + translation);
                        start.setTranslationY((percentage * ((float) UiUtils.convertDpToPx(80.0f))) / -100.0f);
                    }
                }

                public void onPageSelected(int position) {
                    hudUiPager.setCurrentItem(position);
                    pagination.setImageResource(MarketingFlowActivity.PAGINATIONS[position]);
                    if (position == 3) {
                        hand.setVisibility(VISIBLE);
                    } else {
                        hand.setVisibility(GONE);
                    }
                    if (position >= MarketingFlowActivity.TITLE_TEXTS.length - 2) {
                        start.setVisibility(VISIBLE);
                    } else {
                        start.setVisibility(GONE);
                    }
                    switch (position) {
                        case 1:
                            Tracker.tagScreen(Marketing.NEVER_MISS_A_TURN);
                            return;
                        case 2:
                            Tracker.tagScreen(Marketing.STAY_CONNECTED);
                            return;
                        case 3:
                            Tracker.tagScreen(Marketing.EFFORTLESS_CONTROL);
                            return;
                        case 4:
                            Tracker.tagScreen(Marketing.GET_STARTED);
                            return;
                        default:
                            Tracker.tagScreen(Marketing.MEET_NAVDY);
                            return;
                    }
                }

                public void onPageScrollStateChanged(int state) {
                    if (state == 0) {
                        bgPager.setCurrentItem(textPager.getCurrentItem());
                    }
                }
            });
            Tracker.tagScreen(Marketing.MEET_NAVDY);
        }
    }

    public void onTextViewPagerClick(View view) {
        ViewPager textPager = (ViewPager) findViewById(R.id.text_view_pager);
        if (textPager != null) {
            int nextItem = textPager.getCurrentItem() + 1;
            PagerAdapter adapter = textPager.getAdapter();
            if (adapter != null && nextItem < adapter.getCount()) {
                textPager.setCurrentItem(nextItem);
            }
        }
    }

    public void onBuyOneClick(View view) {
        openBrowserFor(Uri.parse(getString(R.string.purchase_link)));
    }

    public void onStartInstallClick(View view) {
        SettingsUtils.getSharedPreferences().edit().putBoolean(SettingsConstants.FINISHED_MARKETING, true).apply();
        callInstallActivity(false);
    }

    public void callInstallActivity(boolean wasSkiped) {
        Intent installIntent = new Intent(getApplicationContext(), InstallIntroActivity.class);
        installIntent.putExtra(SettingsConstants.EXTRA_WAS_SKIPPED, wasSkiped);
        startActivity(installIntent);
    }

    public static boolean userHasFinishedMarketing() {
        return SettingsUtils.getSharedPreferences().getBoolean(SettingsConstants.FINISHED_MARKETING, false);
    }

    public void onNavdyLogoClick(View view) {
    }
}
