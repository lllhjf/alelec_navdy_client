package com.navdy.client.app.ui.homescreen;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.UiThread;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.RecycledViewPool;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout.LayoutParams;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.framework.models.Suggestion;
import com.navdy.client.app.framework.models.Suggestion.SuggestionType;
import com.navdy.client.app.framework.navigation.NavdyRouteHandler;
import com.navdy.client.app.framework.navigation.NavdyRouteHandler.Error;
import com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo;
import com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteListener;
import com.navdy.client.app.framework.util.CustomItemClickListener;
import com.navdy.client.app.framework.util.CustomSuggestionsLongItemClickListener;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.framework.util.SuggestionManager;
import com.navdy.client.app.framework.util.SystemUtils;
import com.navdy.client.app.ui.customviews.ActiveTripCardView;
import com.navdy.client.app.ui.customviews.PendingTripCardView;
import com.navdy.service.library.log.Logger;
import java.util.ArrayList;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static android.view.View.INVISIBLE;

public class SuggestedDestinationsAdapter extends Adapter<ViewHolder> implements OnLongClickListener, NavdyRouteListener {
    private static final float DESELECTED_SUGGESTION_ELEVATION = 0.0f;
    private static final float SELECTED_SUGGESTION_ELEVATION = 8.0f;
    public static final int SUGGESTION_LIST_MAX_SIZE = 20;
    private static final boolean VERBOSE = false;
    private static final Logger logger = new Logger(SuggestedDestinationsAdapter.class);
    private ActiveTripCardView activeTripCardView;
    private final Context context;
    int googleMapHeight = -1;
    int hereMapHeight = -1;
    private CustomItemClickListener listener;
    private CustomSuggestionsLongItemClickListener longClickListener;
    private final NavdyRouteHandler navdyRouteHandler;
    int offlineBannerHeight = -1;
    private PendingTripCardView pendingTripCardView;
    private RecyclerView recyclerView;
    private View selectedItem;
    private int selectedItemPosition = -1;
    private SuggestionType selectedSuggestionType;
    @NonNull
    private ArrayList<Suggestion> suggestions = new ArrayList(20);

    SuggestedDestinationsAdapter(Context context, int googleMapHeight, int hereMapHeight, int offlineBannerHeight) {
        this.context = context;
        this.navdyRouteHandler = NavdyRouteHandler.getInstance();
        this.googleMapHeight = googleMapHeight;
        this.hereMapHeight = hereMapHeight;
        this.offlineBannerHeight = offlineBannerHeight;
        rebuildSuggestions();
    }

    int getGoogleMapHeight() {
        return this.googleMapHeight - (AppInstance.getInstance().canReachInternet() ? 0 : this.offlineBannerHeight);
    }

    int getHereMapHeight() {
        return this.hereMapHeight - (AppInstance.getInstance().canReachInternet() ? 0 : this.offlineBannerHeight);
    }

    @UiThread
    void onSuggestionBuildComplete(ArrayList<Suggestion> suggestions) {
        this.suggestions = suggestions;
        notifyDataSetChanged();
    }

    @MainThread
    void rebuildSuggestions() {
        stopRefreshingAllEtas();
        SystemUtils.ensureOnMainThread();
        this.selectedItem = null;
        this.selectedItemPosition = -1;
        this.selectedSuggestionType = null;
        this.suggestions = SuggestionManager.getEmptySuggestionState();
        notifyDataSetChanged();
        SuggestionManager.rebuildSuggestionListAndSendToHudAsync();
        if (this.pendingTripCardView != null) {
            this.pendingTripCardView.setNameAddressAndIcon(NavdyRouteHandler.getInstance().getCurrentDestination());
        }
    }

    void setClickListener(CustomItemClickListener onClickListener) {
        this.listener = onClickListener;
    }

    void setSuggestionLongClickListener(CustomSuggestionsLongItemClickListener onLongClickListener) {
        this.longClickListener = onLongClickListener;
    }

    @MainThread
    public Suggestion getItem(int position) {
        SystemUtils.ensureOnMainThread();
        position--;
        if (position < 0 || position >= this.suggestions.size()) {
            return null;
        }
        return (Suggestion) this.suggestions.get(position);
    }

    void setSelectedSuggestion(View selectedCardRow, SuggestionType suggestionType, int actionModePosition) {
        if (this.selectedItem != null) {
            setViewToNormalMode(this.selectedItem, this.selectedSuggestionType);
        }
        this.selectedItem = selectedCardRow;
        this.selectedItemPosition = actionModePosition;
        this.selectedSuggestionType = suggestionType;
        if (selectedCardRow != null) {
            setViewToSelectedMode(selectedCardRow, suggestionType);
        }
    }

    void endSelectionMode() {
        setViewToNormalMode(this.selectedItem, this.selectedSuggestionType);
        this.selectedItemPosition = -1;
        this.selectedItem = null;
        this.selectedSuggestionType = null;
    }

    void clickPendingTripCard(Context context) {
        if (this.pendingTripCardView != null) {
            this.pendingTripCardView.handleOnClick(context);
        }
    }

    void clickActiveTripCard() {
        if (this.activeTripCardView != null) {
            this.activeTripCardView.handleOnClick();
        }
    }

    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        this.recyclerView = recyclerView;
        this.navdyRouteHandler.addListener(this);
    }

    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        this.navdyRouteHandler.removeListener(this);
        clearAllAnimations();
        stopRefreshingAllEtas();
        this.recyclerView = null;
        super.onDetachedFromRecyclerView(recyclerView);
    }

    public void onViewRecycled(ViewHolder holder) {
        clearAnimationIfRunning(holder);
        super.onViewRecycled(holder);
    }

    public boolean onFailedToRecycleView(ViewHolder holder) {
        clearAnimationIfRunning(holder);
        return super.onFailedToRecycleView(holder);
    }

    void clearAllAnimations() {
        logger.d("Clearing all animations in the suggestion adapter.");
        int count = this.recyclerView.getChildCount();
        for (int i = 0; i < count; i++) {
            try {
                clearAnimationIfRunning(this.recyclerView.findViewHolderForAdapterPosition(i));
            } catch (Throwable t) {
                logger.e("Unable to clear animations", t);
            }
        }
        RecycledViewPool pool = this.recyclerView.getRecycledViewPool();
        if (pool != null) {
            while (true) {
                ViewHolder holder = pool.getRecycledView(SuggestionType.LOADING.ordinal());
                if (holder != null) {
                    clearAnimationIfRunning(holder);
                } else {
                    return;
                }
            }
        }
    }

    void stopRefreshingAllEtas() {
        logger.d("Removing all ETA refreshers");
        RoutedSuggestionsViewHolder.handler.removeCallbacksAndMessages(null);
    }

    private void clearAnimationIfRunning(ViewHolder holder) {
        if (holder != null && (holder instanceof ContentLoadingViewHolder)) {
            ((ContentLoadingViewHolder) holder).stopAnimation();
        }
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == SuggestionType.LOADING.ordinal()) {
            return new ContentLoadingViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_loading, parent, false));
        }
        View v;
        if (viewType == SuggestionType.GOOGLE_HEADER.ordinal()) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.google_map_header, parent, false);
            v.setLayoutParams(new LayoutParams(-1, getGoogleMapHeight()));
            return new SuggestionsViewHolder(v);
        } else if (viewType == SuggestionType.HERE_HEADER.ordinal()) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.here_map_header, parent, false);
            v.setLayoutParams(new LayoutParams(-1, getHereMapHeight()));
            return new SuggestionsViewHolder(v);
        } else if (viewType == SuggestionType.GOOGLE_FOOTER.ordinal() || viewType == SuggestionType.HERE_FOOTER.ordinal()) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.suggestion_footer, parent, false);
            int footerHeight = getGoogleMapHeight();
            if (viewType == SuggestionType.HERE_FOOTER.ordinal()) {
                footerHeight = getHereMapHeight();
            }
            Resources resources = this.context.getResources();
            if (resources != null) {
                footerHeight += resources.getDimensionPixelSize(R.dimen.suggestion_list_top_height);
            }
            v.setLayoutParams(new LayoutParams(-1, footerHeight));
            return new SectionHeaderViewHolder(v);
        } else if (viewType == SuggestionType.ACTIVE_TRIP.ordinal()) {
            if (this.activeTripCardView == null) {
                this.activeTripCardView = (ActiveTripCardView) LayoutInflater.from(parent.getContext()).inflate(R.layout.active_trip_card, parent, false);
            }
            ImageView chevron = (ImageView) this.activeTripCardView.findViewById(R.id.chevron);
            if (chevron != null) {
                chevron.setVisibility(VISIBLE);
            }
            return new ActiveTripViewHolder(this.activeTripCardView);
        } else if (viewType == SuggestionType.PENDING_TRIP.ordinal()) {
            if (this.pendingTripCardView == null) {
                this.pendingTripCardView = (PendingTripCardView) LayoutInflater.from(parent.getContext()).inflate(R.layout.pending_trip_card, parent, false);
            }
            logger.v("viewType for Pending Route found: " + viewType);
            return new PendingTripViewHolder(this.pendingTripCardView);
        } else if (viewType == SuggestionType.SECTION_HEADER.ordinal()) {
            return new SectionHeaderViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.suggestion_section_header, parent, false));
        } else {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
            if (isRoutedSuggestion(viewType)) {
                return new RoutedSuggestionsViewHolder(v);
            }
            return new SuggestionsViewHolder(v);
        }
    }

    private boolean isRoutedSuggestion(int viewType) {
        return viewType == SuggestionType.CALENDAR.ordinal() || viewType == SuggestionType.RECOMMENDATION.ordinal();
    }

    @MainThread
    public void onBindViewHolder(final ViewHolder viewHolder, int unreliablePosition) {
        SystemUtils.ensureOnMainThread();
        if (viewHolder == null || viewHolder.itemView == null) {
            logger.e("onBindViewHolder called on a null viewHolder !!!");
            return;
        }
        int suggestionPosition = unreliablePosition - 1;
        if (!isValidSuggestion(suggestionPosition) || isNotSuggestionType(viewHolder)) {
            if (viewHolder instanceof ContentLoadingViewHolder) {
                ((ContentLoadingViewHolder) viewHolder).startAnimation();
            }
            if (viewHolder.getItemViewType() == SuggestionType.GOOGLE_HEADER.ordinal()) {
                viewHolder.itemView.setLayoutParams(new LayoutParams(-1, getGoogleMapHeight()));
            }
            if (viewHolder.getItemViewType() == SuggestionType.HERE_HEADER.ordinal()) {
                viewHolder.itemView.setLayoutParams(new LayoutParams(-1, getHereMapHeight()));
            }
            if (this.listener != null) {
                viewHolder.itemView.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        if (SuggestedDestinationsAdapter.this.selectedItemPosition < 0) {
                            SuggestedDestinationsAdapter.this.listener.onClick(viewHolder.itemView, viewHolder.getAdapterPosition());
                        }
                    }
                });
                return;
            }
            return;
        }
        Suggestion suggestion = (Suggestion) this.suggestions.get(suggestionPosition);
        if (viewHolder instanceof ActiveTripViewHolder) {
            onBindActiveTrip((ActiveTripViewHolder) viewHolder, suggestion);
        } else if (viewHolder instanceof PendingTripViewHolder) {
            logger.v("pendingTripViewHolder found: " + suggestion.destination.name + " at adapter position: " + unreliablePosition);
            onBindPendingTrip((PendingTripViewHolder) viewHolder, suggestion);
        } else if (viewHolder instanceof SectionHeaderViewHolder) {
            onBindSectionHeader((SectionHeaderViewHolder) viewHolder, suggestion, suggestionPosition);
        } else if (viewHolder instanceof SuggestionsViewHolder) {
            SuggestionsViewHolder suggestionsViewHolder = (SuggestionsViewHolder) viewHolder;
            onBindSuggestion(suggestionsViewHolder, suggestion);
            if (viewHolder instanceof RoutedSuggestionsViewHolder) {
                ((RoutedSuggestionsViewHolder) viewHolder).onBindViewHolder(suggestion);
            } else {
                suggestionsViewHolder.subIllustrationText.setVisibility(GONE);
            }
        }
    }

    @MainThread
    public int getItemCount() {
        SystemUtils.ensureOnMainThread();
        return this.suggestions.size() + 2;
    }

    @MainThread
    public int getItemViewType(int position) {
        SystemUtils.ensureOnMainThread();
        if (position == 0) {
            if (firstItemIsActiveTrip() || firstItemIsPendingTrip()) {
                return SuggestionType.HERE_HEADER.ordinal();
            }
            return SuggestionType.GOOGLE_HEADER.ordinal();
        } else if (position != getItemCount() - 1) {
            position--;
            if (isValidIndex(position)) {
                return ((Suggestion) this.suggestions.get(position)).getType().ordinal();
            }
            return SuggestionType.RECENT.ordinal();
        } else if (firstItemIsActiveTrip() || firstItemIsPendingTrip()) {
            return SuggestionType.HERE_FOOTER.ordinal();
        } else {
            return SuggestionType.GOOGLE_FOOTER.ordinal();
        }
    }

    public boolean onLongClick(View v) {
        return false;
    }

    public void onPendingRouteCalculating(@NonNull Destination destination) {
        addPendingTripCard(destination);
    }

    public void onPendingRouteCalculated(@NonNull Error error, @NonNull NavdyRouteInfo pendingRoute) {
    }

    public void onRouteCalculating(@NonNull Destination destination) {
    }

    public void onRouteCalculated(@NonNull Error error, @NonNull NavdyRouteInfo route) {
    }

    public void onRouteStarted(@NonNull NavdyRouteInfo route) {
        addActiveTripCard(route.getDestination());
    }

    public void onTripProgress(@NonNull NavdyRouteInfo progress) {
    }

    public void onReroute() {
    }

    public void onRouteArrived(@NonNull Destination destination) {
    }

    public void onStopRoute() {
        removeTripCard();
    }

    @MainThread
    private boolean isValidIndex(int position) {
        SystemUtils.ensureOnMainThread();
        return position >= 0 && !this.suggestions.isEmpty() && position < this.suggestions.size() && this.suggestions.get(position) != null;
    }

    private boolean isNotSuggestionType(ViewHolder viewHolder) {
        return ((viewHolder instanceof SuggestionsViewHolder) || (viewHolder instanceof ActiveTripViewHolder) || (viewHolder instanceof PendingTripViewHolder) || (viewHolder instanceof SectionHeaderViewHolder)) ? false : true;
    }

    @MainThread
    private boolean isValidSuggestion(int suggestionPosition) {
        SystemUtils.ensureOnMainThread();
        return isValidIndex(suggestionPosition) && ((Suggestion) this.suggestions.get(suggestionPosition)).destination != null;
    }

    private void onBindActiveTrip(final ActiveTripViewHolder activeTripViewHolder, final Suggestion suggestion) {
        activeTripViewHolder.itemView.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                SuggestedDestinationsAdapter.this.listener.onClick(view, activeTripViewHolder.getAdapterPosition());
            }
        });
        if (activeTripViewHolder.getAdapterPosition() == this.selectedItemPosition) {
            setViewToSelectedMode(activeTripViewHolder.itemView, SuggestionType.ACTIVE_TRIP);
        } else {
            setViewToNormalMode(activeTripViewHolder.itemView, SuggestionType.ACTIVE_TRIP);
        }
        activeTripViewHolder.itemView.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                int adapterPosition = activeTripViewHolder.getAdapterPosition();
                if (SuggestedDestinationsAdapter.this.selectedItemPosition >= 0) {
                    SuggestedDestinationsAdapter.this.triggerLongClick(true, activeTripViewHolder.itemView, adapterPosition, suggestion);
                } else if (SuggestedDestinationsAdapter.this.listener != null) {
                    SuggestedDestinationsAdapter.this.listener.onClick(view, adapterPosition);
                }
            }
        });
        if (this.longClickListener != null) {
            activeTripViewHolder.itemView.setOnLongClickListener(new OnLongClickListener() {
                public boolean onLongClick(View view) {
                    SuggestedDestinationsAdapter.this.longClickListener.onItemLongClick(view, activeTripViewHolder.getAdapterPosition(), suggestion);
                    return true;
                }
            });
        }
    }

    private void onBindPendingTrip(final PendingTripViewHolder pendingTripViewHolder, final Suggestion suggestion) {
        if (suggestion != null && suggestion.destination != null) {
            if (suggestion.isPendingTrip()) {
                if (pendingTripViewHolder.getAdapterPosition() == this.selectedItemPosition) {
                    setViewToSelectedMode(pendingTripViewHolder.itemView, SuggestionType.PENDING_TRIP);
                } else {
                    setViewToNormalMode(pendingTripViewHolder.itemView, SuggestionType.PENDING_TRIP);
                }
            }
            pendingTripViewHolder.itemView.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    int adapterPosition = pendingTripViewHolder.getAdapterPosition();
                    if (SuggestedDestinationsAdapter.this.selectedItemPosition >= 0) {
                        SuggestedDestinationsAdapter.this.triggerLongClick(true, pendingTripViewHolder.itemView, adapterPosition, suggestion);
                    } else if (SuggestedDestinationsAdapter.this.listener != null) {
                        SuggestedDestinationsAdapter.this.listener.onClick(view, adapterPosition);
                    }
                }
            });
            if (this.longClickListener != null) {
                pendingTripViewHolder.itemView.setOnLongClickListener(new OnLongClickListener() {
                    public boolean onLongClick(View view) {
                        SuggestedDestinationsAdapter.this.longClickListener.onItemLongClick(view, pendingTripViewHolder.getAdapterPosition(), suggestion);
                        return true;
                    }
                });
            }
        }
    }

    private void onBindSectionHeader(SectionHeaderViewHolder sectionHeaderViewHolder, Suggestion suggestion, int suggestionPosition) {
        if (!(sectionHeaderViewHolder.title == null || suggestion == null || suggestion.destination == null)) {
            sectionHeaderViewHolder.title.setText(suggestion.destination.name);
        }
        if (suggestionPosition == 0) {
            sectionHeaderViewHolder.chevron.setVisibility(VISIBLE);
        } else {
            sectionHeaderViewHolder.chevron.setVisibility(GONE);
        }
    }

    private void onBindSuggestion(SuggestionsViewHolder suggestionViewHolder, Suggestion suggestion) {
        onBindSuggestionIconIllustration(suggestion, suggestionViewHolder);
        if (suggestion.shouldShowRightChevron()) {
            suggestionViewHolder.rightChevron.setVisibility(VISIBLE);
        } else {
            suggestionViewHolder.rightChevron.setVisibility(GONE);
        }
        suggestionViewHolder.infoButton.setImageResource(R.drawable.ic_info_outline_grey_500);
        onBindSuggestionNameAndAddress(suggestion, suggestionViewHolder);
        boolean shouldSendToHud = suggestion.shouldSendToHud();
        suggestionViewHolder.separator.setVisibility(VISIBLE);
        onBindSuggestionClickListeners(suggestionViewHolder, suggestion, suggestionViewHolder, shouldSendToHud);
    }

    private void onBindSuggestionIconIllustration(Suggestion suggestion, SuggestionsViewHolder suggestionViewHolder) {
        boolean isSelected;
        if (suggestionViewHolder.getAdapterPosition() == this.selectedItemPosition) {
            isSelected = true;
        } else {
            isSelected = false;
        }
        int badgeAsset = suggestion.getBadgeAsset(isSelected);
        if (badgeAsset > 0) {
            suggestionViewHolder.icon.setImageResource(badgeAsset);
        } else {
            suggestionViewHolder.icon.setImageBitmap(null);
        }
        setBackground(suggestionViewHolder.background, suggestion.getType(), false);
    }

    private void onBindSuggestionNameAndAddress(Suggestion suggestion, SuggestionsViewHolder suggestionViewHolder) {
        if (!suggestion.hasInfoButton()) {
            suggestionViewHolder.infoButton.setVisibility(INVISIBLE);
        }
        suggestionViewHolder.icon.setVisibility(VISIBLE);
        if (suggestion.destination != null) {
            if (suggestion.isTip()) {
                suggestionViewHolder.firstLine.setText(suggestion.destination.name);
                suggestionViewHolder.secondLine.setText(suggestion.destination.getAddressForDisplay());
            } else if (!suggestion.isCalendar() || suggestion.event == null || StringUtils.isEmptyAfterTrim(suggestion.event.displayName)) {
                Pair<String, String> splitAddress = suggestion.destination.getTitleAndSubtitle();
                suggestionViewHolder.firstLine.setText((CharSequence) splitAddress.first);
                suggestionViewHolder.secondLine.setText((CharSequence) splitAddress.second);
            } else {
                suggestionViewHolder.firstLine.setText(suggestion.event.displayName);
                suggestionViewHolder.secondLine.setText(suggestion.destination.getAddressForDisplay());
            }
            if (suggestion.isCalendar()) {
                suggestionViewHolder.thirdLine.setText(this.context.getString(R.string.event_times, new Object[]{suggestion.event.getStartTime(), suggestion.event.getEndTime()}));
                suggestionViewHolder.thirdLine.setVisibility(VISIBLE);
                return;
            }
            suggestionViewHolder.thirdLine.setVisibility(GONE);
        }
    }

    private void onBindSuggestionClickListeners(SuggestionsViewHolder viewHolder, Suggestion suggestion, SuggestionsViewHolder suggestionViewHolder, boolean shouldSendToHud) {
        final View background = viewHolder.background;
        final SuggestionsViewHolder suggestionsViewHolder = viewHolder;
        final boolean z = shouldSendToHud;
        final Suggestion suggestion2 = suggestion;
        OnClickListener clickListener = new OnClickListener() {
            public void onClick(View view) {
                int adapterPosition = suggestionsViewHolder.getAdapterPosition();
                if (SuggestedDestinationsAdapter.this.selectedItemPosition >= 0) {
                    SuggestedDestinationsAdapter.this.triggerLongClick(z, background, adapterPosition, suggestion2);
                } else if (SuggestedDestinationsAdapter.this.listener != null) {
                    SuggestedDestinationsAdapter.this.listener.onClick(view, adapterPosition);
                }
            }
        };
        suggestionViewHolder.row.setOnClickListener(clickListener);
        suggestionViewHolder.infoButton.setOnClickListener(clickListener);
//        z = shouldSendToHud;
        final SuggestionsViewHolder suggestionsViewHolder2 = viewHolder;
        final Suggestion suggestion3 = suggestion;
        suggestionViewHolder.row.setOnLongClickListener(new OnLongClickListener() {
            public boolean onLongClick(View view) {
                return SuggestedDestinationsAdapter.this.triggerLongClick(z, background, suggestionsViewHolder2.getAdapterPosition(), suggestion3);
            }
        });
    }

    private boolean triggerLongClick(boolean shouldSendToHud, View background, int position, Suggestion suggestion) {
        if (this.longClickListener == null || (!shouldSendToHud && !suggestion.isPendingTrip())) {
            return false;
        }
        this.longClickListener.onItemLongClick(background, position, suggestion);
        return true;
    }

    private void setBackground(View view, SuggestionType type, boolean selected) {
        if (view != null && type != SuggestionType.ACTIVE_TRIP) {
            if (selected) {
                view.setBackgroundColor(ContextCompat.getColor(NavdyApplication.getAppContext(), R.color.grey_light_card));
            } else {
                view.setBackgroundColor(ContextCompat.getColor(NavdyApplication.getAppContext(), R.color.white));
            }
        }
    }

    private void setViewToSelectedMode(View view, SuggestionType suggestionType) {
        if (view != null) {
            setBackground(view, suggestionType, true);
            ImageView illustration = getIllustrationView(view);
            if (illustration != null) {
                illustration.setTag(illustration.getDrawable());
                illustration.setImageResource(R.drawable.icon_suggested_edit);
            }
            if (suggestionType != SuggestionType.PENDING_TRIP && suggestionType != SuggestionType.ACTIVE_TRIP) {
                ViewCompat.setElevation(view, SELECTED_SUGGESTION_ELEVATION);
            }
        }
    }

    private void setViewToNormalMode(View view, SuggestionType suggestionType) {
        if (view != null) {
            setBackground(view, suggestionType, false);
            ImageView illustration = getIllustrationView(view);
            if (illustration != null) {
                illustration.setImageDrawable((Drawable) illustration.getTag());
            }
            ViewCompat.setElevation(view, 0.0f);
        }
    }

    private ImageView getIllustrationView(View view) {
        if (view == null) {
            return null;
        }
        return (ImageView) view.findViewById(R.id.illustration);
    }

    private void addActiveTripCard(Destination destination) {
        addTripCard(destination, SuggestionType.ACTIVE_TRIP);
    }

    private void addPendingTripCard(Destination destination) {
        addTripCard(destination, SuggestionType.PENDING_TRIP);
    }

    @MainThread
    private void addTripCard(Destination destination, SuggestionType suggestionType) {
        SystemUtils.ensureOnMainThread();
        SuggestionManager.setTripAsync(new Suggestion(destination, suggestionType));
    }

    @MainThread
    private void removeTripCard() {
        SystemUtils.ensureOnMainThread();
        SuggestionManager.setTripAsync(null);
    }

    private boolean firstItemIsActiveTrip() {
        return firstItemIs(SuggestionType.ACTIVE_TRIP);
    }

    private boolean firstItemIsPendingTrip() {
        return firstItemIs(SuggestionType.PENDING_TRIP);
    }

    @MainThread
    private boolean firstItemIs(SuggestionType suggestionType) {
        SystemUtils.ensureOnMainThread();
        return this.suggestions.size() > 0 && this.suggestions.get(0) != null && ((Suggestion) this.suggestions.get(0)).getType() == suggestionType;
    }
}
